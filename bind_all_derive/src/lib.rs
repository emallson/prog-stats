extern crate synstructure;
#[macro_use]
extern crate quote;
extern crate proc_macro2;
extern crate syn;

fn bindall_derive(s: synstructure::Structure) -> proc_macro2::TokenStream {
    let body = s.fold(quote! { q }, |acc, bi| {
        quote! {
            #acc.bind(#bi)
        }
    });

    let columns = match s.ast().data {
        syn::Data::Struct(ref s) => {
            let fields = match s.fields {
                syn::Fields::Named(ref fields) => fields,
                _ => panic!("not implemented for unnamed fields"),
            };

            fields
                .named
                .iter()
                .map(|f| {
                    let tok = f.ident.as_ref().unwrap();
                    quote! { stringify!(#tok) }
                })
                .collect::<Vec<_>>()
        }
        _ => panic!("not implemented for enums or unions"),
    };

    s.gen_impl(quote! {
        gen impl crate::bulk_insert::BindAll for @Self {
            fn bind_all<'a>(&'a self, q: ::sqlx::query::Query<'a, ::sqlx::Postgres, ::sqlx::postgres::PgArguments>) -> ::sqlx::query::Query<'a, ::sqlx::Postgres, ::sqlx::postgres::PgArguments> {
                match self {
                    #body
                }
            }

            fn columns() -> Vec<&'static str> {
                vec![#(#columns),*]
            }
        }
    })
}

synstructure::decl_derive!([BindAll] => bindall_derive);

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test() {
        synstructure::test_derive! {
            bindall_derive{
                struct Log {
                    iid: i32,
                    id: i32,
                    code: String,
                }
            }
            expands to {
                #[allow(non_upper_case_globals)]
                const _DERIVE_crate_bulk_insert_BindAll_FOR_Log: () = {
                    impl crate::bulk_insert::BindAll for Log {
                        fn bind_all<'a>(&'a self, q: ::sqlx::query::Query<'a, ::sqlx::Postgres, ::sqlx::postgres::PgArguments>) -> ::sqlx::query::Query<'a, ::sqlx::Postgres, ::sqlx::postgres::PgArguments> {
                            match self {
                                Log { iid: ref __binding_0, id: ref __binding_1, code: ref __binding_2, }
                                => {
                                    q.bind(__binding_0).bind(__binding_1).bind(__binding_2)
                                }
                            }
                        }

                        fn columns() -> Vec<&'static str> {
                            vec![stringify!(iid), stringify!(id), stringify!(code)]
                        }
                    }
                };
            }
            no_build
        }
    }
}
