module.exports = {
  purge: { 
    content: ['./src/**/*.{js,jsx,ts,tsx}', './index.html'],
    options: {
      safelist: ['bg-gradient-to-tl', 'bg-gradient-to-tr', 'bg-gradient-to-t', /grid-cols-[1-4]$/]
    }
  },
  darkMode: 'media',
  theme: {
    extend: {
      colors: {
        tbg: '#fffff8',
        dbg: '#202020',
        'vega-gray-dark': '#888888',
      },
      gridTemplateColumns: {
        'encounter': '20rem auto',
        'labels': 'min-content auto',
        'preview': '40rem auto',
      },
      animation: {
        fadein: 'fade 0.5s ease-in-out 1',
        'fadein-slow': 'fade-1-5 1s ease-in-out 1',
      },
      keyframes: {
        fade: {
          '0%': { opacity: 0 },
          '100%': { opacity: 1 },
        },
        'fade-1-5': {
          '0%': { opacity: 0 },
          '20%': { opacity: 0 },
          '100%': { opacity: 1 },
        },
      },
      listStyleType: {
        dash: '" -  "',
      }
    },
    fontFamily: {
      serif: ['Libertinus Serif', 'ui-serif', 'Georgia'],
      sans: ['Libertinus Sans', 'ui-sans-serif'],
      mono: ['Iosevka Custom', 'ui-mono'],
    }
  },
  variants: {
    extend: {
      margin: ['first', 'last']
    },
  },
  plugins: [],
}
