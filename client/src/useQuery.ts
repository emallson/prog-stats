import { request } from "graphql-request";
import { useCallback, useEffect, useRef, useState } from "react";
import { host_uri } from "./config";

export interface QueryOptions<V extends string> {
  variables?: Record<V, unknown>;
  skip?: boolean;
}

export interface QueryResult<T> {
  data?: T;
  error?: Error;
  loading: boolean;
}

export default function useQuery<T, V extends string = any>(
  query: string,
  options?: QueryOptions<V>,
): QueryResult<T> {
  const [result, setResult] = useState<QueryResult<T>>({ loading: false });

  // react eslint recommends against this, but the alternative is going full deep-equal and i don't wanna do that.

  const dependencies = [
    query,
    ...(options?.variables ? Object.values(options.variables) : []),
    options?.skip,
  ];

  useEffect(() => {
    if (options?.skip) {
      return;
    }

    let active = true;

    setResult({ loading: true });

    request({
      url: `${host_uri()}/graphql`,
      variables: options?.variables,
      document: query,
    })
      .then((result) => {
        if (!active) {
          return;
        }
        setResult({ loading: false, data: result as T });
      })
      .catch((error) => {
        if (!active) {
          return;
        }
        setResult({ loading: false, error });
      });

    return () => {
      active = false;
    };
  }, dependencies);

  return result;
}

type Mutation<V extends string> = (options?: QueryOptions<V>) => void;
export type MutationResult<T, V extends string> = [
  Mutation<V>,
  { called: boolean; data?: T; error?: Error; loading: boolean },
];

export function useMutation<T, V extends string = any>(
  mutation: string,
): MutationResult<T, V> {
  const [called, setCalled] = useState(false);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState<Error | undefined>(undefined);
  const [data, setData] = useState<T | undefined>(undefined);

  const active = useRef(true);

  useEffect(
    () => () => {
      active.current = false;
    },
    [],
  );

  const callMutation = useCallback(
    (options?: QueryOptions<V>): void => {
      setCalled(true);
      setLoading(true);

      request({
        url: `${host_uri()}/graphql`,
        variables: options?.variables,
        document: mutation,
      })
        .then((result) => {
          if (!active.current) {
            return;
          }
          setLoading(false);
          setData(result as T);
        })
        .catch((error) => {
          if (!active.current) {
            return;
          }
          setLoading(false);
          setError(error);
        });
    },
    [mutation],
  );

  return [callMutation, { called, loading, error, data }];
}
