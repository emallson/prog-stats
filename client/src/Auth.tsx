import { useEffect } from "react";
import Modal from "react-modal";
import { useLocation, Redirect } from "react-router-dom";
import { gql } from "graphql-request";
import { frontend_uri } from "./config";
import useMedia from "./useMedia";
import { useMutation } from "./useQuery";

Modal.setAppElement("#root");

const CLIENT_ID = "92b937a6-fd8f-40d6-9d58-7df74f26bd92";
const REDIRECT_URI = `${frontend_uri()}/authorize`;

function redirect(team: number, zone: number, kind: "authorize" | "revoke") {
  const pathname = window.location.pathname;
  const nonce = Math.random();

  const state = btoa(nonce.toString());

  window.localStorage.setItem(
    "auth_state",
    JSON.stringify({ state, pathname, team, zone, kind }),
  );

  window.location.assign(
    `https://www.warcraftlogs.com/oauth/authorize?client_id=${encodeURIComponent(
      CLIENT_ID,
    )}&state=${encodeURIComponent(state)}&redirect_uri=${encodeURIComponent(
      REDIRECT_URI,
    )}&response_type=code`,
  );
}

interface Props {
  open: boolean;
  close: () => void;
  guild: number;
  zone: number;
}

const modalStyle = {
  top: "calc(50% - 20em)",
  bottom: "auto",
  left: "calc(50% - 20em)",
  width: "min(40em, 90vw)",
};
const darkContent: object = { backgroundColor: "hsla(0, 0%, 14%, 1)" };
const darkOverlay: object = { backgroundColor: "rgba(32, 32, 32, 0.75)" };

function AuthModal({
  children,
  open,
}: React.PropsWithChildren<{ open: boolean }>) {
  const [extraStyle, extraOverlay] = useMedia(
    ["(prefers-color-scheme: dark)"],
    [[darkContent, darkOverlay]],
    [{}, {}],
  );
  return (
    <Modal
      isOpen={open}
      contentLabel="Authorization Required"
      style={{
        content: { ...modalStyle, ...extraStyle },
        overlay: extraOverlay,
      }}
    >
      {children}
    </Modal>
  );
}

export function PreAuth({ open, close, guild, zone }: Props) {
  return (
    <AuthModal open={open}>
      <header className="mb-2">
        <legend className="font-serif text-2xl">
          Authorization for Private Logs
        </legend>
      </header>
      <p>
        In order to collect data about private logs, you will need to grant
        access via WarcraftLogs.
      </p>
      <p className="font-bold">Details</p>
      <ul className="list-disc pl-5">
        <li>
          Your authorization will allow collecting both current and future
          private logs.
          <br />
          We will <strong>not</strong> collect logs from future tiers unless you
          authorize access for that tier.
        </li>
        <li>This page and all data on it will be publicly available.</li>
        <li>
          Your log data may be used in <em>aggregate</em> statistics like those
          shown on this page.
          <br />
          We will never show data about individual raiders.
        </li>
        <li>
          You or another individual with access to your private logs can revoke
          access at any time. When access is revoked, we will delete all private
          data.
        </li>
      </ul>
      <p>
        If you would like to authorize access to your private logs, click{" "}
        <em>Continue</em> below.
      </p>
      <footer className="grid gap-4 justify-end grid-flow-col mt-2">
        <button className="underline" onClick={close}>
          Close
        </button>
        <button
          onClick={redirect.bind(null, guild, zone, "authorize")}
          className="py-2 px-4 bg-blue-200 dark:bg-blue-500 font-bold rounded-lg shadow-md hover:bg-blue-300 dark:hover:bg-blue-600 focus:outline-none focus:ring-2"
        >
          Continue
        </button>
      </footer>
    </AuthModal>
  );
}

export function RevokeAuth({ open, close, guild, zone }: Props) {
  return (
    <AuthModal open={open}>
      <header className="mb-2">
        <legend className="font-serif text-2xl">
          Revoke Private Log Access
        </legend>
      </header>
      <p>
        In order to revoke access to private logs, you will need to grant access
        via WarcraftLogs so that we can verify your identity.
      </p>
      <p className="mt-2">
        After verification, all data related to this <strong>guild</strong> will
        be deleted.
      </p>
      <footer className="grid gap-4 justify-end grid-flow-col mt-2">
        <button className="underline" onClick={close}>
          Cancel
        </button>
        <button
          onClick={redirect.bind(null, guild, zone, "revoke")}
          className="py-2 px-4 bg-red-200 dark:bg-red-500 font-bold rounded-lg shadow-md hover:bg-red-300 dark:hover:bg-red-500 focus:outline-none focus:ring-2"
        >
          Revoke Access
        </button>
      </footer>
    </AuthModal>
  );
}

const STORE_CODE = gql`
  mutation StoreCode($team: Int!, $code: String!, $zone: Int!) {
    storeCode(team: $team, code: $code, zone: $zone)
  }
`;

const REVOKE = gql`
  mutation Revoke($team: Int!, $code: String!, $zone: Int!) {
    revokeTeam(code: $code, team: $team, zone: $zone)
  }
`;

interface AuthProps {
  code: string;
  team: number;
  zone: number;
  pathname: string;
}

function RequestAccess({ code, team, zone, pathname }: AuthProps) {
  console.log(code, team, zone);
  const [storeCode, { called, data, loading, error }] =
    useMutation<any>(STORE_CODE);
  console.log(called);
  useEffect(() => {
    if (!called) {
      storeCode({ variables: { code, team, zone } });
    }
  }, [code, team, zone]); // eslint-disable-line react-hooks/exhaustive-deps

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error.message}</div>;
  }

  if (data && data.storeCode) {
    return <Redirect to={pathname} />;
  } else if (data) {
    return (
      <div>
        You do not have access to private, unlisted, or archived logs for that
        guild.
      </div>
    );
  }

  return <div></div>;
}

function RevokeAccess({ code, team, zone }: AuthProps) {
  const [revokeTeam, { called, data, loading, error }] =
    useMutation<any>(REVOKE);
  useEffect(() => {
    if (!called) {
      revokeTeam({ variables: { code, team, zone } });
    }
  }, [code, team, zone]); // eslint-disable-line react-hooks/exhaustive-deps

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error.message}</div>;
  }

  if (data && data.revokeTeam) {
    return <div>Team data has been deleted.</div>;
  } else if (data) {
    return (
      <div>
        You do not have access to private, unlisted, or archived logs for that
        guild.
      </div>
    );
  }

  return <div></div>;
}

export default function Auth() {
  const query = new URLSearchParams(useLocation().search);
  const state = JSON.parse(window.localStorage.getItem("auth_state") || "{}");
  const nonce = query.get("state");
  const code = query.get("code");

  if (!code || state.state !== nonce) {
    return <div>Invalid authorization state.</div>;
  }

  switch (state.kind) {
    case "revoke":
      return (
        <RevokeAccess
          code={code}
          team={state.team}
          zone={state.zone}
          pathname={state.pathname}
        />
      );
    case "authorize":
      return (
        <RequestAccess
          code={code}
          team={state.team}
          zone={state.zone}
          pathname={state.pathname}
        />
      );
    default:
      return null;
  }
}
