import React, { Suspense, useMemo, useState, useEffect } from "react";
import { Link, Redirect, useParams, useLocation } from "react-router-dom";
import slugify from "slugify";
import EncounterOverview, { EncounterStats } from "./EncounterOverview";
import {
  toRegion,
  Region,
  DIFFICULTY_NAMES,
  Encounter,
  SUPPORTED_TIERS,
} from "./model";
import {
  formatShortDate,
  formatDuration,
  formatPrettyDuration,
} from "./format";
import Sync from "./Sync";
import { PreAuth, RevokeAuth } from "./Auth";
import { HoursPlaceholder, RaidTimeChartPlaceholder } from "./placeholders";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown } from "@fortawesome/free-solid-svg-icons";
import "./TierDetails.css";
import { gql } from "graphql-request";
import useQuery from "./useQuery";

const RaidTimeChart = React.lazy(() => import("./charts/RaidTime"));

const TAG_QUERY = gql`
  query DetailsMeta($guildId: Int!, $teamId: Int!) {
    team: tag(id: $teamId) {
      iid
      id
      name
      tagName
    }
    guild: tag(id: $guildId) {
      iid
      id
      name
      serverSlug
      regionSlug
      serverName
    }
  }
`;

const PRIVATE_QUERY = gql`
  query PrivateData($guildId: Int!, $zoneId: Int!) {
    privateAccess(guild: $guildId, zone: $zoneId)
  }
`;

const HOURS_QUERY = gql`
  query TeamHours($team: Int!, $zone: Int!) {
    raidTimes(team: $team, zone: $zone) {
      lower
      upper
      breakdowns {
        week
        prog
        farm
      }
    }
  }
`;

const TEAM_STATS_QUERY = gql`
  query TeamStats($team: Int!, $zone: Int!) {
    teamStats(team: $team, zone: $zone) {
      bossesKilled
    }
  }
`;

const id_pat = /(\d+)/;
export function read_id(s: String): number | undefined {
  const match = s.match(id_pat);
  if (match) {
    return parseInt(match[1]);
  }
}

const name_pat = /\d+-(.+)/;
export function read_name(s: string): string | undefined {
  const match = s.match(name_pat);
  if (match) {
    return match[1];
  }
}

export interface Params {
  region: string;
  server: string;
  guild: string;
  tag: string;
  zone: string;
}

export function url_for(data: any, tier: string): string {
  return `/details/${data.guild.regionSlug.toLowerCase()}/${
    data.guild.serverSlug
  }/${data.guild.id}-${slugify(data.guild.name)}/${
    data.team ? `${data.team.id}-${slugify(data.team.tagName)}` : "all"
  }/${tier}`;
}

export const Q_ZONE = gql`
  query Zone($zone: Int!) {
    encounters(zone: $zone) {
      id
      name
      encounters {
        id
        name
      }
    }
  }
`;

export const Q_ZONE_STATS = gql`
  query ZoneStats($team: Int!, $zone: Int!, $difficulty: Int!) {
    statsForZone(teamId: $team, zone: $zone, difficulty: $difficulty) {
      team
      encounterId
      difficulty
      killLog {
        iid
        startTime
      }
      killWeek
      pullCount
      progTime
      ilvlMin
      ilvlMax
      distributions {
        pullCount: pullCountV2 {
          count
          min
          max
          quantiles
          actualPct
        }
        progTime: progTimeV2 {
          count
          min
          max
          quantiles
          actualPct
        }
      }
    }
  }
`;

function StatRow({
  stats,
  encounter,
}: {
  stats: EncounterStats;
  encounter: Encounter;
}) {
  return (
    <tr>
      <td className="text-left pr-2 border-r border-gray-200">
        {encounter.name}
      </td>
      <td className="pl-2 pr-2 text-center">
        {stats && stats.killLog
          ? formatShortDate(new Date(stats.killLog.startTime))
          : ""}
      </td>
      <td className="pl-2 pr-2 text-center">
        {stats && stats.killWeek ? `Week ${stats.killWeek}` : ""}
      </td>
      <td className="pl-2 pr-2 border-l border-gray-200">
        {stats ? stats.pullCount : ""}
      </td>
      <td className="pl-2 pr-2">
        {stats ? formatDuration(stats.progTime, " hrs") : ""}
      </td>
      <td className="pl-2 pr-2 border-l border-gray-200">
        {stats ? stats.ilvlMin.toFixed(1) : ""}
      </td>
      <td className="pl-2 pr-2">
        {stats && stats.killLog ? stats.ilvlMax.toFixed(1) : ""}
      </td>
    </tr>
  );
}

export function StatTable({
  difficulty,
  team,
  encounters,
  zone,
}: {
  zone: number;
  team: number;
  encounters: Encounter[];
  difficulty: number;
}) {
  const { data } = useQuery<any>(Q_ZONE_STATS, {
    variables: { team, difficulty, zone },
  });
  if (data) {
    return (
      <section className="mb-12">
        <legend className="text-xl md:text-2xl">Progression Overview</legend>
        <div className="overflow-scroll md:overflow-visible w-screen md:w-max">
          <table className="w-max">
            <thead className="text-center">
              <tr>
                <th colSpan={5}></th>
                <th colSpan={2} className="border-l border-gray-200 pl-2 pr-2">
                  Avg Raid ilvl
                </th>
              </tr>
              <tr>
                <th className="pl-2 pr-2 border-b border-gray-200"></th>
                <th className="pl-2 pr-2 border-b border-l border-gray-200">
                  Kill Date
                </th>
                <th className="pl-2 pr-2 border-b border-gray-200">
                  Kill Week
                </th>
                <th className="pl-2 pr-2 border-b border-l border-gray-200">
                  Pulls
                </th>
                <th className="pl-2 pr-2 border-b border-gray-200">
                  Prog Time
                </th>
                <th className="pl-2 pr-2 border-b border-l border-gray-200">
                  First Pull
                </th>
                <th className="pl-2 pr-2 border-b">Kill Pull</th>
              </tr>
            </thead>
            <tbody className="text-right">
              {encounters.map((encounter: Encounter) => (
                <StatRow
                  encounter={encounter}
                  key={encounter.id}
                  stats={data.statsForZone.find(
                    (s: EncounterStats) => s.encounterId === encounter.id,
                  )}
                />
              ))}
            </tbody>
          </table>
        </div>
      </section>
    );
  } else {
    return null;
  }
}

export interface RaidNight {
  startTime: Date;
  endTime: Date;
  observedInstances: number;
}

function PrettyDuration({ ms }: { ms: number }) {
  return <>{formatPrettyDuration(ms)}</>;
}

export function TeamHours(props: {
  team: number;
  zone: number;
  encounters: number[];
  region: Region;
}) {
  const { data } = useQuery<any>(HOURS_QUERY, { variables: props });

  let body = <HoursPlaceholder />;

  if (data && data.raidTimes) {
    const { lower, upper, breakdowns } = data.raidTimes;

    body = (
      <>
        <PrettyDuration ms={lower} />
        <span>
          {" "}
          &mdash; <PrettyDuration ms={upper} />
        </span>
        <div>
          <Suspense fallback={<RaidTimeChartPlaceholder />}>
            <RaidTimeChart data={breakdowns} />
          </Suspense>
        </div>
      </>
    );
  }

  return (
    <>
      <dt
        className="font-bold cursor-help underline"
        style={{
          textDecorationStyle: "dotted",
          textDecorationThickness: "from-font",
        }}
        title="Sum of Progression and Farm time during progression, including
        time between bosses. Median and 90th Percentile. The chart below also
        includes time after progression, but this is not used for calculating
        aggregate hours per week."
      >
        Hours / Week:
      </dt>
      <dd>{body}</dd>
    </>
  );
}

export function TeamOverview(props: {
  team: number;
  zone: number;
  encounters: number[];
  region: Region;
}) {
  const { data } = useQuery<any>(TEAM_STATS_QUERY, { variables: props });

  if (data && data.teamStats) {
    return (
      <section className="md:text-xl">
        <dl className="grid grid-cols-labels whitespace-nowrap gap-x-5">
          <dt className="font-bold">Progress:</dt>
          <dd>
            {data.teamStats.bossesKilled} / {props.encounters.length}{" "}
            {DIFFICULTY_NAMES[5][0]}
          </dd>
          <TeamHours {...props} />
        </dl>
      </section>
    );
  } else {
    return null;
  }
}

export function TierSelect({
  tier,
  className,
  chevronClassName,
  onChange,
}: {
  tier: number | undefined;
  className?: string;
  chevronClassName?: string;
  onChange?: () => void;
}) {
  const [open, setOpen] = useState(false);
  if (!tier) {
    return null;
  }

  return (
    <details
      open={open}
      onToggle={(e) => setOpen((e.target as HTMLDetailsElement).open)}
      className={`text-xl md:text-2xl relative font-serif w-max ${
        className || ""
      }`}
    >
      <summary className="cursor-pointer">
        {SUPPORTED_TIERS.find(({ id }) => id === tier)?.name}
        <FontAwesomeIcon
          icon={faChevronDown}
          className={`ml-1 text-base md:text-lg ${chevronClassName || ""}`}
        />
      </summary>
      <div className="absolute bg-tbg dark:bg-dbg z-50 shadow">
        <legend className="font-bold border-b p-1">Select a Tier</legend>
        <ul className="p-1">
          {SUPPORTED_TIERS.map(({ id, name, slug }) => (
            <li key={id} className={`w-max ${id === tier ? "underline" : ""}`}>
              <Link
                to={`./${slug}`}
                onClick={() => {
                  setOpen(false);
                  if (onChange) {
                    onChange();
                  }
                }}
              >
                {name}
              </Link>
            </li>
          ))}
        </ul>
      </div>
    </details>
  );
}

export default function Details() {
  const params = useParams<Params>();
  const location = useLocation();
  const { region, server, guild, tag, zone } = params;
  const [syncComplete, setSyncComplete] = useState(false);
  const [showPreAuth, setShowPreAuth] = useState(false);
  const [showRevoke, setShowRevoke] = useState(false);

  const tier = read_id(zone);
  const tagid = read_id(tag);
  const guildid = read_id(guild);

  const { loading, error, data } = useQuery<any>(TAG_QUERY, {
    skip: !syncComplete,
    variables: { teamId: tagid || 0, guildId: guildid || 0, zoneId: tier || 0 },
  });

  const { data: hasPrivate } = useQuery<any>(PRIVATE_QUERY, {
    variables: { guildId: guildid || 0, zoneId: tier || 0 },
  });

  const team = useMemo(() => (data ? data.team || data.guild : {}), [data]);
  useEffect(() => {
    if (team.tagName || team.name) {
      document.title = `${team.name}${
        team.tagName ? ` - ${team.tagName}` : ""
      } | WoW Progression Stats`;
    }
  }, [team]);

  const teamid = tagid || guildid;
  const { data: statData, loading: loadingStats } = useQuery<any>(
    Q_ZONE_STATS,
    {
      variables: { team: teamid, difficulty: 5, zone: tier },
      skip: !syncComplete || !teamid,
    },
  );

  const { data: rawZoneData } = useQuery<any>(Q_ZONE, {
    variables: { zone: tier },
    skip: !tier,
  });

  if (error) {
    return <div>An error occurred while fetching data: {error.message}</div>;
  }

  if (loading) {
    return <div>Loading...</div>;
  }

  if (!guildid || (!tagid && tag !== "all") || !tier) {
    return <Redirect to="/" />;
  }

  if (data && data.guild) {
    const url = url_for(data, zone);

    if (location.pathname !== url) {
      return <Redirect to={url} />;
    }
  }

  const zoneData = rawZoneData?.encounters;
  const encounters = zoneData ? zoneData.encounters : undefined;

  let content;
  if (!syncComplete) {
    content = (
      <Sync
        syncCallback={() => setSyncComplete(true)}
        id={guildid}
        tag={tagid}
        zone={tier}
      />
    );
  } else if (encounters && team.id && statData) {
    content = (
      <>
        <div className="grid xl:grid-cols-2 animate-fadein">
          <TeamOverview
            team={team.id}
            zone={tier}
            encounters={encounters}
            region={toRegion(data.guild.regionSlug)}
          />
          <StatTable
            encounters={encounters}
            zone={tier}
            team={team.id}
            difficulty={5}
          />
        </div>
        {encounters.map((encounter: Encounter) => (
          <EncounterOverview
            key={encounter.id}
            encounter={encounter}
            difficulty={5}
            team={team.id}
            stats={statData.statsForZone.find(
              (v: EncounterStats) => v.encounterId === encounter.id,
            )}
            region={toRegion(region)}
          />
        ))}
      </>
    );
  } else if (!tier) {
    content = <div>No tier selected.</div>;
  } else if (!loadingStats) {
    content = <div>No data for selected tier.</div>;
  } else {
    content = <></>;
  }

  return (
    <div className="container mx-auto mt-10">
      <header className="font-serif mb-3">
        <legend className="text-2xl md:text-4xl">
          {team.name || read_name(guild) || guild}{" "}
          {team.tagName ? ` - ${team.tagName}` : ""}
        </legend>
        <p className="text-lg md:text-xl">
          {region.toUpperCase()} -{" "}
          {data && data.guild ? data.guild.serverName : server}
        </p>
      </header>
      <TierSelect tier={tier} onChange={() => setSyncComplete(false)} />
      {content}
      <footer className="mt-5 mb-5 text-center">
        <span>
          Missing private logs?{" "}
          <button className="underline" onClick={() => setShowPreAuth(true)}>
            Click Here
          </button>
        </span>
        {hasPrivate && hasPrivate.privateAccess && (
          <div>
            <button className="underline" onClick={() => setShowRevoke(true)}>
              Revoke Private Access
            </button>
          </div>
        )}
      </footer>
      <PreAuth
        zone={tier}
        guild={guildid}
        open={showPreAuth}
        close={() => setShowPreAuth(false)}
      />
      <RevokeAuth
        zone={tier}
        guild={guildid}
        open={showRevoke}
        close={() => setShowRevoke(false)}
      />
    </div>
  );
}
