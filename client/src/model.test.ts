import { tierStart, tierWeek, differentResets, Region } from './model';
import { addSeconds, addHours, addDays, addWeeks } from 'date-fns/fp';

describe('tierStart', () => {
  it('should report the correct times for Castle Nathria in each region', () => {
    expect(tierStart(26, Region.NA)).toStrictEqual(new Date('2020-12-08T08:00-08:00'));
    expect(tierStart(26, Region.EU)).toStrictEqual(new Date('2020-12-09T08:00+01:00'));
    expect(tierStart(26, Region.CN)).toStrictEqual(new Date('2020-12-10T08:00+08:00'));
  });
});

describe('tierWeek', () => {
  it('should report Dec 8th as the start of Castle Nathria', () => {
    expect(tierWeek(26, new Date('2020-12-08T23:59:59-05:00'), false, Region.NA)).toBe(1);
    expect(tierWeek(26, new Date('2020-12-08T23:59:59-05:00'), true, Region.NA)).toBe(0);
  });

  it('should report Dec 9th as the start of Castle Nathria in EU', () => {
    expect(tierWeek(26, new Date('2020-12-09T08:00+01:00'), false, Region.EU)).toBe(1);
  });
});

describe('differentResets', () => {

  it('should report the same date as the same reset', () => {
    const date = new Date();
    expect(differentResets(Region.NA, date, date)).toBe(false);
  });

  it('should report tues/weds as the same reset in everything but not EU', () => {
    const date = new Date('2020-12-15T08:00:00-08:00');
    const next_day = addDays(1, date);

    expect(differentResets(Region.NA, date, next_day)).toBe(false);
    expect(differentResets(Region.EU, date, next_day)).toBe(true);
  });

  it('should report tues->tues as the same reset in NA', () => {
    const date = new Date('2021-02-09T08:00:00-08:00');
    const next_date = new Date('2021-02-16T07:59:59-08:00');
    
    expect(differentResets(Region.NA, date, next_date)).toBe(false);
    expect(differentResets(Region.NA, date, addSeconds(1, next_date))).toBe(true);
  });

  it('should not report monday night -> tuesday morning as across resets in NA', () => {
    const date = new Date('2020-12-07T23:59:59-05:00');
    const later = addHours(4, date);

    expect(differentResets(Region.NA, date, later)).toBe(false);
  });

  it('should not report tuesday night -> wednesday morning as across resets in EU', () => {
    const date = new Date('2020-12-08T23:59:59+01:00');
    const later = addHours(7, date);

    expect(differentResets(Region.EU, date, later)).toBe(false);
  });

  it('should report one-week offsets as different resets', () => {
    const date = new Date();
    const next = addWeeks(1, date);

    expect(differentResets(Region.NA, date, next)).toBe(true);
  });

});
