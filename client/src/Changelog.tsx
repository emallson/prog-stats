import { formatDate } from "./format";

const CHANGES = [
  {
    date: "2025-01-03T14:55-04:00",
    messages: [
      "Added detailed statistics about throughput and deaths over the course of progression.",
    ],
  },
  {
    date: "2024-12-25T16:20-04:00",
    messages: [
      "Updated hotfix information for Nerub-ar Palace",
      "Added automatic background refresh of known guilds with at least 4 Mythic bosses killed.",
      "Added automatic import of some progress rankings on later Mythic bosses.",
    ],
  },
  {
    date: "2024-10-09T16:55-04:00",
    messages: [
      "Fixed a bug that prevented statistics from being accumulated for some bosses.",
      "Fixed a crashing bug when statistics had not yet been generated for a boss.",
    ],
  },
  {
    date: "2024-08-22T14:40-04:00",
    messages: [
      "Rebuilt internal stats infrastructure to improve performance and allow kill-date-relative percentiles. Note: kill-date-relative percentiles are NOT enabled yet. Let @emallson know if you see errors!",
      "Added Neru-bar Palace data. ",
    ],
  },
  {
    date: "2023-06-07T10:29-05:00",
    messages: [
      "Fixed error when attempting to import logs if your guild had entirely empty reports in its history.",
    ],
  },
  {
    date: "2023-06-03T11:53-05:00",
    messages: [
      "Added support for loading data from logs marked as M+ logs by WCL.",
      "Added hotfix information.",
    ],
  },
  {
    date: "2023-05-08T14:42-05:00",
    messages: ["Added Aberrus raid and made it the default."],
  },
  {
    date: "2023-04-05T00:54-05:00",
    messages: [
      "Added hotfix information for Vault of the Incarnates.",
      "Dude, imagine progressing pre-nerf Raszageth. wtf",
    ],
  },
  {
    date: "2023-03-11T10:51-05:00",
    messages: ["Corrected handling of in-progress pulls."],
  },
  {
    date: "2022-12-28T11:23-05:00",
    messages: [
      "Corrected handling of simultaneous release for Vault of the Incarnates.",
      "Updated bosses shown on the front page.",
    ],
  },
  {
    date: "2022-07-05T11:53-04:00",
    messages: [
      "Added hotfix information for Sepulcher of the First Ones.",
      "Yes, all of them.",
    ],
  },
  {
    date: "2021-11-19T18:23-05:00",
    messages: ["Updated hotfix information for Sanctum of Domination"],
  },
  {
    date: "2021-09-17T13:40-04:00",
    messages: [
      "Added hotfix information for Sanctum of Domination",
      "Changed the bosses shown on the front page.",
    ],
  },
  {
    date: "2021-08-09T12:25-04:00",
    messages: [
      "Fixed handling of private log access with updated WarcraftLogs API.",
    ],
  },
  {
    date: "2021-07-22T14:16-04:00",
    messages: ["Improved spacing of axis labels on Tier Stats page."],
  },
  {
    date: "2021-06-29T13:55-04:00",
    messages: ["Sanctum of Domination is now supported."],
  },
  {
    date: "2021-06-25T16:00-04:00",
    messages: ["Final Castle Nathria hotfix update."],
  },
  {
    date: "2021-05-17T16:27-04:00",
    messages: [
      "Added phase information to log charts. Log data will be re-imported on first viewing as a result of this change.",
    ],
  },
  {
    date: "2021-05-07T14:59-04:00",
    messages: [
      "Added support for previous tiers. <em>This has been live for a couple of weeks but relatively hidden.</em>",
      "The Tier Stats page is now mobile responsive.",
    ],
  },
  {
    date: "2021-04-16T12:15-04:00",
    messages: [
      "Revamped the Raid Time display to free me from this DST prison.",
      "Added Kills per Week chart to Tier Overview page.",
      "Internal database code restructure to (hopefully) further improve performance.",
    ],
  },
  {
    date: "2021-03-28T11:39-04:00",
    messages: [
      "Improved caching system to help cope with Wowhead traffic. Hi!",
      "Fixed an issue that could prevent revoking log access.",
    ],
  },
  {
    date: "2021-03-21T16:58-05:00",
    messages: [
      "Raid Night estimation now automatically shuts off instead of outputting garbage for guilds with inconsistent raid times and/or quick raid clears.",
    ],
  },
  {
    date: "2021-03-19T13:11-05:00",
    messages: [
      "The Tier Stats page is now public. Let's see how long it takes reddit to bring the site to its knees...",
    ],
  },
  {
    date: "2021-03-16T16:00-05:00",
    messages: ["Raid Night estimation has been enabled with DST correction."],
  },
  {
    date: "2021-03-14T14:26-05:00",
    messages: [
      "Raid Night estimation has been disabled for the time being due to issues with <strong>Daylight Savings Time.</strong> This will reduce the accuracy of Hours / Week estimation. When we find a way to account for DST in Raid Night estimation (complicated by DST's regional nature), we will re-enable this feature.",
    ],
  },
  {
    date: "2021-03-11T17:43-05:00",
    messages: [
      "Encounter Overview charts now show pull start time on the X axis (was pull number). As a result, extended breaks are now visible on charts.",
      "Greatly improved mobile support.",
      "Fixed an issue where a log ending exactly as another begins would cause the second to be incorrectly ignored.",
    ],
  },
  {
    date: "2021-03-10T19:41-05:00",
    messages: [
      "Rewrote raid night estimation code to better handle day raiding and very short progression windows (hi max). As always: if your raid time is shown incorrectly please contact me on Discord.",
      "Rewrote stat tracking methods in preparation for raid tier statistics page.",
    ],
  },
  {
    date: "2021-03-09T10:29-05:00",
    messages: [
      "Improved logic for splitting / merging charts and raid weeks in encounter overviews&mdash;notably fixing several issues with Complexity Limit's Sire Denathrius and Stone Legion Generals overviews.",
      "Added dark mode support.",
      "Added support for TW & KR regions.",
    ],
  },
  {
    date: "2021-03-05T16:02-05:00",
    messages: [
      "Misc. bugfixes to (hopefully) decrease the number of errors encountered.",
      "Analysis is no longer re-run every time a team is viewed. It will be re-run automatically when new data is available, and can be forced to run by reloading the page.",
    ],
  },
  {
    date: "2021-03-03T12:04-05:00",
    messages: [
      'Hours / Week now shows median by default. If the maximum observed weekly raid hours is at least 1 hour more than the median, then it shows "median &mdash; maximum".',
      "Private log access and revocation should be much more reliable.",
    ],
  },
  {
    date: "2021-03-02T18:01-05:00",
    messages: [
      "Multi-day logs will no longer exaggerate progression time and are now correctly split across multiple days when charted.",
    ],
  },
  {
    date: "2021-03-02T15:24-05:00",
    messages: [
      'Adjusted server configuration to handle more concurrent connections. Hi <a href="https://www.reddit.com/r/CompetitiveWoW/comments/lw7kn9/new_site_for_mythic_progression_stats/" class="underline">reddit!</a>',
      "Fixed issue with guild link decoding if you tried typing the URL instead of pasting it.",
    ],
  },
  {
    date: "2021-03-01T17:04-05:00",
    messages: [
      "Greatly improved the accuracy of raid time inference.",
      "Increased raid time granularity to 15 minutes (was 30 minutes). Previously, a start time of 8:15pm would have been called 8:30pm due to this.",
      "Added the ability to see infrequent raid times. These raid times may be due to holiday rescheduling or infrequent overtime.",
      "Added support for WarcraftLog links that use numeric IDs instead of region/server/name.",
    ],
  },
  {
    date: "2021-02-28T00:42-05:00",
    messages: [
      'Added distribution information for pull count and progression time to each encounter overview, along with (inverted) percentile information. See the <a class="underline" href="/faq#What-percentile-is-best-for-pull-counts-and-progression-times">FAQ</a> for details.',
      "Mid-reset hotfixes are now correctly attached to the first raid night following the hotfix. Note that we only have estimated times for historical hotfixes, so in some cases it may be one day off due to timing.",
      "Removed log links from encounter charts. These will return at a later point in time when we can guarantee that we won't link to an unlisted log.",
      "Hotfix data now correctly appears even if a week of progression was skipped.",
    ],
  },
  {
    date: "2021-02-27T20:47:00-05:00",
    messages: [
      "Fixed an issue loading guilds on realms with dashes in the name. Wowprogress uses a different encoding than every other WoW site, and I accidentally broke everything else trying to work around that. Oops.",
    ],
  },
  {
    date: "2021-02-26T18:18:00-05:00",
    messages: ["Added data about major hotfixes to the fight overviews."],
  },
  {
    date: "2021-02-25T23:41:00-05:00",
    messages: [
      "Removed PTR logs from analysis.",
      'The "Min ilvl" and "Max ilvl" fields have been replaced by "First Pull Avg Raid ilvl" and "Kill Pull Avg Raid ilvl" to give a more grokkable stat.',
      "Loosened the requirement on the number of times a raid date must appear to be included in the Team Overview. If you were missing a raid date, it is probably present now.",
      "Greatly reduced the frequency with which raid times were 30 minutes off.",
    ],
  },
];

export function Change({
  date,
  messages,
  undocumented,
}: {
  date: string | Date;
  messages: string[];
  undocumented?: boolean;
}) {
  return (
    <>
      <dt className="border-b border-gray-200 w-min whitespace-nowrap">
        {formatDate(new Date(date)!, true)}
      </dt>
      <dd className="mb-5 last:mb-0">
        <ul className="list-dash pl-5">
          {messages.map((msg, ix) => (
            <li
              key={ix}
              dangerouslySetInnerHTML={{
                __html: `${undocumented ? "[Undocumented] " : ""}${msg}`,
              }}
            ></li>
          ))}
        </ul>
      </dd>
    </>
  );
}

export default function Changelog() {
  return (
    <section className="mt-10 mb-10" id="recent-changes">
      <header className="font-serif text-2xl">
        <legend>Recent Changes</legend>
      </header>
      <dl className="text-lg">
        {CHANGES.map((change, ix) => (
          <Change {...change} key={ix} />
        ))}
      </dl>
    </section>
  );
}
