import { useState, useEffect, useMemo } from "react";
import { gql } from "graphql-request";
import { useLocation } from "react-router-dom";
import { createClient } from "graphql-ws";
import { host, isDev } from "./config";

const SYNC_SUB = gql`
  subscription SyncGuild($id: Int!, $tag: Int, $zone: Int!, $force: Boolean) {
    sync(guildId: $id, tag: $tag, zone: $zone, forceAnalysis: $force) {
      kind
      totalLogs
      remainingLogs
    }
  }
`;

const client = createClient({
  url: `ws${isDev ? "" : "s"}://${host}/graphql`,
});

// not even pretending to make this generic
function useSubscription(
  id: number,
  tag: number | undefined,
  zone: number,
  force?: boolean,
) {
  const [state, setState] = useState<{ data?: any; error?: Error }>({});

  useEffect(() => {
    const subscription = client.iterate({
      query: SYNC_SUB,
      variables: { id, tag, zone, force },
    });

    let active = true;

    (async () => {
      try {
        for await (const event of subscription) {
          if (!active) {
            break;
          }
          setState({ data: event.data });
        }
      } catch (err) {
        if (!active) {
          return;
        }
        setState({ error: err as Error });
      }
    })();

    return () => {
      active = false;
    };
  }, [id, tag, zone, force]);

  return state;
}

export default function Sync({
  syncCallback,
  id,
  tag,
  zone,
}: {
  id: number;
  syncCallback: () => void;
  tag?: number;
  zone: number;
}) {
  const loc = useLocation();
  const force = useMemo(() => loc.search.includes("force=true"), [loc.search]);
  const { error, data } = useSubscription(id, tag, zone, force);

  const [cancelled, setCancelled] = useState(false);
  const [progress, setProgress] = useState(0);
  useEffect(() => {
    if (data && data.sync.kind === "COMPLETE") {
      syncCallback();
    }
  }, [data, syncCallback]);

  useEffect(() => {
    if ((data && data.sync.kind === "CANCELLED") || error) {
      setCancelled(true);
      if (progress === 0) {
        setProgress(100);
      }
    } else if (data && data.sync.remainingLogs && data.sync.totalLogs) {
      const new_prog =
        (1 - data.sync.remainingLogs / data.sync.totalLogs) * 100;
      setProgress(new_prog);
    }
  }, [data, error]); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <div className="m-auto mt-10 max-w-xl">
      <span>{cancelled ? "Unable to import logs" : "Importing Logs..."}</span>
      <div className="h-3 relative max-w-xl rounded-full overflow-hidden">
        <div className="w-full h-full bg-gray-200 absolute"></div>
        <div
          className={`h-full absolute ${
            cancelled ? "bg-red-500" : "bg-blue-500"
          }`}
          style={{ width: `${progress}%` }}
        ></div>
      </div>
    </div>
  );
}
