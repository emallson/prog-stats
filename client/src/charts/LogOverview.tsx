import { createClassFromSpec } from "react-vega";
import useMedia from "../useMedia";
import useConfig from "./useConfig";
import * as vega from "vega";

const LogProgChart = createClassFromSpec({
  mode: "vega-lite",
  spec: {
    bounds: "flush",
    width: 250,
    height: 205,
    data: { name: "fights" },
    transform: [
      { calculate: "if(datum.kill, 0, datum.bossPct / 100)", as: "bossPct" },
      {
        calculate: "if(datum.kill, 'kill', datum.lastPhase)",
        as: "lastPhaseOrKill",
      },
      {
        lookup: "lastPhaseOrKill",
        from: {
          data: { name: "phaseColors" },
          key: "phase",
          fields: ["color"],
        },
        default: "#7f7c75",
        as: ["phaseColor"],
      },
      {
        calculate:
          "if(datum.lastPhaseIsIntermission, 'Intermission ', 'Phase ') + max(datum.lastPhase, 1)",
        as: "phaseLabel",
      },
      {
        calculate:
          "format((datum.endTime - datum.startTime) / 1000 / 60, ',d') + 'm ' + format((datum.endTime - datum.startTime) / 1000 % 60, ',d') + 's'",
        as: "duration",
      },
    ],
    layer: [
      {
        mark: "line",
        transform: [{ loess: "bossPct", on: "startTime", bandwidth: 0.3 }],
        encoding: {
          x: {
            scale: { zero: false, nice: false },
            field: "startTime",
            type: "quantitative",
            axis: null,
            title: null,
          },
          y: {
            field: "bossPct",
            type: "quantitative",
            title: null,
            axis: {
              values: [0, 0.5],
              format: "%",
              ticks: false,
              labelFlush: true,
              labelBaseline: "line-bottom",
              labelPadding: -2,
              labelAlign: "left",
              domain: false,
            },
          },
        },
      },
      {
        mark: "point",
        encoding: {
          x: {
            scale: { zero: false, nice: false },
            field: "startTime",
            type: "quantitative",
            axis: null,
            title: null,
          },
          y: {
            field: "bossPct",
            type: "quantitative",
            scale: {
              domain: [0, 1],
            },
            title: null,
          },
          shape: {
            field: "lastPhaseIsIntermission",
            type: "nominal",
            scale: {
              domain: [false, true],
              range: ["circle", "triangle-down"],
            },
            title: null,
            legend: null,
          },
          color: {
            field: "lastPhaseOrKill",
            type: "nominal",
            legend: null,
            title: null,
            scale: { range: { field: "phaseColor" } },
          },
          tooltip: [
            { title: "Phase", field: "phaseLabel" },
            { title: "Duration", field: "duration" },
            { title: "Remaining", field: "bossPct", format: ".1%" },
          ],
        },
      },
    ],
  },
});

const lightFightChart: object = {
  padding: {
    left: 5,
    right: 5,
    top: 7,
    bottom: 7,
  },
  autosize: {
    type: "none",
    contains: "padding",
  },
  range: {
    category: ["grey", "red"],
  },
  point: {
    fill: null,
  },
};

const darkFightChart: object = {
  ...lightFightChart,
  range: {
    category: ["grey", "hsla(0, 100%, 60%, 1)"],
  },
  axis: {
    labelColor: "#fffff8",
    gridColor: "#9ca3af",
  },
};

const hideText = { labels: false };

// stolen from vega-scale because this isn't exported apparently?!?
function quantizeInterpolator(
  interpolator: (ix: number) => string,
  count: number
): string[] {
  const samples = new Array(count),
    n = count + 1;
  for (let i = 0; i < count; ) samples[i] = interpolator(++i / n);
  return samples;
}

interface PhaseColor {
  phase: number | "kill";
  color: string;
}

function phaseColors(numPhases: number, dark: boolean): PhaseColor[] {
  const civ = vega.scheme("cividis");
  const colors: PhaseColor[] = quantizeInterpolator(civ, numPhases + 2)
    .slice(1, -1)
    .map((color, ix) => ({
      phase: ix + 1,
      color,
    }));
  colors.unshift({ color: colors[0].color, phase: 0 });
  colors.push({
    phase: "kill" as const,
    color: dark ? "hsla(0, 100%, 60%, 1)" : "red",
  });

  return colors;
}

export default function LogOverview({
  numPhases,
  fights,
  withLabels,
}: {
  numPhases?: number;
  fights: any;
  withLabels: boolean;
}) {
  const cfg = useConfig(lightFightChart, darkFightChart);
  const darkMode = useMedia(["(prefers-color-scheme: dark)"], [true], false);

  return (
    <LogProgChart
      data={{
        fights: JSON.parse(JSON.stringify(fights)),
        phaseColors: phaseColors(numPhases || 3, darkMode),
      }}
      actions={false}
      renderer="svg"
      config={{ axisY: withLabels ? undefined : hideText, ...cfg }}
    />
  );
}
