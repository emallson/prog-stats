import { createClassFromSpec } from "react-vega";
import useConfig from "./useConfig";
import LightTooltip from "../Tooltip";

export interface DistInfo {
  count: number;
  min: number;
  max: number;
  quantiles: [number, number, number];
  actualPct?: number;
}

const DistPlot = createClassFromSpec({
  mode: "vega-lite",
  spec: {
    bounds: "flush",
    width: 60,
    height: 12,
    background: "#00000000",
    data: { name: "dist" },
    layer: [
      {
        mark: { type: "rule", opacity: 0 },
        encoding: {
          x: {
            field: "min",
            type: "quantitative",
            scale: { zero: false },
            title: null,
            sort: "descending",
          },
          x2: { field: "max", type: "quantitative" },
        },
      },
      {
        mark: { type: "rule" },
        encoding: {
          x: {
            field: "lower",
            type: "quantitative",
            scale: { zero: false },
            title: null,
            axis: null,
          },
          x2: { field: "upper", type: "quantitative" },
        },
      },
      {
        mark: { type: "bar", height: { band: 1.5 } },
        encoding: {
          x: {
            field: "q1",
            type: "quantitative",
            scale: { zero: false },
            title: null,
          },
          x2: { field: "q3", type: "quantitative" },
        },
      },
      {
        mark: { type: "tick" },
        encoding: {
          x: { field: "median", type: "quantitative" },
        },
      },
      {
        mark: { type: "point", opacity: 1 },
        data: { name: "actual" },
        encoding: {
          x: { field: "x", type: "quantitative" },
        },
      },
    ],
  },
});

const darkDistChart: object = {
  bar: {
    color: "gray",
    fill: "hsla(0, 0%, 40%, 1)",
  },
  tick: {
    color: "black",
  },
  rule: {
    color: "white",
  },
};

const lightDistChart: object = {
  bar: {
    color: "gray",
    fill: "lightGray",
  },
  tick: {
    color: "black",
  },
};

export default function BoxPlot({
  id,
  dist,
  actual,
}: {
  actual: number;
  dist?: DistInfo;
  id: string;
}) {
  const config = useConfig(lightDistChart, darkDistChart);

  if (!dist || (dist.count <= 100 && process.env.NODE_ENV !== "development")) {
    return null;
  }

  const scaled_pct =
    dist.actualPct !== undefined ? Math.floor((1 - dist.actualPct) * 100) : 0;
  const suffix =
    scaled_pct >= 10 && scaled_pct <= 20
      ? "th"
      : ["st", "nd", "rd"][(scaled_pct % 10) - 1] || "th";

  const iqr = dist.quantiles[2] - dist.quantiles[0];
  const lower = dist.quantiles[0] - 1.5 * iqr;
  const upper = dist.quantiles[2] + 1.5 * iqr;

  const data = [
    {
      min: dist.min,
      max: dist.max,
      lower,
      upper,
      q1: dist.quantiles[0],
      q3: dist.quantiles[2],
      median: dist.quantiles[1],
    },
  ];

  return (
    <LightTooltip id={id} desc={`${scaled_pct}${suffix} Percentile`}>
      <DistPlot
        data={{ dist: data, actual: [{ x: actual }] }}
        actions={false}
        className="align-text-bottom"
        renderer="svg"
        config={config}
      />
    </LightTooltip>
  );
}
