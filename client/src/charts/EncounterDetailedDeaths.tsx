import { createClassFromSpec } from "react-vega";
import useConfig from "./useConfig";
import { Config } from "vega-lite";

export default function EncounterDetailedDeaths({
  encounterID,
  deaths,
}: {
  encounterID: number;
  deaths: DeathDetails[];
}): JSX.Element | null {
  const config = useConfig({}, darkConfig);
  return (
    <div className="col-start-1 col-end-3 justify-self-end flex flex-col items-center 2xl:justify-self-start 2xl:col-start-2">
      <span className="text-lg">Deaths Over Time</span>
      <Chart
        data={{ data: deaths }}
        config={config}
        renderer="svg"
        actions={false}
      />
      <p className="italic text-center text-sm text-gray-500 dark:text-gray-200">
        Click on a point or on the legend to highlight an ability.
      </p>
    </div>
  );
}

const darkConfig = {
  point: {
    stroke: "#000000",
    strokeWidth: 1,
  },
} satisfies Config;

const Chart = createClassFromSpec({
  mode: "vega-lite",
  spec: {
    data: {
      name: "data",
    },
    bounds: "flush",
    width: 1000,
    height: 350,
    transform: [
      {
        calculate:
          'toString(datum.pullNumber) + " - " + toString(datum.pullNumber + datum.groupSize - 1)',
        as: "pullLabel",
      },
    ],
    layer: [
      {
        mark: {
          type: "point",
          strokeWidth: 1.5,
        },
        params: [
          {
            name: "selected",
            select: {
              type: "point",
              fields: ["abilityName"],
              on: "click",
            },
            bind: "legend",
          },
        ],
        transform: [
          {
            calculate: "datum.timeMs - datum.spanMs / 2",
            as: "lo",
          },
          {
            calculate: "datum.timeMs + datum.spanMs / 2",
            as: "hi",
          },
          {
            calculate:
              'if(datum.timeMs >= 60000, toString(floor(datum.timeMs / 60000)) + "m ", "") + toString(floor(datum.timeMs % 60000 / 1000)) + "s ±" + toString(floor(datum.spanMs / 2 / 1000)) + "s"',
            as: "timeLabel",
          },
          {
            joinaggregate: [{ op: "sum", field: "total", as: "abilityTotal" }],
            groupby: ["abilityName"],
          },
          {
            window: [
              {
                op: "dense_rank",
                as: "freq_rank",
              },
            ],
            sort: [{ field: "abilityTotal", order: "descending" }],
          },
          {
            filter: "datum.freq_rank <= 20",
          },
          {
            window: [
              {
                op: "row_number",
                as: "row",
              },
            ],
            sort: [{ field: "timeMs", order: "ascending" }],
            groupby: ["pullNumber"],
          },
          {
            calculate: "datum.row % 4",
            as: "jitter",
          },
          {
            calculate:
              "if (datum.abilityName === null, 'Unknown', datum.abilityName)",
            as: "abilityName",
          },
        ],
        encoding: {
          x: {
            field: "pullLabel",
            type: "nominal",
            title: "Pull #",
            sort: {
              field: "pullNumber",
              order: "ascending",
            },
            axis: {
              grid: true,
              bandPosition: -0.1,
              ticks: false,
              labelAngle: -45,
            },
          },
          xOffset: {
            field: "jitter",
            type: "quantitative",
            scale: {
              domainMin: -0.5,
              domainMax: 3.5,
            },
          },
          y: {
            field: "timeMs",
            type: "quantitative",
            title: "Time in Fight",
            axis: {
              labelExpr: "toString(datum.value / 1000 / 60) + 'm'",
              values: Array.from({ length: 10 }).map((_, ix) => ix * 120000),
              grid: false,
            },
          },
          fill: {
            field: "abilityName",
            type: "nominal",
            scale: {
              scheme: "tableau20",
            },
            title: "Ability",
          },
          size: {
            legend: null,
            field: "total",
            type: "quantitative",
            scale: {
              rangeMax: 200,
              rangeMin: 25,
            },
          },
          order: {
            field: "total",
            sort: "descending",
          },
          opacity: {
            value: 0.1,
            condition: { param: "selected", value: 0.9 },
          },
          tooltip: [
            { field: "abilityName", title: "Ability" },
            { field: "total", title: "Death Count" },
            { field: "timeLabel", title: "Time" },
          ],
        },
      },
    ],
  },
});

interface DeathDetails {
  pullNumber: number;
  groupSize: number;
  timeMs: number;
  spanMs: number;
  total: number;
  abilityName: string | null;
}
