import useMedia from "../useMedia";
import { Config } from "vega-lite";
import merge from "lodash.merge";
import "./tooltip.css";

const default_color = "hsla(226, 13%, 38%, 1)";
const dark_color = "hsla(226, 15%, 45%, 1)";

const base = {
  padding: 1,
  background: "#00000000",
  axis: {
    labelFontSize: 12,
    labelFont: "Iosevka Custom, ui-sans-serif, sans-serif",
    titleFont: "Libertinus Sans, ui-sans-serif, sans-serif",
    titleFontSize: 14,
    labelSeparation: 10,
  },
  view: {
    stroke: "transparent",
  },
  range: {
    category: { scheme: "cividis" },
    ordinal: { scheme: "cividis" },
    ramp: {
      scheme: "cividis",
    },
  },
  mark: {
    color: default_color,
  },
  point: {
    fill: default_color,
  },
} satisfies Config;

const point_color_dark = "hsla(226, 18%, 70%, 1)";

const baseDark = {
  ...base,
  mark: {
    color: dark_color,
  },
  axis: {
    ...base.axis,
    labelColor: "#fffff8",
    gridColor: "#9ca3af",
    tickColor: "#9ca3af",
    titleColor: "#fffff8",
    domainColor: "#9ca3af",
  },
  point: {
    // lighten this up even more
    color: point_color_dark,
    fill: point_color_dark,
  },
  header: {
    labelColor: "#fffff8",
  },
  legend: {
    titleColor: "#fffff8",
    labelColor: "#fffff8",
  },
} satisfies Config;

enum Mode {
  Light,
  Dark,
}

export default function useConfig(light: Config, dark: Config) {
  const mode = useMedia(
    ["(prefers-color-scheme: dark)"],
    [Mode.Dark],
    Mode.Light,
  );

  switch (mode) {
    case Mode.Light:
      return merge({}, base, light);
    case Mode.Dark:
      return merge({}, baseDark, dark);
  }
}
