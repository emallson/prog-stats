import { createClassFromSpec } from "react-vega";
import { TopLevelSpec } from "vega-lite";
import useConfig from "./useConfig";
import useMedia from "../useMedia";
import max from "date-fns/max";
import min from "date-fns/min";
import differenceInWeeks from "date-fns/differenceInWeeks";
import { useMemo } from "react";
import { NonNormalizedSpec } from "vega-lite/build/src/spec";

enum MetricType {
  ProgTime = "PROG_TIME",
  PullCount = "PULL_COUNT",
  StartIlvl = "START_ILVL",
  EndIlvl = "END_ILVL",
}

function histogram(
  title: string,
  field: string,
  extraTransforms: any[] = [],
): NonNormalizedSpec {
  return {
    data: { name: "stats" },
    transform: [
      {
        filter: { param: "range" },
      },
      ...extraTransforms,
    ],
    mark: "bar",
    encoding: {
      x: {
        field,
        type: "quantitative",
        title,
        bin: { maxbins: 20 },
        scale: {
          zero: false,
        },
      },
      y: {
        aggregate: "count",
        title: null,
      },
    },
  };
}

const EncounterBreakdownChart = createClassFromSpec({
  mode: "vega-lite",
  spec: {
    data: { name: "stats" },
    concat: [
      {
        layer: [
          {
            params: [
              {
                name: "range",
                select: { type: "interval", encodings: ["x"] },
              },
            ],
            mark: "line",
            encoding: {
              x: {
                field: "killTime",
                type: "temporal",
                axis: {
                  grid: false,
                },
                title: "Kill Date",
              },
              y: {
                field: "killNumber",
                type: "quantitative",
                axis: {
                  grid: false,
                },
                title: "# of Kills",
              },
            },
          },
          {
            mark: { type: "rule", strokeDash: [4, 4], width: 4 },
            data: { name: "hotfixes" },
            encoding: {
              x: {
                field: "applicationDate",
                type: "temporal",
              },
            },
          },
          {
            mark: { type: "rule", strokeWidth: 10, color: "#00000000" },
            data: { name: "hotfixes" },
            encoding: {
              x: {
                field: "applicationDate",
                type: "temporal",
              },
              tooltip: [
                {
                  field: "applicationDate",
                  type: "temporal",
                  title: "Hotfix",
                },
              ],
            },
          },
        ],
      },
      histogram("Pull Count", "pullCount"),
      histogram("Progression Time (Hours)", "progTimeHr", [
        {
          filter: "datum.progTime > 0",
        },
        {
          calculate: "datum.progTime / 1000 / 60 / 60",
          as: "progTimeHr",
        },
      ]),
      {
        mark: "bar",
        transform: [
          {
            filter: { param: "range" },
          },
        ],
        encoding: {
          x: {
            field: "killTime",
            timeUnit: "yearweek",
            type: "temporal",
            axis: {
              grid: false,
            },
            title: "Kill Date",
          },
          y: {
            aggregate: "count",
            title: "Kills per Week",
          },
        },
      },
      histogram("Kill ilvl", "endIlvl", [{ filter: "datum.endIlvl > 0" }]),
      histogram("Starting ilvl", "startIlvl", [
        { filter: "datum.startIlvl > 0" },
      ]),
    ],
  },
});

function histogramBars(
  title: string,
  type: string,
  extraTransforms: any[] = [],
  fieldStart?: string,
  fieldEnd?: string,
): NonNormalizedSpec {
  const clipZero =
    type === MetricType.ProgTime || type === MetricType.PullCount;
  return {
    data: {
      name: type,
    },
    transform: [{ filter: { param: "range" } }, ...extraTransforms],
    mark: { type: "bar", clip: true, strokeWidth: 1 },
    encoding: {
      x: {
        field: fieldStart ?? "binStart",
        type: "quantitative",
        title,
        scale: {
          domainMin: clipZero ? 0 : undefined,
          zero: clipZero,
        },
      },
      x2: {
        field: fieldEnd ?? "binEnd",
      },
      y: {
        field: "count",
        aggregate: "sum",
        type: "quantitative",
        title: null,
      },
    },
  };
}

const EncounterBreakdownChartV2 = createClassFromSpec({
  mode: "vega-lite",
  spec: {
    concat: [
      {
        layer: [
          {
            data: { name: MetricType.PullCount },
            params: [
              {
                name: "range",
                select: { type: "interval", encodings: ["x"] },
              },
            ],
            mark: "line",
            transform: [
              {
                window: [{ op: "sum", field: "count", as: "cumsum" }],
                frame: [null, 0],
                sort: [{ field: "killDate", order: "ascending" }],
              },
            ],
            encoding: {
              x: {
                field: "killDate",
                type: "temporal",
                axis: {
                  grid: false,
                  labelOverlap: "greedy",
                  labelSeparation: 20,
                },
                title: "Kill Date",
              },
              y: {
                field: "cumsum",
                type: "quantitative",
                axis: {
                  grid: false,
                },
                title: "# of Kills",
              },
            },
          },
          {
            mark: { type: "rule", strokeDash: [4, 4], width: 4 },
            data: { name: "hotfixes" },
            encoding: {
              x: {
                field: "applicationDate",
                type: "temporal",
              },
            },
          },
          {
            mark: { type: "rule", strokeWidth: 10, color: "#00000000" },
            data: { name: "hotfixes" },
            encoding: {
              x: {
                field: "applicationDate",
                type: "temporal",
              },
              tooltip: [
                {
                  field: "applicationDate",
                  type: "temporal",
                  title: "Hotfix",
                },
              ],
            },
          },
        ],
      },
      histogramBars("Pull Count", MetricType.PullCount),
      histogramBars(
        "Progression Time (Hours)",
        MetricType.ProgTime,
        [
          { calculate: "datum.binStart / 1000 / 60 / 60", as: "progTimeStart" },
          { calculate: "datum.binEnd / 1000 / 60 / 60", as: "progTimeEnd" },
        ],
        "progTimeStart",
        "progTimeEnd",
      ),
      {
        data: { name: MetricType.PullCount },
        mark: "bar",
        transform: [
          {
            filter: { param: "range" },
          },
        ],
        encoding: {
          x: {
            field: "killDate",
            timeUnit: "yearweek",
            type: "temporal",
            axis: {
              grid: false,
              labelOverlap: "greedy",
              labelSeparation: 20,
            },
            title: "Kill Date",
          },
          y: {
            aggregate: "count",
            title: "Kills per Week",
          },
        },
      },
      histogramBars("Kill ilvl", MetricType.EndIlvl),
      histogramBars("Starting ilvl", MetricType.StartIlvl),
    ],
  },
});

const lightChart = {
  bar: {
    stroke: "white",
  },
};
const darkChart = {
  bar: {
    stroke: "black",
  },
  rule: {
    color: "white",
  },
};

const shortTier = {
  axisTemporal: {
    format: "%e %b %g",
  },
};

const longTier = {
  axisTemporal: {
    format: "%b %g",
  },
};

function long_tier(stats: EncounterSummaryCollection["data"] | HistogramBin[]) {
  const last = max(stats.map(killDate));
  const first = min(stats.map(killDate));
  return differenceInWeeks(last, first) >= 4;
}

function killDate(datum: EncounterSummaryRow | HistogramBin): Date {
  if ("metricType" in datum) {
    return new Date((datum.dayIndex - 719163) * 24 * 60 * 60 * 1000);
  } else {
    return new Date(datum.killTime);
  }
}

export default function EncounterBreakdown({
  stats: statsUnion,
  hotfixes,
}: {
  stats: EncounterSummaryCollection | HistogramBinCollection;
  hotfixes: any;
}) {
  const cfg = useConfig(lightChart, darkChart);
  const columns = useMedia(["(min-width: 768px)"], [3], 1);

  cfg.concat = { columns };

  const data = useMemo(() => {
    return {
      bins:
        statsUnion.__typename === "ColumnarBins"
          ? columnsToBins(statsUnion)
          : undefined,
      stats:
        statsUnion.__typename === "EncounterSummaryCollection"
          ? statsUnion.data
          : [],
    };
  }, [statsUnion]);

  const hotfixesToShow = useMemo(() => {
    const firstKillSeen = min(
      data.bins
        ? data.bins[MetricType.PullCount].map(killDate)
        : data.stats.map(killDate),
    );
    return hotfixes.filter(
      (hotfix: { applicationDate: string }) =>
        new Date(hotfix.applicationDate).getTime() >= firstKillSeen.getTime(),
    );
  }, [data]);

  const tier_cfg = useMemo(() => {
    return long_tier(data.bins ? data.bins[MetricType.PullCount] : data.stats)
      ? longTier
      : shortTier;
  }, [data]);

  if (statsUnion.__typename === "ColumnarBins") {
    return (
      <EncounterBreakdownChartV2
        data={{
          ...data.bins,
          hotfixes: hotfixesToShow,
        }}
        config={{ ...cfg, ...tier_cfg }}
        renderer="svg"
        actions={false}
      />
    );
  }

  return (
    <EncounterBreakdownChart
      data={{
        stats: data.stats,
        hotfixes: hotfixesToShow,
      }}
      renderer="svg"
      actions={false}
      config={{ ...cfg, ...tier_cfg }}
    />
  );
}

interface EncounterSummaryCollection {
  __typename: "EncounterSummaryCollection";
  data: Array<EncounterSummaryRow>;
}

interface EncounterSummaryRow {
  pullCount: number;
  progTime: number;
  startIlvl: number;
  endIlvl: number;
  startTime: number;
  killTime: number;
  killNumber: number;
}

interface HistogramBinCollection {
  __typename: "ColumnarBins";
  metricType: MetricType[];
  binStart: number[];
  binEnd: number[];
  count: number[];
  dayIndex: number[];
}

interface HistogramBin {
  metricType: MetricType;
  dayIndex: number;
  binStart: number;
  binEnd: number;
  binCenter: number;
  count: number;
  killDate: Date;
}

function columnsToBins(
  data: HistogramBinCollection,
): Record<MetricType, HistogramBin[]> {
  const result: Record<MetricType, HistogramBin[]> = {
    [MetricType.PullCount]: [],
    [MetricType.ProgTime]: [],
    [MetricType.StartIlvl]: [],
    [MetricType.EndIlvl]: [],
  };
  for (let i = 0; i < data.metricType.length; i++) {
    const metricType = data.metricType[i];
    result[metricType].push({
      metricType,
      dayIndex: data.dayIndex[i],
      binStart: data.binStart[i],
      binEnd: data.binEnd[i],
      binCenter: (data.binEnd[i] - data.binStart[i]) / 2 + data.binStart[i],
      count: data.count[i],
      killDate: new Date((data.dayIndex[i] - 719163) * 24 * 60 * 60 * 1000),
    });
  }

  return result;
}
