import { createClassFromSpec } from "react-vega";
import { formatPrettyDuration } from "../format";
import useConfig from "./useConfig";

const RaidTimePlot = createClassFromSpec({
  mode: "vega-lite",
  spec: {
    bounds: "flush",
    width: 145,
    height: 24,
    data: { name: "times" },
    mark: "bar",
    transform: [
      {
        fold: ["prog", "farm"],
      },
      {
        calculate: "if(datum.key == 'prog', 0, 1)",
        as: "sort",
      },
    ],
    encoding: {
      x: {
        field: "week",
        type: "ordinal",
        title: "Week",
        axis: null,
      },
      y: {
        field: "value",
        type: "quantitative",
        axis: null,
        stack: true,
      },
      color: {
        field: "key",
        type: "nominal",
        title: null,
        legend: null,
        scale: {
          domain: ["prog", "farm"],
        },
      },
      order: {
        field: "sort",
        type: "ordinal",
      },
      tooltip: [
        {
          field: "week",
          type: "ordinal",
          title: "Week",
        },
        {
          field: "progLabel",
          type: "nominal",
          title: "Progression Time",
        },
        {
          field: "farmLabel",
          type: "nominal",
          title: "Farm Time",
        },
      ],
    },
  },
});

export default function RaidTime({ data }: { data: any }) {
  const config = useConfig(
    {
      bar: { stroke: "white", strokeWidth: 1 },
    },
    {
      bar: { stroke: "black", strokeWidth: 1 },
    },
  );
  const mapped = data.map((datum: any) => ({
    ...datum,
    progLabel: datum.prog ? formatPrettyDuration(datum.prog) : "",
    farmLabel: datum.farm ? formatPrettyDuration(datum.farm) : "",
  }));

  return (
    <RaidTimePlot
      data={{ times: mapped }}
      actions={false}
      config={config}
      renderer="svg"
    />
  );
}
