import { createClassFromSpec } from "react-vega";
import useConfig from "./useConfig";
import { useMemo } from "react";
import { AxisConfig } from "vega-lite/build/src/axis";
import { FixedVegaChartProps } from "react-vega/lib/createClassFromSpec";
import { Config } from "vega-lite";
import { TooltipContent } from "vega-lite/build/src/mark";
import { StringFieldDef } from "vega-lite/build/src/channeldef";

export default function EncounterDetailedStats({
  encounterID,
  stats,
}: {
  encounterID: number;
  stats: FightDetailedStats[];
}): JSX.Element | null {
  const config = useConfig(sharedConfig, darkConfig);
  const data = useMemo(() => toRects(stats), [stats]);
  return (
    <div className="col-start-1 col-end-3 w-min justify-self-end 2xl:col-start-2 2xl:justify-self-start">
      <div className="grid grid-flow-col-dense gap-2">
        {Object.entries(data).map(([key, data]) => (
          <div key={key} className="flex flex-col items-end">
            <span>{labels[key] ?? key}</span>
            <Chart
              statType={key}
              data={{
                data: data,
              }}
              config={{
                ...config,
                axisY: axes[key],
              }}
              renderer="svg"
              actions={false}
            />
          </div>
        ))}
      </div>
      <p className="italic text-center text-sm text-gray-500 dark:text-gray-200">
        Scroll to zoom. Click + Drag to pan. Double click to reset.
      </p>
    </div>
  );
}

const sharedConfig = {
  axisX: {
    grid: true,
    gridColor: "white",
    bandPosition: 0,
  },
} satisfies Config;

const darkConfig = {
  ...sharedConfig,
  axisX: {
    ...sharedConfig.axisX,
    gridColor: "#202020",
  },
} satisfies Config;
const normalAxis: AxisConfig<any> = {
  format: "~s",
};
const spreadAxis: AxisConfig<any> = {};
const axes: Record<string, AxisConfig<any>> = {
  DPS: normalAxis,
  HPS: normalAxis,
  DTPS: normalAxis,
  DPS_SPREAD: spreadAxis,
  HPS_SPREAD: spreadAxis,
  DURATION: {
    labelExpr: "toString(datum.value / 1000 / 60) + 'm'",
  },
};

function toRects(stats: FightDetailedStats[]) {
  const map: Record<string, unknown[]> = {};
  const data = stats.flatMap((stat) =>
    stat.points.slice(1).map((p, ix) => ({
      ...stat,
      points: undefined,
      rect_low: stat.points[ix], // note: lack of -1 due to `slice` changing ix
      rect_hi: p,
      density: 1 - Math.abs(0.5 - (ix + 1) / stat.stepCount),
      densityLabel: `${((ix + 1) / stat.stepCount).toFixed(2)} - ${((ix + 2) / stat.stepCount).toFixed(2)}`,
    })),
  );
  for (const datum of data) {
    if (!map[datum.statType]) {
      map[datum.statType] = [];
    }
    map[datum.statType].push(datum);
  }
  return map;
}

const Chart = ({
  statType,
  ...props
}: { statType: string } & FixedVegaChartProps) => {
  if (statType === "DURATION") {
    return <DurationChart {...props} />;
  }

  if (statType === "DPS_SPREAD" || statType === "HPS_SPREAD") {
    return <SpreadChart {...props} />;
  }

  return <DefaultChart {...props} />;
};

const buildChart = (
  yValues: number[] | undefined,
  yDomain: number[] | undefined,
  tooltip: Array<StringFieldDef<any>>,
  reverseY?: boolean,
) =>
  createClassFromSpec({
    mode: "vega-lite",
    spec: {
      data: {
        name: "data",
      },
      facet: {
        row: {
          field: "phaseLabel",
          title: null,
          sort: {
            field: "phaseIndex",
            order: "ascending",
          },
          header: {
            labelExpr: 'datum.value !== null ? datum.value : "Overall"',
          },
        },
      },
      spec: {
        bounds: "flush",
        width: 120,
        height: 60,
        mark: {
          type: "rect",
        },
        params: [
          {
            name: "brush",
            select: {
              type: "interval",
              encodings: ["y", "y2"],
            },
            bind: "scales",
          },
        ],
        encoding: {
          x: {
            field: "pullNumber",
            type: "ordinal",
            title: "Pull #",
            axis: {
              labelOverlap: "greedy",
            },
          },
          y: {
            field: "rect_low",
            type: "quantitative",
            title: null,
            scale: {
              domain: yDomain,
              reverse: reverseY,
            },
            axis: {
              values: yValues,
              tickCount: yValues ? undefined : 4,
            },
          },
          y2: {
            field: "rect_hi",
            type: "quantitative",
          },
          color: {
            field: "density",
            type: "quantitative",
            legend: null,
          },
          tooltip: [
            { field: "densityLabel", title: "Percentile Range" },
            ...tooltip,
          ],
        },
      },
    },
  });

const DefaultChart = buildChart(undefined, undefined, [
  {
    field: "rect_low",
    title: "Amount (Low End)",
    format: ".3~s",
  },
  {
    field: "rect_hi",
    title: "Amount (High End)",
    format: ".3~s",
  },
]);

const DurationChart = buildChart(
  Array.from({ length: 10 }).map((_, ix) => ix * 120000),
  undefined,
  [],
);
const SpreadChart = buildChart(
  [1.5, 1, 0.5, 0],
  [0, 2],
  [
    {
      field: "rect_low",
      title: "Amount (Low End)",
      format: ".3",
    },
    {
      field: "rect_hi",
      title: "Amount (High End)",
      format: ".3",
    },
  ],
  true,
);

interface FightDetailedStats {
  phaseIndex: number | null;
  phaseLabel: string | null;
  points: number[];
  pullNumber: number;
  statType: string;
  stepCount: number;
  total: number;
}

const labels: Record<string, string> = {
  DPS: "Damage per Second",
  HPS: "Healing per Second",
  DTPS: "Non-Tank DTPS",
  DURATION: "Duration (mins)",
  DPS_SPREAD: "DPS Spread",
  HPS_SPREAD: "HPS Spread",
};
