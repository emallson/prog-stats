import React, { useMemo, useRef, useEffect, Suspense } from "react";
import { gql } from "graphql-request";
import useQuery from "./useQuery";
import slugify from "slugify";
import { useLocation, useParams } from "react-router-dom";

import {
  Region,
  differentResets,
  Encounter,
  DIFFICULTY_NAMES,
  regionReset,
  mod,
  tierWeek,
  SUPPORTED_TIERS,
} from "./model";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faExternalLinkAlt } from "@fortawesome/free-solid-svg-icons";
import { formatDate, formatDuration } from "./format";
import { Change } from "./Changelog";
import differenceInMinutes from "date-fns/differenceInMinutes";
import { DistInfo } from "./charts/BoxPlot";
import { BoxPlotPlaceHolder, LogOverviewPlaceHolder } from "./placeholders";
import LightTooltip from "./Tooltip";
import "tippy.js/dist/tippy.css";
import "tippy.js/themes/light.css";
import "./EncounterOverview.css";
import { Params, read_id } from "./Details";

const LogOverview = React.lazy(() => import("./charts/LogOverview"));
const BoxPlot = React.lazy(() => import("./charts/BoxPlot"));

const Q_LOGS = gql`
  query Logs($team: Int!, $encounterId: Int!, $difficulty: Int!) {
    logsForEncounter(
      teamId: $team
      encounterId: $encounterId
      difficulty: $difficulty
    ) {
      iid
      startTime
      endTime
      zone: zoneId
      fights(encounterId: $encounterId, difficulty: $difficulty) {
        id
        encounterId
        difficulty
        startTime
        endTime
        kill
        bossPct
        avgIlvl
        lastPhase
        lastPhaseIsIntermission
      }
    }
    numPhases(encounterId: $encounterId, difficulty: $difficulty)
    hotfixes(encounterId: $encounterId) {
      id
      applicationDate
      regional
      description
      undocumented
    }
  }
`;

export const Q_STATS = gql`
  query Stats($team: Int!, $encounterId: Int!, $difficulty: Int!) {
    statsForEncounter(
      teamId: $team
      encounterId: $encounterId
      difficulty: $difficulty
    ) {
      team
      encounterId
      difficulty
      killLog {
        iid
        startTime
      }
      killWeek
      pullCount
      progTime
      ilvlMin
      ilvlMax
      distributions {
        pullCount: pullCountV2 {
          count
          min
          max
          quantiles
          actualPct
        }
        progTime: progTimeV2 {
          count
          min
          max
          quantiles
          actualPct
        }
      }
    }
  }
`;

export interface Props {
  encounter: Encounter;
  team: number;
  difficulty: number;
  region: Region;
  stats?: EncounterStats;
}

export interface HeaderProps {
  encounter: Encounter;
  difficulty: number;
}

export function EncounterHeader(props: HeaderProps) {
  const slug = slugify(props.encounter.name);
  const location = useLocation();
  const ref = useRef<HTMLLegendElement>(null);

  useEffect(() => {
    if (ref.current && location.hash === `#${slug}`) {
      setTimeout(() => {
        ref.current!.scrollIntoView({
          behavior: "smooth",
          block: "start",
          inline: "nearest",
        });
      }, 500);
    }
  }, [location.hash, slug]);

  return (
    <legend className="font-serif" id={slug} ref={ref}>
      <span className="block">
        {DIFFICULTY_NAMES[props.difficulty] || props.difficulty}
      </span>
      <a href={`#${slug}`}>
        <span className="text-3xl md:text-5xl" style={{ lineHeight: 0.75 }}>
          {props.encounter.name}
        </span>
      </a>
    </legend>
  );
}

interface Fight {
  id: number;
  encounterId: number;
  startTime: number;
  endTime: number;
  kill: boolean;
  bossPct: number;
  avgIlvl: number;
  log?: number;
}

interface Log {
  iid: number;
  zone: number;
  startTime: Date;
  endTime: Date;
  fights: Fight[];
  __merged?: true;
}

function logsToWeeks(logs: Log[], region: Region): { [week: number]: Log[] } {
  interface State {
    weeks: { [week: number]: Log[] };
    week: number;
    prev_date: Date | null;
  }

  return logs.reduce(
    ({ weeks, week, prev_date }, log) => {
      if (
        prev_date !== null &&
        differentResets(region, prev_date, log.startTime)
      ) {
        week += 1;
        weeks[week] = [];
      }

      prev_date = log.startTime;

      weeks[week].push(log);

      return { weeks, week, prev_date };
    },
    { weeks: { 1: [] }, week: 1, prev_date: null } as State,
  ).weeks;
}

function hotfixDate(region: Region, hotfix: EncounterHotfix): Date {
  if (hotfix.regional) {
    const days = mod(regionReset(region) - hotfix.applicationDate.getDay(), 7);
    return new Date(
      hotfix.applicationDate.valueOf() + days * 24 * 60 * 60 * 1000,
    );
  } else {
    return hotfix.applicationDate;
  }
}

function logHotfixes(
  logs: Log[],
  region: Region,
  hotfixes: EncounterHotfix[],
): { [iid: number]: EncounterHotfix[] } {
  if (logs.length === 0) {
    return {};
  }

  const hs = hotfixes
    .map((h) => ({ ...h, applicationDate: new Date(h.applicationDate) }))
    .sort((a, b) => a.applicationDate.valueOf() - b.applicationDate.valueOf())
    .filter((h) => h.applicationDate.valueOf() >= logs[0].startTime.valueOf());

  return logs.reduce(
    (hm, log) => {
      const log_hs = [];
      while (
        hs.length > 0 &&
        hs[0].applicationDate.valueOf() <= log.startTime.valueOf()
      ) {
        log_hs.push(hs.shift()!);
      }
      hm[log.iid] = log_hs;
      return hm;
    },
    {} as { [iid: number]: EncounterHotfix[] },
  );
}

function weekHotfixes(
  weekStartDate: Date,
  region: Region,
  hotfixes: EncounterHotfix[],
  includePrior: boolean,
): EncounterHotfix[] {
  const week = tierWeek(26, weekStartDate, true, region);
  return hotfixes
    .map((h) => ({ ...h, applicationDate: new Date(h.applicationDate) }))
    .filter((h) => {
      const date = hotfixDate(region, h);
      const hotfixWeek = tierWeek(26, date, true, region);
      return week === hotfixWeek || (includePrior && hotfixWeek < week);
    });
}

function merge_logs(a: Log, b: Log): Log {
  const startTimeBasis = Math.min.apply(
    null,
    a.fights.map((f) => f.startTime),
  );
  const startTimeOffset =
    Math.max.apply(
      null,
      a.fights.map((f) => f.startTime),
    ) -
    startTimeBasis +
    b.startTime.valueOf() -
    a.endTime.valueOf();
  const bStartBasis = Math.min.apply(
    null,
    b.fights.map((f) => f.startTime),
  );
  console.log("merging logs", a, b);

  return {
    __merged: true,
    iid: a.iid,
    startTime: a.startTime,
    endTime: b.endTime,
    fights: a.fights
      .map((f) => ({
        ...f,
        startTime: f.startTime - startTimeBasis,
        log: a.iid,
      }))
      .concat(
        b.fights.map((f) => ({
          ...f,
          id: f.id + a.fights.length,
          startTime: f.startTime + startTimeOffset - bStartBasis,
          log: b.iid,
        })),
      ),
    zone: a.zone,
  };
}

// determine if the logs are close enough that its probably a DC split. assumes a <= b
function close_logs(a: Log, b: Log, threshold: number = 60): boolean {
  return (
    a.endTime.valueOf() <= b.startTime.valueOf() &&
    differenceInMinutes(b.startTime, a.endTime) < threshold
  );
}
// filters the log set, removing logs that overlap (and favoring the longer log
// in such cases) and merging logs that appear to be split due to DCs.
function filter_logs(logs: Log[]): Log[] {
  return logs
    .map((log) => ({
      ...log,
      startTime: new Date(log.startTime),
      endTime: new Date(log.endTime),
    }))
    .sort((a, b) => a.startTime.valueOf() - b.startTime.valueOf())
    .reduce((res, log, ix) => {
      let res_log = log;
      if (ix > 0) {
        const prev = res[res.length - 1];
        if (close_logs(prev, log)) {
          res.pop();
          res_log = merge_logs(prev, log);
        }
      }

      res.push(res_log);
      return res;
    }, [] as Log[]);
}

interface EncounterHotfix {
  id: number;
  applicationDate: Date;
  regional: boolean;
  description?: string[];
  undocumented: boolean;
}

function HotfixTooltip({
  hotfixes,
  id,
  children,
}: React.PropsWithChildren<{ id: string; hotfixes: EncounterHotfix[] }>) {
  const desc = (
    <div className="text-left text-base">
      <dl>
        {hotfixes
          .filter((h) => h.description)
          .map((h, ix) => (
            <Change
              date={h.applicationDate}
              undocumented={h.undocumented}
              messages={h.description!}
              key={ix}
            />
          ))}
      </dl>
    </div>
  );
  return (
    <LightTooltip id={id} desc={desc}>
      {children}
    </LightTooltip>
  );
}

function HotfixMark({ hotfixes }: { hotfixes: EncounterHotfix[] }) {
  const id = useMemo(
    () => `${hotfixes.length}-${Math.random().toString()}`,
    [hotfixes.length],
  );

  if (hotfixes.length === 0) {
    return null;
  }

  return (
    <>
      <HotfixTooltip hotfixes={hotfixes} id={id}>
        <span className="w-min whitespace-nowrap" style={{ gridArea: "h" }}>
          {hotfixes.length > 1 ? "Major Hotfixes" : "Major Hotfix"}
        </span>
      </HotfixTooltip>
      <div
        className="h-max mt-2 mb-2 border-l-2 border-gray-400 border-dashed"
        style={{ gridArea: "l" }}
      ></div>
    </>
  );
}

interface Segments {
  segs: { logs: Log[]; hotfixes?: EncounterHotfix[] }[];
  current: Log[];
  segHotfixes?: EncounterHotfix[];
}

type LogHotfixes = { [iid: number]: EncounterHotfix[] };

function WeekOverview(props: {
  week: number;
  logs: Log[];
  hotfixMap: LogHotfixes;
  numPhases?: number;
}) {
  const raw = props.logs.reduce(
    ({ segs, current, segHotfixes }: Segments, log): Segments => {
      const hs = props.hotfixMap[log.iid];

      if (current.length === 0) {
        return { segs, current: [log], segHotfixes: hs };
      } else if (!hs || hs.length === 0) {
        current.push(log);
        return { segs, current, segHotfixes };
      } else {
        segs.push({ logs: current, hotfixes: segHotfixes });
        return { segs, current: [log], segHotfixes: hs };
      }
    },
    { segs: [], current: [] } as Segments,
  );

  const segments = [
    ...raw.segs,
    { logs: raw.current, hotfixes: raw.segHotfixes },
  ];

  const overview = segments.map(({ logs, hotfixes }, ix) => {
    const max_cols = logs.length;

    const col_setup = `grid-cols-1 sm:grid-cols-${Math.min(
      max_cols,
      2,
    )} md:grid-cols-${Math.min(max_cols, 3)} lg:grid-cols-${Math.min(
      max_cols,
      4,
    )}`;
    return (
      <div
        key={ix}
        className="grid justify-start"
        style={{
          gridTemplateAreas: '"h h" "l w"',
          gridTemplateColumns: "0.25rem auto",
          gridTemplateRows: "1.25em auto",
        }}
      >
        {hotfixes && <HotfixMark hotfixes={hotfixes} />}
        <section
          className="border border-gray-100 dark:border-gray-800 m-1 p-2"
          style={{ gridArea: "w" }}
        >
          <legend className="font-bold ml-1">
            {ix === 0 ? `Week ${props.week}` : ""}&nbsp;
          </legend>
          <div className={`grid ${col_setup} gap-2 items-end`}>
            {logs.map(({ iid, startTime, fights }, iix) => (
              <div
                key={iid}
                className="grid grid-flow-row h-full place-content-between"
              >
                <div className="ml-1 border-b border-gray-200 dark:border-gray-400 w-min whitespace-nowrap">
                  {formatDate(startTime)}
                </div>
                <Suspense fallback={<LogOverviewPlaceHolder />}>
                  <LogOverview
                    fights={fights}
                    withLabels={iix === 0}
                    numPhases={props.numPhases}
                  />
                </Suspense>
              </div>
            ))}
          </div>
        </section>
      </div>
    );
  });

  return <>{overview}</>;
}

export interface StatProps {
  preHotfixes: EncounterHotfix[];
  stats: EncounterStats;
}

export interface EncounterStats {
  encounterId: number;
  killLog?: Log;
  killWeek?: number;
  ilvlMin: number;
  ilvlMax: number;
  pullCount: number;
  progTime: number;
  distributions: {
    pullCount: DistInfo;
    progTime: DistInfo;
  };
}

function EncounterDate({ killText }: { killText?: string }) {
  const { zone } = useParams<Params>();
  const tier = zone ? read_id(zone) : undefined;
  const currentTier = SUPPORTED_TIERS[0]!.id;

  if (killText) {
    return <>{killText}</>;
  } else if (tier && tier < currentTier) {
    return <em>No Kill Recorded</em>;
  } else {
    return <em>In Progress</em>;
  }
}

function EncounterStatColumn(props: StatProps) {
  const {
    killLog,
    ilvlMin,
    ilvlMax,
    pullCount,
    progTime,
    killWeek,
    encounterId,
  } = props.stats;

  const hotfixid = `encounter-pre-hotfixes-${encounterId}`;
  const pctid = `pct-${encounterId}`;

  return (
    <div className="w-max">
      <dl className="grid grid-cols-4 gap-x-2">
        <dt className="col-span-2">Killed On:</dt>
        <dd className="col-span-2">
          <EncounterDate
            killText={
              killLog ? formatDate(new Date(killLog.startTime)) : undefined
            }
          />
        </dd>
        <dt className="col-span-2">Week of Tier:</dt>
        <dd className="col-span-2">
          <EncounterDate killText={killWeek ? `Week ${killWeek}` : undefined} />
        </dd>
        <dt className="col-span-2">Pull Count:</dt>
        <dd>{pullCount}</dd>
        {killLog && (
          <Suspense fallback={<BoxPlotPlaceHolder />}>
            <BoxPlot
              dist={props.stats.distributions.pullCount}
              actual={pullCount}
              id={pctid}
            />
          </Suspense>
        )}
        <dt className="col-span-2">Progression Time:</dt>
        <dd>{formatDuration(progTime)}</dd>
        {killLog && (
          <Suspense fallback={<BoxPlotPlaceHolder />}>
            <BoxPlot
              dist={props.stats.distributions.progTime}
              actual={progTime}
              id={pctid}
            />
          </Suspense>
        )}
        <dt className="col-span-2">Average Item Level:</dt>
        <dd className="col-span-2">
          {ilvlMax - ilvlMin < 0.5 ? (
            ilvlMax.toFixed(1)
          ) : (
            <>
              {ilvlMin.toFixed(1)} &mdash; {ilvlMax.toFixed(1)}
            </>
          )}
        </dd>
        {props.preHotfixes.length > 0 && (
          <>
            <dt className="col-span-full">
              <HotfixTooltip hotfixes={props.preHotfixes} id={hotfixid}>
                Pre-Progression Hotfixes{" "}
                <FontAwesomeIcon
                  icon={faExternalLinkAlt}
                  size="xs"
                  className="ml-1 text-gray-500 dark:text-gray-300"
                  style={{
                    verticalAlign: "-0.125em",
                  }}
                />
              </HotfixTooltip>
            </dt>
            <dd></dd>
          </>
        )}
      </dl>
    </div>
  );
}

export default function EncounterOverview(props: Props) {
  const { loading, error, data } = useQuery<any>(Q_LOGS, {
    variables: { ...props, encounterId: props.encounter.id },
  });
  const { data: statData, loading: statsLoading } = useQuery<any>(Q_STATS, {
    variables: { ...props, encounterId: props.encounter.id },
    skip: !!props.stats,
  });

  let content;

  if (error) {
    content = <>Unable to load content: {error.message}</>;
  }

  if (
    loading ||
    ((statsLoading || !statData) && !props.stats) ||
    !data ||
    (data && data.logsForEncounter.length === 0)
  ) {
    return <></>;
  }

  const stats = statData?.statsForEncounter;

  if (data && (props.stats || stats)) {
    const logs = filter_logs(data.logsForEncounter);
    const hotfixmap = logHotfixes(logs, props.region, data.hotfixes);
    const weekData = logsToWeeks(logs, props.region);
    const weekViews = Object.entries(weekData).map(([week, logs]) => (
      <WeekOverview
        key={week}
        week={parseInt(week)}
        logs={logs}
        hotfixMap={hotfixmap}
        numPhases={data.logsForEncounter.numPhases}
      />
    ));

    content = (
      <div className="grid gap-8 mt-4 xl:grid-cols-encounter">
        <EncounterStatColumn
          stats={props.stats || stats}
          preHotfixes={weekHotfixes(
            weekData[1][0].startTime,
            props.region,
            data.hotfixes,
            true,
          )}
        />
        <div className="flex flex-row flex-wrap">{weekViews}</div>
      </div>
    );
  }

  return (
    <section className="mt-5 mb-5 animate-fadein">
      <EncounterHeader
        encounter={props.encounter}
        difficulty={props.difficulty}
      />
      {content}
    </section>
  );
}
