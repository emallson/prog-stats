import ContentLoader from "react-content-loader";

export const LogOverviewPlaceHolder = () => (
  <ContentLoader speed={2} width={250} height={205} viewBox="0 0 250 205">
    <rect x="0" y="10" rx="3" ry="3" width="250" height="185" />
  </ContentLoader>
);

export const BoxPlotPlaceHolder = () => (
  <ContentLoader speed={2} width={60} height={12} viewBox="0 0 60 12">
    <rect x="10" y="1" rx="3" ry="3" width="40" height="9" />
  </ContentLoader>
);

export const EncounterBreakdownPlaceHolder = () => (
  <ContentLoader
    speed={2}
    width={750}
    height={505}
    viewBox="0 0 750 505"
    className="col-start-2 col-end-3"
  >
    <rect x="0" y="0" width="248" height="248" />
    <rect x="252" y="0" width="248" height="248" />
    <rect x="504" y="0" width="248" height="248" />
    <rect x="0" y="252" width="248" height="248" />
    <rect x="252" y="252" width="248" height="248" />
    <rect x="504" y="252" width="248" height="248" />
  </ContentLoader>
);

const RaidTimeRawPlaceholder = ({ y }: { y: string }) => (
  <rect x="0" y={y} rx="5" ry="5" width="150" height="24" />
);

export const HoursPlaceholder = (props: Record<string, unknown>) => (
  <ContentLoader
    speed={2}
    width={150}
    height={90}
    viewBox="0 0 150 90"
    {...props}
  >
    <rect x="0" y="7" rx="5" ry="5" width="150" height="16" />
    <RaidTimeRawPlaceholder y="30" />
  </ContentLoader>
);

export const RaidTimeChartPlaceholder = () => (
  <ContentLoader speed={2} width={150} height={30} viewBox="0 0 150 30">
    <RaidTimeRawPlaceholder y="0" />
  </ContentLoader>
);
