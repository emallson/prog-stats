import React, { Suspense } from "react";
import useQuery from "./useQuery";
import { Region } from "./model";
import { TeamOverview, StatTable, Q_ZONE } from "./Details";
import { EncounterBreakdown } from "./TierDetails";
import useMedia from "./useMedia";

const EncounterOverview = React.lazy(() => import("./EncounterOverview"));

function PreviewGradient({ to }: { to: "t" | "tr" | "tl" }) {
  return (
    <div
      className={`w-full h-1/2 bg-gradient-to-${to} from-tbg dark:from-dbg absolute left-0 bottom-0`}
    ></div>
  );
}

function PreviewCard({
  title,
  desc,
  children,
  flip,
  previewClass,
  gradientTo,
}: React.PropsWithChildren<{
  title: string;
  desc: string;
  flip?: boolean;
  previewClass?: string;
  gradientTo?: "t" | "tr" | "tl";
}>) {
  const showCard = useMedia(["(min-width: 768px)"], [true], false);
  return (
    <section className="first:mt-10 mt-8 md:mt-20">
      <div className="grid md:grid-cols-preview gap-10 md:grid-flow-col-dense grid-flow-row-dense">
        <div
          className={`md:w-3/4 md:text-lg ${
            flip
              ? "md:col-start-2 justify-self-end mr-10"
              : "md:col-start-1 justify-self-start"
          }`}
        >
          <header>
            <legend className="text-2xl md:text-3xl font-serif">{title}</legend>
          </header>
          <p dangerouslySetInnerHTML={{ __html: desc }} />
        </div>
        {showCard && (
          <div
            className={`overflow-hidden relative ${
              flip ? "md:col-start-1" : "md:col-start-2"
            } ${previewClass || "max-h-52"}`}
          >
            {children}
            <PreviewGradient to={gradientTo || "t"} />
          </div>
        )}
      </div>
    </section>
  );
}

const TEAM = 350026;
const TIER = 38;

export default function Preview() {
  const raidTimeDesc =
    'We automatically calculate raid times from guilds\' progression logs, letting you know whether a guild is really that perfect fit or one report away from the <a class="underline" href="https://twitter.com/overtimepolice">Overtime Police.</a>';

  const { data: encounterData } = useQuery<any>(Q_ZONE, {
    variables: { zone: TIER },
  });

  const zoneData = encounterData?.encounters;

  return (
    <div className="mb-5">
      <PreviewCard
        title="See Your Progression in New Light"
        desc="We collect and aggregate data about Mythic progression on each boss as you progress. See detailed data about pull count, time spent on progression, hotfixes, and a night-by-night breakdown of your progress on a boss!"
        previewClass="max-h-72"
        gradientTo="tl"
      >
        <Suspense fallback={null}>
          <EncounterOverview
            region={Region.NA}
            encounter={{ id: 2920, name: "Nexus-Princess Ky'veza" }}
            difficulty={5}
            team={TEAM}
          />
        </Suspense>
      </PreviewCard>
      <PreviewCard
        title="Get a Detailed Overview of the Tier"
        desc="In addition to our encounter-level breakdowns, we succinctly collate your progression data from the entire tier. This allows you to easily make macro-level comparisons to other raid teams, determine where you excelled and identify where you still need improvement."
      >
        {zoneData && (
          <StatTable
            encounters={zoneData.encounters}
            zone={TIER}
            team={TEAM}
            difficulty={5}
          />
        )}
      </PreviewCard>
      <PreviewCard
        title="View Typical Stats for Each Boss"
        desc={
          'We have a wealth of information about progression on each boss. Wondering what the typical pull count for a boss is? Want to see what ilvl guilds were at when they killed it? Check out our <a class="underline"href="/tier/26-castle-nathria">Tier Stats</a> page!'
        }
      >
        <Suspense fallback={null}>
          <EncounterBreakdown
            encounter={{ id: 2922, name: "Queen Ansurek" }}
            difficulty={5}
            disableDetails={true}
          />
        </Suspense>
      </PreviewCard>
      <PreviewCard
        title="View Accurate Raid Time Data"
        desc={raidTimeDesc}
        previewClass="max-h-32"
      >
        {zoneData && (
          <TeamOverview
            team={TEAM}
            zone={TIER}
            encounters={zoneData.encounters}
            region={Region.NA}
          />
        )}
      </PreviewCard>
    </div>
  );
}
