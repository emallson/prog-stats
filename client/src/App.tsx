import React, { useEffect, Suspense } from "react";
import {
  Switch,
  Route,
  NavLink,
  useLocation,
  useHistory,
} from "react-router-dom";
import slugify from "slugify";
import Details from "./Details";
import TierDetails from "./TierDetails";
import Search from "./Search";
import Auth from "./Auth";
import { BoxPlotPlaceHolder } from "./placeholders";
import { CURRENT_TIER_SLUG } from "./model";

const EncounterDistChart = React.lazy(() => import("./charts/BoxPlot"));

function NavBar() {
  const location = useLocation();
  return (
    <header className="container mx-auto mt-2 grid grid-cols-2">
      {location.pathname !== "/" ? (
        <legend className="justify-self-start font-serif">
          <NavLink to="/">WoW Progression Stats</NavLink>
        </legend>
      ) : (
        <div></div>
      )}
      <div className="grid grid-flow-col auto-cols-max gap-x-2 justify-end">
        <NavLink activeClassName="underline" exact to="/">
          Search
        </NavLink>
        &middot;
        <NavLink
          activeClassName="underline"
          to={`/tier/${CURRENT_TIER_SLUG}`}
          isActive={(_match, location) =>
            location.pathname.startsWith("/tier/")
          }
        >
          Tier Stats
        </NavLink>
        &middot;
        <NavLink activeClassName="underline" exact to="/faq">
          FAQ
        </NavLink>
      </div>
    </header>
  );
}

function QA({
  question,
  children,
}: React.PropsWithChildren<{ question: string }>) {
  return (
    <div className="mt-5" id={slugify(question)}>
      <legend className="font-serif text-3xl">
        <a href={`#${slugify(question)}`}>{question}</a>
      </legend>
      <div className="text-lg">{children}</div>
    </div>
  );
}

function Mono({
  children,
  className,
}: React.PropsWithChildren<{ className?: string }>) {
  return (
    <code
      className={`bg-gray-100 dark:bg-gray-700 inline-block rounded pl-2 pr-2 ${className}`}
    >
      {children}
    </code>
  );
}

function About() {
  useEffect(() => {
    document.title = "FAQ | WoW Progression Stats";
  });

  return (
    <div className="container mx-auto mt-10 mb-10">
      <div className="w-2/3">
        <QA question={"What is this site for?"}>
          <p>
            This site is a way to see some aggregate statistics about World of
            Warcraft guilds' progression through Mythic raid content.
          </p>
        </QA>
        <QA question={"What do these charts mean?"}>
          <p>
            Each chart represents a single raid night. Each dot on the chart
            shows a how far the guild got on a single pull, along with a{" "}
            <a href="https://en.wikipedia.org/wiki/Local_regression">
              trend line
            </a>
            .
          </p>
        </QA>
        <QA question={"What is 'Progression Time'?"}>
          <p>
            The <em>Progression Time</em> statistic measures the actual amount
            of time spent progressing a boss. It is measured by taking the
            amount of time from the start of the first pull to the end of the
            last pull on each raid night, and then summing.
          </p>
          <p className="mt-2">
            For example: suppose you pull a boss at 8:02pm on a Tuesday. You
            don't kill it, and you end your last pull at 10:58pm. The next day,
            you start again with a first pull at 8:05pm and kill it in two
            pulls, ending at 8:18pm. Then your <em>Progression Time</em> on that
            boss would be{" "}
            <Mono className="text-base">2.9 + 0.2 = 3.1 hours</Mono> regardless
            of whether you pull other bosses that night.
          </p>
        </QA>
        <QA
          question={
            "What is the little boxy thing next to my pull count / progression time?"
          }
        >
          <p>
            This{" "}
            <Suspense fallback={<BoxPlotPlaceHolder />}>
              <EncounterDistChart
                id={"qa-tooltip"}
                dist={{
                  count: 200,
                  min: 0,
                  max: 100,
                  quantiles: [25, 50, 75],
                  actualPct: 0.3,
                }}
                actual={30}
              />
            </Suspense>{" "}
            is a <em>boxplot</em>, a succinct way of representing the
            distribution of a set of values. You might recognize it as being the
            same thing as is on the{" "}
            <a
              className="underline"
              href="https://www.warcraftlogs.com/zone/statistics/26#boss=2399"
            >
              WarcraftLogs DPS Statistics
            </a>{" "}
            page.
          </p>
          <p>
            A traditional boxplot has three key parts:{" "}
            <ol className="list-decimal ml-8">
              <li>
                The <strong>whiskers</strong>&mdash;the gray lines&mdash;which
                show the rough upper and lower bounds of the data after
                excluding outliers.
              </li>
              <li>
                The <strong>box</strong>, which contains all data from the 25th
                to 75th percentile. The median (or 50th percentile) is shown as
                the center vertical line.
              </li>
              <li>
                The <strong>outliers</strong>, which are omitted from these
                charts.
              </li>
            </ol>
            Your guild's performance relative to the rest of the community is
            shown as the blue dot.
          </p>
        </QA>
        <QA
          question={
            "What percentile is best for pull counts and progression times?"
          }
        >
          <p>
            <strong>Short Version:</strong> To keep the values consistent with
            how the community already uses percentiles, 0th percentile is worst
            (nobody took longer than you) and 100th percentile is best
            (everybody took longer than you). This matches how percentiles are
            presented in WarcraftLogs.
          </p>
          <p className="mt-4">
            <em>Slightly Longer Version:</em> The scale of a percentile is based
            on the thing being measured. When you measure DPS, bigger is
            (usually) better, so 100th percentile is best and 0th is worst. When
            you measure pull count, this is flipped: fewer pulls is better, so
            0th percentile would be best and 100th percentile would be worst.
            That is not how <em>anyone</em> in the WoW community uses those
            numbers, however. Even WarcraftLogs flips the metric for kill speed
            (lower kill time is better, but they show 100th percentile as best),
            simply because it makes it easier for people to use. We do the same
            here, for largely the same reason.
          </p>
        </QA>
        <QA question={"Why don't all bosses show boxplots and percentiles?"}>
          <p>
            Boxplots and percentiles are not shown until sufficient data is
            collected. WarcraftLogs applies an asterisk to such data like so:{" "}
            <Mono>87*</Mono> .
          </p>
          <p>We omit statistics with insufficient data instead.</p>
        </QA>
        <QA
          question={
            "Aren't these percentile values unfair to guilds that killed bosses before nerfs?"
          }
        >
          <p>
            Yes, they are. Hotfix information is shown in addition to other
            metrics to help contextualize things, but this is obviously an
            incomplete solution.
          </p>
          <p className="mt-2">
            In the future, we hope to introduce adjustments to pull count and
            progression time to account for the impact of hotfixes on the
            difficulty of bosses. However, in the meantime it should simply be
            understood that guilds that killed the boss pre-nerf are likely
            performing better than their percentile ranking would indicate.
          </p>
        </QA>
        <QA question={"Do you support private logs?"}>
          <p>
            <strong>Yes!</strong> To load private logs, use the link at the
            bottom of the page for your guild or team.
          </p>
          <p className="mt-2">
            Note that our stats are <em>always public,</em> but you can revoke
            access at any time to remove your private information.
          </p>
        </QA>
        <QA question={"What makes a hotfix 'major'?"}>
          <p>This is subjective, but in general I used two criteria:</p>
          <ol className="list-decimal ml-8 mt-2">
            <li>Did this hotfix occur after Mythic opened? and</li>
            <li>
              Did this hotfix have a large impact on the fight based on feedback
              from raid teams I've been in communication with?
            </li>
          </ol>
          <p className="mt-2">
            If you disagree about the classification I used for a hotfix, reach
            out to me on Discord.
          </p>
        </QA>
        <QA
          question={
            "What data is included in the throughput stats on the tier page?"
          }
        >
          <p>
            The throughput stats (DPS, HPS, DTPS, (duration and spread get
            lumped in here too)) are loaded for each pull, within each phase of
            each pull (for multi-phase fights).
          </p>
          <ul className="mt-4">
            <li>
              <strong>Damage per Second</strong> is simply the sum of DPS across
              all players, using the default settings for the Damage Done table
              on WarcraftLogs. This means that if an add is excluded from a
              fight, it will be excluded from this data.
            </li>
            <li>
              <strong>Healing per Second</strong> is similar, but{" "}
              <em>excludes tank self-healing.</em> The reason that some tank
              abilities produce ridiculous amounts of "healing". For example:
              the old implementation of Prot Paladin's Holy Shield on Halondrus
              could do more healing than all other healing in the rest of the
              fight if it happened to "block" a bomb exploding.
            </li>
            <li>
              <strong>Non-Tank DTPS</strong> is damage taken per second by all
              non-tank players.
            </li>
            <li>
              <strong>Duration (mins)</strong> is the duration of the fight (or
              phase, for phase breakdowns).
            </li>
            <li>
              <strong>DPS/HPS Spread</strong> is a measure of how spread the
              DPS/HPS is between players in the raid. This{" "}
              <em>only includes within-role players</em>. Only DPS specs are
              included in DPS Spread, and only Healers are included in HPS
              spread. The exact calculation is{" "}
              <Mono>within-role std. deviation / within-role mean</Mono>.
              Dividing by the mean normalizes the scale.
            </li>
          </ul>
          <p className="mt-4">
            The "Overall" charts show data for the entire fight, while each
            phase row only includes data for that phase.
          </p>
        </QA>
        <QA
          question={
            "What data is included in the death stats on the tier page?"
          }
        >
          <p>
            The death stats on the tier page include the{" "}
            <em>first five deaths</em> for each pull. This is a simplistic
            cutoff, yes.
          </p>
        </QA>
        <QA question={"Who runs this?"}>
          <p>
            This site is run by{" "}
            <a
              className="underline"
              href="https://raider.io/characters/us/turalyon/Eisenpelz"
            >
              emallson
            </a>
            , who raids on{" "}
            <a
              className="underline"
              href="https://www.wowprogress.com/team/us/turalyon/raid-OEWeekend"
            >
              Occasional Excellence's Weekend Team.
            </a>
          </p>
          <p className="mt-2">
            You can find him on discord at{" "}
            <Mono className="text-base">emallson#6223</Mono> if you have
            questions or run into problems.
          </p>
        </QA>
        <QA
          question={
            "This is awesome! How can I support this? Do you have a Patreon?"
          }
        >
          <p>
            At this time we do not have a Patreon or other option for donations.
            If you want to support the site, please donate to{" "}
            <a
              href="https://www.patreon.com/warcraftlogs"
              className="underline"
            >
              WarcraftLogs
            </a>{" "}
            (without which this site would not be possible),{" "}
            <a className="underline" href="https://www.patreon.com/seriallos">
              Raidbots
            </a>{" "}
            and other parts of the WoW player-run ecosystem that are key to the
            raiding community.
          </p>
          <p className="mt-2">
            If this site ever reaches the point that I consider it fundamental
            or costs become a problem, I will open up a Patreon. Until then,
            there are more important WoW fansites.
          </p>
        </QA>
      </div>
    </div>
  );
}

// stolen from https://forum.freecodecamp.org/t/routing-issue-in-react-react-router-creates-duplicate-history-to-the-same-location/423089/2
function getLocationId({ pathname, search, hash }: any) {
  return pathname + (search ? "?" + search : "") + (hash ? "#" + hash : "");
}

function App() {
  const history = useHistory();
  useEffect(() => {
    history.listen((location, action) => {
      if (!window.goatcounter || window.goatcounter.filter()) {
        return;
      }

      window.goatcounter.count({
        path: location.pathname + location.search + location.hash,
      });
    });
    history.block((location, action) => {
      const res =
        action !== "PUSH" ||
        getLocationId(location) !== getLocationId(history.location);
      // stupid construct to deal with this version of the history library being...poorly typed.
      if (!res) {
        return false;
      }
    });
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <div className="ml-2 mr-2">
      <NavBar />
      <Switch>
        <Route path="/details/:region/:server/:guild/:tag/:zone">
          <Details />
        </Route>
        <Route path="/tier/:zone">
          <TierDetails />
        </Route>
        <Route path="/faq">
          <About />
        </Route>
        <Route path="/authorize">
          <Auth />
        </Route>
        <Route path="/">
          <Search />
        </Route>
      </Switch>
    </div>
  );
}

export default App;
