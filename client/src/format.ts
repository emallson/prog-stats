import { Result, Ok, Err } from "ts-results";
import { RaidNight } from "./Details";

const dateformat = {
  weekday: "long" as const,
  month: "long" as const,
  day: "numeric" as const,
};

const yeardateformat = {
  ...dateformat,
  year: "numeric" as const,
};

export function formatDate(date: Date, withYear: boolean = false): string {
  return new Intl.DateTimeFormat(
    "en-US",
    withYear ? yeardateformat : dateformat,
  ).format(date);
}

export function formatShortDate(date: Date): string {
  return new Intl.DateTimeFormat("en-US", {
    month: "short",
    day: "numeric",
  }).format(date);
}

export function formatDuration(
  dur_ms: number,
  suffix: string = " hours",
): string {
  return `${(dur_ms / 1000 / 60 / 60).toFixed(1)}${suffix}`;
}

export function formatRaidTime(rn: RaidNight): Result<string, "invalid data"> {
  const start_date = new Date(rn.startTime);
  const end_date = new Date(rn.endTime);

  if (start_date && end_date) {
    const day = new Intl.DateTimeFormat("en-US", { weekday: "short" }).format(
      start_date,
    );
    const start = new Intl.DateTimeFormat("en-US", {
      hour: "numeric",
      minute: "numeric",
    }).format(start_date);
    let end_day = new Intl.DateTimeFormat("en-US", { weekday: "short" }).format(
      end_date,
    );

    if (
      !(
        end_day !== day &&
        (end_date.getHours() > 0 || end_date.getMinutes() > 0)
      )
    ) {
      end_day = "";
    }

    const end = new Intl.DateTimeFormat("en-US", {
      hour: "numeric",
      minute: "numeric",
      timeZoneName: "short",
    }).format(end_date);
    return Ok(`${day} ${start} - ${end_day} ${end}`);
  } else {
    return Err("invalid data" as const);
  }
}

export function formatPrettyDuration(dur_ms: number): string {
  const mins = dur_ms / 60 / 1000;
  return `${Math.floor(mins / 60)}h ${Math.floor(mins % 60)}m`;
}
