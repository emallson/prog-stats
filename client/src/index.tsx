import React, { Suspense } from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { BrowserRouter as Router } from "react-router-dom";

const ErrorBoundary = React.lazy(() => import("./ErrorBoundary"));

const AppContent = React.memo(() => (
  <Router>
    <App />
  </Router>
));

ReactDOM.render(
  <React.StrictMode>
    <Suspense fallback={<AppContent />}>
      <ErrorBoundary fallback={<>An error has occurred.</>} showDialog>
        <AppContent />
      </ErrorBoundary>
    </Suspense>
  </React.StrictMode>,
  document.getElementById("root"),
);
