import differenceInWeeks from "date-fns/differenceInWeeks";
import addDays from "date-fns/addDays";
import addHours from "date-fns/addHours";

export const RELEASE_DATES: { [key: number]: Date } = {
  // a lot of code explicitly uses id 26 to retrieve this date. that code only
  // needs an *anchor* to the start of any reset, so this one is used.
  26: new Date("2020-12-08"),
};

export enum Region {
  NA,
  EU,
  CN,
  TW,
  KR,
}

export function toRegion(s: string): Region {
  switch (s) {
    case "us":
      return Region.NA;
    case "eu":
      return Region.EU;
    case "cn":
      return Region.CN;
    case "tw":
      return Region.TW;
    case "kr":
      return Region.KR;
    default:
      return Region.NA;
  }
}

export function regionOffset(region: Region): number {
  switch (region) {
    case Region.NA:
      return 0;
    case Region.EU:
      return 1;
    case Region.CN:
      return 2;
    case Region.TW:
      return 2;
    case Region.KR:
      return 2;
  }
}

export function mod(n: number, m: number): number {
  return ((n % m) + m) % m;
}

export function regionReset(region: Region): number {
  switch (region) {
    case Region.NA:
      return 2;
    case Region.EU:
      return 3;
    case Region.CN:
      return 4;
    case Region.KR:
      return 4;
    case Region.TW:
      return 4;
  }
}

// The weekly reset is at 8am in this time zone for each region.
export function regionResetTz(region: Region): number {
  switch (region) {
    case Region.NA:
      return -8;
    case Region.EU:
      return 1;
    case Region.CN:
      return 8;
    case Region.KR:
      return 8;
    case Region.TW:
      return 8;
  }
}

export function tierStart(tier: number, region: Region) {
  return addHours(
    addDays(RELEASE_DATES[tier], regionOffset(region)),
    8 - regionResetTz(region),
  );
}

export function tierWeek(
  tier: number,
  date: Date,
  mythic: boolean,
  region: Region,
) {
  const tier_start = tierStart(tier, region);
  const weeks = differenceInWeeks(date, tier_start);
  return weeks + (mythic ? 0 : 1);
}

export function differentResets(region: Region, a: Date, b: Date): boolean {
  const aw = tierWeek(26, a, false, region);
  const bw = tierWeek(26, b, false, region);

  return aw !== bw;
}

export const DIFFICULTY_NAMES: { [id: number]: string } = {
  5: "Mythic",
};

export interface Encounter {
  id: number;
  name: string;
}

export interface Zone {
  id: number;
  name: string;
  encounters: Encounter[];
}

interface SupportedTier {
  id: number;
  name: string;
  slug: string;
}

export const SUPPORTED_TIERS: SupportedTier[] = [
  {
    id: 42,
    name: "Liberation of Undermine",
    slug: "42-undermine",
  },
  {
    id: 38,
    name: "Nerub-ar Palace",
    slug: "38-nerubar-palace",
  },
  {
    id: 35,
    name: "Amirdrassil, the Dream's Hope",
    slug: "35-amirdrassil",
  },
  {
    id: 33,
    name: "Aberrus, the Shadowed Crucible",
    slug: "33-aberrus",
  },
  {
    id: 31,
    name: "Vault of the Incarnates",
    slug: "31-vault-of-the-incarnates",
  },
  {
    id: 29,
    name: "Sepulcher of the First Ones",
    slug: "29-sepulcher-of-the-first-ones",
  },
  { id: 28, name: "Sanctum of Domination", slug: "28-sanctum-of-domination" },
  { id: 26, name: "Castle Nathria", slug: "26-castle-nathria" },
  { id: 24, name: "Ny'alotha", slug: "24-nyalotha" },
  { id: 23, name: "The Eternal Palace", slug: "23-eternal-palace" },
  { id: 22, name: "The Crucible of Storms", slug: "22-crucible-of-storms" },
  { id: 21, name: "Battle of Dazar'alor", slug: "21-battle-of-dazaralor" },
  { id: 19, name: "Ul'dir", slug: "19-uldir" },
];

export const CURRENT_TIER_SLUG: string = SUPPORTED_TIERS.find(
  (tier) => tier.id === 42,
)!.slug;

/**
 * The first tier where detailed stats are supported
 */
export const MIN_TIER_DETAILS = 38;
