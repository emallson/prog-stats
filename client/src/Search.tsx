import React, { useEffect, useState } from "react";
import { gql } from "graphql-request";
import useQuery from "./useQuery";
import { Link, Redirect } from "react-router-dom";
import { url_for } from "./Details";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronRight } from "@fortawesome/free-solid-svg-icons";
import { DebounceInput } from "react-debounce-input";
import Changelog from "./Changelog";
import Preview from "./Preview";
import { CURRENT_TIER_SLUG } from "./model";

const matchers = {
  "raider.io":
    /raider\.io\/guilds\/(?<regionSlug>[^/]+)\/(?<serverSlug>[^/]+)\/(?<guild>[^/]+)/,
  wowprogress:
    /wowprogress\.com\/guild\/(?<regionSlug>[^/]+)\/(?<serverSlug>[^/]+)\/(?<guild>[^/]+)/,
  armory:
    /worldofwarcraft\.com\/[^/]+\/guild\/(?<regionSlug>[^/]+)\/(?<serverSlug>[^/]+)\/(?<guild>[^/]+)/,
  "wcl-calendar":
    /warcraftlogs\.com\/guild\/(?<regionSlug>[^/]+)\/(?<serverSlug>[^/]+)\/(?<guild>[^/]+)/,
};

const id_matchers = {
  team: /warcraftlogs\.com\/guild\/team-[^/]+\/(\d+)\/?/,
  guild: /warcraftlogs\.com\/guild\/[^/]+\/(\d+)\/?/,
};

enum QueryKind {
  Id,
  Name,
}

interface NameQuery {
  queryKind: QueryKind.Name;
  regionSlug: string;
  serverSlug: string;
  guild: string;
  kind: string;
}

interface IdQuery {
  queryKind: QueryKind.Id;
  id: number;
  kind: "guild" | "team";
}

type GuildQuery = NameQuery | IdQuery;

function decodeGuildName(kind: string, guild: string): string | null {
  switch (kind) {
    case "wowprogress":
      return guild.replace(/\+/g, " ");
    case "armory":
      return guild.replace(/-/g, " ");
    default:
      try {
        return decodeURIComponent(guild);
      } catch (_err) {
        return null;
      }
  }
}

function match(url: string | undefined): GuildQuery | undefined {
  if (!url) {
    return;
  }

  for (const [kind, matcher] of Object.entries(id_matchers)) {
    const match = matcher.exec(url);
    if (match) {
      const [, id] = match;

      return {
        queryKind: QueryKind.Id,
        id: parseInt(id),
        kind: kind as "team" | "guild",
      };
    }
  }
  for (const [kind, matcher] of Object.entries(matchers)) {
    const match = matcher.exec(url);
    if (match) {
      const [, regionSlug, serverSlug, rawGuild] = match;

      const guild = decodeGuildName(kind, rawGuild);

      if (!guild) {
        return;
      }

      return {
        queryKind: QueryKind.Name,
        kind,
        regionSlug,
        serverSlug,
        guild,
      };
    }
  }
}

interface GuildRef {
  id: number;
  serverSlug?: string;
  regionSlug?: string;
  name: string;
  isTag: boolean;
}

const Q_TAGS = gql`
  query GuildTags($serverSlug: String!, $regionSlug: String!, $guild: String) {
    guildId(serverSlug: $serverSlug, regionSlug: $regionSlug, guild: $guild) {
      id
      name
      isTag
      serverSlug
      regionSlug
    }
  }
`;

const Q_TAGS_BY_ID = gql`
  query GuildTagsById($id: Int!, $isTag: Boolean!) {
    guildRef(id: $id, isTag: $isTag) {
      id
      name
      isTag
      serverSlug
      regionSlug
    }
  }
`;

function url(tag: GuildRef, guild_tag: GuildRef): string {
  const data: any = {
    guild: guild_tag,
  };

  if (tag.isTag) {
    data.team = {
      id: tag.id,
      tagName: tag.name,
    };
  }
  return url_for(data, CURRENT_TIER_SLUG);
}

function Tag({ tag, guild }: { tag: GuildRef; guild: GuildRef }) {
  return (
    <Link
      to={url(tag, guild)}
      className="flex hover:underline items-center justify-between"
    >
      <span className="mr-5 flex-initial whitespace-nowrap">
        {tag.isTag ? tag.name : `${guild.name} (Untagged Logs)`}
      </span>
      <FontAwesomeIcon icon={faChevronRight} />
    </Link>
  );
}

function Sample({
  children,
  onClick,
}: React.PropsWithChildren<{ onClick: (s: string) => void }>) {
  return (
    <pre
      className="hover:underline cursor-pointer text-sm md:text-base"
      onClick={() => onClick(children as string)}
    >
      {children}
    </pre>
  );
}

const samples: { [s: string]: string } = {
  wowprogress:
    "https://www.wowprogress.com/guild/us/turalyon/Occasional+Excellence",
  "raider.io": "https://raider.io/guilds/us/turalyon/Occasional%20Excellence",
  worldofwarcraft:
    "https://worldofwarcraft.com/en-us/guild/us/turalyon/occasional-excellence",
  "wcl-calendar":
    "https://www.warcraftlogs.com/guild/us/turalyon/Occasional%20Excellence",
  team: "https://www.warcraftlogs.com/guild/team-calendar/14078/",
  guild: "https://www.warcraftlogs.com/guild/reports-list/5536",
};

function Samples({
  kind,
  onClick,
}: {
  kind?: string;
  onClick: (s: string) => void;
}) {
  if (kind) {
    return <Sample onClick={onClick}>{samples[kind]}</Sample>;
  } else {
    return (
      <div>
        {Object.entries(samples).map(([kind, sample]) => (
          <Sample onClick={onClick} key={kind}>
            {sample}
          </Sample>
        ))}
      </div>
    );
  }
}

function TagList({
  tags,
  queryData,
  onClick,
}: {
  tags: GuildRef[] | null;
  queryData: GuildQuery;
  onClick: (s: string) => void;
}) {
  if (tags === null) {
    return (
      <div>
        Guild not found. Make sure you're using a valid URL. For example:{" "}
        <Samples kind={queryData.kind} onClick={onClick} />
      </div>
    );
  } else if (tags.length === 1) {
    return <Redirect to={url(tags[0], tags[0])} />;
  } else {
    return (
      <div className="text-xl md:text-3xl mt-5 w-min">
        {tags.map((tag) => (
          <Tag tag={tag} key={tag.id} guild={tags[0]} />
        ))}
      </div>
    );
  }
}

export default function Search() {
  const [url, setUrl] = useState<string | undefined>();
  const vars = match(url);
  const { data: nameData } = useQuery<any>(Q_TAGS, {
    variables: vars as Record<string, unknown> | undefined,
    skip: !vars || vars.queryKind !== QueryKind.Name,
  });

  const { data: idData } = useQuery<any>(Q_TAGS_BY_ID, {
    variables: { ...vars, isTag: vars ? vars.kind === "team" : false },
    skip: !vars || vars.queryKind !== QueryKind.Id,
  });

  useEffect(() => {
    document.title = "WoW Progression Stats";
  });

  let underdata = null;

  if (url && !vars) {
    underdata = (
      <div>
        Unable to parse URL. Make sure you're using a valid URL. For example:{" "}
        <Samples onClick={setUrl} />
      </div>
    );
  } else if (nameData || idData) {
    underdata = (
      <TagList
        tags={nameData ? nameData.guildId : idData.guildRef}
        queryData={vars!}
        onClick={setUrl}
      />
    );
  }

  return (
    <div className="container mx-auto mt-10">
      <legend className="text-4xl md:text-6xl mb-20 font-serif">
        WoW Progression Stats
      </legend>
      <DebounceInput
        type="text"
        debounceTimeout={300}
        className="text-2xl w-full bg-transparent border-b border-gray-300 dark:border-gray-700 focus:ring-2 focus:outline-none focus:ring-gray-300 dark:focus:ring-gray-700 mb-5"
        autoFocus
        value={url || ""}
        onChange={(ev) => setUrl(ev.target.value)}
        placeholder="Paste a link to WarcraftLogs, Raider.io, Wowprogress, or the WoW Armory."
      />
      <div className="mt-5" style={{ minHeight: 120 }}>
        {underdata}
      </div>
      <div className="animate-fadein-slow">
        <Preview />
        <Changelog />
      </div>
    </div>
  );
}
