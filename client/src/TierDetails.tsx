import React, {
  useEffect,
  Suspense,
  useRef,
  useCallback,
  useState,
} from "react";
import { useParams } from "react-router-dom";
import { EncounterHeader } from "./EncounterOverview";
import { Q_ZONE, read_id, TierSelect } from "./Details";
import { gql } from "graphql-request";
import useQuery from "./useQuery";
import { formatDuration } from "./format";
import { Encounter, MIN_TIER_DETAILS } from "./model";
import { EncounterBreakdownPlaceHolder } from "./placeholders";

const ENCOUNTER_STATS = gql`
  query EncounterStats($encounterId: Int!, $difficulty: Int!) {
    encounterStatOverview: encounterStatOverviewV2(
      encounterId: $encounterId
      difficulty: $difficulty
    ) {
      __typename
      ... on ColumnarBins {
        metricType
        dayIndex
        binStart
        binEnd
        count
      }
      ... on EncounterSummaryCollection {
        data {
          pullCount
          progTime
          startIlvl
          endIlvl
          startTime
          killTime
          killNumber
        }
      }
    }
    encounterStatSummary: encounterStatSummaryV2(
      encounterId: $encounterId
      difficulty: $difficulty
    ) {
      killCount
      pullsRange
      progTimeRange
    }
    hotfixes(encounterId: $encounterId) {
      applicationDate
    }
  }
`;

const DETAILED_STATS = gql`
  query DetailedStats($encounterId: Int!) {
    detailedStatSummary(encounterId: $encounterId) {
      statType
      pullNumber
      phaseIndex
      phaseLabel
      stepCount
      total
      points
    }
    detailedDeathSummary(encounterId: $encounterId) {
      pullNumber
      abilityName
      timeMs
      spanMs
      total
      groupSize
    }
  }
`;

const EncounterBreakdownChart = React.lazy(
  () => import("./charts/EncounterBreakdown"),
);
const EncounterDetailedStats = React.lazy(
  () => import("./charts/EncounterDetailedStats"),
);
const EncounterDetailedDeaths = React.lazy(
  () => import("./charts/EncounterDetailedDeaths"),
);
import useMedia from "./useMedia";

interface EncounterStatSummary {
  killCount: number;
  pullsRange: [number, number];
  progTimeRange: [number, number];
}

const DtTitle = ({ text, tooltip }: { text: string; tooltip: string }) => (
  <dt
    title={tooltip}
    className="cursor-help underline"
    style={{
      textDecorationStyle: "dotted",
      textDecorationThickness: "from-font",
    }}
  >
    {text}
  </dt>
);

function EncounterStatOverview({
  killCount,
  pullsRange,
  progTimeRange,
}: EncounterStatSummary) {
  return (
    <div className="w-max">
      <dl className="grid gap-x-4" style={{ gridTemplateColumns: "auto auto" }}>
        <dt>Recorded Kills:</dt>
        <dd>{killCount}</dd>
        <DtTitle text="Typical Pull Count:" tooltip="25th to 75th Percentile" />
        <dd>
          {Math.round(pullsRange[0])} &mdash; {Math.round(pullsRange[1])}
        </dd>
        <DtTitle
          text="Typical Progression Time:"
          tooltip="25th to 75th Percentile"
        />
        <dd>
          {formatDuration(progTimeRange[0], "")} &mdash;{" "}
          {formatDuration(progTimeRange[1])}
        </dd>
      </dl>
    </div>
  );
}

export function EncounterBreakdown({
  encounter,
  difficulty,
  disableDetails,
}: {
  encounter: Encounter;
  difficulty: number;
  disableDetails: boolean;
}) {
  const [processData, setProcessData] = useState(false);
  const encounterId = encounter.id;
  const { data, error } = useQuery<any>(ENCOUNTER_STATS, {
    variables: { encounterId, difficulty },
    skip: !processData,
  });

  const { data: detailedData } = useQuery<any>(DETAILED_STATS, {
    variables: { encounterId },
    skip: !processData,
  });

  const showDetails = useMedia(
    ["(min-width: 768px)"],
    [!disableDetails],
    false,
  );

  const observer = useRef<IntersectionObserver | undefined>();

  const ref = useCallback((element: HTMLElement | undefined | null) => {
    if (!element) {
      observer.current?.disconnect();
      return;
    }
    if (!observer.current) {
      observer.current = new IntersectionObserver(
        (entries, self) => {
          for (const entry of entries) {
            if (entry.isIntersecting) {
              setProcessData(true);
              self.disconnect();
              observer.current = undefined;
            }
          }
        },
        {
          threshold: 0.2,
        },
      );
    }

    observer.current.observe(element);
  }, []);

  let content = null;
  if (error) {
    content = <p>Error: {error.message}</p>;
  } else if (data?.encounterStatSummary?.killCount === 0) {
    content = (
      <p>
        <em>No recorded kills.</em>
      </p>
    );
  } else if (processData && data && data.encounterStatOverview) {
    content = (
      <>
        <EncounterStatOverview {...data.encounterStatSummary} />
        <Suspense fallback={<EncounterBreakdownPlaceHolder />}>
          <EncounterBreakdownChart
            stats={data.encounterStatOverview}
            hotfixes={data.hotfixes}
          />
        </Suspense>
        {showDetails ? (
          <Suspense fallback={<EncounterBreakdownPlaceHolder />}>
            {detailedData && (
              <EncounterDetailedStats
                encounterID={encounterId}
                stats={detailedData.detailedStatSummary}
              />
            )}
            {detailedData && (
              <EncounterDetailedDeaths
                encounterID={encounterId}
                deaths={detailedData.detailedDeathSummary}
              />
            )}
          </Suspense>
        ) : null}
      </>
    );
  } else {
    content = (
      <>
        <div />
        <EncounterBreakdownPlaceHolder />
      </>
    );
  }

  return (
    <section className="mt-5 mb-5 animate-fadein" ref={ref}>
      <EncounterHeader encounter={encounter} difficulty={difficulty} />
      <div className="grid gap-8 xl:grid-cols-encounter mt-2 w-min">
        {content}
      </div>
    </section>
  );
}

export default function TierDetails() {
  const { zone } = useParams<{ zone: string }>();
  const tier = read_id(zone);

  const { data: zoneData } = useQuery<any>(Q_ZONE, {
    variables: { zone: tier },
    skip: !tier,
  });

  const name = zoneData?.encounters.name;
  const encounters = zoneData?.encounters.encounters;

  useEffect(() => {
    if (name) {
      document.title = `${name} Overview | WoW Progression Stats`;
    }
  }, [name]);

  if (!tier || !encounters) {
    return null;
  }

  return (
    <div className="container mx-auto mt-10">
      <TierSelect
        tier={tier}
        className="text-3xl md:text-5xl"
        chevronClassName="text-2xl md:text-4xl ml-2 md:ml-4"
      />
      {tier < 26 && (
        <div className="text-xl bg-yellow-300 text-black p-2">
          <strong>Note:</strong> Data for this tier was back-filled from
          Shadowlands CE guilds. Total kill counts will be underestimated.
        </div>
      )}
      {encounters.map((encounter: Encounter) => (
        <EncounterBreakdown
          key={encounter.id}
          encounter={encounter}
          difficulty={5}
          disableDetails={tier < MIN_TIER_DETAILS}
        />
      ))}
    </div>
  );
}
