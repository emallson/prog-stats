export const isDev = process.env.NODE_ENV === "development";
export const host = isDev ? "localhost:9998" : "progstats.io";

export function host_uri() {
  if (isDev) {
    return `http://${host}`;
  } else {
    return `https://${host}`;
  }
}

export function frontend_uri() {
  if (isDev) {
    return "http://localhost:3000";
  } else {
    return host_uri();
  }
}

export const sentry_dsn =
  "https://e8e9f0b682f049b0973ae8631710fc0e@o383755.ingest.sentry.io/5655736";
