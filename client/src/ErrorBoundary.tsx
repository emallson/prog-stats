import { sentry_dsn } from "./config";
import * as Sentry from "@sentry/react";

Sentry.init({
  dsn: sentry_dsn,
  tracesSampleRate: 0.7,
});

export default Sentry.ErrorBoundary;
