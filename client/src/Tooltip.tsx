import Tooltip from "@tippyjs/react";
import useMedia from "./useMedia";

export default function LightTooltip({
  id,
  children,
  desc,
}: React.PropsWithChildren<{ id: string; desc: any }>) {
  const theme: "light" | "dark" = useMedia(
    ["(prefers-color-scheme: dark)"],
    ["dark"],
    "light"
  );
  const r = <span>{children}</span>;
  return (
    <Tooltip maxWidth="none" theme={theme} content={desc}>
      {r}
    </Tooltip>
  );
}
