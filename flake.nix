{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
   fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };  
  };

  # note: rust is omitted due to glibc issues on the remote
  outputs = { self, nixpkgs, flake-utils, fenix }:
    flake-utils.lib.eachDefaultSystem
      (system:
      let 
          overlays = [ fenix.overlays.default ];
          pkgs = import nixpkgs { 
            inherit overlays system;
          };
          rustPkg = pkgs.fenix.complete.withComponents [ "cargo" "clippy" "rust-src" "rustc" "rustfmt" ];
      in
        with pkgs; {
          formatter = nixpkgs-fmt;
          devShells.default = mkShell {
            buildInputs = [ nodejs diesel-cli bun rustPkg rust-analyzer-nightly ];
          };
        });
}
