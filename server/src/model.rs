use crate::{
    graphql::Data,
    histogram::{self, HistogramKey, MetricType},
};
use async_graphql::*;
use chrono::{DateTime, Utc};
use sqlx::{Executor, FromRow, PgConnection, Postgres};
use std::collections::HashSet;

pub trait PgExec<'a>: Executor<'a, Database = Postgres> {}

impl<'a, E: Executor<'a, Database = Postgres>> PgExec<'a> for E {}

#[derive(FromRow, SimpleObject, Debug)]
pub struct Team {
    pub iid: i32,
    pub id: i32,
    pub tag: bool,
    pub name: String,
    pub tag_name: Option<String>,
    pub server_slug: String,
    pub region_slug: String,
    pub server_name: String,
}

#[derive(BindAll)]
pub struct NewTeam {
    pub id: i32,
    pub tag: bool,
    pub name: String,
    pub server_name: String,
    pub server_slug: String,
    pub region_slug: String,
    pub tag_name: Option<String>,
}

#[derive(BindAll)]
pub struct NewLog {
    pub code: String,
    pub team: i32,
    pub start_time: DateTime<Utc>,
    pub end_time: DateTime<Utc>,
    pub title: String,
    pub zone_id: i32,
}

#[derive(FromRow, SimpleObject, Clone)]
pub struct Fight {
    pub iid: i32,
    pub log: i32,
    pub id: i32,
    pub start_time: i32,
    pub end_time: i32,
    pub encounter_id: i32,
    pub boss_pct: Option<f32>,
    pub avg_ilvl: Option<f32>,
    pub difficulty: Option<i32>,
    pub kill: bool,
    pub last_phase: Option<i32>,
    pub last_phase_is_intermission: Option<bool>,
}

#[derive(BindAll)]
pub struct NewFight {
    pub log: i32,
    pub id: i32,
    pub start_time: i32,
    pub end_time: i32,
    pub encounter_id: i32,
    pub boss_pct: Option<f32>,
    pub avg_ilvl: Option<f32>,
    pub difficulty: Option<i32>,
    pub kill: bool,
    pub last_phase: Option<i32>,
    pub last_phase_is_intermission: Option<bool>,
}

#[derive(Debug, Clone, FromRow)]
pub struct Log {
    pub iid: i32,
    pub code: String,
    pub team: i32,
    pub start_time: DateTime<Utc>,
    pub end_time: DateTime<Utc>,
    pub title: String,
    pub zone_id: i32,
}

#[Object]
impl Log {
    async fn iid(&self) -> i32 {
        self.iid
    }

    async fn team(&self) -> i32 {
        self.team
    }

    async fn start_time(&self) -> DateTime<Utc> {
        self.start_time
    }

    async fn end_time(&self) -> DateTime<Utc> {
        self.end_time
    }

    async fn title(&self) -> String {
        self.title.clone()
    }

    async fn zone_id(&self) -> i32 {
        self.zone_id
    }

    async fn fights(
        &self,
        ctx: &Context<'_>,
        encounter_id: Option<i32>,
        difficulty: Option<i32>,
    ) -> Result<Vec<Fight>> {
        let Data { conpool, .. } = ctx.data()?;

        let mut con = conpool.acquire().await?;

        let data = sqlx::query_as!(
            Fight,
            "select fights.* from fights join log_encounter_stats as les on fights.log = les.log join logs on logs.iid = les.log
             where les.id = $1
               and logs.start_time + fights.start_time * interval '1 millisecond' >= les.effective_start_time
               and logs.start_time + fights.end_time * interval '1 millisecond' <= les.effective_start_time + les.effective_duration * interval '1 millisecond'
               and (fights.encounter_id = $2 or $2 is null)
               and (fights.difficulty = $3 or $3 is null)",
            self.iid,
            encounter_id,
            difficulty
        )
        .fetch_all(&mut *con)
        .await?;

        Ok(data)
    }
}

#[derive(Debug)]
pub struct EncounterStats {
    pub team: i32,
    pub encounter_id: i32,
    pub difficulty: i32,
    pub kill_log: Option<i32>,
    pub kill_week: Option<i32>,
    pub kill_date: Option<DateTime<Utc>>,
    pub ilvl_min: f32,
    pub ilvl_max: f32,
    pub pull_count: i32,
    pub prog_time: i32,
}

#[derive(SimpleObject, Clone, Debug, FromRow)]
pub struct DistInfo {
    pub min: Option<f64>,
    pub max: Option<f64>,
    pub count: i64,
    pub quantiles: Vec<f64>,
    pub actual_pct: Option<f64>,
}

#[derive(Debug)]
pub struct DistributionData<'a> {
    stats: &'a EncounterStats,
}

async fn dist_info_for_pulls(
    con: &mut PgConnection,
    actual: i32,
    encounter_id: i32,
    difficulty: i32,
) -> Result<Option<DistInfo>> {
    let agg = sqlx::query_file_as_unchecked!(
        DistInfo,
        "src/sql/dist_info_pulls.sql",
        encounter_id,
        difficulty,
        actual
    )
    .fetch_optional(con)
    .await?;

    Ok(agg)
}

async fn dist_info_for_time(
    con: &mut PgConnection,
    actual: i32,
    encounter_id: i32,
    difficulty: i32,
) -> Result<Option<DistInfo>> {
    let agg = sqlx::query_file_as_unchecked!(
        DistInfo,
        "src/sql/dist_info_prog_time.sql",
        encounter_id,
        difficulty,
        actual
    )
    .fetch_optional(con)
    .await?;

    Ok(agg)
}

impl<'a> DistributionData<'a> {
    async fn histogram_data(
        &self,
        ctx: &Context<'_>,
        metric_type: MetricType,
        value: f64,
    ) -> Result<Option<DistInfo>> {
        let Data {
            conpool,
            histogram_cache,
            ..
        } = ctx.data()?;
        let mut con = conpool.acquire().await?;

        let hist = histogram::load(
            &mut *con,
            histogram_cache,
            HistogramKey {
                encounter_id: self.stats.encounter_id,
                difficulty: self.stats.difficulty,
                metric_type,
            },
        )
        .await?;

        Ok(hist.get_dist_info(value))
    }
}

#[Object]
impl<'a> DistributionData<'a> {
    pub async fn pull_count(&self, ctx: &Context<'_>) -> Result<Option<DistInfo>> {
        let Data { conpool, .. } = ctx.data()?;
        let mut con = conpool.acquire().await?;
        dist_info_for_pulls(
            &mut *con,
            self.stats.pull_count,
            self.stats.encounter_id,
            self.stats.difficulty,
        )
        .await
    }

    pub async fn pull_count_v2(&self, ctx: &Context<'_>) -> Result<Option<DistInfo>> {
        self.histogram_data(ctx, MetricType::PullCount, self.stats.pull_count as f64)
            .await
    }

    pub async fn prog_time(&self, ctx: &Context<'_>) -> Result<Option<DistInfo>> {
        let Data { conpool, .. } = ctx.data()?;
        let mut con = conpool.acquire().await?;
        dist_info_for_time(
            &mut *con,
            self.stats.prog_time,
            self.stats.encounter_id,
            self.stats.difficulty,
        )
        .await
    }

    pub async fn prog_time_v2(&self, ctx: &Context<'_>) -> Result<Option<DistInfo>> {
        self.histogram_data(ctx, MetricType::ProgTime, self.stats.prog_time as f64)
            .await
    }
}

#[Object]
impl EncounterStats {
    async fn team(&self) -> i32 {
        self.team
    }

    async fn encounter_id(&self) -> i32 {
        self.encounter_id
    }

    async fn difficulty(&self) -> i32 {
        self.difficulty
    }

    async fn kill_log(&self, ctx: &Context<'_>) -> Result<Option<Log>> {
        if self.kill_log.is_none() {
            return Ok(None);
        }

        let Data { conpool, .. } = ctx.data()?;
        let mut con = conpool.acquire().await?;

        let log = sqlx::query_as("select * from logs where iid = $1")
            .bind(self.kill_log.unwrap())
            .fetch_one(&mut *con)
            .await?;

        Ok(Some(log))
    }

    async fn distributions<'a>(&'a self) -> DistributionData<'a> {
        DistributionData { stats: self }
    }

    async fn kill_week(&self) -> Option<i32> {
        self.kill_week
    }

    async fn ilvl_min(&self) -> f32 {
        self.ilvl_min
    }

    async fn ilvl_max(&self) -> f32 {
        self.ilvl_max
    }

    async fn pull_count(&self) -> i32 {
        self.pull_count
    }

    async fn prog_time(&self) -> i32 {
        self.prog_time
    }
}

#[derive(SimpleObject)]
pub struct TeamHours {
    pub team: i32,
    pub zone_id: i32,
    pub med_hours: Option<i32>,
    pub max_hours: Option<i32>,
    pub raid_nights: Option<Vec<RaidNight>>,
}

#[derive(SimpleObject)]
pub struct TeamStats {
    pub team: i32,
    pub zone_id: i32,
    pub bosses_killed: i32,
    #[graphql(visible = false)]
    pub med_hours: Option<i32>,
    #[graphql(visible = false)]
    pub max_hours: Option<i32>,
    #[graphql(visible = false)]
    pub raid_nights: Option<Vec<RaidNight>>,
}

#[derive(Debug)]
#[allow(unused_attributes, dead_code)]
pub struct RaidNight {
    pub start_time: DateTime<Utc>,
    pub end_time: DateTime<Utc>,
    pub observed_instances: i32,
    pub weeks: HashSet<i32>,
    /// whether to adjust this to respect DST. if the user is not in a DST zone, then they should
    /// get a warning attached to the raid time display indicating that the raid times shift with
    /// the seasons
    pub dst_adjust: bool,
}

#[Object]
impl RaidNight {
    pub async fn start_time(&self) -> DateTime<Utc> {
        self.start_time
    }

    pub async fn end_time(&self) -> DateTime<Utc> {
        self.end_time
    }

    pub async fn duration(&self) -> i32 {
        (self.end_time - self.start_time).num_seconds() as i32
    }

    pub async fn observed_instances(&self) -> i32 {
        self.observed_instances
    }

    pub async fn dst_adjust(&self) -> bool {
        self.dst_adjust
    }
}

#[derive(FromRow, SimpleObject)]
pub struct EncounterHotfix {
    pub id: i32,
    pub encounter_id: i32,
    pub application_date: DateTime<Utc>,
    pub regional: bool,
    pub major: bool,
    pub undocumented: bool,
    pub description: Option<Vec<String>>,
}

#[derive(FromRow, SimpleObject)]
#[allow(unused_attributes, dead_code)]
pub struct EncounterSummary {
    #[graphql(skip)]
    pub team_id: i32,
    pub encounter_id: i32,
    pub difficulty: i32,
    pub pull_count: i64,
    pub prog_time: i64,
    pub start_ilvl: f32,
    pub end_ilvl: f32,
    pub has_kill: bool,
    pub start_time: DateTime<Utc>,
    pub kill_time: DateTime<Utc>,
    pub kill_number: i64,
}

#[derive(FromRow, SimpleObject)]
pub struct EncounterMetaSummary {
    pub kill_count: i64,
    pub pulls_range: Vec<f64>,
    pub prog_time_range: Vec<f64>,
}

#[derive(SimpleObject)]
pub struct Bin {
    start: f32,
    end: f32,
    count: i64,
}

#[derive(SimpleObject)]
pub struct HistogramBin {
    pub metric_type: MetricType,
    pub day_index: i64,
    pub bin_start: f64,
    pub bin_end: f64,
    pub count: i64,
}

#[derive(SimpleObject)]
pub struct EncounterSummaryCollection {
    pub data: Vec<EncounterSummary>,
}

#[derive(SimpleObject)]
pub struct HistogramBinCollection {
    pub data: Vec<HistogramBin>,
}

#[derive(Union)]
pub enum EncounterSummaryDataType {
    LES(EncounterSummaryCollection),
    Hist(ColumnarBins),
}

#[derive(async_graphql::SimpleObject)]
pub struct ColumnarBins {
    pub metric_type: Vec<MetricType>,
    pub bin_start: Vec<f64>,
    pub bin_end: Vec<f64>,
    pub count: Vec<i64>,
    pub day_index: Vec<i64>,
}
