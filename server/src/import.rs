use std::ops::DerefMut;

use crate::analysis::{prog_logs, raw_logs_for_encounter, ProgData};
use crate::bulk_insert::BulkInsert;
use crate::model::{Log, NewFight, NewLog, NewTeam, PgExec, Team};
use crate::queries::fight_details::fight_details;
use crate::queries::{guild, reports};
use crate::{WclMode, WclToken};
use anyhow::{anyhow, Result};
use chrono::{DateTime, NaiveDateTime, Utc};
use reqwest::Client;
use sqlx::{Acquire, PgConnection, PgPool, Postgres, Transaction};

pub async fn import_tags(
    con: &mut PgConnection,
    guild: guild::guild_by_id::GuildByIdGuildDataGuild,
) -> Result<()> {
    let value = NewTeam {
        id: guild.id as i32,
        tag: false,
        name: guild.name.clone(),
        server_name: guild.server.name.clone(),
        server_slug: guild.server.slug.clone(),
        region_slug: guild.server.region.slug.clone(),
        tag_name: None,
    };

    let mut tags = vec![value];

    if let Some(tagset) = &guild.tags {
        for tag in tagset {
            if let Some(t) = tag {
                // HACK: need to confirm with Kihra whether tag ids can overlap with guild ids in general
                if t.id != guild.id {
                    tags.push(NewTeam {
                        id: t.id as i32,
                        tag: true,
                        name: guild.name.clone(),
                        tag_name: Some(t.name.clone()),
                        server_name: guild.server.name.clone(),
                        server_slug: guild.server.slug.clone(),
                        region_slug: guild.server.region.slug.clone(),
                    });
                }
            }
        }
    }

    BulkInsert::new("teams")
        .add_rows(tags)
        .on_conflict(
            vec!["id"],
            vec![
                "name",
                "server_slug",
                "region_slug",
                "server_name",
                "tag_name",
            ],
        )
        .execute(con)
        .await?;

    Ok(())
}

pub fn wcl_timestamp(time: f64) -> DateTime<Utc> {
    DateTime::from_utc(
        NaiveDateTime::from_timestamp((time / 1000.0) as i64, 0),
        Utc,
    )
}

// Import new logs. Does not update logs that may have been partially loaded. That is done
// separately by `replace_log`
pub async fn import_logs(
    con: &mut PgConnection,
    team: &Team,
    reports: &Vec<reports::tier_reports::TierReportsReportDataReportsData>,
) -> Result<()> {
    let mut dbtx = con.begin().await?;

    let data = reports
        .into_iter()
        .filter_map(|report| {
            Some(NewLog {
                code: report.code.clone(),
                team: team.iid,
                start_time: wcl_timestamp(report.start_time),
                end_time: wcl_timestamp(report.end_time),
                title: report.title.clone(),
                zone_id: report.zone.as_ref().map(|z| z.id as i32).unwrap_or(0),
            })
        })
        .collect::<Vec<_>>();

    if data.is_empty() {
        return Ok(());
    }

    let zone = data[0].zone_id;

    BulkInsert::new("logs")
        .add_rows(data)
        .on_conflict(vec!["code"], vec!["team"])
        .execute(&mut *dbtx)
        .await?;

    let codes = reports
        .iter()
        .map(|report| report.code.clone())
        .collect::<Vec<_>>();

    // delete old logs
    sqlx::query!("delete from log_encounter_stats where team_id = $1 and log not in (select iid from logs where team = $1 and code = any($2))", team.iid, &codes)
        .execute(&mut *dbtx)
        .await?;

    sqlx::query!(
        "delete from logs where team = $1 and not code = any($2) and zone_id = $3",
        team.iid,
        &codes,
        zone
    )
    .execute(&mut *dbtx)
    .await?;

    dbtx.commit().await?;

    Ok(())
}

pub async fn replace_log<'a, E: PgExec<'a>>(
    con: E,
    report: &reports::tier_reports::TierReportsReportDataReportsData,
) -> Result<()> {
    sqlx::query!(
        "update logs set end_time = $1 where code = $2",
        wcl_timestamp(report.end_time),
        &report.code
    )
    .execute(con)
    .await?;

    Ok(())
}

pub async fn import_fights(
    con: &mut PgConnection,
    log: &Log,
    data: &reports::fights::ResponseData,
) -> Result<()> {
    let fights = data
        .report_data
        .as_ref()
        .and_then(|d| d.report.as_ref())
        .and_then(|d| d.fights.as_ref());

    if let Some(fights) = fights {
        let data = fights
            .into_iter()
            .filter_map(|fight| {
                fight.as_ref().and_then(|fight| {
                    if fight.size.unwrap_or(5) >= 10 && !fight.in_progress.unwrap_or(false) {
                        Some(NewFight {
                            log: log.iid,
                            id: fight.id as i32,
                            start_time: fight.start_time as i32,
                            end_time: fight.end_time as i32,
                            encounter_id: fight.encounter_id as i32,
                            difficulty: fight.difficulty.map(|d| d as i32),
                            boss_pct: fight.fight_percentage.map(|p| p as f32),
                            kill: fight.kill.unwrap_or(false),
                            avg_ilvl: fight.average_item_level.map(|ilvl| ilvl as f32),
                            last_phase: fight.last_phase.map(|p| p as i32),
                            last_phase_is_intermission: fight.last_phase_is_intermission,
                        })
                    } else {
                        None
                    }
                })
            })
            .collect::<Vec<_>>();

        BulkInsert::new("fights")
            .add_rows(data)
            .on_conflict(
                vec!["log", "id"],
                vec![
                    "last_phase",
                    "last_phase_is_intermission",
                    "kill",
                    "end_time",
                    "boss_pct",
                ],
            )
            .execute(con)
            .await?;

        Ok(())
    } else {
        Ok(())
    }
}
