use sqlx::{postgres::PgArguments, query::Query, PgConnection, Postgres};

pub trait BindAll {
    fn bind_all<'a>(
        &'a self,
        q: Query<'a, Postgres, PgArguments>,
    ) -> Query<'a, Postgres, PgArguments>;

    fn columns() -> Vec<&'static str>;
}

enum OnConflict<'a> {
    Update {
        key: Vec<&'a str>,
        excluded: Vec<&'a str>,
    },
    DoNothing,
}

pub struct BulkInsert<'a, T: BindAll> {
    table: &'a str,
    rows: Vec<T>,
    on_conflict: Option<OnConflict<'a>>,
    chunk_size: usize,
}

impl<'a, T: BindAll> BulkInsert<'a, T> {
    pub fn new(table: &'a str) -> Self {
        BulkInsert {
            table,
            rows: vec![],
            on_conflict: None,
            chunk_size: 1000,
        }
    }

    pub fn add_rows(mut self, rows: Vec<T>) -> Self {
        self.rows.extend(rows);
        self
    }

    pub fn on_conflict(mut self, key: Vec<&'a str>, excluded: Vec<&'a str>) -> Self {
        self.on_conflict = Some(OnConflict::Update { key, excluded });
        self
    }

    pub fn on_conflict_do_nothing(mut self) -> Self {
        self.on_conflict = Some(OnConflict::DoNothing);
        self
    }

    pub async fn execute(self, executor: &mut PgConnection) -> Result<(), sqlx::Error> {
        if self.rows.is_empty() {
            return Ok(());
        }

        let columns = T::columns();

        for chunk in self.rows.chunks(self.chunk_size) {
            let mut q = format!("insert into {} (", self.table);
            q.push_str(&columns[0]);

            for col in &columns[1..] {
                q.push_str(", ");
                q.push_str(col);
            }

            q.push_str(") values ");

            let mut ix: u32 = 0;
            let mut first = true;
            for _ in 0..chunk.len() {
                ix += 1;
                let mut values = if first {
                    format!("(${}", ix)
                } else {
                    format!(", (${}", ix)
                };

                first = false;

                for _ in 1..columns.len() {
                    ix += 1;
                    values.push_str(&format!(", ${}", ix));
                }

                values.push_str(")");

                q.push_str(&values);
            }

            match &self.on_conflict {
                Some(OnConflict::Update { key, excluded }) => {
                    q.push_str(" on conflict (");
                    q.push_str(key[0]);
                    for k in &key[1..] {
                        q.push_str(", ");
                        q.push_str(k);
                    }

                    q.push_str(") do update set ");
                    q.push_str(&format!("{} = excluded.{}", excluded[0], excluded[0]));
                    for ex in &excluded[1..] {
                        q.push_str(&format!(", {} = excluded.{}", ex, ex));
                    }
                }
                Some(OnConflict::DoNothing) => {
                    q.push_str(" on conflict do nothing");
                }
                None => {}
            }

            let mut query = sqlx::query(&q);

            for row in chunk {
                query = row.bind_all(query);
            }

            query.execute(&mut *executor).await?;
        }

        return Ok(());
    }
}
