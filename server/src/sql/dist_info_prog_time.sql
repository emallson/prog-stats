with a as (select count(*) as c from encounter_stats_summary where encounter_id = $1 and difficulty = $2 and has_kill and prog_time < $3)
select count(prog_time) as count,
min(prog_time)::double precision as min,
max(prog_time)::double precision as max,
coalesce(percentile_cont(array[0.25, 0.5, 0.75]) within group (order by prog_time), array[0, 0, 0]) as quantiles,
(select c::double precision from a) / greatest(count(prog_time)::double precision, 1) as actual_pct
from encounter_stats_summary where encounter_id = $1 and difficulty = $2 and has_kill;
