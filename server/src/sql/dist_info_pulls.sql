with a as (select count(*) as c from encounter_stats_summary where encounter_id = $1 and difficulty = $2 and has_kill and pull_count < $3)
select count(pull_count) as count,
min(pull_count)::double precision as min,
max(pull_count)::double precision as max,
coalesce(percentile_cont(array[0.25, 0.5, 0.75]) within group (order by pull_count), array[0, 0, 0]) as quantiles,
(select c::double precision from a) / greatest(count(pull_count)::double precision, 1) as actual_pct
from encounter_stats_summary where encounter_id = $1 and difficulty = $2 and has_kill;
