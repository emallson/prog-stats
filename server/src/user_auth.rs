use crate::{WclSecrets, WclToken};
use anyhow::{anyhow, Result};
use chrono::{Duration, Utc};
use log::error;
use reqwest::Client;
use serde_derive::Deserialize;
use sqlx::PgPool;
use std::collections::HashMap;
use thiserror::Error;

#[derive(Debug, Deserialize, Clone)]
#[allow(unused_attributes, dead_code)]
struct WclErrResponse {
    hint: String,
    error: String,
    message: String,
    error_description: String,
    #[serde(flatten)]
    additional: HashMap<String, String>,
}

#[derive(Debug, Deserialize)]
#[serde(untagged)]
enum TokenResponse {
    Ok(WclToken),
    Err(WclErrResponse),
    Unk(HashMap<String, String>),
}

#[derive(Debug, Error, Clone)]
pub enum Error {
    #[error("Token has been revoked on the WCL end.")]
    TokenRevoked,
    #[error("Token expired")]
    TokenExpired,
}

fn process_wcl_response(res: TokenResponse, scope: &str) -> Result<WclToken> {
    match res {
        TokenResponse::Ok(token) => Ok(token),
        TokenResponse::Err(err) => match err.hint.as_str() {
            "Authorization code has been revoked" | "Token has been revoked" => {
                Err(Error::TokenRevoked.into())
            }
            "Token has expired" => Err(Error::TokenExpired.into()),
            _ => {
                error!(
                    "an error occurred while {}ing a user token: {:?}",
                    scope, err
                );
                return Err(anyhow!("unable to refresh token"));
            }
        },
        TokenResponse::Unk(err) => {
            error!(
                "un unknown error occurred while {}ing a user token: {:?}",
                scope, err
            );
            return Err(anyhow!("unable to {} token", scope));
        }
    }
}

async fn refresh_token(
    client: &Client,
    secrets: &WclSecrets,
    refresh_token: &str,
) -> Result<WclToken> {
    let res = client
        .post("https://www.warcraftlogs.com/oauth/token")
        .form(&[
            ("grant_type", "refresh_token"),
            ("refresh_token", refresh_token),
        ])
        .basic_auth(&secrets.id, Some(&secrets.secret))
        .header("accept", "application/json")
        .send()
        .await?
        .json::<TokenResponse>()
        .await?;

    process_wcl_response(res, "refresh")
}

#[cfg(debug_assertions)]
const REDIRECT_URI: &str = "http://localhost:3000/authorize";
#[cfg(not(debug_assertions))]
const REDIRECT_URI: &str = "https://progstats.io/authorize";

const MAX_TRIES: usize = 3;

// Convert a code to a token. Tries up to three times at 2 second intervals.
pub async fn token_for_code(client: &Client, secrets: &WclSecrets, code: &str) -> Result<WclToken> {
    for _ in 0..MAX_TRIES {
        let raw_res = client
            .post("https://www.warcraftlogs.com/oauth/token")
            .form(&[
                ("grant_type", "authorization_code"),
                ("code", code),
                ("redirect_uri", REDIRECT_URI),
            ])
            .basic_auth(&secrets.id, Some(&secrets.secret))
            .header("accept", "application/json")
            .send()
            .await?
            .text()
            .await?;

        match serde_json::from_str(&raw_res) {
            Ok(res) => return process_wcl_response(res, "acquire"),
            Err(parse_err) => {
                error!(
                    "unable to parse token response from WCL: {:?} (response: {})",
                    parse_err, raw_res
                );
            }
        }

        tokio::time::sleep(Duration::seconds(2).to_std().unwrap()).await;
    }

    Err(anyhow!("unable to acquire token"))
}

pub async fn background_refresh_tokens(con: PgPool, secrets: WclSecrets, client: Client) {
    loop {
        let res = sqlx::query!("delete from auth_codes where expiration < now()")
            .execute(&con)
            .await;
        match res {
            Err(e) => {
                log::error!("unable to delete expired auth codes: {:?}", e);
            }
            Ok(qr) => {
                log::info!("deleted {} expired tokens", qr.rows_affected());
            }
        }

        let soon_codes = sqlx::query!(
            "select id, refresh_token from auth_codes where expiration < now() + interval '1 day'"
        )
        .fetch_all(&con)
        .await;

        let soon_codes = match soon_codes {
            Ok(val) => val,
            Err(e) => {
                log::error!("unable to retrieve codes that expire soon: {:?}", e);
                continue;
            }
        };

        let total = soon_codes.len();
        let mut successes = 0;
        for code in soon_codes {
            match refresh_token(&client, &secrets, &code.refresh_token).await {
                Ok(token) => {
                    let res = sqlx::query!(
                        "update auth_codes set access_token = $1, refresh_token = $2, expiration = $3 where id = $4",
                        &token.access_token,
                        token.refresh_token.as_ref().unwrap_or(&code.refresh_token),
                        Utc::now() + Duration::seconds(token.expires_in as i64),
                        code.id
                    ).execute(&con)
                     .await;

                    if let Err(e) = res {
                        log::error!("unable to insert updated token into database. later refreshes may fail: {:?}", e);
                    } else {
                        successes += 1;
                    }
                }
                Err(e) => {
                    log::error!("unable to refresh token via WCL: {:?}", e);
                }
            };
        }

        log::info!(
            "refreshed {} tokens ({} failed to refresh)",
            successes,
            total - successes
        );

        tokio::time::sleep(chrono::Duration::hours(1).to_std().unwrap()).await;
    }
}

// Get the team-specific token (if one exists and is valid).
pub async fn token_for_team(con: &PgPool, team: i32, zone: i32) -> Result<Option<WclToken>> {
    let access_token = sqlx::query_scalar!(
        "select access_token from auth_codes where team = $1 and zone = $2 and expiration > now()",
        team,
        zone
    )
    .fetch_optional(con)
    .await?;

    Ok(access_token.map(|token| WclToken {
        access_token: token,
        refresh_token: None,
        expires_in: 0,
        token_type: None,
    }))
}
