use crate::analysis::details::{DeathDetails, FightDetailedStats};
use crate::analysis::{
    self, generate_analysis, generate_encounter_analysis, logs_for_encounter, team_region,
    tier_start, tier_start_for, tier_week, LogSegment,
};
use crate::background_jobs::details::{dispatch_progression_details_import, DetailsRequest};
use crate::histogram::{HistogramCache, HistogramKey, MetricType};
use crate::import::{import_fights, import_logs, import_tags, replace_log, wcl_timestamp};
use crate::model::{
    ColumnarBins, EncounterHotfix, EncounterMetaSummary, EncounterStats, EncounterSummaryDataType,
    Fight, Log, Team, TeamHours, TeamStats,
};
use crate::queries::{
    encounters,
    guild::{self, GuildId},
    reports, user_validation,
};
use crate::{WclMode, WclSecrets, WclToken};
use anyhow::{anyhow, Result as AnyResult};
use async_graphql::*;
use chrono::{Duration, Utc};
use log::{debug, error, info, warn};

use reqwest::Client;
use sqlx::{Connection, PgConnection, PgPool};
use std::collections::{BTreeMap, HashSet};
use std::convert::TryFrom;
use std::sync::Arc;
use tokio::sync::mpsc::error::SendError;
use tokio::sync::Semaphore;
use tokio::sync::{
    mpsc::{channel, Sender},
    RwLock,
};
use tokio_stream::wrappers::ReceiverStream;

pub struct Root;

pub struct Data {
    pub client: Client,
    pub token: WclToken,
    pub secrets: WclSecrets,
    pub conpool: PgPool,
    /// Used to automatically remap wowprogress' incorrect slugs to the ones used by literally
    /// everything else.
    ///
    /// This is a bit janky, as it has to recalculate the set on every restart, but it works
    pub realm_drop_dashes: RwLock<HashSet<String>>,
    pub histogram_cache: HistogramCache,
    pub transmit_details_request: Sender<DetailsRequest>,
    pub details_limiter: Arc<Semaphore>,
}

#[derive(SimpleObject)]
struct GuildRef {
    id: i64,
    name: String,
    server_slug: Option<String>,
    region_slug: Option<String>,
    is_tag: bool,
}

#[derive(thiserror::Error, Debug)]
pub enum ImportError {
    #[error("guild with id {id} does not exist")]
    GuildDoesNotExist { id: i64 },
    #[error("{0}")]
    Other(String),
    #[error("Unknown error: {0}")]
    Unknown(String),
}

/// Recursively and_then fields out of a graphql object.
///
/// Example:
/// ```rust
/// assert_eq!(Some("Turalyon"), get_in!(response.data, guild_data, guild, server, name));
/// ```
macro_rules! get_in {
    ($var: expr, $id: ident) => {
        $var.and_then(|d| {
            d.$id
        })
    };
    ($var: expr, $id: ident, $($rest: ident),+) => {
        get_in!(get_in!($var, $id), $($rest),+)
    };
}

/// Workaround for the fact that `graphql_client` doesn't share fragment types. The fields here are
/// all identical, but have distinct struct definitions.
macro_rules! guild_refs {
    ($var: expr) => {{
        let guild = GuildRef {
            id: $var.id,
            name: $var.name,
            server_slug: Some($var.server.slug),
            region_slug: Some($var.server.region.slug),
            is_tag: false,
        };

        let mut tags = vec![guild];

        if let Some(other_tags) = $var.tags {
            for t in other_tags {
                if t.is_none() {
                    continue;
                }

                let t = t.unwrap();
                tags.push(GuildRef {
                    id: t.id,
                    name: t.name,
                    is_tag: true,
                    server_slug: None,
                    region_slug: None,
                });
            }
        }

        tags
    }};
}

#[Object]
impl Root {
    #[graphql(cache_control(max_age = 3600))]
    async fn guild_ref(
        &self,
        ctx: &Context<'_>,
        id: i64,
        is_tag: bool,
    ) -> Result<Option<Vec<GuildRef>>> {
        let Data { client, token, .. } = ctx.data()?;

        if is_tag {
            Ok(get_in!(
                guild::lookup_guild_by_team(client, token, id).await?.data,
                report_data,
                reports,
                data
            )
            .and_then(|d| d.into_iter().next().flatten())
            .and_then(|d| d.guild)
            .map(|guild| guild_refs!(guild)))
        } else {
            Ok(get_in!(
                guild::lookup_guild_by_id(client, token, id).await?.data,
                guild_data,
                guild
            )
            .map(|g| guild_refs!(g)))
        }
    }

    #[graphql(cache_control(max_age = 31557600))]
    async fn encounters(&self, ctx: &Context<'_>, zone: i64) -> Result<encounters::Zone> {
        let Data {
            client,
            token,
            conpool,
            ..
        } = ctx.data()?;

        if tier_start(conpool, zone as i32).await.is_err() {
            return Err("Tier not supported.".into());
        }

        let mut data = encounters::encounters(&client, &token, zone).await?;
        data.encounters.reverse();
        Ok(data)
    }

    /// Fetch the guild id for a (name, server, region) tuple from WCL.
    #[graphql(cache_control(max_age = 3600))]
    async fn guild_id(
        &self,
        ctx: &Context<'_>,
        guild: String,
        server_slug: String,
        region_slug: String,
    ) -> Result<Option<Vec<GuildRef>>> {
        let Data {
            client,
            token,
            realm_drop_dashes,
            ..
        } = ctx.data()?;

        let server_slug = {
            if realm_drop_dashes.read().await.contains(&server_slug) {
                debug!(
                    "server {} is in drop-dashes set. removing -s ",
                    &server_slug
                );
                server_slug.replace("-", "")
            } else {
                server_slug
            }
        };

        let res = guild::lookup_guild(
            &client,
            &token,
            guild.clone(),
            server_slug.clone(),
            region_slug.clone(),
        )
        .await?
        .data
        .and_then(|d| d.guild_data)
        .and_then(|d| d.guild);

        let guild = if res.is_none() {
            // attempt dropping dashes.
            let guild = guild::lookup_guild(
                &client,
                &token,
                guild,
                server_slug.replace("-", ""),
                region_slug,
            )
            .await?
            .data
            .and_then(|d| d.guild_data)
            .and_then(|d| d.guild);

            if guild.is_some() {
                debug!("server {} needs drop-dashes. adding to set", &server_slug);
                realm_drop_dashes.write().await.insert(server_slug);
                guild
            } else {
                None
            }
        } else {
            res
        };

        debug!("{:?}", guild);

        Ok(guild.map(|g| guild_refs!(g)))
    }

    #[graphql(cache_control(max_age = 3600))]
    async fn guild(
        &self,
        ctx: &Context<'_>,
        guild: String,
        server_slug: String,
        region_slug: String,
    ) -> Result<Option<Team>> {
        let Data { conpool, .. } = ctx.data()?;
        let mut con = conpool.acquire().await?;

        let data = sqlx::query_as("select * from teams where name = $1 and (server_slug = $2 or server_slug = $3) and region_slug = $4")
            .bind(&guild)
            .bind(&server_slug)
            .bind(server_slug.replace("-", ""))
            .bind(&region_slug)
            .fetch_optional(&mut *con)
            .await?;

        Ok(data)
    }

    async fn tag(&self, ctx: &Context<'_>, id: i64) -> Result<Option<Team>> {
        let Data { conpool, .. } = ctx.data()?;
        let mut con = conpool.acquire().await?;

        let data = sqlx::query_as("select * from teams where id = $1")
            .bind(id)
            .fetch_optional(&mut *con)
            .await?;

        Ok(data)
    }

    #[graphql(cache_control(max_age = 300))]
    async fn logs_for_encounter(
        &self,
        ctx: &Context<'_>,
        team_id: i32,
        encounter_id: i32,
        difficulty: i32,
    ) -> Result<Vec<Log>> {
        let Data { conpool, .. } = ctx.data()?;

        Ok(logs_for_encounter(conpool, team_id, encounter_id, difficulty, false).await?)
    }

    #[graphql(cache_control(max_age = 300))]
    async fn num_phases(
        &self,
        ctx: &Context<'_>,
        encounter_id: i32,
        difficulty: i32,
    ) -> Result<Option<i32>> {
        let Data { conpool, .. } = ctx.data()?;

        let data = sqlx::query_scalar!(
            "select max(last_phase) from fights where encounter_id = $1 and difficulty = $2",
            encounter_id,
            difficulty
        )
        .fetch_one(conpool)
        .await?;

        Ok(data)
    }

    #[graphql(cache_control(max_age = 300))]
    async fn stats_for_encounter(
        &self,
        ctx: &Context<'_>,
        team_id: i32,
        encounter_id: i32,
        difficulty: i32,
    ) -> Result<Option<EncounterStats>> {
        let Data { conpool, .. } = ctx.data()?;

        Ok(analysis::fetch_encounter_stats(conpool, team_id, encounter_id, difficulty).await?)
    }

    #[graphql(cache_control(max_age = 300))]
    async fn stats_for_zone(
        &self,
        ctx: &Context<'_>,
        team_id: i32,
        zone: i32,
        difficulty: i32,
    ) -> Result<Vec<EncounterStats>> {
        let Data { conpool, .. } = ctx.data()?;

        let log_data: Vec<LogSegment> = sqlx::query_as!(
            LogSegment,
            "select team_id, log, encounter_id, difficulty, effective_start_time, effective_duration,
                    start_ilvl, end_ilvl, has_kill, post_prog, pull_count from log_encounter_stats join encounters using (encounter_id)
             where team_id = $1 and not post_prog and zone = $2 and difficulty = $3
             order by effective_start_time asc",
            team_id, zone, difficulty
        )
            .fetch_all(conpool)
            .await?;

        if log_data.is_empty() {
            Ok(vec![])
        } else {
            let release_date = tier_start(conpool, zone).await?;
            let region = team_region(conpool, team_id).await?;
            let mut stats = BTreeMap::new();

            for seg in &log_data {
                let entry = stats.entry(seg.encounter_id).or_insert(EncounterStats {
                    team: team_id,
                    difficulty,
                    encounter_id: seg.encounter_id,
                    kill_log: None,
                    kill_week: None,
                    kill_date: None,
                    ilvl_min: seg.start_ilvl,
                    ilvl_max: 0.0,
                    pull_count: 0,
                    prog_time: 0,
                });

                if seg.has_kill && !seg.post_prog {
                    entry.kill_log = Some(seg.log);
                    let kill_date = seg.effective_start_time
                        + Duration::milliseconds(seg.effective_duration as i64);
                    entry.kill_date = Some(kill_date);
                    entry.kill_week =
                        Some(tier_week(kill_date, release_date, region, zone, difficulty));
                }

                entry.ilvl_max = seg.end_ilvl;
                entry.pull_count += seg.pull_count;
                entry.prog_time += seg.effective_duration;
            }

            Ok(stats.into_iter().map(|(_, stats)| stats).collect())
        }
    }

    #[graphql(cache_control(max_age = 300))]
    async fn fights(
        &self,
        ctx: &Context<'_>,
        log: i32,
        encounter_id: i32,
        difficulty: i32,
    ) -> Result<Vec<Fight>> {
        let Data { conpool, .. } = ctx.data()?;
        let mut con = conpool.acquire().await?;

        let data = sqlx::query_as!(
            Fight,
            "select * from fights where log = $1 and difficulty = $2 and encounter_id = $3",
            log,
            difficulty,
            encounter_id
        )
        .fetch_all(&mut *con)
        .await?;

        Ok(data)
    }

    #[graphql(cache_control(max_age = 3600))]
    async fn raid_nights(&self, _ctx: &Context<'_>, team: i32, zone: i32) -> Result<TeamHours> {
        let (days, hours): (Option<_>, Option<(Duration, Duration)>) = (None, None);

        Ok(TeamHours {
            team,
            zone_id: zone,
            med_hours: hours.as_ref().map(|d| d.0.num_milliseconds() as i32),
            max_hours: hours.as_ref().map(|d| d.1.num_milliseconds() as i32),
            raid_nights: days,
        })
    }

    #[graphql(cache_control(max_age = 3600))]
    async fn raid_times(
        &self,
        ctx: &Context<'_>,
        team: i32,
        zone: i32,
    ) -> Result<Option<crate::analysis::raid_time::TimeStats>> {
        let Data { conpool, .. } = ctx.data()?;

        let data = crate::analysis::raid_time::weekly_time(conpool, team, zone, 5).await?;
        Ok(data)
    }

    #[graphql(cache_control(max_age = 60))]
    async fn team_stats(&self, ctx: &Context<'_>, team: i32, zone: i32) -> Result<TeamStats> {
        let Data { conpool, .. } = ctx.data()?;
        let mut con = conpool.acquire().await?;

        let bosses_killed = sqlx::query_scalar!(
            "select count(*) from log_encounter_stats join encounters using (encounter_id)
             where team_id = $1 and zone = $2 and difficulty = 5 and not post_prog and has_kill",
            team,
            zone
        )
        .fetch_one(&mut *con)
        .await?
        .unwrap_or(0) as i32;

        Ok(TeamStats {
            team,
            zone_id: zone,
            bosses_killed,
            med_hours: None,
            max_hours: None,
            raid_nights: None,
        })
    }

    /// Whether we have access to private logs for `guild` / `zone`.
    #[graphql(cache_control(max_age = 0))]
    async fn private_access(&self, ctx: &Context<'_>, guild: i32, zone: i32) -> Result<bool> {
        let Data { conpool, .. } = ctx.data()?;
        let mut con = conpool.acquire().await?;

        let codes_present = sqlx::query_scalar!(
            "select count(*) from auth_codes where team = $1 and zone = $2",
            guild,
            zone
        )
        .fetch_one(&mut *con)
        .await?;

        Ok(codes_present.unwrap_or(0) > 0)
    }

    #[graphql(cache_control(max_age = 3600))]
    async fn hotfixes(
        &self,
        ctx: &Context<'_>,
        encounter_id: i32,
        only_major: Option<bool>,
    ) -> Result<Vec<EncounterHotfix>> {
        let Data { conpool, .. } = ctx.data()?;
        let mut con = conpool.acquire().await?;

        let data = sqlx::query_as!(
            EncounterHotfix,
            "select * from encounter_hotfixes where encounter_id = $1 and (major or not $2)",
            encounter_id,
            only_major.unwrap_or(true)
        )
        .fetch_all(&mut *con)
        .await?;

        Ok(data)
    }

    #[graphql(cache_control(max_age = 3600))]
    async fn encounter_stat_overview_v2(
        &self,
        ctx: &Context<'_>,
        encounter_id: i32,
        difficulty: i32,
    ) -> Result<EncounterSummaryDataType> {
        let Data {
            conpool,
            histogram_cache,
            ..
        } = ctx.data()?;

        if tier_start_for(conpool, encounter_id).await.is_err() {
            return Err("Tier not supported.".into());
        }

        let bin_data = sqlx::query!(
                "select metric_type, bin_index, day_index, count from histogram_bins where encounter_id = $1 and difficulty = $2 order by bin_index asc, day_index asc", encounter_id, difficulty
            ).fetch_all(conpool).await?;

        let mut result = ColumnarBins {
            metric_type: vec![],
            bin_start: vec![],
            bin_end: vec![],
            count: vec![],
            day_index: vec![],
        };

        let compressed_pulls = crate::histogram::compress_scale(
            &*crate::histogram::load(
                conpool,
                histogram_cache,
                HistogramKey {
                    encounter_id,
                    difficulty,
                    metric_type: MetricType::PullCount,
                },
            )
            .await?,
            20,
        );

        let compressed_time = crate::histogram::compress_scale(
            &*crate::histogram::load(
                conpool,
                histogram_cache,
                HistogramKey {
                    encounter_id,
                    difficulty,
                    metric_type: MetricType::ProgTime,
                },
            )
            .await?,
            20,
        );

        let mut pull_bins = BTreeMap::new();
        let mut time_bins = BTreeMap::new();

        let median_ilvl = crate::histogram::load(
            conpool,
            histogram_cache,
            HistogramKey {
                encounter_id,
                difficulty,
                metric_type: MetricType::EndIlvl,
            },
        )
        .await?
        .quantile(0.5);

        for row in bin_data {
            if let Ok(mtype) = MetricType::try_from(row.metric_type) {
                match mtype {
                    mtype @ MetricType::ProgTime | mtype @ MetricType::PullCount => {
                        let (scale, bins) = match mtype {
                            MetricType::ProgTime => (compressed_time, &mut time_bins),
                            MetricType::PullCount => (compressed_pulls, &mut pull_bins),
                            _ => unreachable!(),
                        };
                        let new_index = scale.bin_index(mtype.scale().bin_bounds(row.bin_index).1);
                        bins.entry((row.day_index, new_index))
                            .or_insert_with(Vec::new)
                            .push(row);
                    }
                    other @ MetricType::StartIlvl | other @ MetricType::EndIlvl => {
                        let (lb, ub) = other.scale().bin_bounds(row.bin_index);
                        if median_ilvl
                            .map(|med| (med - lb).abs() > 20.0)
                            .unwrap_or(false)
                        {
                            // discard ilvls that are too far from the median. this helps deal with junk data or pulls on early bosses that only have an alt present
                            continue;
                        }
                        result.metric_type.push(other);
                        result.bin_start.push(lb);
                        result.bin_end.push(ub);
                        result.count.push(row.count as i64);
                        result.day_index.push(row.day_index as i64);
                    }
                }
            }
        }

        for ((day_index, bin_index), rows) in pull_bins {
            let (lb, ub) = compressed_pulls.bin_bounds(bin_index);
            result.metric_type.push(MetricType::PullCount);
            result.day_index.push(day_index as i64);
            result.bin_start.push(lb);
            result.bin_end.push(ub);
            result
                .count
                .push(rows.into_iter().map(|v| v.count as i64).sum());
        }

        for ((day_index, bin_index), rows) in time_bins {
            let (lb, ub) = compressed_time.bin_bounds(bin_index);
            result.metric_type.push(MetricType::ProgTime);
            result.day_index.push(day_index as i64);
            result.bin_start.push(lb);
            result.bin_end.push(ub);
            result
                .count
                .push(rows.into_iter().map(|v| v.count as i64).sum());
        }

        Ok(EncounterSummaryDataType::Hist(result))
    }
    #[graphql(cache_control(max_age = 3600))]
    async fn encounter_stat_summary_v2(
        &self,
        ctx: &Context<'_>,
        encounter_id: i32,
        difficulty: i32,
    ) -> Result<EncounterMetaSummary> {
        let Data {
            conpool,
            histogram_cache,
            ..
        } = ctx.data()?;

        if tier_start_for(conpool, encounter_id).await.is_err() {
            // unsupported
            return Err("Tier not supported.".into());
        }

        let mut con = conpool.acquire().await?;

        let pulls_histogram = crate::histogram::load(
            &mut *con,
            histogram_cache,
            HistogramKey {
                encounter_id,
                difficulty,
                metric_type: MetricType::PullCount,
            },
        )
        .await?;

        let prog_time_histogram = crate::histogram::load(
            &mut *con,
            histogram_cache,
            HistogramKey {
                encounter_id,
                difficulty,
                metric_type: MetricType::ProgTime,
            },
        )
        .await?;

        let data = EncounterMetaSummary {
            kill_count: pulls_histogram.total_count() as i64,
            pulls_range: vec![
                pulls_histogram.quantile(0.25),
                pulls_histogram.quantile(0.75),
            ]
            .into_iter()
            .filter_map(|v| v)
            .collect(),
            prog_time_range: vec![
                prog_time_histogram.quantile(0.25),
                prog_time_histogram.quantile(0.75),
            ]
            .into_iter()
            .filter_map(|v| v)
            .collect(),
        };

        Ok(data)
    }

    #[graphql(cache_control(max_age = 3600))]
    async fn detailed_stat_summary(
        &self,
        ctx: &Context<'_>,
        encounter_id: i32,
    ) -> Result<Vec<FightDetailedStats>> {
        let Data {
            conpool,
            histogram_cache,
            details_limiter,
            ..
        } = ctx.data()?;
        let pulls_histogram = crate::histogram::load(
            conpool,
            histogram_cache,
            HistogramKey {
                encounter_id,
                difficulty: 5,
                metric_type: MetricType::PullCount,
            },
        )
        .await?;

        let pull_group_size = pulls_histogram
            .quantile(0.9)
            .map(|pull_count| (pull_count / 20.0).ceil() as i32)
            .unwrap_or(5);
        let data = analysis::details::detailed_stats(
            conpool.clone(),
            encounter_id,
            pull_group_size,
            pulls_histogram.quantile(0.8).map(|q| q.ceil() as i32),
            details_limiter.clone(),
        )
        .await?;
        Ok(data)
    }

    #[graphql(cache_control(max_age = 3600))]
    async fn detailed_death_summary(
        &self,
        ctx: &Context<'_>,
        encounter_id: i32,
    ) -> Result<Vec<DeathDetails>> {
        let Data {
            conpool,
            histogram_cache,
            client,
            token,
            details_limiter,
            ..
        } = ctx.data()?;
        let pulls_histogram = crate::histogram::load(
            conpool,
            histogram_cache,
            HistogramKey {
                encounter_id,
                difficulty: 5,
                metric_type: MetricType::PullCount,
            },
        )
        .await?;

        let pull_group_size = pulls_histogram
            .quantile(0.9)
            .map(|pull_count| (pull_count / 20.0).ceil() as i32)
            .unwrap_or(5);
        let data = analysis::details::detailed_deaths(
            client.clone(),
            token.clone(),
            conpool.clone(),
            encounter_id,
            pull_group_size,
            pulls_histogram.quantile(0.8).map(|q| q.ceil() as i32),
            details_limiter.clone(),
        )
        .await?;
        Ok(data)
    }
}

pub struct SubscriptionRoot;

async fn lookup_guild(con: &mut PgConnection, guild_id: GuildId) -> AnyResult<Option<Team>> {
    let data = sqlx::query_as!(
        Team,
        "select * from teams where id = $1 and tag = $2",
        guild_id.id() as i64,
        guild_id.is_tag()
    )
    .fetch_optional(con)
    .await?;

    Ok(data)
}

async fn lookup_log(con: &mut PgConnection, code: &str) -> AnyResult<Option<Log>> {
    let data = sqlx::query_as!(Log, "select * from logs where code = $1", code)
        .fetch_optional(con)
        .await?;

    Ok(data)
}

#[derive(Enum, Copy, Clone, Eq, PartialEq, Debug)]
pub enum UpdateKind {
    Initiated,
    GuildLoaded,
    LogsListed,
    LogLoaded,
    Complete,
    /// Cancellation due to an error.
    Cancelled,
}

impl Default for UpdateKind {
    fn default() -> Self {
        UpdateKind::Cancelled
    }
}

#[derive(SimpleObject, Default, Debug)]
pub struct StatusUpdate {
    kind: UpdateKind,
    total_logs: Option<i64>,
    remaining_logs: Option<i64>,
}

impl StatusUpdate {
    fn with_kind(kind: UpdateKind) -> Self {
        StatusUpdate {
            kind,
            ..Default::default()
        }
    }
}

async fn have_log(con: &PgPool, code: &str, end_time: f64) -> AnyResult<bool> {
    let data = sqlx::query_scalar!(
        "select count(*) from logs join fights on fights.log = logs.iid where code = $1 and logs.end_time = $2 and last_phase is not null",
        code,
        wcl_timestamp(end_time)
    )
    .fetch_one(con)
    .await?;

    Ok(data.unwrap_or(0) > 0)
}

pub(crate) async fn background_import(
    client: Client,
    token: WclToken,
    con: &PgPool,
    tx: Option<Sender<StatusUpdate>>,
    guild_id: i64,
    tag_id: Option<i64>,
    zone: i64,
    force_analysis: Option<bool>,
    tx_details: Sender<DetailsRequest>,
) -> anyhow::Result<()> {
    if let Some(ref tx) = tx {
        tx.send(StatusUpdate::with_kind(UpdateKind::Initiated))
            .await?;
    }
    let guild = guild::lookup_guild_by_id(&client, &token, guild_id).await?;
    let guild = guild.data.and_then(|d| d.guild_data).and_then(|d| d.guild);

    if guild.is_none() {
        if let Some(ref tx) = tx {
            tx.send(StatusUpdate::with_kind(UpdateKind::Cancelled))
                .await?;
        }
        debug!("guild DNE");
        return Err(ImportError::GuildDoesNotExist { id: guild_id }.into());
    }
    if let Some(ref tx) = tx {
        tx.send(StatusUpdate::with_kind(UpdateKind::GuildLoaded))
            .await?;
    }

    let guild = guild.unwrap();
    debug!("loading private token");
    let private_token = crate::user_auth::token_for_team(con, guild_id as i32, zone as i32).await?;

    let (mode, token) = if let Some(token) = private_token {
        debug!("private token found. using for sync of {:?}", guild_id);
        (WclMode::Private, token)
    } else {
        debug!("no private token found for {:?}", guild_id);
        (WclMode::Public, token)
    };

    let guild_id = tag_id.map_or(GuildId::Guild(guild_id), |tid| GuildId::Tag(tid));

    // collect reports within the zone range +- 3 days (to capture people starting logs early / our
    // release dates being off on the order of hours)
    struct ZoneRange {
        start_date: Option<f64>,
        end_date: Option<f64>,
    }
    let ZoneRange {
        start_date,
        end_date,
    } = sqlx::query_as!(
        ZoneRange,
        "select cast(extract(epoch from release_date - interval '3 days') * 1000 as double precision) as start_date,
                cast(extract(epoch from end_date + interval '3 days') * 1000 as double precision) as end_date
                    from release_dates
                    where zone = $1",
        zone as i32
    )
    .fetch_one(con)
    .await?;

    debug!("listing reports");
    let reports = reports::guild_reports(
        &client,
        &token,
        mode,
        guild_id,
        start_date.expect("each zone to have a start date"),
        end_date,
    )
    .await?;
    if reports.is_empty() {
        debug!("importing tags");
        let mut con = con.acquire().await?;
        import_tags(&mut *con, guild).await?;
        if let Some(ref tx) = tx {
            tx.send(StatusUpdate::with_kind(UpdateKind::Cancelled))
                .await?;
        }
        debug!("guild has no reports");
        // this cancelation is not a real error---just nothing to do
        return Ok(());
    }
    if let Some(ref tx) = tx {
        tx.send(StatusUpdate::with_kind(UpdateKind::LogsListed))
            .await?;
    }

    let mut new_encounters = HashSet::new();
    let count = reports.len() as i64;
    let mut new_logs = Vec::new();

    for (ix, report) in reports.iter().enumerate() {
        let remaining = Some(count - ix as i64 - 1);

        if have_log(con, &report.code, report.end_time).await? {
            if let Some(ref tx) = tx {
                tx.send(StatusUpdate {
                    kind: UpdateKind::LogLoaded,
                    remaining_logs: remaining,
                    total_logs: Some(count),
                })
                .await?;
            }
            debug!("log {} already in db. skipping", &report.code);
            continue;
        } else {
            // log is missing one or more fights
            debug!("log {} missing some fights. reloading.", &report.code);
        }

        let fights =
            reports::report_fights(&client, &token, mode, report.code.clone(), None, None).await;

        if fights.is_err() {
            if let Some(ref tx) = tx {
                tx.send(StatusUpdate {
                    kind: UpdateKind::Cancelled,
                    ..Default::default()
                })
                .await?;
            }
            return Err(fights.unwrap_err().into());
        }

        new_logs.push((report, fights.unwrap()));

        if let Some(ref tx) = tx {
            tx.send(StatusUpdate {
                total_logs: Some(count),
                remaining_logs: remaining,
                kind: UpdateKind::LogLoaded,
            })
            .await?;
        }
    }

    let mut dbtx = con.begin().await?;

    debug!("importing tags");
    import_tags(&mut *dbtx, guild).await?;
    debug!("done importing tags");
    let team = lookup_guild(&mut *dbtx, guild_id).await?.ok_or_else(|| {
        anyhow!(
            "team {:?} does not exist during import. this should not occur. tx isolation issue?",
            guild_id
        )
    })?;

    import_logs(&mut *dbtx, &team, &reports).await?;

    for (report, fights) in new_logs {
        replace_log(&mut *dbtx, &report).await?;
        let log = lookup_log(&mut *dbtx, &report.code).await;
        let log = if let Ok(Some(log)) = log {
            log
        } else if let Err(e) = log {
            return Err(e.into());
        } else {
            return Err(anyhow!("unable to load log data for {}", &report.code).into());
        };

        if let Some(fights) = fights.data {
            // what a lovely line of code
            if let Some(fights) = fights
                .report_data
                .as_ref()
                .and_then(|d| d.report.as_ref())
                .and_then(|d| d.fights.as_ref())
            {
                for fight in fights {
                    if let Some(fight) = fight {
                        new_encounters.insert((fight.encounter_id, fight.difficulty));
                    }
                }
            }
            import_fights(&mut *dbtx, &log, &fights)
                .await
                .expect("to be able to import the fights");
        } else {
            info!("no fights reported for log {}", report.code.clone());
        }
    }

    info!("force? {:?}", force_analysis);
    match force_analysis {
        Some(true) => {
            generate_analysis(&mut dbtx, guild_id.id() as i32, zone as i32).await?;
        }
        _ => {
            if !new_encounters.is_empty() {
                for (encounter, difficulty) in new_encounters {
                    if let Some(difficulty) = difficulty {
                        generate_encounter_analysis(
                            &mut dbtx,
                            guild_id.id() as i32,
                            encounter as i32,
                            difficulty as i32,
                        )
                        .await?
                    }
                }
            }
        }
    }

    dbtx.commit().await?;

    if let Some(ref tx) = tx {
        tx.send(StatusUpdate::with_kind(UpdateKind::Complete))
            .await?;
    }

    dispatch_progression_details_import(tx_details, guild_id.id() as i32, zone as i32).await;

    Ok(())
}

#[Subscription]
impl SubscriptionRoot {
    async fn sync(
        &self,
        ctx: &Context<'_>,
        guild_id: i64,
        zone: i64,
        tag: Option<i64>,
        force_analysis: Option<bool>,
    ) -> Result<ReceiverStream<StatusUpdate>> {
        log::info!(
            "synchronizing data for guild {} {} {} {}",
            guild_id,
            zone,
            tag.unwrap_or(-1),
            force_analysis.unwrap_or(false),
        );
        let (tx, rx) = channel(5);
        let (con, client, token, tx_details) = {
            let Data {
                client,
                token,
                conpool,
                transmit_details_request,
                ..
            } = ctx.data()?;

            (
                conpool.clone(),
                client.clone(),
                token.clone(),
                transmit_details_request.clone(),
            )
        };

        tokio::spawn(async move {
            let res = background_import(
                client,
                token,
                &con,
                Some(tx.clone()),
                guild_id,
                tag,
                zone,
                force_analysis,
                tx_details,
            )
            .await;

            if let Err(e) = res {
                match e.downcast_ref::<SendError<StatusUpdate>>() {
                    Some(SendError(_)) => {
                        debug!("channel closed: {:?}", e);
                    }
                    _ => {
                        error!("an error occurred during import: {:?}", e);
                        tx.send(StatusUpdate::with_kind(UpdateKind::Cancelled))
                            .await
                            .unwrap_or_else(|e| {
                                error!("unable to communicate error to client: {:?}", e)
                            });
                    }
                }
            }
        });

        Ok(ReceiverStream::new(rx))
    }
}

pub struct MutationRoot;

#[Object]
impl MutationRoot {
    async fn store_code(
        &self,
        ctx: &Context<'_>,
        code: String,
        zone: i64,
        team: i64,
    ) -> Result<bool> {
        let Data {
            client,
            conpool,
            secrets,
            ..
        } = ctx.data()?;

        let token = match crate::user_auth::token_for_code(&client, &secrets, &code).await {
            Ok(t) => t,
            Err(e) => {
                match e.downcast_ref::<crate::user_auth::Error>() {
                    Some(crate::user_auth::Error::TokenRevoked) => {
                        // not allowed to use this token. possible race condition with WCL?
                        warn!("token revoked during store_code handling for {}", team);
                        return Ok(false);
                    }
                    Some(crate::user_auth::Error::TokenExpired) | None => return Err(e.into()),
                }
            }
        };

        if !user_validation::privileged_user(&client, &token, team, zone).await? {
            return Ok(false);
        }

        let mut con = conpool.acquire().await?;

        sqlx::query!(
            "insert into auth_codes (expiration, team, zone, access_token, refresh_token)
             values ($1, $2, $3, $4, $5)
             on conflict do nothing",
            Utc::now() + Duration::seconds(token.expires_in as i64),
            team as i32,
            zone as i32,
            &token.access_token,
            &token
                .refresh_token
                .ok_or(anyhow!("missing refresh token"))?
        )
        .execute(&mut *con)
        .await?;

        Ok(true)
    }

    async fn revoke_team(
        &self,
        ctx: &Context<'_>,
        code: String,
        team: i64,
        zone: i64,
    ) -> Result<bool> {
        let Data {
            client,
            conpool,
            secrets,
            ..
        } = ctx.data()?;

        let token = match crate::user_auth::token_for_code(&client, &secrets, &code).await {
            Ok(t) => t,
            Err(e) => {
                match e.downcast_ref::<crate::user_auth::Error>() {
                    Some(crate::user_auth::Error::TokenRevoked) => {
                        // not allowed to use this token. possible race condition with WCL?
                        warn!("token revoked during revoke_team handling for {}", team);
                        return Ok(false);
                    }
                    Some(crate::user_auth::Error::TokenExpired) | None => return Err(e.into()),
                }
            }
        };

        if !user_validation::privileged_user(&client, &token, team, zone).await? {
            return Ok(false);
        }

        let mut con = conpool.acquire().await?;

        let mut dbtx = con.begin().await?;

        sqlx::query!("delete from auth_codes where team = $1", team as i32)
            .execute(&mut *dbtx)
            .await?;

        // TODO: rewrite to avoid round-trip
        let datum = sqlx::query!(
            "select name, region_slug, server_slug from teams where id = $1",
            team as i32
        )
        .fetch_one(&mut *dbtx)
        .await?;

        sqlx::query!(
            "delete from teams where name = $1 and region_slug = $2 and server_slug = $3",
            datum.name,
            datum.region_slug,
            datum.server_slug
        )
        .execute(&mut *dbtx)
        .await?;

        dbtx.commit().await?;

        Ok(true)
    }
}

pub type Schema = async_graphql::Schema<Root, MutationRoot, SubscriptionRoot>;
