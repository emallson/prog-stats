use crate::background_jobs::background_refresh::background_refresh_progress;
use crate::background_jobs::details::progression_details_job;
use crate::background_jobs::progress_import::import_progress_job;
use crate::graphql::{Data, MutationRoot, Root, Schema, SubscriptionRoot};
use crate::user_auth::background_refresh_tokens;
use crate::{ServerOpt, WclSecrets, WclToken};
use anyhow::Result;
use async_graphql::Request;
use async_graphql_warp::{BadRequest, Response};
use log::error;
use moka::future::Cache;
use sqlx::postgres::{PgConnectOptions, PgPoolOptions};
use sqlx::{ConnectOptions, PgPool};
use std::collections::HashSet;
use std::convert::Infallible;
use std::env;
use std::net::IpAddr;
use std::str::FromStr;
use std::sync::Arc;
use std::time::Duration;
use tokio::sync::{RwLock, Semaphore};
use warp::http::{Method, StatusCode};
use warp::{Filter, Rejection};

const SERVE_DIR: &str = "./public";

pub async fn connect_db() -> Result<PgPool> {
    let opts = env::var("DATABASE_URL")
        .expect("the database url to be set")
        .parse::<PgConnectOptions>()?;

    let opts = opts
        .log_statements(log::LevelFilter::Trace)
        .log_slow_statements(
            log::LevelFilter::Warn,
            core::time::Duration::from_millis(50),
        );
    let conpool = PgPoolOptions::new()
        .max_connections(90)
        .connect_with(opts)
        .await?;

    Ok(conpool)
}

#[deprecated(
    since = "25 December 2024",
    note = "this table is no longer in active use. it remains present for backup purposes only"
)]
async fn refresh_ess() {
    let opts = env::var("DATABASE_URL")
        .expect("the database url to be set")
        .parse::<PgConnectOptions>()
        .expect("the database url to be valid");
    let opts = opts
        .log_statements(log::LevelFilter::Trace)
        .log_slow_statements(
            log::LevelFilter::Warn,
            core::time::Duration::from_millis(5000),
        );

    loop {
        log::info!("starting encounter_stats_summary refresh...");
        if let Ok(mut con) = opts.connect().await {
            let res =
                sqlx::query!("refresh materialized view concurrently encounter_stats_summary")
                    .execute(&mut con)
                    .await;

            if let Err(e) = res {
                error!("unable to refresh encounter_stats_summary: {:?}", e);
            } else {
                log::info!("finished encounter_stats_summary refresh");
            }
        } else {
            error!("Unable to connect to database for refresh");
        }

        tokio::time::sleep(chrono::Duration::hours(1).to_std().unwrap()).await;
    }
}

pub async fn build_web_server(opts: ServerOpt, token: WclToken, secrets: WclSecrets) -> Result<()> {
    let client = reqwest::Client::new();

    let conpool = connect_db().await?;
    let schema = Schema::build(Root, MutationRoot, SubscriptionRoot);

    let _token_handle = tokio::spawn(background_refresh_tokens(
        conpool.clone(),
        secrets.clone(),
        client.clone(),
    ));

    let (tx_details, rx_details) = tokio::sync::mpsc::channel(100);
    let _details = tokio::spawn(progression_details_job(
        rx_details,
        conpool.clone(),
        client.clone(),
        token.clone(),
    ));

    let _background_progress = tokio::spawn(background_refresh_progress(
        conpool.clone(),
        client.clone(),
        token.clone(),
        tx_details.clone(),
    ));

    let _progress_import = tokio::spawn(import_progress_job(
        conpool.clone(),
        client.clone(),
        token.clone(),
        tx_details.clone(),
    ));

    let schema = schema
        .data(Data {
            secrets,
            client,
            token,
            conpool,
            realm_drop_dashes: RwLock::new(HashSet::new()),
            histogram_cache: Cache::builder().time_to_live(Duration::new(600, 0)).build(),
            transmit_details_request: tx_details,
            details_limiter: Arc::new(Semaphore::new(2)),
        })
        .finish();

    let cors = warp::filters::cors::cors()
        .allow_any_origin()
        .allow_header("content-type")
        .allow_method(Method::POST)
        .build();

    let gql = warp::path("graphql")
        .and(warp::post())
        .and(warp::filters::header::optional("x-forwarded-for"))
        .and(async_graphql_warp::graphql(schema.clone()))
        .and_then(
            |fwd: Option<String>, (schema, req): (Schema, Request)| async move {
                log::info!(
                    "graphql request: {} {}",
                    req.operation_name
                        .clone()
                        .unwrap_or("Unknown-Operation".to_string()),
                    fwd.unwrap_or("Unknown".to_string())
                );
                let q = req.query.clone();
                let res = schema.execute(req).await;
                if res.is_err() {
                    error!("graphql error response: {} {:?}", q, &res.errors);
                }
                Ok::<_, Infallible>(Response::from(res))
            },
        )
        .with(cors);

    let routes = gql
        .or(async_graphql_warp::graphql_subscription(schema))
        .recover(|err: Rejection| async move {
            if let Some(BadRequest(err)) = err.find() {
                return Ok(warp::reply::with_status(
                    err.to_string(),
                    StatusCode::BAD_REQUEST,
                ));
            }

            Err(err)
        });

    let routes = routes
        .or(warp::fs::dir(SERVE_DIR).with(warp::reply::with::header(
            "Cache-Control",
            "max-age=31536000",
        )))
        .or(warp::fs::file(format!("{}/index.html", SERVE_DIR)))
        .with(warp::filters::compression::gzip());

    let _handle = tokio::spawn(refresh_ess());

    warp::serve(routes.with(warp::log("server")))
        .run((IpAddr::from_str(&opts.address)?, opts.port))
        .await;

    Ok(())
}
