use std::collections::HashSet;

use anyhow::anyhow;
use reqwest::Client;
use sqlx::PgPool;
use tokio::sync::mpsc::Sender;

use crate::{
    analysis::{encounters_for_zone, latest_open_zone},
    graphql::background_import,
    queries::progress_ranks::encounter_progress_rankings,
    WclToken,
};

use super::details::DetailsRequest;

/// how long to go between progress import refreshes
const PROGRESS_REFRESH_DELAY: chrono::TimeDelta = chrono::TimeDelta::days(3);
/// how long to go between progress import refreshes in case of an error response.
/// we want to retry sooner than the next week.
const PROGRESS_REFRESH_DELAY_ERR: chrono::TimeDelta = chrono::TimeDelta::days(1);
/// how long to go between importing guild data
const GUILD_IMPORT_DELAY: chrono::TimeDelta = chrono::TimeDelta::minutes(1);

/// the first encounter (in order) to include in progress import
const MIN_ENCOUNTER_ORDER: usize = 5;
const DIFFICULTY: i64 = 5;

pub async fn import_progress_job(
    conpool: PgPool,
    client: Client,
    token: WclToken,
    tx_details: Sender<DetailsRequest>,
) {
    loop {
        log::info!("attempting progress data import");
        match import_progress(&conpool, &client, &token, tx_details.clone()).await {
            Err(error) => {
                log::error!("failed to import progress data: {}", error);
                tokio::time::sleep(PROGRESS_REFRESH_DELAY_ERR.to_std().unwrap()).await;
            }
            Ok(_) => {
                log::info!("completed progress data import");
                tokio::time::sleep(PROGRESS_REFRESH_DELAY.to_std().unwrap()).await;
            }
        }
    }
}

async fn import_progress(
    conpool: &PgPool,
    client: &Client,
    token: &WclToken,
    tx_details: Sender<DetailsRequest>,
) -> anyhow::Result<()> {
    let zone = match latest_open_zone(conpool).await? {
        Some(id) => id,
        None => return Err(anyhow!("no zones are currently open")),
    };

    let encounters = encounters_for_zone(conpool, zone).await?;
    let mut encounters = encounters
        .into_iter()
        .skip(MIN_ENCOUNTER_ORDER - 1)
        .collect::<Vec<_>>();

    if encounters.is_empty() {
        return Err(anyhow!(
            "there are too few encounters in the list for zone {}",
            zone
        ));
    }

    // latest boss first
    encounters.reverse();

    let mut processed_guilds = HashSet::new();

    for encounter_id in encounters {
        log::info!("loading progress data for boss {}", encounter_id);
        let rankings =
            encounter_progress_rankings(client, token, encounter_id as i64, DIFFICULTY).await?;

        for ranking in rankings {
            if !processed_guilds.insert(ranking.clone()) {
                continue; // we have already seen this guild during the import. don't duplicate it.
            }

            let (server, guild) = ranking;

            let guild_id = match guild.id {
                Some(id) => id,
                None => continue,
            };

            if should_skip(conpool, guild_id as i32, zone, DIFFICULTY as i32).await? {
                log::info!(
                    "skipping progress import for guild {} on server {}",
                    guild.name,
                    server.name
                );
                continue;
            }

            log::info!(
                "importing data for progress guild {} on server {}",
                guild.name,
                server.name
            );

            match background_import(
                client.clone(),
                token.clone(),
                conpool,
                None,
                guild_id as i64,
                None,
                zone as i64,
                None,
                tx_details.clone(),
            )
            .await
            {
                Err(err) => log::error!(
                    "failed to import guild {} on server {}: {}",
                    guild.name,
                    server.name,
                    err
                ),
                _ => {}
            };

            tokio::time::sleep(GUILD_IMPORT_DELAY.to_std().unwrap()).await;
        }
    }

    Ok(())
}

/// Check if we should skip progress-based discovery. If there is already data for relevant bosses,
/// then we rely on background refresh.
async fn should_skip(con: &PgPool, team_id: i32, zone: i32, difficulty: i32) -> sqlx::Result<bool> {
    let boss_kills = sqlx::query_scalar!(
        "
        select count(1) as kill_count
        from log_encounter_stats
        join encounters using (encounter_id)
        where zone = $1
          and difficulty = $2
          and has_kill
          and not post_prog
          and team_id = $3
        ",
        zone,
        difficulty,
        team_id
    )
    .fetch_optional(con)
    .await?;

    Ok(boss_kills
        .flatten()
        .map(|k| k >= MIN_ENCOUNTER_ORDER as i64)
        .unwrap_or(false))
}
