use anyhow::anyhow;
use chrono::{DateTime, Utc};
use reqwest::Client;
use sqlx::postgres::types::PgInterval;
use sqlx::prelude::*;
use sqlx::PgPool;
use tokio::sync::mpsc::Sender;

use crate::analysis::latest_open_zone;
use crate::graphql;
use crate::graphql::ImportError;
use crate::model::Team;
use crate::WclToken;

use super::details::DetailsRequest;

/// How often to check each individual guild for updates.
const REFRESH_INTERVAL: PgInterval = PgInterval {
    days: 3,
    months: 0,
    microseconds: 0,
};
/// How long to sleep between each loop iteration
const LOOP_INTERVAL: chrono::Duration = chrono::Duration::minutes(1);
/// The minimum number of bosses a guild needs to have killed in order to be eligible for automatic progress updates.
const MIN_PROGRESS: i64 = 4;
const PROGRESS_DIFFICULTY: i32 = 5; // Mythic difficulty

/// Refresh the progress information for guilds currently in the dataset for the latest tier automatically in the background.
pub async fn background_refresh_progress(
    conpool: PgPool,
    client: Client,
    token: WclToken,
    tx_details: Sender<DetailsRequest>,
) {
    loop {
        log::info!("starting background refresh iteration");

        match update_single_guild(&conpool, client.clone(), token.clone(), tx_details.clone()).await
        {
            Err(error) => {
                log::error!("Guild background update failed: {}", error);
            }
            _ => {}
        }
        log::info!("finished background refresh iteration. sleeping.");

        tokio::time::sleep(LOOP_INTERVAL.to_std().unwrap()).await;
    }
}

async fn update_single_guild(
    pool: &PgPool,
    client: Client,
    token: WclToken,
    tx_details: Sender<DetailsRequest>,
) -> anyhow::Result<()> {
    let zone_id = match latest_open_zone(pool).await? {
        Some(id) => id,
        None => return Err(anyhow!("no open zones found")),
    };

    if let Some(team) = select_team(pool, zone_id, PROGRESS_DIFFICULTY).await? {
        log::info!(
            "attempting background update of team with iid {} ({:?})",
            team.iid,
            team
        );
        // terrible copy-paste from backfill code
        let (guild_id, tag_id) = if team.tag {
            let gid = sqlx::query_scalar!(
                "select id from teams where name = $1 and server_slug = $2 and region_slug = $3 and not tag",
                team.name, team.server_slug, team.region_slug
            ).fetch_optional(pool).await?;

            if let Some(gid) = gid {
                (gid, Some(team.id as i64))
            } else {
                // mark the team as updated and check again later. this happens in rare cases where the base guild was never imported.
                mark_updated(&pool, team.iid).await?;
                return Ok(());
            }
        } else {
            (team.id, None)
        };
        match graphql::background_import(
            client,
            token,
            pool,
            None,
            guild_id as i64,
            tag_id,
            zone_id as i64,
            None,
            tx_details,
        )
        .await
        {
            Ok(_) => {}
            Err(error) => {
                match error.downcast_ref::<ImportError>() {
                    Some(ImportError::GuildDoesNotExist { .. }) => {
                        // ignore, mark the update as complete and move on
                    }
                    Some(_) | None => {
                        return Err(anyhow!("Background import failed: {}", error));
                    }
                }
            }
        }
        mark_updated(&pool, team.iid).await?;
    }

    Ok(())
}

async fn mark_updated(pool: &PgPool, team_iid: i32) -> sqlx::Result<()> {
    sqlx::query!(
        "insert into team_updates (team_iid, last_update) values ($1, now())
         on conflict (team_iid) do update set last_update = now()",
        team_iid,
    )
    .execute(pool)
    .await?;

    Ok(())
}

async fn select_team(con: &PgPool, zone_id: i32, difficulty: i32) -> sqlx::Result<Option<Team>> {
    let team = sqlx::query_as!(
        Team,
        "with boss_kills as (
            select team_id, count(1) as kill_count
            from log_encounter_stats
            join encounters using (encounter_id)
            where zone = $1
              and difficulty = $2
              and has_kill
              and not post_prog
            group by team_id
         )
         select teams.* from teams
         left join team_updates tu on teams.iid = tu.team_iid
         join boss_kills k on k.team_id = teams.id
         where k.kill_count >= $3
           and (tu.last_update is null or tu.last_update <= (now() - $4::interval))
         order by coalesce(tu.last_update, '2000-01-01')
         limit 1
        ",
        zone_id,
        difficulty,
        MIN_PROGRESS,
        REFRESH_INTERVAL,
    )
    .fetch_optional(con)
    .await?;

    Ok(team)
}
