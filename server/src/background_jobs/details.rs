use std::ops::DerefMut;

use anyhow::anyhow;
use reqwest::Client;
use sqlx::{PgPool, Postgres, Transaction};
use tokio::sync::mpsc::error::TrySendError;
use tokio::sync::mpsc::{Receiver, Sender};

use crate::analysis::latest_open_zone;
use crate::queries::fight_details::fight_details;
use crate::queries::rate_limit::rate_limit;
use crate::{
    analysis::{prog_logs, raw_logs_for_encounter, ProgData},
    bulk_insert::BulkInsert,
    model::Log,
    queries::reports,
    WclMode, WclToken,
};

pub type DetailsRequest = (i32, i32);

pub async fn dispatch_progression_details_import(
    tx: Sender<DetailsRequest>,
    team_id: i32,
    zone_id: i32,
) {
    match tx.try_send((team_id, zone_id)) {
        Ok(_) => {}
        Err(TrySendError::Full(_)) => {
            // do nothing. this is going to happen because of rate limits
        }
        Err(TrySendError::Closed(_)) => {
            log::error!("unable to dispatch progress details import, listener is dead.");
        }
    }
}

pub async fn progression_details_job(
    mut rx: Receiver<DetailsRequest>,
    pool: PgPool,
    client: Client,
    public_token: WclToken,
) -> anyhow::Result<()> {
    while let Some((team_id, zone_id)) = rx.recv().await {
        let latest_zone = latest_open_zone(&pool).await;
        match latest_zone {
            Ok(Some(latest_id)) => {
                if latest_id != zone_id {
                    log::info!(
                        "requested zone {} is not latest open zone {}, skipping",
                        zone_id,
                        latest_id
                    );
                    continue;
                }
            }
            Ok(None) => {
                log::info!("no zones are currently open, skipping");
            }
            Err(error) => {
                log::error!("unable to retrieve latest open zone: {}", error);
                continue;
            }
        }
        if let Ok(private_token) = crate::user_auth::token_for_team(&pool, team_id, zone_id).await {
            let (token, mode) = match private_token {
                Some(ref t) => (t, WclMode::Private),
                None => (&public_token, WclMode::Public),
            };
            match import_progression_details(&client, token, mode, &pool, team_id, zone_id).await {
                Ok(_) => {}
                Err(error) => {
                    log::error!("unable to complete progress details import: {}", error);
                }
            };
        } else {
            log::debug!(
                "skipping team {} in zone {} because token is bad",
                team_id,
                zone_id
            );
        }
    }

    Ok(())
}

async fn import_progression_details(
    client: &Client,
    token: &WclToken,
    mode: WclMode,
    pool: &PgPool,
    team_id: i32,
    zone_id: i32,
) -> anyhow::Result<()> {
    log::info!(
        "importing progression details for team {} in zone {}",
        team_id,
        zone_id
    );
    let encounters = sqlx::query!(
        "select distinct encounter_id from fights
         join logs on fights.log = logs.iid
         join teams on teams.iid = logs.team
         join encounters using (encounter_id)
         where teams.id = $1 and zone = $2 and difficulty = 5",
        team_id,
        zone_id
    )
    .fetch_all(pool)
    .await?;

    let mut fights_til_recheck = 0;

    for record in encounters {
        let encounter_id = record.encounter_id;
        let difficulty = 5;

        let raw_logs = raw_logs_for_encounter(pool, team_id, encounter_id, difficulty).await?;

        let ProgData { logs, .. } = prog_logs(pool, encounter_id, difficulty, raw_logs).await?;
        'log_loop: for log in &logs {
            log::info!(
                "importing progression details for log {} and encounter {}",
                log.code,
                encounter_id
            );
            let mut tx = pool.begin().await?;
            // even though we have the fight list from `prog_logs`, we need to refetch in order to get phases.
            let fights = reports::report_fights(
                client,
                token,
                mode,
                log.code.clone(),
                Some(encounter_id as i64),
                Some(difficulty as i64),
            )
            .await?;

            if let Some(errors) = fights.errors {
                if !errors.is_empty() {
                    return Err(anyhow!("unable to retrieve report fights: {:?}", errors));
                }
            }

            for fight in fights
                .data
                .and_then(|d| d.report_data)
                .and_then(|rd| rd.report)
                .and_then(|r| r.fights)
                .unwrap_or_default()
            {
                if let Some(ref fight) = fight {
                    if fights_til_recheck <= 0 {
                        fights_til_recheck = conserve_rate_limit(client, token, mode).await;
                    }
                    if import_fights_details(client, token, mode, &mut tx, log, fight).await? {
                        fights_til_recheck -= 1;
                    }

                    if fight.kill.unwrap_or(false) {
                        // `prog_logs` is kind of a lie. it only deduplicates. it includes farm kills.
                        log::info!(
                            "saw kill of {} by {}, going to next encounter",
                            encounter_id,
                            team_id
                        );
                        // early commit.
                        tx.commit().await?;
                        break 'log_loop; // go to next encounter
                    }
                }
            }

            tx.commit().await?;
        }
    }
    Ok(())
}

/// Actually do the work of importing fight details. Returns `true` if we made fight data requests.
async fn import_fights_details(
    client: &Client,
    token: &WclToken,
    mode: WclMode,
    tx: &mut Transaction<'_, Postgres>,
    log: &Log,
    fight: &reports::fights::FightsReportDataReportFights,
) -> anyhow::Result<bool> {
    let fight_iid = sqlx::query_scalar!(
        "
        select iid from fights
        where fights.log = $1
          and fights.id = $2
    ",
        log.iid,
        fight.id as i32
    )
    .fetch_optional(tx.deref_mut())
    .await?;

    let fight_iid = match fight_iid {
        Some(iid) => iid,
        None => {
            return Err(anyhow!("fight is not present in the database. ignoring."));
        }
    };

    let existing_stats = sqlx::query_scalar!(
        "select count(1) from fight_detailed_stats
         where fight_iid = $1",
        fight_iid,
    )
    .fetch_one(tx.deref_mut())
    .await?;

    if existing_stats.unwrap_or(0) > 0 {
        // stats are already present. we're done
        return Ok(false);
    }

    log::info!("importing fight details for fight id {}", fight.id);

    let details = fight_details(client, token, mode, log.code.clone(), &fight).await?;

    // we don't worry about clearing old data before re-import because we
    // first check for the presence of data and end early if it is present

    let stats = details
        .overall_data
        .into_iter()
        .map(|overall| NewFightDetailedStats {
            fight_iid,
            phase_index: overall.phase_index.unwrap_or(-1),
            duration_ms: overall.duration_ms as i32,
            overall_dps: overall.overall_dps,
            overall_hps: overall.overall_hps,
            nontank_dtps: overall.nontank_dtps,
            dps_spread: overall.dps_spread,
            hps_spread: overall.hps_spread,
        })
        .collect();

    BulkInsert::new("fight_detailed_stats")
        .add_rows(stats)
        .on_conflict(
            vec!["fight_iid", "phase_index"],
            vec![
                "duration_ms",
                "overall_dps",
                "overall_hps",
                "nontank_dtps",
                "dps_spread",
                "hps_spread",
            ],
        )
        .execute(tx.deref_mut())
        .await?;

    let deaths = details
        .deaths
        .into_iter()
        .map(|death| NewFightDetailedDeaths {
            fight_iid,
            death_index: death.death_index,
            death_offset_ms: death.death_offset_ms,
            death_role: death.death_role.id(),
            duration_ms: death.duration_ms,
            killing_ability_id: death.killing_ability_id,
        })
        .collect();

    BulkInsert::new("fight_detailed_deaths")
        .add_rows(deaths)
        .on_conflict(
            vec!["fight_iid", "death_index"],
            vec![
                "death_offset_ms",
                "death_role",
                "killing_ability_id",
                "duration_ms",
            ],
        )
        .execute(tx.deref_mut())
        .await?;

    let specs = details
        .specs
        .into_iter()
        .map(|(spec, present_count)| NewFightDetailedSpecs {
            fight_iid,
            present_count: present_count as i32,
            class_name: spec.class_name,
            spec_name: spec.spec_name,
        })
        .collect();

    BulkInsert::new("fight_detailed_specs")
        .add_rows(specs)
        .on_conflict(
            vec!["fight_iid", "class_name", "spec_name"],
            vec!["present_count"],
        )
        .execute(tx.deref_mut())
        .await?;

    Ok(true)
}

#[derive(BindAll)]
struct NewFightDetailedStats {
    fight_iid: i32,
    phase_index: i32,
    duration_ms: i32,
    overall_dps: f32,
    overall_hps: f32,
    nontank_dtps: f32,
    dps_spread: f32,
    hps_spread: f32,
}

#[derive(BindAll)]
struct NewFightDetailedDeaths {
    fight_iid: i32,
    death_index: i32,
    death_offset_ms: i32,
    death_role: i32,
    killing_ability_id: Option<i32>,
    duration_ms: i32,
}

#[derive(BindAll)]
struct NewFightDetailedSpecs {
    fight_iid: i32,
    class_name: String,
    spec_name: String,
    present_count: i32,
}

const MIN_POINTS_REMAINING: i64 = 4000;

/// Spin as needed to conserve the rate limit for interactive use. Returns the number of fights to import before checking again.
async fn conserve_rate_limit(client: &Client, token: &WclToken, mode: WclMode) -> i64 {
    loop {
        match rate_limit(client, token, mode).await {
            Err(error) => {
                log::error!("unable to retrieve rate limit, sleeping for 5m: {}", error);
                tokio::time::sleep(std::time::Duration::new(300, 0)).await;
            }
            Ok(limit) => {
                let remaining_limit =
                    limit.limit_per_hour - limit.points_spent_this_hour.ceil() as i64;
                if remaining_limit <= MIN_POINTS_REMAINING {
                    log::info!(
                        "conserving rate limit, pausing details import for {} seconds",
                        limit.points_reset_in
                    );
                    tokio::time::sleep(std::time::Duration::new(
                        (limit.points_reset_in + 10) as u64,
                        0,
                    ))
                    .await;
                } else {
                    // this estimate assumes that each fight is 3-5x the point cost that it will actually be
                    let fights_til_recheck = (remaining_limit - MIN_POINTS_REMAINING) / 50;
                    // cap at 200 fights to make sure that if something else starts eating our points, we
                    // don't miss it
                    let fights_til_recheck = fights_til_recheck.min(200);
                    log::info!(
                        "{} available points, checking again after {} fights",
                        remaining_limit,
                        fights_til_recheck
                    );
                    return fights_til_recheck;
                }
            }
        }
    }
}
