use crate::bulk_insert::BulkInsert;
use crate::histogram;
use crate::model::{EncounterStats, Fight, Log, PgExec};
use anyhow::Result;
use chrono::prelude::*;
use chrono::Duration;
use log::debug;
use sqlx::{Connection, Postgres, Transaction};
use std::borrow::Borrow;
use std::collections::HashMap;
use std::convert::TryFrom;

pub mod details;
pub mod raid_time;

/// Collect the **pre-analysis** list of logs for an encounter. This includes logs that may be
/// ignored by later calculations in the pipeline.
pub async fn raw_logs_for_encounter<'a, E: PgExec<'a>>(
    con: E,
    team_id: i32,
    encounter_id: i32,
    difficulty: i32,
) -> Result<Vec<Log>> {
    let data = sqlx::query_as!(
        Log,
        "select distinct on (logs.iid, logs.start_time)
           logs.*
         from fights
         join logs on fights.log = logs.iid
         join teams on teams.iid = logs.team
         join encounters on encounters.encounter_id = fights.encounter_id
         join release_dates as rd on rd.zone = encounters.zone
         where teams.id = $1
           and difficulty = $2
           and fights.encounter_id = $3
           and release_date <= logs.start_time
           and (end_date is null or end_date >= logs.start_time)
         order by logs.start_time asc",
        team_id,
        difficulty,
        encounter_id
    )
    .fetch_all(con)
    .await?;

    Ok(data)
}

// TODO: this should use effective start/end times (aka time of first fight / time of last fight)
// to avoid situations where player B starts logging before player A stops logging. that still
// won't catch partial overlaps, but it'd be better
fn overlapping_logs(a: &Log, b: &Log) -> bool {
    (a.start_time <= b.start_time && b.start_time < a.end_time)
        || (b.start_time <= a.start_time && a.start_time < b.end_time)
}

// Log used in statistics and analysis when two logs overlap. Defaults to the
// log with a kill (if present), or the log with more pulls (if not).
fn log_of_record(
    (fight_a, log_a): (&Vec<Fight>, Log),
    (fight_b, log_b): (&Vec<Fight>, Log),
) -> (Log, Log) {
    let a_kill = fight_a.iter().any(|f| f.kill);
    let b_kill = fight_b.iter().any(|f| f.kill);

    if a_kill && !b_kill {
        (log_a, log_b)
    } else if b_kill && !a_kill {
        (log_b, log_a)
    } else if fight_a.len() >= fight_b.len() {
        (log_a, log_b)
    } else {
        (log_b, log_a)
    }
}

#[derive(Debug, Copy, Clone)]
pub enum Region {
    NA,
    EU,
    CN,
    TW,
    KR,
}

impl TryFrom<String> for Region {
    type Error = anyhow::Error;

    fn try_from(value: String) -> Result<Self> {
        match value.as_str() {
            "US" | "us" => Ok(Region::NA),
            "EU" | "eu" => Ok(Region::EU),
            "CN" | "cn" => Ok(Region::CN),
            "TW" | "tw" => Ok(Region::TW),
            "KR" | "kr" => Ok(Region::KR),
            _ => Err(anyhow::anyhow!("invalid region code {}", value)),
        }
    }
}

// Region offset. Assuming release time is noon, GMT-5.
//
// The release date data is a date at midnight, GMT+0 so there is additional correction here.
fn region_offset(region: Region) -> Duration {
    use Region::*;
    match region {
        NA => Duration::days(0) + Duration::hours(7),
        EU => Duration::days(1) + Duration::hours(4),
        CN => Duration::days(2) + Duration::hours(7),
        KR => Duration::days(2) + Duration::hours(7),
        TW => Duration::days(2) + Duration::hours(7),
    }
}

pub async fn zone_for<'a, E: PgExec<'a>>(con: E, encounter_id: i32) -> Result<i32> {
    let zone = sqlx::query_scalar!(
        "select zone from encounters where encounter_id = $1",
        encounter_id
    )
    .fetch_one(con)
    .await?;

    Ok(zone)
}

pub async fn latest_open_zone<'a, E: PgExec<'a>>(con: E) -> sqlx::Result<Option<i32>> {
    sqlx::query_scalar!("select zone from release_dates where release_date <= now() and (end_date is null or end_date >= now()) limit 1")
        .fetch_optional(con)
        .await
}

pub async fn encounters_for_zone<'a, E: PgExec<'a>>(con: E, zone: i32) -> sqlx::Result<Vec<i32>> {
    sqlx::query_scalar!(
        "select encounter_id from encounters where zone = $1 order by display_order asc",
        zone
    )
    .fetch_all(con)
    .await
}

pub async fn tier_start_for<'a, E: PgExec<'a>>(con: E, encounter_id: i32) -> Result<DateTime<Utc>> {
    let tier_start = sqlx::query_scalar!(
        "select release_date from release_dates join encounters using (zone) where encounter_id = $1",
        encounter_id
    )
        .fetch_one(con)
        .await?;

    Ok(tier_start)
}

pub async fn tier_start<'a, E: PgExec<'a>>(con: E, zone_id: i32) -> Result<DateTime<Utc>> {
    let tier_start = sqlx::query_scalar!(
        "select release_date from release_dates where zone = $1",
        zone_id
    )
    .fetch_one(con)
    .await?;

    Ok(tier_start)
}

const VOTI_ZONE_ID: i32 = 31;
const NP_ZONE_ID: i32 = 38;

fn mythic_offset_for_zone(zone: i32) -> Duration {
    // starting in VotI, there isn't a Heroic week with no Mythic
    if zone >= VOTI_ZONE_ID && zone != NP_ZONE_ID {
        Duration::weeks(0)
    } else {
        Duration::weeks(1)
    }
}

/// Release date should have region correction applied.
pub fn tier_week(
    start_time: DateTime<Utc>,
    release_date: DateTime<Utc>,
    region: Region,
    zone: i32,
    difficulty: i32,
) -> i32 {
    let diff = start_time
        - (release_date
            + region_offset(region)
            + if difficulty == 5 {
                mythic_offset_for_zone(zone)
            } else {
                Duration::weeks(0)
            });

    diff.num_weeks() as i32 + 1
}

pub async fn team_region<'a, E: PgExec<'a>>(con: E, team_id: i32) -> Result<Region> {
    let region: String =
        sqlx::query_scalar!("select region_slug from teams where id = $1", team_id)
            .fetch_one(con)
            .await?;

    Region::try_from(region)
}

#[derive(Copy, Clone, sqlx::Type)]
#[sqlx(type_name = "ignore_reason", rename_all = "lowercase")]
pub enum IgnoreReason {
    Overlap,
}

#[allow(dead_code)]
pub struct ProgData {
    pub logs: Vec<Log>,
    pub ignored_logs: Vec<(IgnoreReason, Log, Log)>,
    pub kill: bool,
    pub fights: HashMap<i32, Vec<Fight>>,
}

/// Get **deduplicated** logs, along relevant fights. This actually includes non-prog logs, despite the name.
/// TODO: fix this to only produce prog logs? or fix the name
pub async fn prog_logs<'a, E: PgExec<'a>>(
    con: E,
    encounter_id: i32,
    difficulty: i32,
    raw_logs: Vec<Log>,
) -> Result<ProgData> {
    let mut logs: Vec<Log> = Vec::with_capacity(raw_logs.len());
    let mut ignored = Vec::new();
    let mut fightmap = HashMap::new();
    let mut kill = false;

    let fight_data = sqlx::query_as!(
        Fight,
        "select * from fights where log = any($1) and encounter_id = $2 and difficulty = $3",
        &raw_logs.iter().map(|l| l.iid).collect::<Vec<i32>>(),
        encounter_id,
        difficulty
    )
    .fetch_all(con)
    .await?;

    for log in raw_logs {
        let code = log.code.clone();
        debug!("processing log {}", &code);

        let f = fight_data
            .iter()
            .filter(|f| f.log == log.iid)
            .cloned()
            .collect::<Vec<_>>();

        // this THROWS AWAY some fights in order to handle logs with multiple
        // kills of the same boss, which will have an impact on post-prog
        // calculations when we eventually reach that point. I will have to
        // revisit this then.
        let log_kill = f.iter().find(|fight| fight.kill).cloned();
        fightmap.insert(
            log.iid,
            f.into_iter()
                .filter(|f| log_kill.as_ref().map(|k| k.id >= f.id).unwrap_or(true))
                .collect(),
        );

        if logs.is_empty() {
            logs.push(log);
        } else {
            let overlap = {
                let prev = logs.last().expect("the log vec to be non-empty");
                overlapping_logs(prev, &log)
            };

            // merging close logs happens on the frontend. it isn't super relevant for the backend
            // analysis, only for charting
            if overlap {
                let prev = logs.pop().unwrap();
                let prev_code = prev.code.clone();
                let (keep, discard) =
                    log_of_record((&fightmap[&prev.iid], prev), (&fightmap[&log.iid], log));

                debug!(
                    "log {} overlaps with {}. using {}. ignoring {}",
                    code, prev_code, &keep.code, &discard.code
                );
                debug!(
                    "keeping: {} {} {}",
                    &keep.code, keep.start_time, keep.end_time
                );
                debug!(
                    "discarding: {} {} {}",
                    &discard.code, discard.start_time, discard.end_time
                );

                fightmap.remove(&discard.iid);
                ignored.push((IgnoreReason::Overlap, keep.clone(), discard));
                logs.push(keep);
            } else {
                logs.push(log);
            }
        }

        if log_kill.is_some() {
            kill = true;
        }
    }

    Ok(ProgData {
        logs,
        kill,
        fights: fightmap,
        ignored_logs: ignored,
    })
}

/// A segment of a log, with associated stats.
#[derive(sqlx::FromRow, Debug, BindAll)]
pub struct LogSegment {
    pub team_id: i32,
    pub log: i32,
    pub encounter_id: i32,
    pub difficulty: i32,
    pub effective_start_time: DateTime<Utc>,
    /// Duration of the log segment in milliseconds
    pub effective_duration: i32,
    pub start_ilvl: f32,
    pub end_ilvl: f32,
    pub has_kill: bool,
    pub post_prog: bool,
    pub pull_count: i32,
}

fn build_log_segment<F: Borrow<Fight>>(
    log: &Log,
    team_id: i32,
    encounter_id: i32,
    difficulty: i32,
    prior_kill: bool,
    fights: &Vec<F>,
) -> LogSegment {
    let effective_duration = fights
        .last()
        .expect("there to be a fight")
        .borrow()
        .end_time
        - fights[0].borrow().start_time;

    LogSegment {
        team_id,
        log: log.iid,
        encounter_id,
        difficulty,
        effective_start_time: log.start_time
            + Duration::milliseconds(fights[0].borrow().start_time as i64),
        effective_duration,
        start_ilvl: fights[0].borrow().avg_ilvl.unwrap_or(0.0),
        end_ilvl: fights.last().unwrap().borrow().avg_ilvl.unwrap_or(0.0),
        has_kill: fights.iter().any(|f| f.borrow().kill),
        post_prog: prior_kill,
        pull_count: fights.len() as i32,
    }
}

// Calculate the progression time for a log, taking into account the presence of multi-day logs.
fn prog_time_for(
    log: &Log,
    team_id: i32,
    encounter_id: i32,
    difficulty: i32,
    prior_kill: bool,
    fights: &Vec<Fight>,
) -> Vec<LogSegment> {
    // no fights, no prog
    if fights.is_empty() {
        return vec![];
    }

    let basic = fights.last().expect("there to be a fight").end_time - fights[0].start_time;

    if Duration::milliseconds(basic as i64) <= Duration::hours(8) {
        // fast path. regular-ass single-day log
        vec![build_log_segment(
            log,
            team_id,
            encounter_id,
            difficulty,
            prior_kill,
            fights,
        )]
    } else {
        debug!(
            "encountered a log that is {}ms long. assuming multi-day",
            basic
        );
        let mut prev = &fights[0];

        let mut current_fights = vec![&fights[0]];
        let mut segments = vec![];

        for fight in &fights[1..] {
            let start_delta = Duration::milliseconds((fight.start_time - prev.start_time) as i64);
            if start_delta >= Duration::hours(8) {
                // split here. ignore gap between current start time and previous
                debug!(
                    "splitting log after fight {} (gap: {})",
                    prev.id, start_delta
                );
                segments.push(build_log_segment(
                    log,
                    team_id,
                    encounter_id,
                    difficulty,
                    prior_kill,
                    &current_fights,
                ));
                current_fights.clear();
            }

            current_fights.push(fight);
            prev = fight;
        }

        segments.push(build_log_segment(
            log,
            team_id,
            encounter_id,
            difficulty,
            prior_kill,
            &current_fights,
        ));

        segments
    }
}

#[derive(BindAll)]
struct RawIgnoredLog {
    log: i32,
    difficulty: i32,
    encounter_id: i32,
    reason: IgnoreReason,
    relevant_log: i32,
}

/// Add / update the ignore data for a log set.
async fn update_ignored_logs(
    con: &mut Transaction<'_, Postgres>,
    encounter_id: i32,
    difficulty: i32,
    unignored: &Vec<Log>,
    ignored: &Vec<(IgnoreReason, Log, Log)>,
) -> Result<()> {
    let mut dbtx = con.begin().await?;

    sqlx::query!(
        "delete from ignored_logs where log = any($1)",
        &unignored.iter().map(|l| l.iid).collect::<Vec<_>>()
    )
    .execute(&mut *dbtx)
    .await?;

    let logs = ignored
        .iter()
        .map(|(reason, rel, log)| RawIgnoredLog {
            log: log.iid,
            difficulty,
            encounter_id,
            reason: *reason,
            relevant_log: rel.iid,
        })
        .collect::<Vec<_>>();

    BulkInsert::new("ignored_logs")
        .on_conflict(
            vec!["log", "difficulty", "encounter_id"],
            vec!["reason", "relevant_log"],
        )
        .add_rows(logs)
        .execute(&mut *dbtx)
        .await?;

    dbtx.commit().await?;
    Ok(())
}

pub async fn logs_for_encounter<'a, E: PgExec<'a>>(
    con: E,
    team_id: i32,
    encounter_id: i32,
    difficulty: i32,
    include_farm: bool,
) -> Result<Vec<Log>> {
    let data = sqlx::query_as_unchecked!(
        Log,
        "select les.id as iid,
                logs.code,
                logs.team,
                les.effective_start_time as start_time,
                (les.effective_start_time + effective_duration * interval '1 millisecond') as end_time,
                logs.title,
                logs.zone_id
         from logs join log_encounter_stats as les on les.log = logs.iid
         where les.team_id = $1 and les.encounter_id = $2 and les.difficulty = $3 and (not les.post_prog or $4)",
        team_id, encounter_id, difficulty, include_farm
    )
        .fetch_all(con)
        .await?;
    Ok(data)
}

async fn valid_encounter<'a, E: PgExec<'a>>(con: E, encounter_id: i32) -> Result<bool> {
    let res = sqlx::query_scalar!(
        "select count(*) from encounters where encounter_id = $1",
        encounter_id
    )
    .fetch_one(con)
    .await?;

    Ok(res.unwrap_or(0) > 0)
}

pub async fn fetch_encounter_stats<'a, E: PgExec<'a>>(
    con: E,
    team_id: i32,
    encounter_id: i32,
    difficulty: i32,
) -> Result<Option<EncounterStats>> {
    let raw = sqlx::query!(
        "
        select region_slug,
               encounters.zone as zone,
               rd.release_date,
               last_value(has_kill) over w as \"has_kill!\",
               last_value(log) over w as \"log!\",
               last_value(effective_start_time) over w as \"last_start_time!\",
               last_value(effective_duration) over w as \"last_duration!\",
               first_value(start_ilvl) over w as \"ilvl_min!\",
               last_value(end_ilvl) over w as \"ilvl_max!\",
               sum(pull_count) over w as \"pull_count!\",
               sum(effective_duration) over w as \"prog_time!\"
         from log_encounter_stats les
         join encounters on les.encounter_id = encounters.encounter_id
         join teams on teams.id = team_id
         join release_dates rd on rd.zone = encounters.zone
         where team_id = $1 and les.encounter_id = $2 and difficulty = $3 and not post_prog
         window w as (order by effective_start_time asc)
",
        team_id,
        encounter_id,
        difficulty
    )
    .fetch_all(con)
    .await?;

    if let Some(raw) = raw.into_iter().last() {
        let region = Region::try_from(raw.region_slug)?;
        let (kill_date, kill_week) = if raw.has_kill {
            let kill_date = raw.last_start_time + Duration::milliseconds(raw.last_duration as i64);
            (
                Some(kill_date),
                Some(tier_week(
                    kill_date,
                    raw.release_date,
                    region,
                    raw.zone,
                    difficulty,
                )),
            )
        } else {
            (None, None)
        };

        Ok(Some(EncounterStats {
            kill_log: if raw.has_kill { Some(raw.log) } else { None },
            kill_week,
            kill_date,
            team: team_id,
            encounter_id,
            difficulty,
            ilvl_min: raw.ilvl_min,
            ilvl_max: raw.ilvl_max,
            pull_count: raw.pull_count as i32,
            prog_time: raw.prog_time as i32,
        }))
    } else {
        Ok(None)
    }
}

// Generate the analysis entries for a team on the given encounter and difficulty. This overwites
// any existing analysis for that 3-tuple.
pub async fn generate_encounter_analysis(
    con: &mut Transaction<'_, Postgres>,
    team_id: i32,
    encounter_id: i32,
    difficulty: i32,
) -> Result<()> {
    let mut dbtx = con.begin().await?;
    if !valid_encounter(&mut *dbtx, encounter_id).await? {
        debug!("skipping encounter {}", encounter_id);
        return Ok(());
    }

    let old_data = fetch_encounter_stats(&mut *dbtx, team_id, encounter_id, difficulty).await?;

    if let Some(data) = old_data {
        histogram::decrement(&mut *dbtx, data).await?;
    }

    // clear old stats for this encounter
    sqlx::query!(
        "delete from log_encounter_stats where team_id = $1 and encounter_id = $2 and difficulty = $3",
        team_id, encounter_id, difficulty
    ).execute(&mut *dbtx).await?;

    let raw_logs = raw_logs_for_encounter(&mut *dbtx, team_id, encounter_id, difficulty).await?;

    debug!(
        "generating analysis for {} {} {}",
        team_id, encounter_id, difficulty
    );

    let ProgData {
        logs,
        fights: fightmap,
        ignored_logs,
        ..
    } = prog_logs(&mut *dbtx, encounter_id, difficulty, raw_logs).await?;

    if logs.is_empty() {
        debug!("no logs for {} {} {}", team_id, encounter_id, difficulty);
        return Ok(());
    }

    update_ignored_logs(&mut dbtx, encounter_id, difficulty, &logs, &ignored_logs).await?;

    let mut kill = false;
    let mut results = vec![];
    for log in &logs {
        let fights = &fightmap[&log.iid];
        let res = prog_time_for(&log, team_id, encounter_id, difficulty, kill, fights);
        kill = kill || fights.iter().any(|f| f.kill);
        results.extend(res);
    }

    let bulk = BulkInsert::new("log_encounter_stats");
    bulk.add_rows(results).execute(&mut *dbtx).await?;

    // fixme this really shouldn't do another round trip but re-using the logic is SO MUCH easier
    let new_data = fetch_encounter_stats(&mut *dbtx, team_id, encounter_id, difficulty).await?;
    if let Some(data) = new_data {
        histogram::increment(&mut *dbtx, data).await?;
    }

    debug!(
        "calculated stats for {} {} {}",
        team_id, encounter_id, difficulty
    );

    dbtx.commit().await?;

    Ok(())
}

pub async fn generate_analysis(
    con: &mut Transaction<'_, Postgres>,
    team_id: i32,
    zone: i32,
) -> Result<()> {
    let mut dbtx = con.begin().await?;
    let encounters = sqlx::query!(
        "select distinct encounter_id, difficulty from fights
         join logs on fights.log = logs.iid
         join teams on teams.iid = logs.team
         join encounters using (encounter_id)
         where teams.id = $1 and zone = $2",
        team_id,
        zone
    )
    .fetch_all(&mut *dbtx)
    .await?;

    for e in encounters {
        if let Some(difficulty) = e.difficulty {
            generate_encounter_analysis(&mut dbtx, team_id, e.encounter_id, difficulty).await?;
        }
    }

    dbtx.commit().await?;

    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_na_tier_week_monday() {
        let release = Utc.ymd(2020, 12, 7).and_hms(12, 0, 0);
        let date = Utc.ymd(2020, 12, 22).and_hms(4, 59, 59);
        let zone = 0;
        assert_eq!(tier_week(date, release, Region::NA, zone, 5), 1);
        let date = Utc.ymd(2021, 1, 18).and_hms(4, 59, 59);
        assert_eq!(tier_week(date, release, Region::NA, zone, 5), 5);

        let date = Utc.ymd(2021, 1, 4).and_hms(1, 59, 8);
        assert_eq!(tier_week(date, release, Region::NA, zone, 5), 3);
    }
}
