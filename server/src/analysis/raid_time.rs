use crate::model::PgExec;
use anyhow::Result;
use async_graphql::*;
use chrono::prelude::*;
use chrono::{DateTime, Duration, Utc};
use std::collections::BTreeMap;

const ROUND_INTERVAL: u32 = 15;

pub trait RoundTo {
    fn round_to(&self, minutes: u32) -> Self;
}

fn round_offset(min: u32, interval: u32) -> Duration {
    if min % interval >= interval / 2 {
        Duration::minutes((interval - min % interval) as i64)
    } else {
        -Duration::minutes((min % interval) as i64)
    }
}

impl RoundTo for DateTime<Utc> {
    fn round_to(&self, minutes: u32) -> Self {
        let min = self.minute();
        let dt = Utc
            .ymd(self.year(), self.month(), self.day())
            .and_hms(self.hour(), min, 0);
        dt + round_offset(min, minutes)
    }
}

impl RoundTo for Duration {
    fn round_to(&self, minutes: u32) -> Self {
        let min = self.num_minutes();
        Duration::minutes(min) + round_offset(min as u32, minutes)
    }
}

pub struct TimeBreakdown {
    pub week: i32,
    pub prog: Duration,
    pub farm: Duration,
}

impl TimeBreakdown {
    pub fn new(week: i32) -> Self {
        Self {
            week,
            prog: Duration::zero(),
            farm: Duration::zero(),
        }
    }
}

#[Object]
impl TimeBreakdown {
    pub async fn week(&self) -> i32 {
        self.week
    }

    pub async fn prog(&self) -> i64 {
        self.prog.num_milliseconds()
    }

    pub async fn farm(&self) -> i64 {
        self.farm.num_milliseconds()
    }
}

#[derive(SimpleObject)]
pub struct TimeStats {
    pub lower: i64,
    pub upper: i64,
    pub mean: i64,
    pub breakdowns: Vec<TimeBreakdown>,
}

#[derive(sqlx::FromRow)]
struct TimeData {
    pub effective_start_time: DateTime<Utc>,
    pub effective_duration: i32,
    pub post_prog: bool,
}

pub async fn weekly_time<'a, E: PgExec<'a> + Copy>(
    con: E,
    team_id: i32,
    zone: i32,
    difficulty: i32,
) -> Result<Option<TimeStats>> {
    let data = sqlx::query_as!(
        TimeData,
        "select les.effective_start_time, les.effective_duration, les.post_prog from log_encounter_stats as les join encounters using (encounter_id)
         where les.team_id = $1 and difficulty = $2 and zone = $3 order by les.effective_start_time asc",
        team_id,
        difficulty,
        zone
    )
    .fetch_all(con)
    .await?;

    if data.is_empty() {
        return Ok(None);
    }

    let tier_start = super::tier_start(con, zone).await?;
    let region = super::team_region(con, team_id).await?;

    let mut weeks = BTreeMap::new();

    let count = data.len();

    let data = data
        .into_iter()
        .fold(Vec::<TimeData>::with_capacity(count), |mut res, datum| {
            if let Some(prev) = res.last_mut() {
                if datum.effective_start_time - prev.effective_start_time
                    + Duration::milliseconds(prev.effective_duration as i64)
                    <= Duration::hours(1)
                {
                    // merge
                    prev.effective_duration = (datum.effective_start_time
                        - prev.effective_start_time)
                        .num_milliseconds() as i32
                        + datum.effective_duration;
                    return res;
                }
            }

            res.push(datum);
            res
        });

    for log in data {
        let week = super::tier_week(log.effective_start_time, tier_start, region, zone, difficulty);
        let entry = weeks
            .entry(week)
            .or_insert_with(|| TimeBreakdown::new(week));

        log::debug!(
            "adding time for team {} in week {} (farm? {}): {}",
            team_id,
            week,
            log.post_prog,
            log.effective_duration
        );

        let dur = Duration::milliseconds(log.effective_duration as i64);
        if log.post_prog {
            entry.farm = entry.farm + dur;
        } else {
            entry.prog = entry.prog + dur;
        }
    }

    let breakdowns = weeks
        .into_iter()
        .map(|(_, v)| TimeBreakdown {
            week: v.week,
            prog: v.prog.round_to(ROUND_INTERVAL),
            farm: v.farm.round_to(ROUND_INTERVAL),
        })
        .collect::<Vec<TimeBreakdown>>();

    let mut totals = breakdowns
        .iter()
        .filter(|&TimeBreakdown { prog, .. }| *prog > Duration::zero())
        .map(|TimeBreakdown { prog, farm, .. }| prog.num_milliseconds() + farm.num_milliseconds())
        .collect::<Vec<_>>();

    if totals.is_empty() {
        return Ok(None);
    }

    totals.sort_unstable();

    // not exact quartiles but close enough
    let lower_ix = totals.len() / 2;
    let upper_ix = totals.len() * 9 / 10;
    let mean = totals.iter().cloned().sum::<i64>() / totals.len() as i64;
    let lower = totals[lower_ix];
    let upper = totals[upper_ix];

    log::debug!(
        "quartiles: {} ({}) {} ({})",
        lower,
        lower_ix,
        upper,
        upper_ix
    );

    Ok(Some(TimeStats {
        lower,
        upper,
        mean,
        breakdowns,
    }))
}
