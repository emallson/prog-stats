use crate::{
    histogram::{Histogram, HistogramScale},
    model::PgExec,
    queries::ability_name::{ability_name, prefetch_ability_names},
    WclToken,
};
use async_graphql::SimpleObject;
use chrono::{DateTime, Utc};
use futures::TryFutureExt;
use moka::future::Cache;
use reqwest::Client;
use sqlx::PgPool;
use std::{
    collections::{BTreeMap, BTreeSet, VecDeque},
    iter::FromIterator,
    sync::{Arc, LazyLock},
};
use tokio::sync::Semaphore;

static STATS_CACHE: LazyLock<Cache<i32, (chrono::DateTime<Utc>, Vec<FightDetailedStats>)>> =
    LazyLock::new(|| {
        Cache::builder()
            .max_capacity(1000)
            .time_to_live(std::time::Duration::from_days(1))
            .build()
    });

static DEATHS_CACHE: LazyLock<Cache<i32, (chrono::DateTime<Utc>, Vec<DeathDetails>)>> =
    LazyLock::new(|| {
        Cache::builder()
            .max_capacity(1000)
            .time_to_live(std::time::Duration::from_days(1))
            .build()
    });

pub async fn detailed_stats(
    con: PgPool,
    encounter_id: i32,
    pull_group_size: i32,
    max_pull_count: Option<i32>,
    limiter: Arc<Semaphore>,
) -> anyhow::Result<Vec<FightDetailedStats>> {
    let future = async move {
        let _permit = limiter.acquire().await;

        if let Some((revalidate_time, value)) = STATS_CACHE.get(&encounter_id).await {
            if revalidate_time > Utc::now() {
                return Ok(value);
            }
        }

        let data = sqlx::query!(
            r#"
        select
            logs.team,
            logs.start_time + (fights.start_time * '1 millisecond'::interval) as fight_time,
            fights.encounter_id,
            fights.kill,
            st.*
        from fight_detailed_stats st
        join fights on st.fight_iid = fights.iid
        join logs on fights.log = logs.iid
        where fights.encounter_id = $1 and fights.difficulty = 5
        order by
            logs.start_time asc,
            fights.start_time asc
    "#,
            encounter_id
        )
        .fetch_all(&con)
        .await?;

        let labels = phase_labels(&con, encounter_id).await?;

        let mut counter = PullCounter::new();
        let mut histogram_counts = BTreeMap::<(StatType, i32, i32), BTreeMap<i32, i32>>::new();

        let mut increment =
            |stat_type: StatType, phase_index: i32, pull_number: i32, value: f32| {
                *histogram_counts
                    .entry((stat_type, pull_number, phase_index))
                    .or_insert_with(BTreeMap::default)
                    .entry(stat_type.scale().bin_index(value as f64))
                    .or_insert(0) += 1;
            };

        let mut max_phase_index = -1;

        for record in data {
            use StatType::*;

            if blacklist_phase(record.encounter_id, record.phase_index) {
                continue;
            }

            let fight_time = record.fight_time.unwrap();
            let pull_number = counter.pull_index(
                record.team,
                record.encounter_id,
                record.fight_iid.unwrap(),
                fight_time,
            )?;

            if let Some(max) = max_pull_count {
                if pull_number > max {
                    continue; // exclude very high pull counts to keep the chart from bricking
                }
            }

            let pull_number = pull_number / pull_group_size;

            max_phase_index = max_phase_index.max(record.phase_index);

            increment(Dps, record.phase_index, pull_number, record.overall_dps);
            increment(Hps, record.phase_index, pull_number, record.overall_hps);
            increment(Dtps, record.phase_index, pull_number, record.nontank_dtps);
            increment(
                DpsSpread,
                record.phase_index,
                pull_number,
                record.dps_spread,
            );
            increment(
                HpsSpread,
                record.phase_index,
                pull_number,
                record.hps_spread,
            );
            increment(
                Duration,
                record.phase_index,
                pull_number,
                record.duration_ms as f32,
            );
        }

        let only_overall = max_phase_index == 0;

        let result = histogram_counts
            .into_iter()
            .map(|((stat_type, pull_number_group, raw_phase_index), bins)| {
                let histogram = Histogram::from_bins(stat_type.scale(), bins);

                let pct_step = 1.0 / (STEP_COUNT + 2) as f64;

                FightDetailedStats {
                    stat_type,
                    pull_number: pull_number_group * pull_group_size,
                    phase_index: if raw_phase_index < 0 {
                        None
                    } else {
                        Some(raw_phase_index)
                    },
                    phase_label: if raw_phase_index < 0 {
                        None
                    } else {
                        Some(match labels.get(&raw_phase_index) {
                            Some(label) => label.clone(),
                            None => format!("Phase {}", raw_phase_index),
                        })
                    },
                    step_count: STEP_COUNT + 2,
                    total: histogram.total_count(),
                    points: (0..=STEP_COUNT)
                        .filter_map(|i| histogram.quantile(pct_step + i as f64 * pct_step))
                        .collect(),
                }
            })
            .filter(|stats| {
                stats.total >= STEP_COUNT
                    && if only_overall {
                        stats.phase_index.is_none()
                    } else {
                        true
                    }
            })
            .collect();

        Ok(result)
    };

    if let Some((revalidate_time, value)) = STATS_CACHE.get(&encounter_id).await {
        if Utc::now() <= revalidate_time {
            tokio::spawn(
                future.and_then(move |value: Vec<FightDetailedStats>| async move {
                    STATS_CACHE
                        .insert(encounter_id, (revalidation_time(), value.clone()))
                        .await;
                    Ok(())
                }),
            );
        }

        Ok(value)
    } else {
        let result: anyhow::Result<Vec<FightDetailedStats>> = future.await;
        let value = result?;

        STATS_CACHE
            .insert(encounter_id, (revalidation_time(), value.clone()))
            .await;

        Ok(value)
    }
}

fn revalidation_time() -> DateTime<Utc> {
    Utc::now() + chrono::Duration::hours(2)
}

pub async fn detailed_deaths(
    client: Client,
    token: WclToken,
    con: PgPool,
    encounter_id: i32,
    pull_group_size: i32,
    max_pull_count: Option<i32>,
    limiter: Arc<Semaphore>,
) -> anyhow::Result<Vec<DeathDetails>> {
    let future = async move {
        let _permit = limiter.acquire().await;

        // after acquiring the permit, double check the cache to see if someone else raced us.
        // this can refresh the revalidation time but since the response is basically instant it shouldn't get
        // to refresh it for too long
        if let Some((revalidate_time, value)) = DEATHS_CACHE.get(&encounter_id).await {
            if revalidate_time > Utc::now() {
                return Ok(value);
            }
        }

        let data = sqlx::query!(
            r#"
        select
            logs.team,
            logs.start_time + (fights.start_time * '1 millisecond'::interval) as fight_time,
            fights.encounter_id,
            fights.kill,
            d.*
        from fight_detailed_deaths d
        join fights on d.fight_iid = fights.iid
        join logs on fights.log = logs.iid
        where encounter_id = $1 and difficulty = 5
        order by
            logs.start_time asc,
            fights.start_time asc
    "#,
            encounter_id
        )
        .fetch_all(&con)
        .await?;

        let mut counter = PullCounter::new();
        let mut map = BTreeMap::<(Option<String>, i32), Vec<i32>>::new();

        tokio::spawn(prefetch_ability_names(
            client.clone(),
            token.clone(),
            crate::WclMode::Public,
            data.iter()
                .filter_map(|record| record.killing_ability_id)
                .collect::<BTreeSet<_>>(),
        ));

        for record in data {
            let fight_time = record.fight_time.unwrap();
            let pull_number = counter.pull_index(
                record.team,
                record.encounter_id,
                record.fight_iid.unwrap(),
                fight_time,
            )?;

            if let Some(max) = max_pull_count {
                if pull_number > max {
                    continue; // exclude very high pull counts to keep the chart from bricking
                }
            }

            let pull_number = pull_number / pull_group_size;
            let name = if let Some(id) = record.killing_ability_id {
                Some(
                    match ability_name(client.clone(), token.clone(), crate::WclMode::Public, id)
                        .await
                    {
                        Ok(Some(name)) => name,
                        Ok(None) => format!("{}", id),
                        Err(error) => {
                            log::error!("unable to retrieve ability name from WCL: {}", error);
                            format!("{}", id)
                        }
                    },
                )
            } else {
                None
            };

            map.entry((name, pull_number * pull_group_size + 1))
                .or_insert_with(Vec::new)
                .push(record.death_offset_ms);
        }

        let mut result = vec![];

        // we are doing the simplest span implementation for now because there are at most 5 points
        // per pull
        for ((ability_name, pull_number), mut times) in map {
            times.sort();

            let mut spans = vec![];
            let mut current_span = VecDeque::new();
            for time in times {
                if current_span.is_empty() {
                    current_span.push_back(time);
                } else {
                    let last = *current_span.back().unwrap();
                    if time - last >= DEATH_GAP_WIDTH {
                        // easy case. put the current span into spans and start a new one
                        spans.push(current_span);
                        current_span = VecDeque::new();
                        current_span.push_back(time);
                    } else {
                        current_span.push_back(time);
                    }
                }
            }

            if !current_span.is_empty() {
                spans.push(current_span);
            }

            for details in
                DeathDetails::from_spans(pull_number, pull_group_size, ability_name, spans)
            {
                let cutoff: f32 =
                    MIN_DEATHS - details.time_ms as f32 / 1000.0 / 60.0 * DEATH_STEPDOWN_PER_MIN;
                if details.total as f32 >= cutoff.max(MIN_DEATHS_ABS) {
                    result.push(details);
                }
            }
        }

        Ok(result)
    };

    if let Some((revalidate_time, value)) = DEATHS_CACHE.get(&encounter_id).await {
        if Utc::now() <= revalidate_time {
            tokio::spawn(future.and_then(move |value: Vec<DeathDetails>| async move {
                DEATHS_CACHE
                    .insert(encounter_id, (revalidation_time(), value.clone()))
                    .await;
                Ok(())
            }));
        }

        Ok(value)
    } else {
        let result: anyhow::Result<Vec<DeathDetails>> = future.await;
        let value = result?;

        DEATHS_CACHE
            .insert(encounter_id, (revalidation_time(), value.clone()))
            .await;

        Ok(value)
    }
}

const MIN_DEATHS: f32 = 20.0;
const DEATH_STEPDOWN_PER_MIN: f32 = 15.0 / 8.0;
const MIN_DEATHS_ABS: f32 = 5.0;
const STEP_COUNT: i32 = 10;
const DEATH_GAP_WIDTH: i32 = 8000;
const DEATH_TARGET_SPAN_WIDTH: i32 = 15000;
const DEATH_SPAN_WIDTH_SLOP: i32 = 3000;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Copy, Clone, async_graphql::Enum)]
enum StatType {
    Dps,
    Hps,
    Dtps,
    Duration,
    DpsSpread,
    HpsSpread,
}

impl StatType {
    fn scale(&self) -> HistogramScale {
        HistogramScale::with_percentile_error(0.01)
    }
}

#[derive(Debug, SimpleObject, Clone)]
pub struct FightDetailedStats {
    stat_type: StatType,
    pull_number: i32,
    /// The phase index, or null if overall.
    phase_index: Option<i32>,
    phase_label: Option<String>,
    /// The number of steps from 0.0 to 1.0
    step_count: i32,
    /// The total number of data points summarized here.
    total: i32,
    /// point values for each step, with the first (0.0) and last (1.0) dropped
    points: Vec<f64>,
}

#[derive(Debug, SimpleObject, Clone)]
pub struct DeathDetails {
    pull_number: i32,
    group_size: i32,
    total: i32,
    time_ms: i32,
    /// Total width of the span, in ms.
    span_ms: i32,
    ability_name: Option<String>,
}

impl DeathDetails {
    pub fn from_spans(
        pull_number: i32,
        pull_group_size: i32,
        ability_name: Option<String>,
        spans: Vec<VecDeque<i32>>,
    ) -> Vec<Self> {
        // we know that each span contains only deaths within `DEATH_GAP_WIDTH` of each other in a continuous chain.
        // now we want to split each span into spans that are around `DEATH_SPAN_TARGET_WIDTH` long
        let mut result = vec![];
        for mut span in spans {
            if span.len() == 0 {
                continue; // this should not happen but I'm not hard erroring at this stage.
            }
            let last = *span.back().unwrap();
            let first = *span.front().unwrap();
            let total_width = last - first;
            if total_width <= DEATH_TARGET_SPAN_WIDTH + DEATH_SPAN_WIDTH_SLOP {
                let time_ms = total_width / 2 + first;
                result.push(DeathDetails {
                    pull_number,
                    group_size: pull_group_size,
                    ability_name: ability_name.clone(),
                    time_ms,
                    span_ms: total_width,
                    total: span.len() as i32,
                });
            } else {
                // we need to split.
                // TODO: better split method. this ended up being the simple method, written complicatedly
                let mut total = 0;
                let mut cutoff = first + DEATH_TARGET_SPAN_WIDTH;
                let mut first = None;
                let mut last = None;
                while let Some(time) = span.pop_front() {
                    if time <= cutoff {
                        total += 1;
                        if first == None {
                            first = Some(time);
                        }
                        last = Some(time);
                    } else {
                        if total > 0 {
                            let width = last.unwrap() - first.unwrap();

                            result.push(DeathDetails {
                                pull_number,
                                group_size: pull_group_size,
                                ability_name: ability_name.clone(),
                                time_ms: first.unwrap() + width / 2,
                                total,
                                span_ms: width,
                            });
                        }
                        cutoff += DEATH_TARGET_SPAN_WIDTH;
                        total = 0;
                        first = None;
                        last = None;
                    }
                }

                if total > 0 {
                    let width = last.unwrap() - first.unwrap();

                    result.push(DeathDetails {
                        pull_number,
                        group_size: pull_group_size,
                        ability_name: ability_name.clone(),
                        time_ms: first.unwrap() + width / 2,
                        total,
                        span_ms: width,
                    });
                }
            }
        }

        result
    }
}

#[derive(Debug)]
struct PullCounter {
    lookup: BTreeMap<(i32, i32), PullCounterInner>,
}

#[derive(Debug)]
struct PullCounterInner {
    last_seen: Option<DateTime<Utc>>,
    pull_indices: BTreeMap<i32, i32>,
}

impl PullCounter {
    pub fn new() -> Self {
        PullCounter {
            lookup: BTreeMap::new(),
        }
    }

    /// Get the 1-based pull index for a fight. This assumes that we see fights in increasing order, and
    /// returns an error if we do not.
    pub fn pull_index(
        &mut self,
        team_iid: i32,
        encounter_id: i32,
        fight_iid: i32,
        fight_time: DateTime<Utc>,
    ) -> std::result::Result<i32, PullCounterError> {
        let entry = self
            .lookup
            .entry((team_iid, encounter_id))
            .or_insert_with(|| PullCounterInner {
                last_seen: None,
                pull_indices: BTreeMap::new(),
            });

        if let Some(ref last_seen) = entry.last_seen {
            if *last_seen >= fight_time {
                return Err(PullCounterError::OutOfOrder {
                    old: last_seen.to_owned(),
                    new: fight_time,
                });
            }
        }

        if let Some(index) = entry.pull_indices.get(&fight_iid) {
            Ok(*index)
        } else {
            let index = entry.pull_indices.len() as i32 + 1;
            entry.pull_indices.insert(fight_iid, index);
            Ok(index)
        }
    }
}

#[derive(thiserror::Error, Debug)]
enum PullCounterError {
    #[error("out of order fight observed (last seen: {old}, this fight: {new})")]
    OutOfOrder {
        old: DateTime<Utc>,
        new: DateTime<Utc>,
    },
}

async fn phase_labels<'a, E: PgExec<'a>>(
    con: E,
    encounter_id: i32,
) -> anyhow::Result<BTreeMap<i32, String>> {
    let rows = sqlx::query!(
        r#"
        select phase_index, label from encounter_phase_labels
        where encounter_id = $1
    "#,
        encounter_id
    )
    .fetch_all(con)
    .await?;

    Ok(BTreeMap::from_iter(
        rows.into_iter()
            .map(|record| (record.phase_index, record.label)),
    ))
}

fn blacklist_phase(encounter_id: i32, phase_index: i32) -> bool {
    match (encounter_id, phase_index) {
        (2920, p) if p >= 6 => true,
        _ => false,
    }
}
