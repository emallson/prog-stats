use crate::analysis::raid_nights::LogTime;
use anyhow::Result;
use chrono::serde::ts_milliseconds;
use chrono::{DateTime, Utc};
use serde_derive::Deserialize;
use std::fs::File;
use std::path::PathBuf;

#[derive(Debug, Deserialize)]
struct LocalLogTime {
    code: String,
    #[serde(with = "ts_milliseconds")]
    effective_start_time: DateTime<Utc>,
    effective_duration: i32,
}

impl Into<LogTime> for LocalLogTime {
    fn into(self) -> LogTime {
        LogTime {
            code: self.code,
            start_time: self.effective_start_time,
            duration: self.effective_duration,
        }
    }
}

pub(crate) fn load_raid_nights(key: &str) -> Result<Vec<LogTime>> {
    let mut path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    path.push(format!("test-resources/raid-nights/{}.json", key));

    let data: Vec<LocalLogTime> = serde_json::from_reader(File::open(path)?)?;

    Ok(data.into_iter().map(|t| t.into()).collect())
}
