use std::collections::HashMap;

use anyhow::anyhow;
use graphql_client::{GraphQLQuery, Response};
use reqwest::Client;

use crate::{WclMode, WclToken};

use super::reports::fights;

#[derive(serde::Deserialize, Debug)]
struct TableData<Entry> {
    data: Data<Entry>,
}

#[derive(serde::Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct Data<Entry> {
    entries: Vec<Entry>,
    #[serde(default)]
    total_time: Option<usize>,
}

mod overall {
    use super::*;

    #[derive(serde::Deserialize, Debug)]
    pub struct Entry {
        pub(super) name: String,
        pub(super) total: isize, // negative damage/healing is possible due to redistribution effects
    }

    type JSON = TableData<Entry>;

    #[derive(GraphQLQuery)]
    #[graphql(
        schema_path = "schema.json",
        query_path = "queries/fight_details.graphql",
        response_derives = "Debug"
    )]
    pub struct FightDetailedData;
}

mod deaths {
    use super::*;

    #[derive(serde::Deserialize, Debug)]
    #[serde(untagged)]
    pub(super) enum JSON {
        PlayerDetails(PlayerDetailsData),
        Deaths(TableData<DeathEntry>),
    }

    // this is an artifact of how graphql_client works intersecting with how the WCL API encodes its types.
    // "JSON" is a "scalar" which is returned from multiple fields, representing opaque json-encoded data.
    // we know at every field what the type is, but graphql_client doesn't give an easy way to handle that.
    // so we do it with an enum and unwraps.
    impl JSON {
        pub fn unwrap_deaths(self) -> TableData<DeathEntry> {
            match self {
                JSON::Deaths(value) => value,
                _ => panic!("unable to convert player details data into deaths data"),
            }
        }

        pub fn unwrap_player_details(self) -> PlayerDetails {
            match self {
                JSON::PlayerDetails(value) => value.data.player_details,
                _ => panic!("unable to convert deaths data into player details data"),
            }
        }
    }

    #[derive(serde::Deserialize, Debug)]
    #[serde(rename_all = "camelCase")]
    pub(super) struct DeathEntry {
        pub name: String,
        pub guid: usize,
        pub timestamp: isize,
        pub death_window: isize,
        #[serde(default)]
        pub killing_blow: Option<KillingBlow>,
    }

    #[derive(serde::Deserialize, Debug)]
    pub(super) struct KillingBlow {
        pub guid: isize,
    }

    #[derive(serde::Deserialize, Debug)]
    #[serde(deny_unknown_fields)]
    pub(super) struct PlayerDetails {
        #[serde(default)]
        pub tanks: Vec<Player>,
        #[serde(default)]
        pub dps: Vec<Player>,
        #[serde(default)]
        pub healers: Vec<Player>,
    }

    #[derive(serde::Deserialize, Debug)]
    pub(super) struct PlayerDetailsData {
        data: PlayerDetailsDataInner,
    }

    #[derive(serde::Deserialize, Debug)]
    pub(super) struct PlayerDetailsDataInner {
        #[serde(rename = "playerDetails")]
        player_details: PlayerDetails,
    }

    impl PlayerDetails {
        pub fn players<'a>(&'a self) -> impl Iterator<Item = (Role, &'a Player)> + 'a {
            let tanks = self.tanks.iter().map(|tank| (Role::Tank, tank));
            let healers = self.healers.iter().map(|healer| (Role::Healer, healer));
            let dps = self.dps.iter().map(|dps| (Role::DPS, dps));

            tanks.chain(healers).chain(dps)
        }
    }

    #[derive(serde::Deserialize, Debug)]
    pub(super) struct Player {
        pub guid: usize,
        pub name: String,
        #[serde(rename = "type")]
        pub class: String,
        pub specs: Vec<Spec>,
    }

    #[derive(serde::Deserialize, Debug)]
    pub(super) struct Spec {
        pub spec: String,
    }

    #[derive(GraphQLQuery)]
    #[graphql(
        schema_path = "schema.json",
        query_path = "queries/fight_details.graphql",
        response_derives = "Debug"
    )]
    pub struct FightDetailedDeathsAndRoles;

    #[cfg(test)]
    mod test {
        use super::*;

        #[test]
        fn test_deserialize_response() {
            let result = serde_json::from_str::<
                Response<fight_detailed_deaths_and_roles::ResponseData>,
            >(include_str!("../../test-resources/deaths-sample.json"));

            assert!(result.is_ok());
        }
    }
}

#[derive(Debug, PartialEq, Eq, Hash)]
pub struct Spec {
    pub class_name: String,
    pub spec_name: String,
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Role {
    Tank,
    Healer,
    DPS,
}

impl Role {
    pub fn id(&self) -> i32 {
        match self {
            Role::Tank => 1,
            Role::Healer => 2,
            Role::DPS => 3,
        }
    }
}

#[derive(Debug)]
pub struct OverallData {
    pub duration_ms: usize,
    pub phase_index: Option<i32>,
    pub overall_dps: f32,
    pub overall_hps: f32,
    pub nontank_dtps: f32,
    pub dps_spread: f32,
    pub hps_spread: f32,
}

#[derive(Debug)]
pub struct Death {
    pub death_index: i32,
    pub death_offset_ms: i32,
    pub death_role: Role,
    pub killing_ability_id: Option<i32>,
    pub duration_ms: i32,
}

#[derive(Debug)]
pub struct FightDetails {
    pub fight_index: i32,
    pub overall_data: Vec<OverallData>,
    pub deaths: Vec<Death>,
    pub specs: HashMap<Spec, usize>,
}

type OverallVars = overall::fight_detailed_data::Variables;
type DeathVars = deaths::fight_detailed_deaths_and_roles::Variables;

pub async fn fight_details(
    client: &Client,
    token: &WclToken,
    mode: WclMode,
    code: String,
    fight: &fights::FightsReportDataReportFights,
) -> anyhow::Result<FightDetails> {
    let (overall, deaths, phases) = list_phase_variables(code, fight);
    let (deaths, specs, roles) = build_deaths(
        fight.start_time as isize,
        extract_response(
            client
                .post(mode.url())
                .bearer_auth(&token.access_token)
                .json(&deaths::FightDetailedDeathsAndRoles::build_query(deaths))
                .header("accept", "application/json")
                .send()
                .await?
                .json()
                .await?,
        )?,
    );

    let mut overall_data = vec![];

    // table data does not include the guid, so we just have to guess. last seen player wins. this only breaks
    // if multiple players with the same name (but different realms) exist in the same fight at the same with
    // different roles. duplicates within the same role are fine.
    let roles = roles
        .into_iter()
        .map(|((name, _guid), role)| (name, role))
        .collect::<HashMap<String, Role>>();

    overall_data.push(build_overall(
        (fight.end_time - fight.start_time) as usize,
        None,
        &roles,
        extract_response(
            client
                .post(mode.url())
                .bearer_auth(&token.access_token)
                .json(&overall::FightDetailedData::build_query(overall))
                .header("accept", "application/json")
                .send()
                .await?
                .json()
                .await?,
        )?,
    ));

    for (index, phase) in phases.into_iter().enumerate() {
        overall_data.push(build_overall(
            (phase.end_time.unwrap() - phase.start_time.unwrap()) as usize,
            Some(index as i32),
            &roles,
            extract_response(
                client
                    .post(mode.url())
                    .bearer_auth(&token.access_token)
                    .json(&overall::FightDetailedData::build_query(phase))
                    .header("accept", "application/json")
                    .send()
                    .await?
                    .json()
                    .await?,
            )?,
        ));
    }

    Ok(FightDetails {
        fight_index: fight.id as i32,
        overall_data,
        deaths,
        specs,
    })
}

fn extract_response<T>(response: Response<T>) -> anyhow::Result<T> {
    if let Some(errors) = response.errors {
        if !errors.is_empty() {
            return Err(anyhow!(
                "error occurred in GraphQL query: {}",
                errors
                    .into_iter()
                    .map(|err| err.message + ", ")
                    .collect::<String>()
            ));
        }
    }

    match response.data {
        Some(data) => Ok(data),
        None => Err(anyhow!("GraphQL query returned no data")),
    }
}

/// Convert a fight into a sequence of variables to run queries against.
fn list_phase_variables(
    code: String,
    fight: &fights::FightsReportDataReportFights,
) -> (OverallVars, DeathVars, Vec<OverallVars>) {
    let overall = OverallVars {
        code: code.clone(),
        fight: fight.id,
        start_time: None,
        end_time: None,
    };
    let deaths = DeathVars {
        code: code.clone(),
        fight: fight.id,
    };

    let mut phases = vec![];
    let mut start_time = fight.start_time;
    if let Some(ref transitions) = fight.phase_transitions {
        for transition in transitions {
            if transition.start_time as f64 > start_time {
                phases.push(OverallVars {
                    code: code.clone(),
                    fight: fight.id,
                    start_time: Some(start_time),
                    end_time: Some(transition.start_time as f64),
                });
            }
            start_time = transition.start_time as f64;
        }
    }

    // add the final phase, ending at the end of the fight
    phases.push(OverallVars {
        code: code.clone(),
        fight: fight.id,
        start_time: Some(start_time),
        end_time: Some(fight.end_time),
    });

    (overall, deaths, phases)
}

fn build_overall(
    duration_ms: usize,
    phase_index: Option<i32>,
    roles: &HashMap<String, Role>,
    data: overall::fight_detailed_data::ResponseData,
) -> OverallData {
    // we are assuming that the response was *actually* successful here.
    let report = data.report_data.and_then(|data| data.report).unwrap();

    let damage_done = report.damage_done.unwrap();
    let damage_taken = report.damage_taken.unwrap();
    let healing_done = report.healing_done.unwrap();

    let dps_spread = spread(
        damage_done
            .data
            .entries
            .iter()
            .filter(|entry| {
                roles
                    .get(&entry.name)
                    .map(|role| role == &Role::DPS)
                    .unwrap_or(false)
            })
            .collect(),
    );
    let hps_spread = spread(
        healing_done
            .data
            .entries
            .iter()
            .filter(|entry| {
                roles
                    .get(&entry.name)
                    .map(|role| role == &Role::Healer)
                    .unwrap_or(false)
            })
            .collect(),
    );

    let duration_s: f64 = duration_ms as f64 / 1000.0;

    let ps = |value: isize| (value as f64 / duration_s) as f32;

    OverallData {
        phase_index,
        duration_ms,
        overall_dps: ps(damage_done
            .data
            .entries
            .into_iter()
            .map(|entry| entry.total)
            .sum::<isize>()),
        overall_hps: ps(healing_done
            .data
            .entries
            .into_iter()
            .map(|entry| entry.total)
            .sum::<isize>()),
        nontank_dtps: ps(damage_taken
            .data
            .entries
            .into_iter()
            .map(|entry| entry.total)
            .sum::<isize>()),
        dps_spread,
        hps_spread,
    }
}

fn build_deaths(
    fight_start: isize,
    data: deaths::fight_detailed_deaths_and_roles::ResponseData,
) -> (
    Vec<Death>,
    HashMap<Spec, usize>,
    HashMap<(String, usize), Role>,
) {
    let mut spec_counts = HashMap::new();
    let mut player_roles = HashMap::new();

    let report = data
        .report_data
        .and_then(|rd| rd.report)
        .expect("the report to be present");
    let player_details = report
        .player_details
        .expect("player details to be present")
        .unwrap_player_details();
    let deaths = report.deaths.expect("deaths to be present").unwrap_deaths();

    for (role, player) in player_details.players() {
        player_roles.insert((player.name.clone(), player.guid), role);
        if let Some(spec) = player.specs.first() {
            let entry = spec_counts
                .entry(Spec {
                    spec_name: spec.spec.clone(),
                    class_name: player.class.clone(),
                })
                .or_insert(0);
            *entry += 1;
        }
    }

    let deaths = deaths
        .data
        .entries
        .into_iter()
        .enumerate()
        .map(|(index, death)| Death {
            death_index: index as i32,
            death_role: player_roles
                .get(&(death.name, death.guid))
                .unwrap_or(&Role::DPS)
                .to_owned(),
            duration_ms: death.death_window as i32,
            death_offset_ms: (death.timestamp - fight_start) as i32,
            killing_ability_id: death.killing_blow.map(|kb| kb.guid as i32),
        })
        .collect();

    (deaths, spec_counts, player_roles)
}

/// Sample standard deviation of entries, scaled by the mean.
fn spread(data: Vec<&overall::Entry>) -> f32 {
    if data.len() <= 1 {
        return 0.0;
    }

    let n = data.len() as f64;
    let mean = data.iter().map(|entry| entry.total as f64).sum::<f64>() / n;
    let var = data
        .iter()
        .map(|entry| (entry.total as f64 - mean).powi(2) / (n - 1.0))
        .sum::<f64>();

    (var.sqrt() / mean) as f32
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn phase_variables_includes_all_phases() {
        let fight = fights::FightsReportDataReportFights {
            average_item_level: Some(635.6),
            difficulty: Some(5),
            kill: Some(false),
            encounter_id: 2922,
            id: 1,
            start_time: 23369.0,
            end_time: 350902.0,
            fight_percentage: Some(34.66),
            size: Some(20),
            in_progress: Some(false),
            last_phase: Some(3),
            last_phase_as_absolute_index: Some(3),
            last_phase_is_intermission: Some(false),
            phase_transitions: Some(vec![
                fights::FightsReportDataReportFightsPhaseTransitions { start_time: 23369 },
                fights::FightsReportDataReportFightsPhaseTransitions { start_time: 181260 },
                fights::FightsReportDataReportFightsPhaseTransitions { start_time: 221758 },
                fights::FightsReportDataReportFightsPhaseTransitions { start_time: 341977 },
            ]),
        };
        let (_overall, _deaths, phases) = list_phase_variables("test".to_string(), &fight);
        assert_eq!(phases.len(), 4);
    }
}
