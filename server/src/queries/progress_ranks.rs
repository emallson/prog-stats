use anyhow::anyhow;
use graphql_client::{GraphQLQuery, Response};
use reqwest::Client;

use crate::{WclToken, WCL_ENDPOINT};

#[derive(serde::Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct ProgressRankingsPage {
    page: i64,
    has_more_pages: bool,
    rankings: Vec<ProgressRanking>,
}

#[derive(serde::Deserialize, Debug)]
struct ProgressRanking {
    server: Server,
    guild: Guild,
}

#[derive(serde::Deserialize, Debug, PartialEq, Eq, Hash, Clone)]
pub struct Server {
    pub id: usize,
    pub name: String,
    pub region: String,
}

#[derive(serde::Deserialize, Debug, PartialEq, Eq, Hash, Clone)]
pub struct Guild {
    pub id: Option<usize>,
    pub name: String,
}

/// the response type from the API
type JSON = ProgressRankingsPage;

#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "schema.json",
    query_path = "queries/fight_progress.graphql",
    response_derives = "Debug",
    variables_derives = "Debug,Clone"
)]
struct EncounterProgressRankings;

// this could be stream-based, not sure if worth.
pub async fn encounter_progress_rankings(
    client: &Client,
    token: &WclToken,
    encounter_id: i64,
    difficulty: i64,
) -> anyhow::Result<Vec<(Server, Guild)>> {
    let mut page = None;
    let mut result = vec![];
    // only include up to the first 1000 on each boss
    while page.map(|v| v <= 20).unwrap_or(true) {
        let res = client
            .post(WCL_ENDPOINT)
            .bearer_auth(&token.access_token)
            .json(&EncounterProgressRankings::build_query(
                encounter_progress_rankings::Variables {
                    encounter_id,
                    difficulty,
                    page,
                },
            ))
            .header("accept", "application/json")
            .send()
            .await?
            .json::<Response<encounter_progress_rankings::ResponseData>>()
            .await
            .map_err(|err| {
                anyhow!(
                    "failed to decode rankings page {:?} for boss {}: {}",
                    page,
                    encounter_id,
                    err
                )
            })?;

        if res.errors.is_some() {
            log::error!(
                "errors encountered while retrieving progress rankings {:?}",
                res.errors
            );
            break;
        }

        let data = res
            .data
            .and_then(|d| d.world_data)
            .and_then(|d| d.encounter)
            .and_then(|d| d.fight_rankings);

        if let Some(data) = data {
            for ranking in data.rankings {
                result.push((ranking.server, ranking.guild));
            }
            if data.has_more_pages {
                page = Some(data.page + 1);
            } else {
                break;
            }
        }
    }

    Ok(result)
}
