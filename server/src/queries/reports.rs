use super::guild::GuildId;
use crate::{WclMode, WclToken};
use anyhow::Result;
use graphql_client::{GraphQLQuery, Response};
use reqwest::Client;

#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "schema.json",
    query_path = "queries/tier_reports.graphql",
    response_derives = "Debug",
    variables_derives = "Debug,Default,Clone"
)]
pub struct TierReports;

#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "schema.json",
    query_path = "queries/fights.graphql",
    response_derives = "Debug",
    variables_derives = "Debug,Default"
)]
pub struct Fights;

pub async fn guild_reports(
    client: &Client,
    token: &WclToken,
    mode: WclMode,
    id: GuildId,
    start_time: f64,
    end_time: Option<f64>,
) -> Result<Vec<tier_reports::TierReportsReportDataReportsData>> {
    let mut vars = tier_reports::Variables {
        start_time,
        end_time,
        page: 1,
        ..Default::default()
    };

    match id {
        GuildId::Guild(id) => {
            vars.guild = Some(id);
        }
        GuildId::Tag(id) => {
            vars.guild_tag = Some(id);
        }
    }

    let mut results = vec![];
    loop {
        let cursor = client
            .post(mode.url())
            .bearer_auth(&token.access_token)
            .json(&TierReports::build_query(vars.clone()))
            .header("accept", "application/json")
            .send()
            .await?
            .json::<Response<tier_reports::ResponseData>>()
            .await?
            .data
            .and_then(|d| d.report_data)
            .and_then(|d| d.reports);

        let more = if let Some(c) = cursor.as_ref() {
            c.has_more_pages
        } else {
            false
        };

        if let Some(data) = cursor.and_then(|d| d.data) {
            results.extend(data.into_iter().filter_map(|d| d));
        }

        if !more {
            break;
        } else {
            vars.page += 1;
        }
    }

    Ok(results)
}

pub async fn report_fights(
    client: &Client,
    token: &WclToken,
    mode: WclMode,
    code: String,
    encounter_id: Option<i64>,
    difficulty: Option<i64>,
) -> Result<Response<fights::ResponseData>> {
    let vars = fights::Variables {
        code,
        encounter_id,
        difficulty,
    };

    client
        .post(mode.url())
        .bearer_auth(&token.access_token)
        .json(&Fights::build_query(vars))
        .header("accept", "application/json")
        .send()
        .await?
        .json()
        .await
        .map_err(anyhow::Error::from)
}
