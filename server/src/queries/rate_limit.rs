use anyhow::anyhow;
use graphql_client::{GraphQLQuery, Response};
use reqwest::Client;

use crate::{WclMode, WclToken};

mod internal {
    use graphql_client::GraphQLQuery;

    #[derive(GraphQLQuery)]
    #[graphql(
        schema_path = "schema.json",
        query_path = "queries/rate_limit.graphql",
        response_derives = "Debug"
    )]
    pub(super) struct RateLimit;
}

pub type RateLimit = internal::rate_limit::RateLimitRateLimitData;

pub async fn rate_limit(
    client: &Client,
    token: &WclToken,
    mode: WclMode,
) -> anyhow::Result<RateLimit> {
    let res = client
        .post(mode.url())
        .bearer_auth(&token.access_token)
        .json(&internal::RateLimit::build_query(
            internal::rate_limit::Variables,
        ))
        .header("accept", "application/json")
        .send()
        .await?
        .json::<Response<internal::rate_limit::ResponseData>>()
        .await?;

    res.data
        .and_then(|data| data.rate_limit_data)
        .ok_or(anyhow!("unable to retrieve rate limit data"))
}
