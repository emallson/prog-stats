use crate::{WclToken, WCL_USER_ENDPOINT};
use anyhow::{anyhow, Result};
use graphql_client::{GraphQLQuery, Response};
use log::{debug, info};
use reqwest::Client;

#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "user_schema.json",
    query_path = "queries/user_validation.graphql",
    response_derives = "Debug",
    variables_derives = "Debug,Clone"
)]
struct UserLogs;

/// Returns `true` if the user corresponding to `token` is privileged to see private/unlisted logs
/// for the given guild.
pub async fn privileged_user(
    client: &Client,
    token: &WclToken,
    guild: i64,
    zone: i64,
) -> Result<bool> {
    let mut vars = user_logs::Variables {
        guild,
        zone,
        page: 1,
    };
    let privileged = loop {
        let data = client
            .post(WCL_USER_ENDPOINT)
            .bearer_auth(&token.access_token)
            .json(&UserLogs::build_query(vars.clone()))
            .header("accept", "application/json")
            .send()
            .await?
            .json::<Response<user_logs::ResponseData>>()
            .await?;

        info!("report data: {:?}", data);
        let res = data
            .data
            .and_then(|d| d.report_data)
            .and_then(|d| d.reports)
            .ok_or(anyhow!("unable to load report data for guild {}", guild))?;

        if res.data.ok_or(anyhow!("bad report data"))?.iter().any(|v| {
            debug!("examining log {:?}", v);
            if let Some(vis) = v {
                vis.visibility != "public"
                    || vis
                        .archive_status
                        .as_ref()
                        .map(|status| status.is_archived && status.is_accessible)
                        .unwrap_or(false)
            } else {
                false
            }
        }) {
            break true;
        } else if !res.has_more_pages {
            break false;
        } else {
            vars.page += 1;
        }
    };

    Ok(privileged)
}
