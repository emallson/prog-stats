use crate::{WclToken, WCL_ENDPOINT};
use anyhow::Result;
use graphql_client::{GraphQLQuery, Response};
use reqwest::Client;

#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "schema.json",
    query_path = "queries/guild_by_name.graphql",
    response_derives = "Debug"
)]
pub struct GuildByName;

#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "schema.json",
    query_path = "queries/guild_by_id.graphql",
    response_derives = "Debug"
)]
pub struct GuildById;

#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "schema.json",
    query_path = "queries/guild_by_tagid.graphql",
    response_derives = "Debug,Clone"
)]
pub struct TagReport;

pub async fn lookup_guild(
    client: &Client,
    token: &WclToken,
    name: String,
    server: String,
    region: String,
) -> Result<Response<guild_by_name::ResponseData>> {
    client
        .post(WCL_ENDPOINT)
        .bearer_auth(&token.access_token)
        .json(&GuildByName::build_query(guild_by_name::Variables {
            name,
            server,
            region,
        }))
        .header("accept", "application/json")
        .send()
        .await?
        .json()
        .await
        .map_err(anyhow::Error::from)
}

pub async fn lookup_guild_by_id(
    client: &Client,
    token: &WclToken,
    id: i64,
) -> Result<Response<guild_by_id::ResponseData>> {
    client
        .post(WCL_ENDPOINT)
        .bearer_auth(&token.access_token)
        .json(&GuildById::build_query(guild_by_id::Variables { id }))
        .header("accept", "application/json")
        .send()
        .await?
        .json()
        .await
        .map_err(anyhow::Error::from)
}

pub async fn lookup_guild_by_team(
    client: &Client,
    token: &WclToken,
    id: i64,
) -> Result<Response<tag_report::ResponseData>> {
    client
        .post(WCL_ENDPOINT)
        .bearer_auth(&token.access_token)
        .json(&TagReport::build_query(tag_report::Variables { tag: id }))
        .header("accept", "application/json")
        .send()
        .await?
        .json()
        .await
        .map_err(anyhow::Error::from)
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum GuildId {
    Guild(i64),
    Tag(i64),
}

impl GuildId {
    pub fn id(&self) -> i64 {
        match self {
            GuildId::Guild(id) => *id,
            GuildId::Tag(id) => *id,
        }
    }

    pub fn is_tag(&self) -> bool {
        match self {
            GuildId::Tag(_) => true,
            _ => false,
        }
    }
}
