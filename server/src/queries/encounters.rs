use crate::{WclToken, WCL_ENDPOINT};
use anyhow::{anyhow, Result};
use async_graphql::SimpleObject;
use graphql_client::{GraphQLQuery, Response};
use reqwest::Client;

#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "schema.json",
    query_path = "queries/encounters.graphql",
    response_derives = "Debug",
    variables_derives = "Debug,Clone"
)]
pub struct Encounters;

#[derive(Debug, Clone, SimpleObject)]
pub struct Encounter {
    pub id: i64,
    pub name: String,
}

#[derive(Debug, Clone, SimpleObject)]
pub struct Zone {
    pub id: i64,
    pub name: String,
    pub encounters: Vec<Encounter>,
}

pub async fn encounters(client: &Client, token: &WclToken, zone: i64) -> Result<Zone> {
    let res = client
        .post(WCL_ENDPOINT)
        .bearer_auth(&token.access_token)
        .json(&Encounters::build_query(encounters::Variables { zone }))
        .header("accept", "application/json")
        .send()
        .await?
        .json::<Response<encounters::ResponseData>>()
        .await?;

    let zone_data = res
        .data
        .and_then(|d| d.world_data)
        .and_then(|d| d.zone)
        .map(|d| Zone {
            id: d.id,
            name: d.name,
            encounters: d
                .encounters
                .map(|es| {
                    es.into_iter()
                        .filter_map(|e| {
                            if let Some(e) = e {
                                Some(Encounter {
                                    id: e.id,
                                    name: e.name,
                                })
                            } else {
                                None
                            }
                        })
                        .collect()
                })
                .unwrap_or_else(Vec::new),
        });

    zone_data.ok_or(anyhow!("no zone data for {}", zone))
}
