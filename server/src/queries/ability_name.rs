use futures::future::try_join_all;
use graphql_client::{GraphQLQuery, Response};
use moka::future::Cache;
use reqwest::Client;
use std::{sync::LazyLock, time::Duration};

use crate::{WclMode, WclToken};

static ABILITY_CACHE: LazyLock<Cache<i32, Option<String>>> = LazyLock::new(|| {
    Cache::builder()
        .max_capacity(2_000)
        .time_to_live(Duration::new(24 * 60 * 60, 0))
        .build()
});

#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "schema.json",
    query_path = "queries/ability_name.graphql",
    response_derives = "Debug",
    variables_derives = "Debug,Clone"
)]
pub struct AbilityName;

pub async fn prefetch_ability_names(
    client: Client,
    token: WclToken,
    mode: WclMode,
    ability_ids: impl IntoIterator<Item = i32>,
) -> anyhow::Result<()> {
    let mut chunk = vec![];
    for id in ability_ids {
        chunk.push(tokio::spawn(ability_name(
            client.clone(),
            token.clone(),
            mode,
            id,
        )));
        if chunk.len() >= 10 {
            try_join_all(chunk).await?;
            chunk = vec![];
        }
    }

    if !chunk.is_empty() {
        try_join_all(chunk).await?;
    }

    Ok(())
}

pub async fn ability_name(
    client: Client,
    token: WclToken,
    mode: WclMode,
    ability_id: i32,
) -> anyhow::Result<Option<String>> {
    match ability_id {
        id if id <= 1 => Ok(Some("Melee".to_owned())),
        3 => Ok(Some("Falling".to_owned())),
        _ => {
            if let Some(name) = ABILITY_CACHE.get(&ability_id).await {
                return Ok(name);
            }

            let res = client
                .post(mode.url())
                .bearer_auth(&token.access_token)
                .json(&AbilityName::build_query(ability_name::Variables {
                    id: ability_id as i64,
                }))
                .header("accept", "application/json")
                .send()
                .await?
                .json::<Response<ability_name::ResponseData>>()
                .await?;

            if let Some(game_data) = res.data.and_then(|d| d.game_data) {
                // assuming we get the game data field, we are always going to store something in the cache to prevent repeat lookups.
                // the cache TTL is 1 day, so bad names will fix themselves.
                let name = game_data.ability.and_then(|ability| ability.name);
                ABILITY_CACHE.insert(ability_id, name.clone()).await;
                return Ok(name);
            }

            Ok(None)
        }
    }
}
