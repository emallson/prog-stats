#![feature(const_fn_floating_point_arithmetic)]
#![feature(duration_constructors)]
#[macro_use]
extern crate bind_all_derive;

use anyhow::Result;
use dotenv::dotenv;
use reqwest::Client;
use serde_derive::Deserialize;
use std::borrow::Cow;
use std::fs::read_to_string;
use structopt::StructOpt;

mod analysis;
mod background_jobs;
mod bulk_insert;
mod graphql;
mod histogram;
mod import;
mod model;
mod queries;
mod server;
mod user_auth;

#[derive(Deserialize, Debug)]
struct Secrets {
    wcl: WclSecrets,
    sentry: SentrySecrets,
}

#[derive(Debug, Copy, Clone)]
pub enum WclMode {
    Public,
    Private,
}

impl WclMode {
    fn url(&self) -> &str {
        match *self {
            WclMode::Public => WCL_ENDPOINT,
            WclMode::Private => WCL_USER_ENDPOINT,
        }
    }
}

#[derive(Deserialize, Debug, Clone)]
pub struct WclSecrets {
    id: String,
    secret: String,
}

#[derive(Deserialize, Debug, Clone)]
pub struct SentrySecrets {
    pub ingest: String,
}

#[derive(Deserialize, Debug, Clone)]
pub struct WclToken {
    pub token_type: Option<String>,
    pub expires_in: u64,
    pub access_token: String,
    pub refresh_token: Option<String>,
}

#[derive(StructOpt, Debug)]
pub struct ServerOpt {
    #[structopt(default_value = "9998")]
    port: u16,
    #[structopt(default_value = "0.0.0.0")]
    address: String,
}

#[derive(StructOpt, Debug)]
pub struct HistogramOpt {
    zone: i32,
    #[structopt(default_value = "5")]
    difficulty: i32,
}

#[derive(StructOpt, Debug)]
enum Opt {
    /// Start the server.
    Server(ServerOpt),
    RebuildHistograms(HistogramOpt),
}

async fn get_wcl_access_token(client: &Client, secrets: &Secrets) -> Result<WclToken> {
    client
        .post("https://www.warcraftlogs.com/oauth/token")
        .form(&[("grant_type", "client_credentials")])
        .basic_auth(&secrets.wcl.id, Some(&secrets.wcl.secret))
        .send()
        .await?
        .json::<WclToken>()
        .await
        .map_err(anyhow::Error::from)
}

pub const WCL_ENDPOINT: &str = "https://www.warcraftlogs.com/api/v2/client";
pub const WCL_USER_ENDPOINT: &str = "https://www.warcraftlogs.com/api/v2/user";

#[tokio::main]
async fn main() -> Result<()> {
    dotenv().ok();

    #[cfg(debug_assertions)]
    femme::with_level(log::LevelFilter::Debug);

    #[cfg(not(debug_assertions))]
    {
        let mut log_builder = pretty_env_logger::formatted_builder();
        log_builder.parse_filters("info");
        let logger = log_builder.build();
        let logger = sentry::integrations::log::SentryLogger::with_dest(logger);

        log::set_boxed_logger(Box::new(logger)).unwrap();
        log::set_max_level(log::LevelFilter::Info);
    }

    let data = read_to_string("secrets.toml")?;
    let secrets: Secrets = toml::from_str(&data)?;

    let _guard = sentry::init((
        secrets.sentry.ingest.as_str(),
        sentry::ClientOptions {
            release: option_env!("VCS_CHANGESET").map(Cow::Borrowed),
            ..Default::default()
        },
    ));

    let args = Opt::from_args();

    match args {
        Opt::Server(opts) => {
            let client = Client::new();
            let token = get_wcl_access_token(&client, &secrets).await?;

            server::build_web_server(opts, token, secrets.wcl.clone()).await?;

            Ok(())
        }
        Opt::RebuildHistograms(opts) => {
            let pool = server::connect_db().await?;

            histogram::rebuild_histogram_data(&pool, opts.zone, opts.difficulty).await?;

            Ok(())
        }
    }
}
