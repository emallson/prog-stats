use anyhow::Result;
use async_graphql::Enum;
use chrono::Datelike;
use sqlx::{Acquire, Connection, PgConnection, Postgres};
use std::collections::BTreeMap;
use std::{collections::HashMap, convert::TryFrom, sync::Arc};

use moka::future::Cache;

use crate::bulk_insert::BulkInsert;
use crate::model::{DistInfo, EncounterStats, EncounterSummary, PgExec};

#[derive(Copy, Clone, Debug)]
pub(crate) struct Bin {
    bin_index: i32,
    count: i32,
}

impl Into<Bin> for (i32, i32) {
    fn into(self) -> Bin {
        Bin {
            bin_index: self.0,
            count: self.1,
        }
    }
}

#[derive(Clone, Debug)]
pub(crate) struct Histogram {
    scale: HistogramScale,
    bins: Vec<Bin>,
    total: i32,
}

impl Histogram {
    pub fn from_bins<T: Into<Bin>>(
        scale: HistogramScale,
        values: impl IntoIterator<Item = T>,
    ) -> Self {
        let bins = values.into_iter().map(|v| v.into()).collect::<Vec<Bin>>();
        let total = bins.iter().map(|b| b.count).sum::<i32>();
        Histogram { scale, bins, total }
    }

    pub fn total_count(&self) -> i32 {
        self.total
    }

    /// Compute the value of a given quantile
    pub fn quantile(&self, q: f64) -> Option<f64> {
        if self.bins.is_empty() {
            return None;
        }

        let target_count = ((self.total - 1) as f64 * q).ceil() as i32;

        let mut running_count = 0;
        for &Bin { bin_index, count } in &self.bins {
            running_count += count;
            if running_count >= target_count {
                let (_, ub) = self.scale.bin_bounds(bin_index);
                return Some(ub);
            }
        }

        None
    }

    /// Compute the percentile of an observed `value`, treating values in the same bin as greater.
    ///
    /// Note that we are flipping percentiles (aka 1 - p) for display, so this is *displaying* values in the same bin as lesser.
    pub fn percentile_of_obs(&self, value: f64) -> f64 {
        if self.bins.is_empty() {
            return 1.0;
        }

        let target = self.scale.bin_index(value);
        let mut running_count = 0;

        for &Bin { bin_index, count } in &self.bins {
            if bin_index >= target {
                break;
            }
            running_count += count;
        }

        running_count as f64 / self.total as f64
    }

    pub fn get_dist_info(&self, actual: f64) -> Option<DistInfo> {
        if self.bins.is_empty() {
            return None;
        }
        let min = self.bins.first().map(|bin| {
            let lb = self.scale.bin_bounds(bin.bin_index).0;
            if lb.is_finite() {
                lb
            } else {
                log::warn!(
                    "saw infinite bin lb for scale {:?} (index: {})",
                    self.scale,
                    bin.bin_index
                );
                0.0
            }
        });
        let max = self
            .bins
            .last()
            .map(|bin| self.scale.bin_bounds(bin.bin_index).1);

        let count = self.total as i64;
        let actual_pct = Some(self.percentile_of_obs(actual));
        let quantiles = vec![0.25, 0.5, 0.75]
            .into_iter()
            .filter_map(|q| self.quantile(q))
            .collect();

        let result = Some(DistInfo {
            min,
            max,
            count,
            actual_pct,
            quantiles,
        });

        log::info!("generated v2 distinfo {:?}", result);

        result
    }
}

#[derive(Copy, Clone, Hash, Eq, PartialEq)]
pub struct HistogramKey {
    pub encounter_id: i32,
    pub difficulty: i32,
    pub metric_type: MetricType,
}

pub type HistogramCache = Cache<HistogramKey, Arc<Histogram>>;

pub async fn load<'a, E: PgExec<'a>>(
    con: E,
    cache: &HistogramCache,
    key: HistogramKey,
) -> Result<Arc<Histogram>> {
    if let Some(value) = cache.get(&key).await {
        return Ok(value);
    }

    let db_value = histogram_for(con, key.encounter_id, key.difficulty, key.metric_type).await?;

    let value = Arc::new(db_value);
    cache.insert(key, value.clone()).await;

    Ok(value)
}

async fn histogram_for<'a, E: PgExec<'a>>(
    con: E,
    encounter_id: i32,
    difficulty: i32,
    metric_type: MetricType,
) -> Result<Histogram> {
    let bin_data = sqlx::query_as!(
        Bin,
        "
        select bin_index, cast(sum(count) as integer) as \"count!\"
          from histogram_bins
         where encounter_id = $1
           and difficulty = $2
           and metric_type = $3
         group by bin_index
         order by bin_index asc
        ",
        encounter_id,
        difficulty,
        metric_type as i32
    )
    .fetch_all(con)
    .await?;

    let total = bin_data.iter().map(|bin| bin.count).sum::<i32>();
    Ok(Histogram {
        scale: metric_type.scale(),
        bins: bin_data,
        total,
    })
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, Enum, PartialOrd, Ord)]
#[repr(u8)]
pub enum MetricType {
    ProgTime = 1,
    PullCount = 0,
    StartIlvl = 2,
    EndIlvl = 3,
}

impl TryFrom<i32> for MetricType {
    type Error = anyhow::Error;

    fn try_from(value: i32) -> Result<MetricType> {
        use MetricType::*;
        match value {
            1 => Ok(ProgTime),
            0 => Ok(PullCount),
            2 => Ok(StartIlvl),
            3 => Ok(EndIlvl),
            _ => Err(anyhow::anyhow!("invalid metric type")),
        }
    }
}

impl MetricType {
    pub const fn scale(&self) -> HistogramScale {
        use MetricType::*;
        match self {
            ProgTime | PullCount => HistogramScale::with_percentile_error(0.01),
            StartIlvl | EndIlvl => HistogramScale::with_bin_size(1.0),
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub enum HistogramScale {
    Logarithmic {
        gamma: f64,
    },
    Linear {
        bin_width: f64,
    },
    LinearCapped {
        bin_width: f64,
        lower: f64,
        upper: f64,
        bin_count: i32,
    },
}

impl HistogramScale {
    pub const fn with_percentile_error(error: f64) -> Self {
        HistogramScale::Logarithmic {
            gamma: (1.0 + error) / (1.0 - error),
        }
    }

    pub const fn with_bin_size(width: f64) -> Self {
        HistogramScale::Linear { bin_width: width }
    }

    // Convert an observation to a bin index on this scale.
    pub fn bin_index(&self, value: f64) -> i32 {
        use HistogramScale::*;

        match self {
            Linear { bin_width } => (value / *bin_width).ceil() as i32,
            Logarithmic { gamma } => value.log(*gamma).ceil() as i32,
            &LinearCapped {
                bin_width,
                lower,
                upper,
                bin_count,
            } => {
                if lower >= value {
                    0
                } else if upper <= value {
                    bin_count + 1
                } else {
                    ((value - lower) / bin_width).ceil() as i32
                }
            }
        }
    }

    /// Convert a bin index into a (lb, ub] range.
    pub fn bin_bounds(&self, index: i32) -> (f64, f64) {
        use HistogramScale::*;

        match self {
            Linear { bin_width } => ((index - 1) as f64 * bin_width, index as f64 * bin_width),
            Logarithmic { gamma } => (
                2.0 * gamma.powi(index - 1) / (1.0 + gamma),
                2.0 * gamma.powi(index) / (1.0 + gamma),
            ),
            &LinearCapped {
                bin_width,
                lower,
                upper,
                bin_count,
            } => {
                // due to issues with the float handling in async-graphql, we model the cap bins as finite-width
                if index <= 0 {
                    (lower - bin_width, lower)
                } else if index > bin_count {
                    (upper, upper + bin_width)
                } else {
                    (
                        lower + (index - 1) as f64 * bin_width,
                        lower + index as f64 * bin_width,
                    )
                }
            }
        }
    }

    const fn linear_capped(lower: f64, upper: f64, bin_count: i32) -> HistogramScale {
        HistogramScale::LinearCapped {
            lower,
            upper,
            bin_count,
            bin_width: (upper - lower) / bin_count as f64,
        }
    }
}

pub async fn rebuild_histogram_data<'a, E: PgExec<'a> + Acquire<'a, Database = Postgres>>(
    con: E,
    zone: i32,
    difficulty: i32,
) -> anyhow::Result<()> {
    let mut tx = con.acquire().await?;

    let data = sqlx::query_as_unchecked!(
        EncounterSummary,
        "select encounter_stats_summary.*, 0 as kill_number from encounter_stats_summary
         join encounters on encounter_stats_summary.encounter_id = encounters.encounter_id
         where encounters.zone = $1 and difficulty = $2 and prog_time > 0 and has_kill
        ",
        zone,
        difficulty
    )
    .fetch_all(&mut *tx)
    .await?;

    // we are going to built this up in-memory. then transform it into NewHistogramBin entries and do a bulk insertion.
    let mut result_data: HashMap<HistogramKey, BTreeMap<(i32, i32), i64>> = HashMap::new();

    for datum in data {
        for metric_type in [
            MetricType::ProgTime,
            MetricType::PullCount,
            MetricType::StartIlvl,
            MetricType::EndIlvl,
        ] {
            let table = result_data
                .entry(HistogramKey {
                    metric_type,
                    encounter_id: datum.encounter_id,
                    difficulty,
                })
                .or_default();

            let obs = match metric_type {
                // prog time is converted to minutes for storage
                MetricType::ProgTime => datum.prog_time as f64,
                MetricType::PullCount => datum.pull_count as f64,
                MetricType::StartIlvl => datum.start_ilvl as f64,
                MetricType::EndIlvl => datum.end_ilvl as f64,
            };

            let bin_index = metric_type.scale().bin_index(obs);

            *table
                .entry((datum.kill_time.num_days_from_ce(), bin_index))
                .or_default() += 1;
        }
    }

    let output_data = result_data
        .into_iter()
        .flat_map(|(key, table)| {
            table
                .into_iter()
                .map(move |((day_index, bin_index), count)| NewHistogramBin {
                    encounter_id: key.encounter_id,
                    difficulty: key.difficulty,
                    metric_type: key.metric_type as i32,
                    day_index,
                    bin_index,
                    count,
                })
        })
        .collect::<Vec<_>>();

    let mut tx = <sqlx::PgConnection>::begin(&mut tx).await?;

    sqlx::query!(
        "
         delete from histogram_bins
          where encounter_id in (select encounter_id from encounters where zone = $1)
            and difficulty = $2
        ",
        zone,
        difficulty
    )
    .execute(&mut *tx)
    .await?;

    BulkInsert::new("histogram_bins")
        .add_rows(output_data)
        .execute(&mut *tx)
        .await?;

    tx.commit().await?;

    Ok(())
}

#[derive(BindAll)]
struct NewHistogramBin {
    encounter_id: i32,
    difficulty: i32,
    metric_type: i32,
    day_index: i32,
    bin_index: i32,
    count: i64,
}

pub fn compress_scale(input: &Histogram, bin_count: usize) -> HistogramScale {
    let upper = input.quantile(0.975).unwrap_or(1.0);
    let lower = input.quantile(0.025).unwrap_or(0.0);

    HistogramScale::linear_capped(lower, upper, bin_count as i32)
}

/// Decrement histogram counters for the provided `EncounterStats`.
///
/// Note that this method has NO WAY of knowing whether the data is already present in the dataset. As a result, it the responsibility of the caller to ensure that decrements happen only after increments.
///
/// This DOES NOT begin a new transaction.
pub async fn decrement(con: &mut PgConnection, stats: EncounterStats) -> Result<()> {
    if stats.difficulty != 5 {
        return Ok(()); // Only keeping Mythic stats for now.
    }
    let day_index = stats.kill_date.map(|date| date.num_days_from_ce());

    if let Some(day_index) = day_index {
        // there is a kill, we can decrement
        let entries = [
            (MetricType::ProgTime, stats.prog_time as f64),
            (MetricType::PullCount, stats.pull_count as f64),
            (MetricType::StartIlvl, stats.ilvl_min as f64),
            (MetricType::EndIlvl, stats.ilvl_max as f64),
        ];
        // TODO This could be a bulk operation
        for (mtype, obs) in entries {
            sqlx::query!(
                "
                 update histogram_bins
                    set count = greatest(count - 1, 0)
                  where encounter_id = $1
                    and difficulty = $2
                    and day_index = $3
                    and bin_index = $4
                    and metric_type = $5
                 ",
                stats.encounter_id,
                stats.difficulty,
                day_index,
                mtype.scale().bin_index(obs),
                mtype as i32
            )
            .execute(&mut *con)
            .await?;
        }
    }
    Ok(())
}

/// Increment histogram counters for the provided `EncounterStats`.
///
/// This DOES NOT begin a new transaction.
pub async fn increment(con: &mut PgConnection, stats: EncounterStats) -> Result<()> {
    if stats.difficulty != 5 {
        return Ok(()); // Only keeping Mythic stats for now.
    }
    let day_index = stats.kill_date.map(|date| date.num_days_from_ce());

    if let Some(day_index) = day_index {
        // there is a kill, we can decrement
        let entries = [
            (MetricType::ProgTime, stats.prog_time as f64),
            (MetricType::PullCount, stats.pull_count as f64),
            (MetricType::StartIlvl, stats.ilvl_min as f64),
            (MetricType::EndIlvl, stats.ilvl_max as f64),
        ];
        // TODO this could be a bulk operation
        for (mtype, obs) in entries {
            sqlx::query!(
                "
                 insert into histogram_bins (encounter_id, difficulty, day_index, bin_index, metric_type, count)
                 values ($1, $2, $3, $4, $5, 1)
                 on conflict (encounter_id, difficulty, day_index, bin_index, metric_type)
                 do update set count = histogram_bins.count + 1
                 ",
                stats.encounter_id,
                stats.difficulty,
                day_index,
                mtype.scale().bin_index(obs),
                mtype as i32
            )
            .execute(&mut *con)
            .await?;
        }
    }
    Ok(())
}

#[cfg(test)]
mod test {
    #[test]
    fn sanity_test_linear_bins() {
        assert_eq!(super::MetricType::StartIlvl.scale().bin_index(480.0), 160,);
    }
}
