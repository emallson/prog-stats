INSERT INTO encounter_hotfixes (encounter_id, application_date, regional, major, description)
    VALUES
        -- forgotten experiments
        (2693, '2023-05-09 15:00:00 GMT-08:00', FALSE, FALSE, ARRAY['Fixed an issue that allowed resetting The Forgotten Experiments encounter unintentionally.']),
        (2693, '2023-05-30 15:00:00 GMT-08:00', FALSE, FALSE, ARRAY['Fixed an issue where players sometimes left combat between when boss creatures were active.']),
        -- amalgamation chamber
        (2687, '2023-05-11 15:00:00 GMT-08:00', FALSE, FALSE, ARRAY['After a soft reset, the door from Amalgamation Chamber to the Forgotten Experiments will now properly open if you have defeated the Amalgamation Chamber.', 'Adjusted the damage of Umbral Detonation and Blistering Twilight to scale appropriately with group size on Normal and Heroic difficulties.']),
        -- scalecommander sarkareth
        (2685, '2023-05-11 15:00:00 GMT-08:00', FALSE, FALSE, ARRAY['Adjusted the damage of Echoing Howl to scale appropriately with group size.']),
        (2685, '2023-05-15 15:00:00 GMT-08:00', FALSE, FALSE, ARRAY['Players are now unable to be struct by Motes of Oblivion for a short time after leaving the Emptiness Between Stars.']),
        -- zskarn
        (2689, '2023-05-16 15:00:00 GMT-08:00', TRUE, FALSE, ARRAY['Fixed an issue that would cause Zskarn''s ability use to be inconsistent.', 'Fixed an issue causing Blast Wave to knock back pets.']),
        (2689, '2023-05-26 15:00:00 GMT-08:00', FALSE, TRUE, ARRAY['Removed the Meme Strat.', 'The Vigilant Steward, Zskarn health reduced by 15% on Mythic difficulty.', 'The Vigilant Steward, Zskarn health reduced by an additional 10% on Mythic difficulty (total: 25%).', 'Dragonfire Golem health reduced by 15% on Mythic difficulty.', 'Zskarn will now Berserk at 8m 30s on all difficulties.', 'Adjusted the position of Dragonfire Traps to eliminate safe zones.', 'Shrapnel Bomb reduced to 3 on Mythic difficulty (was 4).', 'Fixed an issue that could cause an inappropriate amount of Shrapnel Bombs to spawn.', 'Zskarn no longer activates Tactical Destruction based on his location, but instead based on the average location of the raid.']),
        -- magmorax
        (2683, '2023-05-15 15:00:00 GMT-08:00', FALSE, FALSE, ARRAY['Magma Puddles are no longer affected by Spirit of Redemption']),
        (2683, '2023-05-30 15:00:00 GMT-08:00', FALSE, FALSE, ARRAY['No longer targets pets with Blazing Breath']),
        -- echo of neltharion
        (2684, '2023-05-17 15:00:00 GMT-08:00', FALSE, FALSE, ARRAY['Fixed an issue where players could get stuck with Sundered Reality during Phase 3 of the encounter.']),
        (2684, '2023-05-30 15:00:00 GMT-08:00', FALSE, FALSE, ARRAY['Shatter no longer damages pets.']),
        -- rashok
        (2680, '2023-05-19 15:00:00 GMT-08:00', FALSE, FALSE, ARRAY['Fixed an issue where Rashok can sometimes not jump to target player''s location during Searing Slam.']),
        (2680, '2023-05-30 15:00:00 GMT-08:00', FALSE, FALSE, ARRAY['Fixed an issue where Lava Wave was inflicting damage more often than stated in the spell description.']),
        (2680, '2023-05-30 15:00:00 GMT-08:00', TRUE, FALSE, ARRAY['[Undocumented] Healers are no longer targeted by Searing Slam on Mythic difficulty.']);

