insert into encounter_hotfixes (encounter_id, application_date, regional, major, description)
values
  -- rashok
  (2680, '2023-07-18 15:00:00 GMT-08:00', true, true, array[
    'Searing Slam base damage reduced by 6.5% on Mythic difficulty.',
    'Overcharged damage reduced by 10% in Mythic difficulty.',
    'Conduit Flare damage reduced by 12% in Mythic difficulty.',
    'Fixed an issue where killing Conduit Guardians near Rashok can cause him to not gain energy.'
    ]),
  -- experiments
  (2693, '2023-08-07 15:00:00 GMT-08:00', false, false, array[
    'Fixed an issue causing the periodic damage of Unstable Essence to not apply correctly.'
    ]),
  -- echo of nelth
  (2684, '2023-08-22 15:00:00 GMT-08:00', true, true, array[
    'Neltharion''s health reduced by 5% on mythic difficulty.',
    'Voice from Beyond''s health reduced by 5% on Mythic difficulty.',
    'Ebon Destruction''s periodic damage reduced by 10% on Mythic difficulty.',
    'Shatter''s periodic damage reduced by 10% on Mythic difficulty.',
    'Fixed an issue where Neltharion may fail to apply Corruption on players during phase 2 of the encounter on Heroic and Mythic difficulties.'
    ]),
  -- sarkareth
  (2685, '2023-08-22 15:00:00 GMT-08:00', true, true, array[
    'Sarkareth''s berserk time increased 8 minutes (was 7:30) on Mythic difficulty.',
    'Echoing Howl damage per tick reduced by 20% on Mythic difficulty.',
    'Empty Recollection health reduced by 17% on Mythic difficulty.',
    'Blasting Scream cast time increased by 25%.'
    ]),
  (2685, '2023-08-22 15:00:00 GMT-08:00', true, false, array[
    'Astral Flare duration increased to 33 seconds (was 30 seconds) for 10-player groups on Normal and Heroic.',
    'Oblivion damage per tick reduced by 10% for 10-player groups on Normal and Heroic.',
    'Void Surge damage per tick reduced by 20% for 10-player groups on Normal and Heroic.',
    'Astral Eruption damage reduced by 60% for 10-player groups on Normal and Heroic.'
    ]),
  -- nelth hotfix
  (2684, '2023-08-24 15:00:00 GMT-08:00', false, false, array[
    'Fixed an issue where Volcanic Heart can target players with Corruption.'
    ]);
