alter table log_encounter_stats
drop constraint log_encounter_stats_log_fkey,
add constraint log_encounter_stats_log_fkey foreign key (log) references logs(iid) on delete cascade;
