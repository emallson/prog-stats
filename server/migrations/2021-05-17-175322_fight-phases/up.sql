alter table fights add column last_phase integer,
                   add column last_phase_is_intermission boolean;

create index if not exists fight_phases on fights (encounter_id, difficulty, last_phase);
