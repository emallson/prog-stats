-- Your SQL goes here
create table team_stats (
  id serial primary key,
  team integer not null references teams(id),
  zone_id integer not null,
  bosses_killed integer not null,
  min_hours real not null,
  max_hours real not null,
  unique(team, zone_id)
);

create table raid_nights (
  id serial primary key,
  ts integer not null references team_stats(id),
  -- we store the start time with date here because trying to store the time of
  -- day just causes headaches with TZ conversions and raid times that go past
  -- the 23:59 mark
  start_time timestamp with time zone not null,
  duration integer not null,
  observed_instances integer not null
);
