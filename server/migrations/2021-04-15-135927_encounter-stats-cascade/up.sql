alter table encounter_stats
drop constraint encounter_stats_kill_log_fkey,
add constraint encounter_stats_kill_log_fkey foreign key (kill_log) references logs(iid) on delete cascade;
