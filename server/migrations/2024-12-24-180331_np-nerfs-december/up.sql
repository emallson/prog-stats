-- Your SQL goes here
insert into encounter_hotfixes (encounter_id, application_date, regional, major, description)
values
(2922, '2024-10-15 15:00:00 GMT-08:00', true, true, array[
    'Queen Ansurek’s health reduced by 5% on Mythic difficulty.',
    'Frothy Toxin damage reduced by 26.7% on Mythic difficulty.',
    'Silken Tomb health reduced by 20% on Mythic difficulty.',
    'Wrest damage (Intermission) reduced by 25% on Mythic difficulty.',
    'Grasping Silk maximum size reduced by 10% on Mythic difficulty.',
    'Shadowgate cast time increased by 20% on Mythic difficulty.',
    'Gloom Blast damage reduced by 25% on Mythic difficulty.',
    'Acolyte’s Essence periodic damage reduced by 15% on Mythic difficulty.',
    'Essence Scarred duration reduced by 50% on Mythic difficulty.',
    'Royal Condemnation damage reduced by 10% on Mythic difficulty.',
    'Summoned Acolyte health and Dark Barrier shield amount reduced by 15% on Mythic difficulty',
    'Royal Shackle health reduced by 15% on Mythic difficulty.',
    'When calculating applications of Frothy Toxin, only living players are now counted towards the maximum possible application amount on Heroic and Mythic difficulties.',
    'Frothy Toxin damage reduced by 15% on Heroic difficulty.',
    'Gloom Blast damage reduced by 15% on Heroic difficulty.'
]),
(2921, '2024-10-15 15:00:00 GMT-08:00', true, true, array[
    'Mote duration before explosion increased to 20 seconds (was 15 seconds).',
    'Void Degeneration and Burning Rage now kill the player at 4 stacks (was 3).',
    'Fixed an issue with Anub''arash not casting Spike Storm if in the very middle of the room when it burrows.'
]),
(2922, '2024-10-29 15:00:00 GMT-08:00', true, false, array[
    'Reduced the number of players targeted by Web Blades to 3 on Heroic difficulty (was 4).',
    'The first cast of Silken Tomb in the final cycle has been removed on Heroic difficulty.',
    'The timing of the first Web Blades cast has been adjusted to accommodate the removal of the preceding Silken Tomb in the final cycle on Heroic difficulty.',
    'Grasping Silk periodic damage reduced by 25% on Heroic difficulty.',
    'Gloom Blast damage reduced by 15% on Heroic difficulty.'
]),
(2921, '2024-11-06 15:00:00 GMT-08:00', false, false, array[
    'Fixed issue where Stinging Swarm will fizzle on the player if both Anti-Magic Shell and Prismatic Barrier are active.',
    'Fixed an issue causing Oppressing Roar to extend the duration of Stinging Delirium on Takazj.'
]),
(2921, '2024-12-10 15:00:00 GMT-08:00', true, true, array[
    'Stinging Swarm now releases only 4 Swarms (was 5), and having 4 stacks of Stinging Swarm now triggers Stinging Delirium on Mythic difficulty.',
    'Stinging Swarm damage taken percent per stack increased to 25% (was 20%) on Mythic difficulty.',
    'Decreased Shattershell Scarab melee damage by 15% on Mythic difficulty.',
    'Increased number of stacks of Void Degeneration and Burning Touch that trigger Touch of Death to 5 (was 4) on Mythic difficulty.',
    'Being damaged by Web Bombs no longer create Motes on Mythic difficulty.'
]);
