-- Your SQL goes here
create table auth_codes (
  id serial primary key,
  access_token text not null,
  refresh_token text not null,
  expiration timestamp with time zone not null,
  team int not null references teams(id),
  zone int not null,
  unique(team, zone)
);
