-- Your SQL goes here
insert into encounter_hotfixes (encounter_id, application_date, regional, major, description)
values
  (2407, '2021-03-25 15:00:00 GMT-08:00', false, false, array['Fixed an issue where players who fell off the platform could Neutralize their allies on Mythic difficulty.']),
  (2399, '2021-03-22 15:00:00 GMT-08:00', true, true, array[
    'Fractured Debris missile travel time increased to 2.5 seconds (was 1.5s) on Mythic difficulty.',
    'The impact locations of Fractured Debris should now be more visible onf Mythic difficulty.',
    'Destructive Stomp radius decreased to 16 yards (was 20 yards).',
    'Chain Link range increased to 14 yards (was 12 yards).'
    ]),
  (2417, '2021-03-22 15:00:00 GMT-08:00', true, true, array[
    'Volatile Stone Shell''s absorb channel increased to 5 seconds (was 4 seconds).',
    'Wicked Blast damage decreased by 15% on Mythic difficulty.',
    'Wicked Lacerations damage decreased by 10% on Mythic difficulty.',
    'Anima Orb, Volatile Anima Infusion, and Volatile Anima Infection now last 20 seconds (was 15, 16, and 16 seconds, respectively).',
    'Volatile Anima Detonation radius decreased to 40 yards (was 45 yards).',
    'Soultaint Effigy now stacks to 80% healing reduction (was 100%).',
    'Stonegale Effigy movement force decreased by 20%.'
    ])
