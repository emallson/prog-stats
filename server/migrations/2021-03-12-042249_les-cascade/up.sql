-- Your SQL goes here
alter table log_encounter_stats
drop constraint log_encounter_stats_team_id_fkey,
add constraint log_encounter_stats_team_id_fkey foreign key (team_id) references teams(id) on delete cascade;
