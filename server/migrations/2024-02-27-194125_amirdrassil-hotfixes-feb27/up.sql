-- Your SQL goes here
insert into encounter_hotfixes (encounter_id, application_date, regional, major, description)
values
(2677, '2024-02-01 15:00:00 GMT-08:00', true, false, array[
  'The Flamebound and Shadowbound intermission debuffs are no longer private auras.'
]),
(2677, '2024-02-06 15:00:00 GMT-08:00', true, true, array[
  'Reduced the number of Aflame targets from 4 to 3 on Mythic difficulty.',
  'Fyrakk health reduced by 10% on Mythic difficulty.',
  'Burning and Dark Colossus health reduced by 10% on Mythic difficulty.',
  'Spirit of the Kaldorei health reduced by 20% on Mythic difficulty.',
  'Darnassian Ancient health reduced by 20% on Mythic difficulty.',
  'The Heart of Amirdrassil now begins stage two with 60% health remaining.',
  'Reduced the number of Corrupted Seeds created from 2 to 1.'
]),
(2786, '2024-02-27 15:00:00 GMT-08:00', true, true, array[
'Tindral''s health reduced by 10% on Mythic difficulty.',
'Fiery Vines'' health reduced by 10% on Mythic difficulty.'
]);
