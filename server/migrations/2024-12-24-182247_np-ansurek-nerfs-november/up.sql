-- Your SQL goes here
insert into encounter_hotfixes (encounter_id, application_date, regional, major, description)
values
(2922, '2024-11-12 15:00:00 GMT-08:00', true, true, array[
    'Number of Web Blades targets reduced to 3 (was 4).',
    'Grasping Silk damage reduced by 25%.',
    'Number of Reactive Toxins applied per cycle no longer increases beyond 3 (was up to 4).',
    'Removed second Silken Tomb cast from final cycle of the phase, and adjusted the timings of other abilities in this final cycle to accommodate this change.',
    'Silken Tomb health reduced by 30%.',
    'Frothy Toxin damage reduced by 23%.',
    'Shadowy Distortion [Shadowgate debuff] duration reduced to 30 seconds (was 3 minutes).',
    'Gloom Blast damage reduced by 20%.',
    'Number of Summoned Acolytes per cycle no longer increases beyond 4 (was up to 5).',
    'Removed the first cast of Royal Condemnation from the final cycle of the phase, and adjusted the timings of some other abilities in this final cycle to accommodate this change.',
    'Removed Web Blades casts that overlapped with applications of Abyssal Infusion.'
]);
