-- this includes the upcoming tindral changes
insert into encounter_hotfixes (encounter_id, application_date, regional, major, description)
values
(2824, '2023-11-15 15:00:00 GMT-08:00', false, false, array[
  'Reduced the duration of Smoldering Ground puddles to 2.5 minutes (was 5 minutes) on Raid Finder difficulty.'
  ]),
(2737, '2023-11-15 15:00:00 GMT-08:00', false, false, array[
  'Fixed an issue that caused Shadowstrike to teleport Subtlety Rogues into the lava.'
  ]),
(2677, '2023-11-16 15:00:00 GMT-08:00', false, false, array[
'Burning Scales damage increased by 40%.',
'There is now an additional occurrence of Blaze during Stage Three.',
'Blaze damage increased by 40%.',
'Damage of colliding with a Swirling Firestorm increased by 60%.'
  ]),
(2728, '2023-11-17 15:00:00 GMT-08:00', false, false, array[
'Fixed a rare timing issue where the Council members could finish casting Rebirth when the encounter should have ended.'
  ]),
(2731, '2023-11-21 15:00:00 GMT-08:00', false, false, array[
    'Reduced the number of Flash Fires that target players each cast by 1 on Heroic difficulty.'
  ]),
(2677, '2023-11-21 15:00:00 GMT-08:00', false, false, array[
    'The extra action button that allows players to drop Seeds of Amirdrassil should now be more responsive.'
  ]),
(2708, '2023-11-27 15:00:00 GMT-08:00', false, false, array[
    'Fixed an issue where Cycle Wardens were not correctly scaling for larger raid sizes on Normal difficulty.'
  ]),
(2728, '2023-11-28 15:00:00 GMT-08:00', false, false, array[
    'Addressed an issue causing Barreling Charge to hit players standing closely behind Urctos.'
  ]),
(2677, '2023-12-01 15:00:00 GMT-08:00', false, false, array[
    'Resolved an issue causing Fyrakk to become unlootable if killed during Shadowflame Devastation in Stage Two.',
    'Resolved an issue causing Fyrakk''s energy bar to increase too quickly in Stage Three on Mythic difficulty.'
  ]),
(2786, '2023-12-04 15:00:00 GMT-08:00', false, false, array[
'Fixed an issue where multiple players can benefit from the same Fallen Feather buff.',
'Fixed an issue where Diffuse Magic can cause Tindral to be inflicted with Fiery Growth.',
'Fixed an issue where Dream Essence does not get removed if Tindral finishes casting Supernova during intermission.',
'Ride Along disabled on players with the Fallen Feather debuff on Mythic difficulty.'
  ]),
(2786, '2023-12-05 15:00:00 GMT-08:00', true, true, array[
'Seed of Flame duration increased to 5 seconds (was 3 seconds) in phase two on Mythic difficulty.',
'Falling Star damage reduced by 15% and Fallen Feathers will now spawn around the boss (was spawning behind the boss) on Mythic difficulty.'
  ]),
(2677, '2023-12-05 15:00:00 GMT-08:00', true, true, array[
    'Blaze now hits fewer targets on Mythic difficulty.',
    'Fyrakk no longer hits all targets with Aflame when casting Incarnate on Mythic difficulty.',
    'Searing Screams damage increase reduced to 100% (was 300%) on Mythic difficulty.',
    'Reduced the health of Burning Colossus and Dark Colossus by 15% on Mythic difficulty.'
  ]),
(2786, '2023-12-06 15:00:00 GMT-08:00', false, false, array[
    'Fallen Feathers now spawn 6 feathers on Mythic Difficulty (was 4).'
  ]),
(2824, '2023-12-06 15:00:00 GMT-08:00', false, false, array[
    'Fixed an issue where players would sometimes gain multiple stacks of Ignited Essence from 1 Living Flame.',
    'Fixed an issue where a Seeking Inferno could explode twice on Mythic difficulty.'
  ]),
-- not sure if this was regional. hotfix doesn't list it as such but it is the kind of patch that is *usually* regional
(2786, '2023-12-12 15:00:00 GMT-08:00', true, true, array[
    'Fire Beam number of beam spawns per pulse reduced to 6 (was 8).',
    'Fiery Growth duration increased to 20 seconds (was 16 seconds) in Mythic difficulty.',
    'Lingering Cinder duration reduced to 2 seconds (was 3 seconds).',
    'Pulsing Heat damage reduced by 33%.',
    'Fiery Vines health reduced by 7% in Mythic difficulty.',
    'Addressed an issue where Mass Entanglement can fail to root tank players. [note: this causes all but 2 tanks to get rooted in cases where more than 2 tanks are present]',
    'Addressed a performance issue that caused a server delay at the start of the encounter.'
  ]),
-- also not sure if these are regional, but same deal
(2677, '2023-12-19 15:00:00 GMT-08:00', true, true, array[
    'Removed the first instance of Blaze from intermission on Mythic difficulty.',
    'Increased the cast time of Searing Screams to 4 seconds (was 2) on Mythic difficulty.',
    'Reduced the health of Screaming Souls by 20% on Mythic difficulty.'
  ]),
(2824, '2023-12-19 15:00:00 GMT-08:00', true, true, array[
    'Fixed an issue with Seeking Infernos that allowed threat information to be generated from them.',
    'Reduced the number of Seeking Infernos that spawn each set to 3 (was 4).',
    'Reduced the number of Flame Waves that spawn from Overheated targets to 4 (was 5) on Mythic difficulty.'
  ]),
(2677, '2024-01-05 15:00:00 GMT-08:00', false, false, array[
'Aflame now targets fewer players.',
'Screaming Soul health reduced for smaller group sizes.',
'Spirit of the Kaldorei health reduced for smaller group sizes.',
'Blaze damage reduced for smaller group sizes.',
'Corrupt absorb value reduced for smaller group sizes.'
  ]),
(2786, '2024-01-16 15:00:00 GMT-08:00', true, true, array[
    'Removed a cast of Fire Beam in phase 1 on Mythic difficulty.',
    'Flaming Germination’s damage reduced by 25%.',
    'The number of Flare Bombs reduced to 3 (was 4) and Empowering Flame’s damage increased by 35%.',
    'Germinating Aura now prevents players from destroying a seed, but no longer spawns a Flaming Tree.',
    'The number of seeds spawned is now reduced by 4.'
  ]);
