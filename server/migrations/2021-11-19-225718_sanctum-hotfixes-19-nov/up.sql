-- Your SQL goes here
insert into encounter_hotfixes (encounter_id, application_date, regional, major, description)
values
(2435, '2021-09-20 15:00:00 GMT-08:00', true, true, array[
  'Domination Arrows health reduced by 15% on Mythic difficulty.',
  'Dark Sentinels health reduced by 15% on Mythic difficulty.',
  'Curse of Lethargy base duration reduced to 4 seconds on Mythic difficulty (was 6 seconds).',
  'Decrepit Orb Detonation damage reduced by 10% on Mythic difficulty.',
  'Veil of Darkness radius during Phase 2 reduced to 12 yards on Mythic difficulty (was 15 yards).',
  'Banshee''s Fury unavoidable damage reduced by 15% on Mythic difficulty.',
  'Ranger''s Heartseeker, Banshee''s Heartseeker, and Banshee''s Blades damage reduced by 8% on Mythic difficulty.',
  'Resolved an issue which could cause Ruin to unintentionally fail to inflict damage.'
]),
(2422, '2021-09-20 15:00:00 GMT-08:00', true, true, array[
  'Kel''Thuzad health reduced by 5% on Mythic difficulty.',
  'Remnant of Kel''Thuzad health reduced by 20% on Mythic difficulty.',
  'Unstoppable Abomination health reduced by 20% on Mythic difficulty.',
  'Soul Reaver health reduced by 20% on Mythic difficulty.',
  'Frozen Destruction''s initial damage reduced by 40% on Mythic difficulty.',
  'Frozen Destruction''s damage over time effect reduced by 10% and the duration reduced to 8 seconds (was 10 seconds) on Mythic difficulty.',
  'Fixed an issue where Kel''Thuzad would cast abilities during Howling Blizzard on Mythic difficulty.',
  'Howling Blizzard now inflicts stacking damage over the duration of the channel on Mythic difficulty.'
  ]),
(2431, '2021-09-20 15:00:00 GMT-08:00', true, false, array[
  'Updated the rune locations to be more consistent with clockwise rotations and slightly less demanding with counterclockwise rotations on Normal and Heroic difficulties.',
  'Players now have an additional 10 seconds on Raid Finder difficulty, 10 seconds on Normal difficulty, and 5 seconds on Heroic difficulty to realign their fate.'
  ]),
(2430, '2021-09-20 15:00:00 GMT-08:00', true, true, array[
  'Reduced the distance between floor spike gaps during Stage Two on Mythic difficulty.',
  'Shadowsteel Chains periodic damage reduced by 15% on Mythic difficulty.'
  ]),
(2430, '2021-09-22 15:00:00 GMT-08:00', false, false, array[
  'Fixed an issue causing players to be hit by two floor spikes in quick succession when moving through them at very high speeds.'
  ]),
(2435, '2021-09-22 15:00:00 GMT-08:00', false, false, array[
  'Implemented additional performance improvements to the beginning of the encounter.'
  ]),
(2422, '2021-09-27 15:00:00 GMT-08:00', false, true, array[
  'Howling Blizzard ice patches are now immediately removed when pushing to his Phylactery Phase.'
  ]),
(2422, '2021-09-28 15:00:00 GMT-08:00', false, false, array[
  'Fixed an issue where Howling Blizzard ice patches would still explode after entering the Phylactery Phase.'
  ]),
(2430, '2021-10-01 15:00:00 GMT-08:00', false, false, array[
  'Fixed an issue causing Painsmith to sometimes stop and cast the weapon throw of Cruciform Axe, Reverberating Hammer, and Dualblade Scythe during his transition.',
  'Fixed an issue causing Shadowsteel Chains to persist into Stage Two if players were affected by Podtender (Dreamweaver Soulbind) during the transition.'
  ]),
(2433, '2021-09-22 15:00:00 GMT-08:00', false, false, array['Fixed an issue that sometimes caused Annihilating Glare to be cast on transition into Phase 2.']),
(2435, '2021-10-22 15:00:00 GMT-08:00', false, false, array['Fixed an issue where the Death Knives warning aura could be accidentally removed by immunity effects, causing incalculable chaos.']),
(2432, '2021-11-01 15:00:00 GMT-08:00', true, true, array[
  'Remnant of Ner''zhul health reduced by 5% on Mythic difficulty.',
  'Orb of Torment health reduced by 10% on Mythic difficulty.',
  'Orbs of Torment should now trigger kill credit effects for abilities like Warlock''s Shadowburn.'
  ]),
(2434, '2021-11-01 15:00:00 GMT-08:00', true, true, array[
  'Unleashed Tyranny damage reduced by 15% on Mythic difficulty.',
  'Mawsworn Agonizer and Mawsworn Overlord health reduced by 5% on Mythic difficulty.'
  ]),
(2430, '2021-11-01 15:00:00 GMT-08:00', true, true, array[
  'Spiked Balls have been removed from the first intermission on Mythic difficulty.'
  ]),
(2435, '2021-11-01 15:00:00 GMT-08:00', true, false, array[
  'Reduced the damage of Ranger Shot by 15% on all difficulties.',
  'Reduced the damage of Haunting Wave by 33% on Heroic difficulty.',
  'Reduced the damage of Domination Chains by 15% on Heroic difficulty.',
  'Reduced the radius of Veil of Darkness during Stage 2 to 8 yards on Heroic difficulty (was 10 yards).'
  ]),
(2435, '2021-11-09 15:00:00 GMT-08:00', false, false, array[
  'Fixed an issue that could cause Veil of Darkness to persist on players after encounter completion.',
  'Fixed an issue where Crushing Dread would remain even when the caster was no longer present.',
  'Fixed a rare issue which could cause Sylvanas to cast Wailing Arrow after casting Raze before returning to the fight arena.'
  ]);
