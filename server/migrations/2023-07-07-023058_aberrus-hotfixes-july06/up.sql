insert into encounter_hotfixes (encounter_id, application_date, regional, major, description)
values
    -- kazzara
    (2688, '2023-06-20 15:00:00 GMT-08:00', false, true, array[
        'Health reduced by 10% on Mythic difficulty.',
        'Infernal Fusion damage reduced by 10%.'
    ]),
    -- forgotten experiments
    (2693, '2023-06-06 15:00:00 GMT-08:00', true, false, array[
        'Reduced Thadrion''s health on lower group sizes on Heroic difficulty.'
    ]),
    -- rashok
    (2680, '2023-06-06 15:00:00 GMT-08:00', true, false, array[
        'Reduced the number of missiles from Doom Flames on non-Mythic difficulties.',
        'Scorching Heatwave damage now scales with group size for non-Mythic difficulties.'
    ]),
    -- zskarn
    (2689, '2023-06-27 15:00:00 GMT-08:00', TRUE, TRUE, array[
        'Dragonfire Traps now activate consistently based on Zskarn''s location. [ed: No more random traps]',
        'Elimination Protocol damage reduced by 20%.',
        'Elimination Protocol damage amplification reduced to 300% (was 500%).',
        'Salvage Parts duration increased to 120 seconds (was 60 seconds).'
    ]),
    (2689, '2023-06-13 15:00:00 GMT-08:00', true, false, array[
        'Blast Wave damage reduced by 10% on Normal and Heroic difficulties.',
        'Dragonfire Traps is now magic dispelable on Heroic difficulty.',
        'Dragonfire Traps activation time increase to 5 seconds on Normal difficulty and 4 seconds on Heroic difficulty (was 3 seconds).',
        'Added visuals to Dragonfire Traps prior to activation on all difficulties.'
    ]),
    (2689, '2023-06-07 15:00:00 GMT-08:00', true, false, array[
        'Zskarn health reduced by 10% for small group sizes on Normal and Heroic difficulties.',
        'Shrapnel Bomb initial damage and DoT reduced by 50% on Normal and Heroic difficulties.',
        'Searing Claws damage reduced by 25% on Normal and Heroic difficulties.',
        'Unstable Embers damage reduced by 30% on Heroic difficulty.'
    ]),
    -- magmorax
    -- ommitted achievement hotfix
    -- echo of neltharion
    (2684, '2023-06-07 15:00:00 GMT-08:00', false, true, array[
        'Calamitous Strike''s debuff duration reduced from 40 seconds to 37 seconds on all difficulties. This is done to remove a problematic overlap in phase 1.',
        'Calamitous Strike''s debuff will now be removed when the encounter enters intermission on all difficulties.',
        'Fixed an issue where Neltharion immediately melee hits targets after Calamitous Strike on all difficulties.',
        'Added a slight delay before the same player can trigger an additional stack of Shatter after destroying Twisted Earth on all difficulties.',
        'Volcanic Heart''s radius reduced from 25 yards to 20 yards on Mythic difficulty.'
    ]),
    (2684, '2023-06-06 15:00:00 GMT-08:00', false, false, array[
        'Slightly reduced Neltharion''s health for smaller groups.',
        'Volcanic Heart''s initial damage reduced on Normal and Heroic difficulty.',
        'Echoing Fissures''s damage now scales with group size on LFR, Normal, and Heroic difficulty.',
        'Slightly reduced the Twisted Aberration''s health for smaller groups.'
    ]),
    -- sarkareth
    (2685, '2023-06-20 15:00:00 GMT-08:00', false, true, array[
        'Lowered the damage from Scorching Detonation by 10% on all difficulties.',
        'Lowered the damage from Oblivion by 10% on all difficulties. [ed: The actual reduction was 33%. It was not changed to 10%]',
        'Updated the visual clarity for Drifting Embers.',
        'Fixed a bug that caused players to take extra durability loss when falling off the platform.'
    ]),
    (2685, '2023-06-14 15:00:00 GMT-08:00', false, false, array[
        'Motes of Oblivion should be more visible against the ground.'
    ]),
    (2685, '2023-06-08 15:00:00 GMT-08:00', false, false, array[
        'The contribution of damage towards Blazing Blast and Void Blast is now properly reduced by absorbs.'
    ]),
    (2685, '2023-06-07 15:00:00 GMT-08:00', true, false, array[
        'Health of Sarkareth and Null Glimmers reduced by 10% on 10-player Normal/Heroic difficulties.',
        'Duration of Astral Flare buff increased from 23 seconds to 30 seconds in 10 player raids.',
        'Oblivion damage per tick reduced by 50% in 10 player raids.',
        'Void Surge damage per tick reduced by 40% in 10 player raids.',
        'Dread raid wide damage reduced by 25% in 10 player raids.'
    ]);