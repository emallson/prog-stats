-- Your SQL goes here
create table release_dates (
  zone integer primary key,
  release_date timestamp with time zone not null
);

insert into release_dates values (24, '2020-07-09'), (25, '2020-01-14'), (26, '2020-12-08');
