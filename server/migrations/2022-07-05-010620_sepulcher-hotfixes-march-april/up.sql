insert into encounter_hotfixes (encounter_id, application_date, regional, major, description)
values
-- Vigilant Guardian
(2512, '2022-03-08 21:30:00 GMT-05:00', false, true, array[
    'Force Field now reduces damage taken by exposed core by 90% on Mythic Difficulty (was 80%).',
    'Slightly lowered the spawn rate of Volatile Materium and Pre-Fabricate on Mythic difficulty.',
    'Reduced the damage of Radioactive Core by 12% on Mythic difficulty.',
    'Reduced the damage of Fractured Core by 15% on Mythic difficulty.'
]),
(2512, '2022-03-09 15:00:00 GMT-08:00', false, true, array[
    'Vigilant Guardian health reduced by 10% on Mythic difficulty.',
    'Spike of Creation damage reduced by 50% on Mythic difficulty.',
    'Force Field is now cancelled 3 seconds after Exposed Core completes.',
    'Pneumatic Impact now resets Vigilant Guardian''s melee swing timer.',
    'Point Defense Drones should no longer cast Blast when the Vigilant Guardian is channeling Exposed Core.',
    'Blast range reduced to 40 yards (was 100 yards).'
]),
(2512, '2022-03-15 15:00:00 GMT-08:00', false, false, array[
    'Spawn rates for the Volatile Materium and Pre-Fabricated Sentries are less aggressive on Mythic difficulty.',
    'Increased the grace period before Exposed Core where Refracted Blast and Deresolution won''t be cast on all difficulties.'
]),
(2512, '2022-03-17 15:00:00 GMT-08:00', false, false, array[
'Fixed an issue that would break player’s ability to pick up the Unstable Orbs after they had used certain player abilities or mounts.'
]),
(2512, '2022-03-18 15:00:00 GMT-08:00', false, false, array[
'Point Defense Drones will now cancel Searing Ablation while shutting down during the Exposed Core sequence.'
]),
(2512, '2022-03-21 15:00:00 GMT-08:00', false, false, array[
'Fixed an issue that caused Mistweaver Monk’s Crane Adepts summoned by Fallen Order (Venthyr Ability) to fail to cast Enveloping Mist immediately after they spawn during the encounter.'
]),
(2512, '2022-03-23 15:00:00 GMT-08:00', false, false, array[

    'Fixed an issue that could cause the Vigilant Guardian to unintentionally reset.'

]),
(2512, '2022-03-29 15:00:00 GMT-08:00', false, false, array[
'Fixed an issue that could prevent players from picking up unstable cores.'
]),
(2512, '2022-03-30 15:00:00 GMT-08:00', false, false, array[

    'Fixed an issue where Unstable Orbs would fail to detonate and become non-interactive if a player attempted to detonate them in mid-air.',
    'Fixed an issue that incorrectly gave players an unusable extra action button during the encounter.'

]),
(2512, '2022-04-08 15:00:00 GMT-08:00', false, false, array[
'Fixed an issue where Dancing Rune Weapon would not correctly mimic a Blood Death Knight’s ability while in combat with the Vigilant Guardian.'
]),
-- Skolex
(2542, '2022-03-29 15:00:00 GMT-08:00', true, true, array[

    '[With realm restarts] Skolex''s health has been reduced by ~6% in Mythic and Heroic difficulties.'

]),
-- Xy'mox
(2553, '2022-03-01 15:00:00 GMT-08:00', false, false, array[
'Reduced the damage of Hyperlight Sparknova by 30% on all difficulties below Mythic.',
'Artificer Xy''mox now uses abilities at a slightly less frantic pace while below 30% health.',
'The visual of Genesis Rings now better matches the effect.'
]),
(2553, '2022-03-02 15:00:00 GMT-08:00', false, false, array[
'Xy Spellslingers will no longer target the same player simultaneously.',
'Fixed an issue where defeating Xy reinforcements could fail to interrupt Decipher Relic.'
]),
(2553, '2022-03-03 15:00:00 GMT-08:00', false, false, array[
    'Health of Xy Acolytes reduced by 15% on Raid Finder, Normal, and Heroic difficulties.',
    'Resolved an issue where Interdimensional Wormholes could despawn while Genesis Rings were active when Xy''mox was below 30% health.',
    'Reduced the pacing of the encounter by 15% on Normal and Heroic difficulty.',
    'Improved the visual clarity of Gunship Barrage.',
    'Reduced the radius effect of Hyperlight Sparknova.'
]),
(2553, '2022-03-07 15:00:00 GMT-08:00', false, false, array[
'Fixed issue where the visual effect of Xy Acolyte’s Massive Blast rendered smaller than the actual area of effect.'
]),
(2553, '2022-03-08 21:30:00 GMT-05:00', false, false, array[
    'Reduced the energy regeneration rate of Xy Acolytes on Raid Finder and Normal difficulty.'
]),
(2553, '2022-03-09 15:00:00 GMT-08:00', false, false, array[
 'Genesis Rings initial hit now only occurs once, and the lingering additional damage reduced by 50% on Raid Finder difficulty.',
 'The encounter''s enrage timer has been increased to 10 minutes on Mythic difficulty (was 7 minutes).'
]),
(2553, '2022-03-15 15:00:00 GMT-08:00', false, true, array[
    'Artificer Xy’mox health reduced by 15% on Mythic difficulty.',
    'Ancient Exhaust now properly deals damage on the final platform.'
]),
(2553, '2022-03-17 15:00:00 GMT-08:00', false, false, array[
'Fixed and issue where the visual effect of Massive Blast did not accurately portray its damaging area.'
]),
(2553, '2022-03-22 15:00:00 GMT-08:00', false, false, array[
'Players affected by Spirit of Redemption will no longer trigger Interdimensional Wormholes during the encounter.'
]),
(2553, '2022-03-29 15:00:00 GMT-08:00', false, false, array[

    'Artificer Xy’mox health reduced by up to 10% based on raid size (10% at 10-player, 0% at 30-player) on Normal and Heroic difficulty.',
    'Reduced Genesis Rings initial and periodic damage by 40% on Normal difficulty.'

]),
-- Halondrus
(2529, '2022-03-01 15:00:00 GMT-08:00', false, false, array[
'Reduced the amount of damage absorption provided by Reclaim on Normal and Heroic.',
'Reduced the amount of damage Reclaim gains periodically to 25% of its initial value on Normal and Heroic (was 50%).',
'Reduced the base speed of Ephemeral Motes by 20% on Heroic.'
]),
(2529, '2022-03-07 15:00:00 GMT-08:00', false, false, array[
    'Fixed an issue preventing Theotar the Mad Duke’s Token of Appreciation ability (Venthyr Soulbind) from functioning during the encounter.',
    'Fixed an issue where Priest’s Unholy Nova (Necrolord Ability) was unusable on Halondrus except in specific spots.'
]),
(2529, '2022-03-08 21:30:00 GMT-05:00', false, false, array[
    'Fixed an issue causing players to be unable to mount in the Shimmering Cliffs.',
    'Fixed an issue causing rogues to be unable to use Shadowstep against Halondrus.'
]),
(2529, '2022-03-12 15:00:00 GMT-05:00', false, true, array[
'Reduced HP by 10% on Mythic difficulty.',
'Planet Cracker spawn locations are no longer as variable.',
'Crushing Prism is now less likely to target ranged players.'
]),
(2529, '2022-03-15 15:00:00 GMT-08:00', false, false, array[
    'Reclaim initial damage reduced by 10% on Raid Finder, Normal, and Heroic difficulty.',
    'Decreased additional speed Ephemeral Motes gain from Eternity Overdrive by 50% on Raid Finder, Normal, and Heroic difficulty.',
    'Eternity Overdrive scaling damage reduced on Raid Finder, Normal, and Heroic difficulty.',
    'Earthbreaker Missiles damage reduced by 15% on Raid Finder and Normal difficulty.',
    'Fixed an issue causing Lightshatter Beam''s periodic effect to inflict the incorrect amount of damage.'
]),
(2529, '2022-03-17 15:00:00 GMT-08:00', false, false, array[
'Fixed an issue that prevented Brewmaster Monk’s Invoke Niuzao, the Black Ox from auto-casting Stomp.'
]),
(2529, '2022-03-29 15:00:00 GMT-08:00', false, true, array[

    'Health reduced by 10% on Mythic difficulty.',
    'Volatile Charges no longer trigger a Charge Detonation when coming into contact with Ephemeral Motes or Lightshatter Beam on Mythic difficulty.',
    'Fixed an issue causing Druid’s Mount Form to interact with Volatile Charges in unintended ways.',
    'Ephemeral Motes base movement speed reduced on Heroic difficulty.',
    'Number of Ephemeral Motes in Stage 3 reduced on Heroic difficulty.'

]),
(2529, '2022-04-26 15:00:00 GMT-08:00', true, true, array[
    'Time required for Volatile Charges to trigger a Charge Detonation increased to 30 seconds (was 10 seconds) in Mythic difficulty.',
    'Charge Exposure duration increased to 40 seconds (was 16 seconds) in Mythic difficulty.',
    'Earthbreaker Missile damage reduced by 20% in Mythic difficulty.',
    'Changed the pattern of Planetcracker Beam during Stage Three in Mythic difficulty.',
    'Reduced Halondrus’ health by 10% in Mythic difficulty.',
    'Reduced damage of Eternity Engine by 10% in Mythic difficulty.'

]),
-- Dausegne
(2540, '2022-03-16 15:00:00 GMT-08:00', false, false, array[
'Fixed an issue that could cause the visual effect for Disintegration Nova to appear larger or smaller based on character size on Mythic difficulty.'
]),
(2540, '2022-03-29 15:00:00 GMT-08:00', true, false, array[

    '[With realm restarts] Dausegne’s health has been reduced by ~6% in Heroic difficulty.',
    '[With realm restarts] Disintegration Halo vulnerability reduced to 200% in Normal and Raid Finder difficulties.'

]),
-- Pantheon
(2544, '2022-03-03 15:00:00 GMT-08:00', false, false, array[
    'Burden of Sin damage reduced by 25% on all difficulties except Mythic.',
    'Added more time before Bastion''s Ward dispel zones despawn upon transitioning between phases.',
    'The Prototype Pantheon now pause in their place when beginning Complete Reconstruction and will remain there until all members of the Pantheon are defeated.',
    'Improved the visual effect for Pinning Weapon.',
    'Fixed an issue where Complete Reconstruction could fail to apply to a Pantheon member.',
    'Holy Priest’s Guardian Angel (Talent) will now correctly reduce the cooldown of Guardian Spirit when cast on the Withering Seeds.'
]),
(2544, '2022-03-07 15:00:00 GMT-08:00', false, false, array[
    'Fixed an issue where Humbling Strikes could critically strike.',
    'Updated the Dungeon Journal entry for Bastion''s Ward to accurately reflect the damage reduction provided to Runecarver''s Deathtouch.'
]),
(2544, '2022-03-08 21:30:00 GMT-05:00', false, false, array[
'Withering Seeds now spawn nearby Necrotic Ritualists on all difficulties.'
]),
(2544, '2022-03-09 15:00:00 GMT-05:00', false, false, array[
     'Bastion''s Ward should no longer spawn atop Necrotic Ritualists.'
]),
(2544, '2022-03-15 15:00:00 GMT-08:00', false, false, array[
    'Burden of Sin damage reduced by 20% on Normal and Heroic difficulty.',
    'Resolved an issue where Withering Seeds could fail to cast.',
    'Adjusted an unfair timing of Hand of Destruction and Night Hunter in the final phase.'
]),
(2544, '2022-03-29 15:00:00 GMT-08:00', false, true, array[

    'Health reduced by 10%.',
    'Members of the Prototype Pantheon now retreat at 40% health during Stage 1 and Stage 2.',
    'Sinful Projections has been removed from Normal difficulty.'

]),
-- Lihuvim
(2539, '2022-03-08 15:00:00 GMT-08:00', true, false, array[
       '[With realm restarts] The first cast of Synthesize now occurs much earlier in the encounter.',
       '[With realm restarts] Reduced the time between casts of Synthesize slightly.',
       '[With realm restarts] The damage reduction granted by Protoform Disalignment has been reduced to 50% on Normal difficulty and 20% on Heroic difficulty (was 90%).',
       '[With realm restarts] The damage inflicted by Cosmic Shift has been increased by 50% on Heroic Difficulty.',
       '[With realm restarts] The damage inflicted when an Unstable Mote is created has been increased by approximately 25% on Heroic Difficulty.',
       '[With realm restarts] Damage inflicted by Deconstructing Energy has been increased by 50% on Heroic Difficulty.'
]),
(2539, '2022-03-16 15:00:00 GMT-08:00', false, false, array[
'Protoform Radiance now has a duration of 10 seconds on Normal difficulty.'
]),
(2539, '2022-04-08 15:00:00 GMT-08:00', false, false, array[

    'Deconstructing Blast should now be cast more reliably on all difficulties.'

]),
-- Anduin
(2546, '2022-03-01 15:00:00 GMT-08:00', false, false, array[
'The healing absorbed by Anduin''s Befouled Barrier has been reduced by 25% on Normal difficulty and 30% on Heroic difficulty.'
]),
(2546, '2022-03-02 15:00:00 GMT-08:00', false, false, array[
    'Hopebreaker damage over time reduced by 10% on Heroic difficulty.',
    'Befouled Barrier requires 25% less healing on Heroic and Mythic difficulty.',
    'Befouled Barrier requires 50% less healing on Raid Finder and Normal difficulty.',
    'Reduced the health of Grim Reflections by 20% on all difficulties except Mythic.',
    'Reduced Beacon of Hope’s Purging Light damage by 20% on Heroic difficulty.',
    'Fixed an issue where Rain of Despair was able to damage pets.'
]),
(2546, '2022-03-03 15:00:00 GMT-08:00', false, false, array[
    'Anduin''s health reduced by 5% on Heroic difficulty.',
    'Remorseless Winter damage reduced by 10% on Normal and Heroic difficulty.',
    'Fiendish Soul Health reduced by 25% on Normal and Heroic difficulty.',
    'Grim Reflection health reduced by 10% on Normal and Heroic difficulty.',
    'Befouled Barrier requires 10% less healing on Heroic difficulty.',
    'Befouled Barrier requires 20% less healing on Normal difficulty.'
]),
(2546, '2022-03-08 15:00:00 GMT-08:00', true, false, array[
'[With realm restarts] If Anduin Wrynn reaches 10% health in non-Mythic difficulty before starting the final phase of the encounter, he will now immediately begin the final phase.'
]),
(2546, '2022-03-08 21:30:00 GMT-05:00', false, false, array[
    '3 Grim Reflections are now summoned on Heroic difficulty (was 4).',
    'Monstrous Soul Health reduced by 5% on Heroic difficulty.',
    'Monstrous Soul’s Necrotic Detonation can be cast while moving on all difficulties.',
    'Befouled Barrier absorb amount reduced by 25% on Normal and Heroic difficulty.',
    'Remorseless Winter damage reduced by 20% on Normal and Heroic difficulty.',
    'Holy Priest’s healing Anduin’s Hope with Guardian Spirit active should now reduce the cooldown of Guardian Spirit.',
    'Fixed an issue causing Soul Explosion to hit pets.'
]),
(2546, '2022-03-15 15:00:00 GMT-08:00', false, false, array[
    'Anduin Wrynn''s health reduced by 5% on Normal and Heroic difficulty.',
    'Anduin''s Despair health reduced by 25% on Normal and Heroic difficulty.',
    'Remnant of the Fallen King''s Army of the Dead summons a Monstrous Soul once per intermission on Heroic difficulty.',
    'Wicked Star damage reduced by 20% on Normal and Heroic difficulty.',
    'The targeting visual effect for Rain of Despair has been updated.'
]),
(2546, '2022-03-17 15:00:00 GMT-08:00', false, true, array[
    'Remorseless Winter damage reduced by 20% on Mythic difficulty.',
    'Anduin’s Hope health reduced by 15% on Mythic difficulty.',
    'Anduin’s Despair health reduced by 15% on Mythic difficulty.',
    'Increased the amount of time it takes for Remnant of a Fallen King to reach maximum energy by an additional 2 seconds on Mythic difficulty.',
    'Fixed an issue that made Lost Souls treat crowd control effects with PvP diminishing returns.'
]),
(2546, '2022-03-18 15:00:00 GMT-08:00', false, true, array[
    'Anduin’s health has been reduced by 10% on Mythic difficulty.',
    'Fixed an issue that could cause the application of Domination’s Grasp to the Remnant of a Fallen King to be delayed.'
]),
(2546, '2022-03-22 15:00:00 GMT-08:00', true, false, array[

    '[With realm restarts] Wicked Star duration decreased to 2 seconds on Normal and Heroic difficulty (was 4 seconds).',
    '[With realm restarts] Anduin’s Doubt and Anduin’s Hope health reduced by 20% on Normal and Heroic difficulty.',
    '[With realm restarts] Anduin’s Doubt and Anduin’s Hope movement speed reduced by 20% on Normal and Heroic difficulty.',
    '[With realm restarts] Purging Light damage reduced by 15% on Normal difficulty.',
    '[With realm restarts] Fixed an issue that could cause Empowered Wicked Stars to not damage and silence players.',
    '[With realm restarts] Fixed an issue that could cause Fragment of Hope to not properly deal damage.'

]),
(2546, '2022-03-23 15:00:00 GMT-08:00', false, false, array[

    'Fixed an issue causing Fragment of Hope to trigger on Normal difficulty.'

]),
(2546, '2022-04-04 15:00:00 GMT-08:00', false, false, array[
'Resolved an issue that could cause Hopelessness to not properly trigger Blasphemy.'
]),
(2546, '2022-03-29 15:00:00 GMT-08:00', false, false, array[
'Fixed an issue that could cause Anduin to improperly enter Phase 2 if he was brought down to 10% health remaining too quickly.'
]),
(2546, '2022-03-30 15:00:00 GMT-08:00', false, false, array[
'Fixed an issue with the pathing of Empowered Wicked Stars.'
]),
(2546, '2022-04-05 15:00:00 GMT-08:00', false, false, array[
    'Fixed an issue that could prevent players from progressing past Anduin in Raid Finder difficulty.'
]),
(2546, '2022-04-26 15:00:00 GMT-08:00', true, true, array[

    'Anduin’s Despair and Anduin’s Doubt health reduced by 15% in Heroic and Mythic difficulties.',
    'Befouled Barrier’s absorbs 10% less healing in Heroic and Mythic difficulties.',
    'Fiendish Soul’s Soul Explosion missile travel speed reduced by 20% in Heroic and Mythic difficulties.',
    'Monstrous Soul health reduced by 6% in Mythic difficulty.',
    'Lost Soul’s Banish Soul cast time increased to 12 seconds (was 8 seconds).',
    'Grim Reflection’s Grim Fate haste bonus reduced to 200% (was 250%).'

]),
-- Lords of Dread
(2543, '2022-03-08 21:30:00 GMT-05:00', false, false, array[
'Players are now protected from Cloud of Carrion for a brief moment after being afflicted with Paranoia.'
]),
(2543, '2022-03-09 15:00:00 GMT-08:00', false, false, array[
    'Mal''Ganis and Kin''tessa health reduced by 10% on Normal difficulty.',
    'The encounter''s enrage timer has been increased to 13 minutes on Normal difficulty (was 11 minutes).'
]),
(2543, '2022-03-18 15:00:00 GMT-08:00', false, false, array[
    'Cloud of Carrion will now prefer targets without Biting Wounds.',
    'Fixed an issue where Anguishing Strike could do damage more often than intended.',
    'Fixed an issue where Cloud of Carrion could erroneously pass to the tank if it moved over a player inside Podtender (Dreamweaver Soulbind).',
    'Several spells no longer hit pets.'
]),
(2543, '2022-03-21 15:00:00 GMT-08:00', false, false, array[

    'Mal’Ganis and Kin’tessa no longer clear their debuffs when entering Infiltration of Dread.',
    'Fixed an issue that could allow Kin’tessa or Mal’Ganis to strike an unexpected direction with Anguishing Strike or Leeching Claws.'

]),
(2543, '2022-03-22 15:00:00 GMT-08:00', false, false, array[
'Fearful Trepidation’s visual should more closely match its effect radius.'
]),
(2543, '2022-03-23 15:00:00 GMT-08:00', false, false, array[

    'Fixed an issue where Mal’Ganis was not saying his last text line upon being defeated.'

]),
(2543, '2022-03-29 15:00:00 GMT-08:00', true, true, array[
'[With realm restarts] Swarm of Darkness and Swarm of Decay caused by Mal''Ganis'' Unto Darkness will now increase all damage taken by 100% rather than only area-of-effect abilities.'
]),
(2543, '2022-03-30 15:00:00 GMT-08:00', false, false, array[

    'Time to unmask Mal''Ganis and Kin''tessa during Infiltration of Dread has been increased to 40 seconds on Normal difficulty (was 30 seconds).',
    'If players are unable to unmask the dreadlords during Infiltration of Dread, a shadowy visual will persist on the infiltrated targets so players can see who the infiltrated were.'

]),
-- Rygelon
(2549, '2022-03-09 15:00:00 GMT-08:00', false, false, array[
'Fixed a rare issue that could sometimes cause Celestial Collapse to be cast a third time during a single phase.'
]),
(2549, '2022-03-15 15:00:00 GMT-08:00', false, false, array[
'Fixed an issue that could cause Cosmic Ejection to strike players outside of the intended radius.'
]),
(2549, '2022-03-17 15:00:00 GMT-08:00', false, false, array[
    'Fixed an issue which could cause Dark Eclipse to not trigger a Dark Eruption.'
]),
(2549, '2022-03-18 15:00:00 GMT-08:00', false, false, array[
'Fixed an issue where certain mechanic’s visual effects would become hidden when Hunters Feign Death.'
]),
(2549, '2022-03-21 15:00:00 GMT-08:00', false, true, array[

    'Reduced Rygelon’s health by 6% on Mythic difficulty.',
    'Reduced the damage of Stellar Decay and the health of Unstable Cores by 25% on Mythic difficulty.',
    'Reduced the magnitude of the healing absorb applied through Stellar Shroud by 10% on Mythic difficulty.',
    'Reworked the implementation for how Rygelon moves through phases, resulting in a more favorably consistent experience.',
    'Increased the responsiveness of Collapsing Quasar Fields if the contacting player has Dark Eclipse.',
    'Fixed an issue where Unstable Matter could fail to set their encounter boss UI frame after the first Shatter Sphere.',
    'Fixed an issue where Unstable Quasars could remain during Dark Quasar.',
    'Fixed an issue where Cosmic Radiation could persist after defeating the encounter.'

]),
(2549, '2022-03-22 15:00:00 GMT-08:00', false, false, array[

    'Fixed an issue where Unstable Quasar could strike players outside of the Singularity.'

]),
(2549, '2022-03-28 15:00:00 GMT-08:00', false, false, array[

    '[With realm restarts] Reduced boss health by 5% on Heroic difficulty.',
    '[With realm restarts] Reduced the damage of Stellar Decay by 20% on Heroic difficulty.',
    '[With realm restarts] Reduced the health of Unstable Cores by approximately 33% on Heroic difficulty.',
    'Fixed an issue with Corrupted Wound’s spell description in the Dungeon Journal. Functionality remains unchanged.'

]),
(2549, '2022-04-05 15:00:00 GMT-08:00', false, false, array[
'Fixed an issue where the damage from Massive Bang could be reduced through clever use of game mechanics.'
]),
-- Jailer
(2537, '2022-03-15 15:00:00 GMT-08:00', false, false, array[
'Fixed an issue with Defile that prevented it from inflicting the proper damage, as such the damage of Defile has been increased by 85% on Heroic difficulty.'
]),
(2537, '2022-03-16 15:00:00 GMT-08:00', false, false, array[
'Fixed issue that allowed Shaman’s Tremor Totem to remove Domination.'
]),
(2537, '2022-03-17 15:00:00 GMT-08:00', false, false, array[
    'Player guardians, such as Fire Elemental, are now protected while players are affected by Rune of Compulsion.',
    'Fixed an issue where Dominating Will could be removed via immunities. Dominating Will’s heal absorb reduced by 10% on Normal difficulty and reduced by 20% on Heroic difficulty to account for the potential increase in difficulty of the ability.'
]),
(2537, '2022-03-21 15:00:00 GMT-08:00', false, false, array[
'The Persecution debuff now lasts 30 seconds on Mythic difficulty (was 40 seconds).'
]),
(2537, '2022-03-22 15:00:00 GMT-08:00', false, false, array[
'Fixed an issue where Tainted Azerite could critically hit.'
]),
(2537, '2022-03-23 15:00:00 GMT-08:00', false, false, array[

    'Fixed an issue that allowed players under Compulsion to use Demonic Gateways.'

]),
(2537, '2022-03-24 15:00:00 GMT-08:00', false, true, array[

    'Fixed an issue with Desolation''s spell description in the Dungeon Journal on Mythic difficulty that caused it to display an incorrect amount of damage inflicted to Azeroth by unblocked missiles.',
    'Reduced the number of missiles fired by Desolation in Mythic difficulty from 20 to 17.',
    'Fixed an issue that would occasionally cause Chains of Anguish to pull players who had already broken their chains.'

]),
(2537, '2022-03-28 15:00:00 GMT-08:00', false, false, array[
'Fixed an issue with Diverted Life Shield''s spell description that caused it to display the improper amount of healing. It will now properly show it heals 2% per tick.'
]),
(2537, '2022-03-29 15:00:00 GMT-08:00', false, false, array[
'Fixed issue where Defile’s growth can be triggered by Priests in Spirit of Redemption form.'
]),
(2537, '2022-04-05 15:00:00 GMT-08:00', false, true, array[

    'Jailer’s health reduced by 5% on Mythic difficulty.',
    'Fixed an issue causing Chains of Anguish to sometimes not cast.',
    'Fixed an issue that caused the last proc of Mirrors of Torment (Mage Venthyr Ability) to cause Diverted Life Shield to cancel.'

]),
(2537, '2022-04-26 15:00:00 GMT-08:00', true, true, array[

    'Jailer’s melee attack decreased 10% in Mythic difficulty.',
    'Jailer’s health decreased 5% in Mythic difficulty.',
    'Martyrdom damage decreased 15% in Mythic difficulty.',
    'Torment damage decreased 10% in Mythic difficulty.',
    'Boon of Azeroth buff increased by 20% in Mythic difficulty.'

]);
