-- Your SQL goes here
create table encounter_hotfixes (
  id serial primary key,
  encounter_id integer not null references encounters(encounter_id),
  application_date timestamp with time zone not null,
  -- whether the hotfix applied with regional restarts
  regional boolean not null default false,
  -- subjective, but how important the hotfix is for prog
  major boolean not null,
  description text[]
);

insert into encounter_hotfixes (encounter_id, application_date, regional, major, description)
values
  (2398, '2021-02-18 15:00:00 GMT-08:00', false, false, array['Fixed an issue causing Exsanguinating Bite to sometimes not hit her current target.']),
  (2405, '2021-02-16 15:00:00 GMT-08:00', false, true, array['There is now an additional delay before Edge of Annihilation is cast upon entering the third phase.']),
  (2417, '2021-02-11 15:00:00 GMT-08:00', false, false, array['General Grashaal will no longer cast Seismic Upheaval as his first spell in the final phase of Mythic difficulty.']),
  (2412, '2021-02-08 15:00:00 GMT-08:00', false, false, array['Nadjia''s Familiar Predicaments (Venthyr Soulbind) will no longer unintentionally reduce the duration of Dancing Fever.']),
  (2417, '2021-02-08 15:00:00 GMT-08:00', true, true, array['Reduced the health of Stone Legion Goliath by 11.1% and Stone Legion Skirmisher by 10% on Mythic difficulty.']),
  (2407, '2021-02-08 15:00:00 GMT-08:00', false, true, array['Improved the visibility of Crescendo.']),
  (2399, '2021-01-27 15:00:00 GMT-08:00', false, false, array['Protection Paladins blocking Giant Fists should now trigger thh damage component of Holy Shield (Talent).']),
  (2399, '2021-01-25 15:00:00 GMT-08:00', true, true, array['Reduced the damage of Colosal Roar by 10% and Chain Slaim and Chain Bleed by 35% on Mythic difficulty.']),
  (2417, '2021-01-25 15:00:00 GMT-08:00', true, true, array['"Jesus christ this boss is broken" in hotfix form.', 'Stone Legion Goliath health reduced by 10% on Mythic difficulty.', 'Stone Legion Goliath''s Ravenous Feast cooldown increased by 6 seconds on Mythic difficulty.', 'Stone Legion Skirmisher health reduced by 6% on Mythic difficulty.', 'Serrated Tear and Stone Fist damage reduced by 20% on Mythic difficulty.', 'Serrated Swipe, Serrated Tear, and Stone Fist can no longer be out-ranged by tanks.', 'Heart Rend can no longer be out-ranged.', 'Dispelling Heart Rend no longer applies the Heart Hemorrhage debuff to the dispeller.', 'General Kaal and General Grashaal''s spell cooldowns now reset when a new phase begins. <em>This does not apply to entering an intermission phase.</em>', 'Volatile Stone Shell absorb shield reduced by 20% on Mythic difficulty.']),
  (2406, '2021-01-22 15:00:00 GMT-08:00', false, false, array['Bottled Anima no longer targets pets and guardians.']),
  (2405, '2020-12-22 15:00:00 GMT-08:00', false, true, array['Resolved an issue where abilities would change their sequence if Artificer Xy''mox changed phases before casting Dimensional Tear.']),
  (2405, '2020-12-22 15:00:00 GMT-08:00', false, false, array['Glyph of Destruction now applies to targets that are not in line of sight.']),
  (2406, '2020-12-22 15:00:00 GMT-08:00', false, false, array['Warrior''s Spell Reflect will no longer negate the Expose Desires ability.']),
  (2412, '2020-12-22 15:00:00 GMT-08:00', false, true, array['Fixed an issue preventing Dancing Fever from being reapplied properly on Mythic difficulty.']),
  (2407, '2020-12-22 15:00:00 GMT-08:00', false, false, array['Fixed an issue where Dusk Elegy was not correctly reducing incoming healing.']),
  (2417, '2020-12-21 15:00:00 GMT-08:00', false, true, array['Anima Orbs and Massive Anima Orbs can no longer be picked up by players already carrying orbs.', 'Stone Legion Skirmishers now cast Wicked Slaughter every 10 seconds on Mythic difficulty (was 8 seconds).', 'Stone Legion Commando''s Volatile Stone Shell absorb shield has been reduced by 10% on Mythic difficulty.']),
  (2418, '2020-12-18 15:00:00 GMT-08:00', false, false, array['Huntsman Altimor is now correctly afflicted with Broken Bond after Hecutis is defeated.']),
  (2412, '2020-12-18 15:00:00 GMT-08:00', false, false, array['Fixed an issue that would prevent Veteran Stoneguard from being attackable during Danse Macabre.']),
  (2417, '2020-12-18 15:00:00 GMT-08:00', false, false, array['Anima Orbs will no longer be dropped when using an immunity.', 'Fixed issue that sometimes caused Wicked Blade to hit moving players twice.', 'Fixed issue that sometimes caused 2 Wicked Blades to be launched simultaneously. <em>???????</em>']),
  (2412, '2020-12-17 15:00:00 GMT-08:00', false, false, array['Dutiful Attendants should no longer become stuck evading.']),
  (2406, '2020-12-16 15:00:00 GMT-08:00', false, false, array['Fixed an issue that prevented the UI Widget from displaying open Anima Containers.']),
  (2399, '2020-12-16 15:00:00 GMT-08:00', false, false, array['Fixed an issue where Chain Slam would sometimes hit players twice.', 'Fixed an issue where Giant fists would sometimes not hit the closest player to Sludgefist''s melee target.']),
  (2407, '2020-12-16 15:00:00 GMT-08:00', false, false, array['Fixed an issue where Druids could <s>have fun</s> cancel their shapeshift forms to escape the reduced movement speed effect from March of the Penitent.']),
  (2418, '2020-12-15 15:00:00 GMT-08:00', false, false, array['Increased the range of Spreadshot.']),
  (2412, '2020-12-15 15:00:00 GMT-08:00', false, false, array['Fixed an issue where Danse Macabre''s spotlight visual effect would sometimes not appear.']),
  (2399, '2020-12-15 15:00:00 GMT-08:00', false, false, array['Sludgefist''s Giant Fists now deal full damage to the secondary target instead of copying post-mitigation damage from the primary target.']),
  (2417, '2020-12-15 15:00:00 GMT-08:00', false, false, array['Ravenous Feast and Wicked Mark will no longer target the same player at the same time unless there are no other options.']),
  (2399, '2020-12-11 15:00:00 GMT-08:00', false, false, array['Stonequake''s ground effect and screen effect have been updated to be more visible.', 'Reduced the rate of Falling Rubble drops by 25%.']),
  (2407, '2020-12-11 15:00:00 GMT-08:00', false, false, array['Warlock''s Corruption will no longer be removed from enemies when using the teleporters.']),
  (2418, '2020-12-09 15:00:00 GMT-08:00', false, false, array['Sinseeker''s damage now scales less aggressively with raid size.', 'Sinseeker''s cast time increased to 5.5 seconds (was 4 seconds).']),
  (2406, '2020-12-09 15:00:00 GMT-08:00', false, false, array['Harnessed Specteres should no longer cast Condemn while actively engaged by a tank.']),
  (2402, '2020-12-09 15:00:00 GMT-08:00', false, false, array['Soul Pedestal healing "substantially increased" (so much detail) on all difficulties.']),
  (2405, '2020-12-09 15:00:00 GMT-08:00', false, false, array['Limited the number of healers that can be targeted by Fleeting Spirits at the same time.']),
  (2399, '2020-12-09 15:00:00 GMT-08:00', false, false, array['Fixed an issue where Hateful Gaze would sometimes not cas immediately at 100 rage.']),
  (2412, '2020-12-08 15:00:00 GMT-08:00', false, true, array['Fight redesigned from every PTR-tested version. It''s actually better now, but hope you aren''t too attached to your previous strat!']);
