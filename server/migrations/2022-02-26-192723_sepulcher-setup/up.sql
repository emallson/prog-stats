-- Your SQL goes here
insert into encounters
values
    (2512, 29, 1),
    (2540, 29, 2),
    (2553, 29, 3),
    (2544, 29, 4),
    (2542, 29, 5),
    (2529, 29, 6),
    (2539, 29, 7),
    (2546, 29, 8),
    (2543, 29, 9),
    (2549, 29, 10),
    (2537, 29, 11);

insert into release_dates values (29, '2022-03-01');
