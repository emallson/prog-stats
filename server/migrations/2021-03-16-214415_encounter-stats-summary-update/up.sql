-- Your SQL goes here
create or replace view encounter_stats_summary as
select 
distinct on (team_id, encounter_id, difficulty)
  team_id, 
  encounter_id,
  difficulty,
  sum(pull_count) over w as pull_count,
  sum(effective_duration) over w as prog_time,
  first_value(start_ilvl) over w as start_ilvl,
  last_value(end_ilvl) over w as end_ilvl,
  (max(has_kill::integer) over w)::boolean as has_kill,
  min(effective_start_time) over w as start_time,
  max(effective_start_time + effective_duration * interval '1 millisecond') over w as kill_time
  from log_encounter_stats
  where not post_prog
  window w as (partition by team_id, encounter_id, difficulty order by effective_start_time asc rows between unbounded preceding and unbounded following);
