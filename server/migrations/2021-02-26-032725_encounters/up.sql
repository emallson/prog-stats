-- Your SQL goes here
create table encounters (
  encounter_id int primary key,
  zone int not null,
  display_order int not null,
  unique(zone, display_order)
);

insert into encounters 
values
  (2398, 26, 10),
  (2418, 26, 9),
  (2383, 26, 8),
  (2402, 26, 7),
  (2405, 26, 6),
  (2406, 26, 5),
  (2412, 26, 4),
  (2399, 26, 3),
  (2417, 26, 2),
  (2407, 26, 1);

truncate fights;
