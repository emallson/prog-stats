-- Your SQL goes here
alter table team_stats
drop constraint team_stats_team_fkey,
add constraint team_stats_team_fkey foreign key (team) references teams(id) on delete cascade;

alter table encounter_stats
drop constraint encounter_stats_team_fkey,
add constraint encounter_stats_team_fkey foreign key (team) references teams(id) on delete cascade;

alter table fights
drop constraint fights_log_fkey,
add constraint fights_log_fkey foreign key (log) references logs(iid) on delete cascade;

alter table logs
drop constraint logs_team_fkey,
add constraint logs_team_fkey foreign key (team) references teams(iid) on delete cascade;

alter table raid_nights
drop constraint raid_nights_ts_fkey,
add constraint raid_nights_ts_fkey foreign key (ts) references team_stats(id) on delete cascade;
