-- Your SQL goes here
update encounter_hotfixes
   set application_date = application_date + interval '23 hours'
  from encounters
 where encounters.zone = 28
   and not encounter_hotfixes.regional
   and encounters.encounter_id = encounter_hotfixes.encounter_id;
