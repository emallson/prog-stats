create index if not exists ess_tier_stats on encounter_stats_summary (has_kill, encounter_id, difficulty);
create index if not exists logs_team_iid on logs(team);
