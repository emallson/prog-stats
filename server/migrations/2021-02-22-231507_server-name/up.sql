-- Your SQL goes here
alter table teams add column server_name text;
update teams set server_name = server_slug;
alter table teams alter column server_name set not null;
