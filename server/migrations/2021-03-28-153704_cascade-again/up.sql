alter table ignored_logs
drop constraint ignored_logs_log_fkey,
add constraint ignored_logs_log_fkey foreign key (log) references logs(iid) on delete cascade;

alter table ignored_logs
drop constraint ignored_logs_relevant_log_fkey,
add constraint ignored_logs_relevant_log_fkey foreign key (relevant_log) references logs(iid) on delete cascade;
