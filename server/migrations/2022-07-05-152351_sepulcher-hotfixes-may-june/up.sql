insert into encounter_hotfixes (encounter_id, application_date, regional, major, description)
values
-- Vigilant Guardian
(2512, '2022-05-02 15:00:00 GMT-08:00', false, false, array[

    'Fixed an issue where dying with the Unstable Core could cause it to become non-interactive.'

]),
-- Skolex
(2542, '2022-05-03 15:00:00 GMT-08:00', true, false, array[
       '[With realm restarts] Missile travel time for Devouring Blood increased to 2.5s (was 2s) on Heroic difficulty.'
]),
-- Xy'mox
(2553, '2022-05-03 15:00:00 GMT-08:00', true, false, array[

    '[With realm restarts] Xy Spellslinger health reduced by 20%.',
    '[With realm restarts] Debilitating Ray periodic damage reduced by 20%.'

]),
-- Halondrus
(2529, '2022-05-02 15:00:00 GMT-08:00', false, false, array[
       'Fixed an issue causing damage from Lance and Lightshatter Beam to ignore some damage reduction effects.',
       'Fixed an issue causing Volatile Charges to break if the player carrying the Volatile Charge disconnected.'
]),
(2529, '2022-05-03 15:00:00 GMT-08:00', true, false, array[
       '[With realm restarts] Lightshatter Beam damage decreased by 30% on Raid Finder, Normal and Heroic difficulties.',
       '[With realm restarts] Reclaim''s periodic damage effect scales more slowly on Heroic difficulty.'
]),
(2529, '2022-05-11 15:00:00 GMT-08:00', false, false, array[

    'Fixed an issue causing Lance to not inflict damage while Halondrus was affected by Relocation Form.'

]),
(2529, '2022-06-15 15:00:00 GMT-08:00', false, false, array[

    'Fixed an issue causing Planetcracker Beams to be invisible during the final stage.'

]),
(2529, '2022-06-16 15:00:00 GMT-08:00', false, false, array[

    'Fixed an issue that could cause Volatile Charge to not display correctly.'

]),
(2529, '2022-06-21 15:00:00 GMT-08:00', true, true, array[

    'Aftershock damage reduced by 75% on Mythic difficulty.',
    'Planetcracker Beam damage reduced by 65% and its damage occurs less frequently on Mythic difficulty.'

]),
(2529, '2022-06-21 15:00:00 GMT-08:00', false, false, array[

    'Fixed several visual issues with the environment disappearing during the encounter.'

]),
(2529, '2022-06-24 15:00:00 GMT-08:00', false, false, array[
       'Fixed an issue causing players to be unable to pick up Earthbreaker Charge immediately after recovering from the effects of Podtender (Dreamweaver Soulbind).'
]),
-- Dausegne
(2540, '2022-05-03 15:00:00 GMT-08:00', true, false, array[
       '[With realm restarts] Duration of Disintegration Halo debuff lowered to 5s (was 6s) on Heroic difficulty.'
]),
-- Pantheon
(2544, '2022-05-03 15:00:00 GMT-08:00', true, false, array[

    '[With realm restarts] Wildstorm has been removed from Normal and Raid finder difficulties.',
    '[With realm restarts] Sinful Projections has been removed from Heroic difficulty.',
    '[With realm restarts] Decreased the health of Necrotic Ritualists by 30% health on Normal difficulty, 15% health on Heroic difficulty.',
    '[With realm restarts] Decreased the health of Withering Seeds by 30% health on Normal and Raid Finder difficulties, 20% health on Heroic difficulty, and 10% health on Mythic difficulty, but also resolved a bug that caused them to trigger an Invigorating Bloom at 90% health instead of 100% health as the spell description states.',
    '[With realm restarts] Resolved a scaling issue causing the Withering Seeds to increase their health too steeply on larger raid sizes.'

]),
(2544, '2022-05-03 15:00:00 GMT-08:00', false, false, array[

    'The encounter''s enrage timer has been increased to 11 minutes on Heroic difficulty (was 9 minutes).'

]),
(2544, '2022-05-11 15:00:00 GMT-08:00', false, false, array[

    'Resolved an issue where the Prototype of War could incidentally not cast Necrotic Ritual.'

]),
(2544, '2022-06-06 15:00:00 GMT-08:00', false, false, array[
'Fixed an issue where players could be afflicted by Runecarver''s Deathtouch but have no Bastion''s Ward to reduce the damage.'
]),
-- Lihuvim
(2539, '2022-05-03 15:00:00 GMT-08:00', true, false, array[
'[With realm restarts] Acquisitions Automa''s Detonation radius reduced to 20 yards (was 40) on Normal and Heroic difficulties.',
'[With realm restarts] The cast time of Detonation has been increased to 2.5 seconds (was 2 seconds) on Normal and Heroic difficulties.'
]),
(2539, '2022-05-02 15:00:00 GMT-08:00', false, false, array[
'Lihuvim''s corpse can no longer become stuck in the air if killed while casting Realignment.'
]),
(2539, '2022-06-21 15:00:00 GMT-08:00', true, true, array[

    'Lihuvim health reduced by 15% on Mythic difficulty.',
    'Resonance Cascade damage reduced by 90% on Mythic difficulty.',
    'Increased the cooldown of Guardian Automa''s Form Sentry Automa on Mythic difficulty.'

]),
-- Anduin
(2546, '2022-05-04 15:00:00 GMT-08:00', false, false, array[

    'Increased the time it takes for players to be susceptible to Blasphemy after returning to their body.'

]),
(2546, '2022-05-17 15:00:00 GMT-08:00', true, true, array[

    '[With realm restarts] Blasphemy duration increased to 12 seconds in all difficulties.',
    '[With realm restarts] Lost Soul duration increased to 40 seconds in all difficulties (was 35 seconds).',
    '[With realm restarts] Wicked Star now targets one player at a time in Mythic difficulty.',
    '[With realm restarts] Players are now invalid targets for Blasphemy for 20 seconds after the effects of Lost Soul expire (was 10 seconds).',
    '[With realm restarts] Reduced the health of Grim Reflections by 15% in Mythic difficulty.'

]),
(2546, '2022-06-21 15:00:00 GMT-08:00', true, true, array[

    'Hopelessness and Overconfidence duration increased to 16 seconds on all difficulties (was 12 seconds).',
    'Blasphemy will no longer be triggered when coming in contact with an unmarked player on Mythic difficulty. The tooltip will reflect this change in a future patch.',
    'March of the Damned damage reduced by 75% on all difficulties.',
    'Soul Explosion damage and travel speed reduced by 30% on all difficulties.',
    'Banish Soul cast time increased to 16 seconds on Mythic difficulty (was 12 seconds).',
    'Necrotic Detonation cast time increased by 30% on Mythic difficulty.'

]),
(2546, '2022-07-05 15:00:00 GMT-08:00', true, false, array[
    'Anduin’s Hope and Anduin’s Doubt health reduced by 20% on Heroic and Normal difficulties.',
    'Reduced the movement speed of Anduin’s Hope by 15% on Heroic and Normal difficulties.',
    'Grim Fate reduced from 200% to 150% on Mythic difficulty.'
]),
-- Lords of Dread
(2543, '2022-07-05 15:00:00 GMT-08:00', true, true, array[
    'Boss Health reduced by 10% on all difficulties.'
]),
(2543, '2022-07-05 15:00:00 GMT-08:00', true, false, array[
    'Swarm of Decay and Swarm of Darkness damage reduced by 15% on Heroic difficulty.'
]),
-- Rygelon
(2549, '2022-05-03 15:00:00 GMT-08:00', true, false, array[

    '[With realm restarts] Increased the duration of Collapsing Quasar Field to 10 seconds on Normal and Heroic difficulties.',
    '[With realm restarts] Decreased Rygelon''s health by 5% on Heroic difficulty.'

]),
(2549, '2022-05-17 15:00:00 GMT-08:00', true, true, array[

    '[With realm restarts] Rygelon’s health has been reduced by 5% in Mythic difficulty.',
    '[With realm restarts] Collapsing Quasar Fields now last 8 seconds (was 6 seconds) in Mythic difficulty.',
    '[With realm restarts] Dark Collapse damage has been reduced by 25% in Mythic difficulty.',
    '[With realm restarts] Unstable Quasar radius reduced by 25%, and damage reduced by 20% in Mythic difficulty.',
    '[With realm restarts] Cosmic Ejection damage-over-time reduced by 33% in Mythic difficulty.'

]),
(2549, '2022-05-27 15:00:00 GMT-08:00', false, false, array[
       'Dark Eruption damage reduced by 45% on Heroic difficulty.',
       'Collapsing Quasar Field duration increased to 12 seconds on Heroic difficulty (was 10 seconds).',
       'Cosmic Ejection initial damage decreased by 25%, and the damage over time decreased by 73% on Heroic difficulty. '
]),
(2549, '2022-06-07 15:00:00 GMT-08:00', true, true, array[
'[With realm restarts] Rygelon''s health reduced by 5% on Mythic difficulty.',
'[With realm restarts] Rygelon''s auto-attack damage has been reduced by 15% on all difficulties.'
]),
(2549, '2022-06-13 15:00:00 GMT-08:00', false, false, array[

    'Fixed an issue where players could incidentally consume two Collapsing Quasar Fields that overlap.'

]),
(2549, '2022-06-21 15:00:00 GMT-08:00', true, true, array[

    'Dark Eclipse now targets 3 players on Mythic difficulty (was 4).',
    'Unstable Matter health reduced by 25% on Mythic difficulty.'

]),
(2549, '2022-07-05 15:00:00 GMT-08:00', true, true, array[
    'Boss Health reduced by 10% on all difficulties.'
]),
(2549, '2022-07-05 15:00:00 GMT-08:00', true, false, array[
    'Damage of Dark Eruption reduced by 75% on Heroic difficulty.'
]),
-- Jailer
(2537, '2022-05-03 15:00:00 GMT-08:00', true, false, array[

    '[With realm restarts] All trapdoors will now close in Phase 3 on Normal difficulty.',
    '[With realm restarts] Decreased knockback of Decimator on Normal and Raid Finder difficulties.',
    '[With realm restarts] Decreased knockback of Misery on Normal difficulty.',
    '[With realm restarts] Decreased rate of growth of Defile on Normal difficulty.'

]),
(2537, '2022-05-17 15:00:00 GMT-08:00', true, true, array[

    '[With realm restarts] Azerite Radiation increased Arcane damage is now 80% (was 100%) in Mythic difficulty.',
    '[With realm restarts] Meteor Cleave damage decreased 10% in Mythic difficulty.',
    '[With realm restarts] Torment initial explosion damage decreased by 10% in all difficulties.',
    '[With realm restarts] Shattering Blast damage over time decreased by 33% in Mythic difficulty.',
    '[With realm restarts] Chain Breaker damage decreased by 10% in all difficulties.'

]),
(2537, '2022-06-21 15:00:00 GMT-08:00', true, true, array[

    'The Jailer''s melee damage reduced by 25% on all difficulties.',
    'Torment damage reduced by 30% on all difficulties.',
    'Unholy Eruption damage reduced by 30% on all difficulties.',
    'Rune of Compulsion absorb reduced by 30% on Mythic difficulty.',
    'Chain Breaker now targets the nearest 3 allies on Mythic difficulty (was 4).',
    'Incarnation of Torment health reduced by 20% on Mythic difficulty.',
    'Diverted Life Shield reduced by 75% on Mythic difficulty.',
    'Chains of Anguish will no longer reposition linked targets when the primary target moves out of range on Mythic difficulty.'

]),
(2537, '2022-07-05 15:00:00 GMT-08:00', true, true, array[
    'Health reduced by 10% on Mythic difficulty.',
    'Azerite Radiation increased from 3%/stack to 7%/stack.',
    'Domination is now triggered on players with 3 stacks of Tyranny (was 2 stacks).',
    'Decimator no longer includes knockback on Heroic and Mythic difficulties.',
    'Surging Azerite damage reduced by 15% on Mythic difficulty.',
    'Surging Azerite heals Azeroth for 5% per tick (was 4%).',
    'Rune of Damnation damage reduced by 15% on all difficulties.',
    'Rune of Domination will be cast 3 times during Phase 2 on Mythic difficulty (was 4 casts).',
    'Shattering Blast damage reduced by 20% on all difficulties.',
    'Chains of Anguish damage reduced by 30% on Mythic difficulty.',
    'Dominating Will absorb reduced by 15% on Heroic and Mythic difficulties.'
]),
(2537, '2022-07-05 15:00:00 GMT-08:00', true, false, array[
    'Health reduced by 15% on Heroic and Normal difficulties.',
    'Rune of Compulsion absorb reduced by 35% on Heroic difficulty.',
    'Chain Breaker damage reduced by 20% on Heroic difficulty.'
]);
