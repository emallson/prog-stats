update encounter_hotfixes set description = array['Veil of Darkness now targets players in Phase 2, allowing it to target the same location multiple times.', 'Ruin sometimes fails to wipe the raid in Phase 2.']
where encounter_id = 2435 and undocumented = true;
