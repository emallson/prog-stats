alter table teams add unique(id);

create table encounter_stats (
  id serial primary key,
  team integer not null references teams(id),
  encounter_id integer not null,
  difficulty integer not null,
  kill_log integer references logs(iid),
  kill_week integer,
  ilvl_min real not null,
  ilvl_max real not null,
  pull_count integer not null,
  prog_time integer not null,
  unique(team, encounter_id, difficulty)
);
