-- This file should undo anything in `up.sql`
drop table if exists fight_detailed_stats;

drop table if exists fight_detailed_deaths;

drop table if exists fight_detailed_specs;
