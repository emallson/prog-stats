create table fight_detailed_stats (
    id serial primary key,
    fight_iid integer references fights (iid) on delete cascade,
    phase_index integer not null,
    duration_ms integer not null,
    overall_dps real not null,
    overall_hps real not null,
    nontank_dtps real not null,
    dps_spread real not null,
    hps_spread real not null,
    unique (fight_iid, phase_index)
);

comment on column fight_detailed_stats.phase_index is '-1 for the whole fight, or a 0-based index of phase index matching WCL usage.';

create table fight_detailed_deaths (
    id serial primary key,
    fight_iid integer references fights (iid) on delete cascade,
    death_index integer not null,
    death_offset_ms integer not null,
    death_role integer not null,
    killing_ability_id integer,
    duration_ms integer not null,
    unique (fight_iid, death_index)
);

create table fight_detailed_specs (
    id serial primary key,
    fight_iid integer references fights (iid) on delete cascade,
    class_name text not null,
    spec_name text not null,
    present_count integer not null,
    unique (fight_iid, class_name, spec_name)
);
