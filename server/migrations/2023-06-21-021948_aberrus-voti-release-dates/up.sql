-- Your SQL goes here
update release_dates
set release_date = '2023-05-09 00:00:00+00:00'
where zone = 33;

update release_dates
set release_date = '2022-12-13 00:00:00+00:00', end_date = '2023-05-09 00:00:00+00:00'
where zone = 31;
