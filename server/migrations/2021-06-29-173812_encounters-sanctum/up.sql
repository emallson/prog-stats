-- Your SQL goes here
insert into encounters
values
    (2423, 28, 10),
    (2433, 28, 9),
    (2429, 28, 8),
    (2432, 28, 7),
    (2434, 28, 6),
    (2430, 28, 5),
    (2436, 28, 4),
    (2431, 28, 3),
    (2422, 28, 2),
    (2435, 28, 1);

insert into release_dates values (28, '2021-07-06');
