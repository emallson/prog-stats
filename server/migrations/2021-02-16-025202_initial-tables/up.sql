-- Your SQL goes here
create table teams (
  iid serial primary key,
  -- this is the WCL id
  id integer not null,
  -- boolean field for whether this is a tag (team) or a whole guild
  tag boolean not null,
  name text not null,
  tag_name text,
  server_slug text not null,
  region_slug text not null,
  unique(id, tag)
);

create table logs (
  iid serial primary key,
  code text not null,
  team integer not null,
  start_time timestamp with time zone not null,
  end_time timestamp with time zone not null,
  title text not null,
  zone_id integer not null,
  foreign key(team) references teams(iid),
  unique(code)
);

create table fights (
  iid serial primary key,
  log integer not null,
  id integer not null,
  -- these are time *offsets* in ms
  start_time integer not null,
  end_time integer not null,
  encounter_id integer not null,
  boss_pct real,
  avg_ilvl real,
  difficulty integer,
  kill boolean not null,
  foreign key(log) references logs(iid),
  unique(log, id)
);
