update encounter_hotfixes set major = false where encounter_id = 2435;

alter table encounter_hotfixes add column undocumented boolean not null default false;

insert into encounter_hotfixes (encounter_id, application_date, regional, major, undocumented, description)
values (2435, '2021-09-14 15:00 GMT-08:00', false, true, true, array['Veil of Darkness now targets players in Phase 2, allowing it to target the same location multiple times.', 'Ruin no longer wipes the raid in Phase 2.']);
