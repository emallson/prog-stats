alter table release_dates add column end_date timestamp with time zone;

update release_dates set end_date = '2019-01-22' where zone = 19;
update release_dates set end_date = '2019-07-09' where zone = 21 or zone = 22;
update release_dates set end_date = '2020-01-14' where zone = 23;
update release_dates set end_date = '2020-10-10' where zone = 24;
