-- Your SQL goes here
insert into encounter_hotfixes (encounter_id, application_date, regional, major, description)
values
(2919, '2024-10-01 15:00:00 GM-08:00', true, true, array[       
    'Broodtwister Ovi’nax health reduced by 15% on Mythic difficulty.',
    'Experimental Dosage duration increased to 8 seconds on Mythic difficulty.',
    'Poison Burst cast time increased to 4 seconds on Mythic difficulty.',
    'Volatile Concoction periodic damage reduced by 30% on Mythic difficulty.',
    'Mutation: Accelerated increases movement speed of Blood Parasites by 10% every 2 seconds (was every 1 second) on Heroic and Mythic difficulties.',
    'Blood Parasites are no longer immune to crowd control while casting Infest.',
    'Fixed an issue where Blood Parasites would sometimes melee players.',
    'Vile Discharge damage reduced by 20% on Mythic difficulty.'
]),
(2920, '2024-10-01 15:00:00 GMT-08:00', true, true, array[

    'Health reduced by 10% on Mythic difficulty.',
    'Chasmal Gash now persists through death on all difficulties.',
    'Assassination damage reduced by 20% on Mythic difficulty.',
    'Twilight Massacre damage reduced by 15% on Mythic difficulty.',
    'Queensbane damage over time effect reduced by 17% on Mythic difficulty.',
    'Queensbane expiration damage reduced by 25% on Mythic difficulty.',
    'Starless Night damage reduced by 15% on Mythic difficulty.',
    'Eternal Night damage reduced by 15% on Mythic difficulty.',
    'Fixed an issue causing Assassination and Twilight Massacre to pierce some immunities on all difficulties.'

]),
(2921, '2024-10-01 15:00:00 GMT-08:00', false, false, array[
       'Motes will now spawn more reliably at each player’s exact location.'
]),
(2922, '2024-10-01 15:00:00 GMT-08:00', false, false, array[
      
    'Adjusted the visual of Web Blades to more closely match the damage event timing.',
    'Fixed an issue specific to Raid Finder that sometimes caused pets and guardians to not follow their owner when traveling between platforms in Phase 2.'
 
]),
(2922, '2024-09-27 15:00:00 GMT-08:00', false, false, array[

    'Acolyte''s Essence periodic damage reduced by 12.5% on Mythic difficulty.',
    'Royal Condemnation damage reduced by 8% on Mythic difficulty.',
    'Demonic Gateway, Transcendence spirit, Spirit Link Totem, and Wind Rush Totem are no longer moved unexpectedly during certain moments of the encounter.',
    'Fixed issue in Mythic difficulty where warning visualization effects for Shadowgates would sometimes fail to appear after the first cast.'

]),
(2919, '2024-09-24 15:00:00 GMT-08:00', true, true, array[
       
    '[With realm restarts] Colossal Spider health reduced by 25% on Mythic difficulty.',
    '[With realm restarts] Blood Parasite health reduced by 21% on Mythic difficulty.',
    '[With realm restarts] Healing absorb effect of Unstable Infusion reduced by 12.5%.'

]),
(2921, '2024-09-23 15:00:00 GMT-08:00', false, true, array[
       'Fixed an issue with Piercing Strike debuff not increasing Piercing Strike''s damage. Piercing Strike debuff now persists through death.'
]);
