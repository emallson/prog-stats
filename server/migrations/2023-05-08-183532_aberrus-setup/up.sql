-- close VotI on May 9th
UPDATE
    release_dates
SET
    end_date = '2023-05-09 10:00:00-04:00'
WHERE
    zone = 31;

INSERT INTO encounters
    VALUES (2688, 33, 1),
    (2687, 33, 2),
    (2693, 33, 3),
    (2682, 33, 4),
    (2680, 33, 5),
    (2689, 33, 6),
    (2683, 33, 7),
    (2684, 33, 8),
    (2685, 33, 9);

INSERT INTO release_dates
    VALUES (33, '2023-05-09 10:00:00-05:00');

