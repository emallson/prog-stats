update release_dates
set end_date = '2023-11-14 10:00:00-04:00'
where zone = 33;

insert into release_dates
values (35, '2023-11-14 10:00:00-04:00');

insert into encounters
values
(2820, 35, 1), -- GNARLROOT
(2709, 35, 2), -- IGIRA_THE_CRUEL
(2737, 35, 3), -- VOLCOROSS
(2728, 35, 4), -- COUNCIL_OF_DREAMS
(2731, 35, 5), -- LARODAR_KEEPER_OF_THE_FLAME
(2708, 35, 6), -- NYMUE_WEAVER_OF_THE_CYCLE
(2824, 35, 7), -- SMOLDERON
(2786, 35, 8), -- TINDRAL_SAGESWIFT
(2677, 35, 9); -- FYRAKK_THE_BLAZING
