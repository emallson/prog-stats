-- Your SQL goes here
create table team_updates (
    id serial primary key,
    team_iid integer not null references teams (iid),
    last_update timestamp
    with
        time zone not null,
        unique (team_iid)
);

create index team_updates_last_update on team_updates (last_update);
