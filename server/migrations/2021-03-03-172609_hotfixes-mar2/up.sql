-- Your SQL goes here
insert into encounter_hotfixes (encounter_id, application_date, regional, major, description)
values
  (2383, '2021-03-01 15:00:00 GMT-08:00', true, true, array['Health reduced by 5% on Mythic difficulty.', 'Reduced damage from Gluttonous Miasma by 10% on Mythic difficulty.', 'Reduced the damage increase of Essence Sap by 20% per stack.']),
  (2402, '2021-03-01 15:00:00 GMT-08:00', true, true, array['Increased the amount of healing from Essence Overflow to 2.5% of Kael''thas'' max HP (was 2%).', 'Increased the healing bonus from Sinfall Boon to 100% on Mythic difficulty (was 50%).', 'Reduced the size of theh Cloak of Flames damage shield on Shade of Kael''thas by 10%.']);
