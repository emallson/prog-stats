-- Your SQL goes here
insert into encounter_hotfixes (encounter_id, application_date, regional, major, description)
values
  (2407, '2021-06-01 15:00:00 GMT-08:00', false, true, array['Phase Two now ends when Denathrius'' health reaches 37.5% (was 40.5%) or after 225 seconds (was 210 seconds) on Mythic difficulty.']),
  (2407, '2021-05-19 15:00:00 GMT-08:00', false, false, array['Players are now briefly protected from Crescendo after Hand of Destruction is cast on Heroic and Mythic difficulties.']),
  (2406, '2021-05-07 15:00:00 GMT-08:00', true, true, array['Health reduced by 10% on Mythic difficulty.', 'Conjured Manifestation and Harnessed Specter health reduced by 10% on Mythic difficulty.', 'Fixed an issue where Concentrated Anima and Shared Suffering could choose a player currently affected by Forgeborne Reveries (Necrolord Soulbind).']),
  (2407, '2021-05-05 15:00:00 GMT-08:00', false, false, array['Fixed an issue that caused Painful Memories to damage pets and guardians.']);
