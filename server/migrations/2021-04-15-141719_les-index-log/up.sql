-- clearing old encounter stats uses this. should speed up those queries a good bit
create index if not exists les_log on log_encounter_stats(log);
