update release_dates
set
    end_date = '2025-03-04 10:00:00-04:00'
where
    zone = 38;

insert into
    release_dates
values
    (42, '2025-03-04 10:00:00-04:00');

insert into
    encounters
values
    (3009, 42, 1),
    (3010, 42, 2),
    (3011, 42, 3),
    (3012, 42, 4),
    (3013, 42, 5),
    (3014, 42, 6),
    (3015, 42, 7),
    (3016, 42, 8);
