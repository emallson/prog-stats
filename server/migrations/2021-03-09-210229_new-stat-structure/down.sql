-- This file should undo anything in `up.sql`
drop view encounter_stats_summary;
drop table ignored_logs;
drop table log_encounter_stats;
drop type ignore_reason;
