-- Your SQL goes here
create table log_encounter_stats (
  id serial primary key,
  -- wcl team id. duplicated from log, but we use this for lookup a LOT so i want to remove the extra join
  team_id integer not null references teams(id),
  -- no unique constraint here: there may be multiple stat blocks on long logs
  log integer not null references logs(iid),
  encounter_id integer not null references encounters(encounter_id),
  difficulty integer not null,
  -- prog time = eet - est
  effective_start_time timestamp with time zone not null,
  effective_duration integer not null,
  start_ilvl real not null,
  end_ilvl real not null,
  has_kill boolean not null,
  post_prog boolean not null,
  pull_count integer not null,
  sync_date timestamp with time zone not null default now()
);

create type ignore_reason as enum ('overlap');

-- logs that are ignored for the stat purposes
-- a log may be ignored for one fight but not another (for example: reclearing
-- bosses before progressing a new boss)
create table ignored_logs (
  id serial primary key,
  log integer not null references logs(iid),
  encounter_id integer not null references encounters(encounter_id),
  difficulty integer not null,
  reason ignore_reason not null,
  -- log with more information about why this log is ignored. for example:
  -- overlap will reference the log that overlaps with it here. post-prog
  -- references the kill log here.
  relevant_log integer references logs(iid),
  unique (log, encounter_id, difficulty)
);

create view encounter_stats_summary as
select 
distinct on (team_id, encounter_id, difficulty)
  team_id, 
  encounter_id,
  difficulty,
  sum(pull_count) over w as pull_count,
  sum(effective_duration) over w as prog_time,
  first_value(start_ilvl) over w as start_ilvl,
  last_value(end_ilvl) over w as end_ilvl,
  (max(has_kill::integer) over w)::boolean as has_kill
  from log_encounter_stats
  where not post_prog
  window w as (partition by team_id, encounter_id, difficulty order by effective_start_time asc rows between unbounded preceding and unbounded following);
