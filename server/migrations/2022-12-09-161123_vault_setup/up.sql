-- Your SQL goes here
insert into encounters
values
        (2587, 31, 1),
        (2639, 31, 2),
        (2590, 31, 3),
        (2592, 31, 4),
        (2635, 31, 5),
        (2605, 31, 6),
        (2614, 31, 7),
        (2607, 31, 8);

insert into release_dates values (31, '2022-12-13 10:00:00-05:00');
