-- Your SQL goes here
alter table team_stats drop column min_hours, drop column max_hours;
alter table team_stats add column min_hours int not null default 0, add column max_hours int not null default 0;
