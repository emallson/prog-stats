create table histogram_bins (
    id serial primary key,
    encounter_id integer not null,
    difficulty integer not null,
    metric_type integer not null,
    day_index integer not null,
    bin_index integer not null,
    count integer not null,
    unique (encounter_id, difficulty, metric_type, day_index, bin_index)
);
