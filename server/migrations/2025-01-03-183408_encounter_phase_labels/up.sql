create table if not exists encounter_phase_labels (
    id serial primary key,
    encounter_id integer not null references encounters (encounter_id),
    phase_index integer not null,
    label text not null,
    unique (encounter_id, phase_index)
);

insert into
    encounter_phase_labels (encounter_id, phase_index, label)
values
    -- ulgrax
    (2902, 0, 'Phase 1'),
    (2902, 1, 'Intermission 1'),
    (2902, 2, 'Phase 2'),
    (2902, 3, 'Intermission 2'),
    (2902, 4, 'Phase 3'),
    (2902, 5, 'Intermission 3'),
    (2902, 6, 'Phase 4'),
    -- kyveza
    (2920, 0, 'Phase 1'),
    (2920, 1, 'Intermission 1'),
    (2920, 2, 'Phase 2'),
    (2920, 3, 'Intermission 2'),
    (2920, 4, 'Phase 3'),
    (2920, 5, 'Intermission 3'),
    -- silken court
    (2921, 0, 'Phase 1'),
    (2921, 1, 'Intermission 1'),
    (2921, 2, 'Phase 2'),
    (2921, 3, 'Intermission 2'),
    (2921, 4, 'Phase 3'),
    -- queen ansurek
    (2922, 0, 'Phase 1'),
    (2922, 1, 'Intermission'),
    (2922, 2, 'Phase 2'),
    (2922, 3, 'Phase 3');
