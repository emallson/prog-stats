"""
Download log data to use in raid night test cases.

Usage:
    raid-nights.py (tag|guild) <id> [options]
    raid-nights.py --help

Options:
    --help            Show this screen.
    --zone <id>       Select a zone. [default: 26]
    --difficulty <d>  Select a difficulty. [default: 5]
"""
import toml
from docopt import docopt
import psycopg2 as pg
import pandas as pd

with open('secrets.toml') as f:
    secrets = toml.load(f)

args = docopt(__doc__)

con = pg.connect('postgres://{}:{}@emallson.net/scorecard'.format(secrets['db']['user'], secrets['db']['pw']))

kills = pd.read_sql("""
        select effective_start_time
        from log_encounter_stats les
        join encounters using (encounter_id)
        where team_id = {}
        and zone = {}
        and difficulty = {}
        and has_kill
        and not post_prog
        """.format(args['<id>'], args['--zone'], args['--difficulty']), con)

total_bosses = 10 # sue me

complete = total_bosses == len(kills)

q_extra = ""
if complete:
    q_extra = " and effective_start_time <= '{}'".format(kills.effective_start_time.max())

log_times = pd.read_sql('select effective_start_time, effective_duration, code from log_encounter_stats join logs on logs.iid = log where zone_id = {} and difficulty = {} and team_id = {}{}'.format(args['--zone'], args['--difficulty'], args['<id>'], q_extra), con)

print(log_times.to_json(orient='records'))
