use std::process::Command;

fn main() {
    let output = Command::new("git")
        .args(&["rev-parse", "HEAD"])
        .output()
        .unwrap();
    let hashes = String::from_utf8(output.stdout).unwrap();
    let hash = hashes.lines().next().unwrap();
    println!("cargo:rustc-env=VCS_CHANGESET={}", hash);
}
