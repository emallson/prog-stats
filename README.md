# WoW Progression Stats

This repository contains the code for the [WoW Progression Stats](https://prog-stats.emallson.net) website.

## Setup

### Frontend

The frontend is in TypeScript + React. If you have [yarn](https://yarnpkg.com)
installed, then a simple `yarn install` will have you good to go.

### Backend

The backend is in Rust. If you have [cargo](https://rustup.rs/) installed, then `cargo run server` will build and start the server---though before it will be *successful* you need a database.

#### The Database

This project uses a PostgreSQL database. How you set one up is up to you. I typically use docker because it allows me to keep DBs for different projects isolated:

```bash
docker run -d --name prog-stats-db -v "$(pwd)/postgres-data/:/var/lib/postgresql/data" -p 5433:5432 -e POSTGRES_PASSWORD=scorecard postgres
```

will spin one up for you.

After setting up a postgres instance, run:

```bash
cargo install diesel_cli
cd server
diesel migration run
```


## Contributing

If you are interested in contributing, please **talk to me on Discord**
(`emallson#6223`) before starting. My DMs are open to anyone that shares a
Discord with me, and if you're in any class discord or the WarcraftLogs or
WoWAnalyzer discord then you will be able to DM me.

## License

[BSD 3-Clause](./LICENSE)
